@extends('layouts.wrapper', [
    'pageTitle' => 'Manual Enrollments | List Accounts'
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <th>ID</th>
                                <th>Account Name</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach($result['results'] as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td><a href="{{route('manualenrollments.list-roles')}}?accountId={{$item->id}}" title="Select this account to view its roles"><btn class="btn btn-info">Select</btn></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
