@permission('manual-enrollments.update')
@if($batch->status === 'pending' || $batch->status === 'canceled')
    {!! Form::open()->patch()->route('manual-enrollments.update', ['manualEnrollmentBatch' => $batch]) !!}
    {!! Form::hidden('operation', 'approve') !!}
    <button class="btn btn-sm btn-success ml-2" onclick="return confirm('Are you sure you want to approve this Batch? This will queue the enrollments and cannot be undone.');"><i class="fas fa-thumbs-up"></i> Approve</button>
    {!! Form::close() !!}
@endif
@if($batch->status === 'pending')
    {!! Form::open()->patch()->route('manual-enrollments.update', ['manualEnrollmentBatch' => $batch]) !!}
    {!! Form::hidden('operation', 'cancel') !!}
    <button class="btn btn-sm btn-danger ml-2" onclick="return confirm('Are you sure you want to cancel this Batch? You can re-approve it later if needed.');"><i class="fas fa-ban"></i> Cancel</button>
    {!! Form::close() !!}
@endif
@if($batch->status === 'error')
    {!! Form::open()->post()->route('manual-enrollments.retry-all', ['manualEnrollmentBatch' => $batch]) !!}
    <button class="btn btn-sm btn-warning ml-2" onclick="return confirm('All errored enrollments will be retried. Are you sure you want to continue?');"><i class="fas fa-redo"></i> Retry All</button>
    {!! Form::close() !!}
@endif
@endpermission
