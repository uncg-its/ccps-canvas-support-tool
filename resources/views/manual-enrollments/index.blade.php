@extends('layouts.wrapper', [
    'pageTitle' => 'Manual Enrollments | Index'
])

@section('content')
    {!! Breadcrumbs::render('manual-enrollments.index') !!}

    <div class="row">
        <div class="col">
            <h1>Manual Enrollment Batches</h1>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            @permission('manual-enrollments.create')
            <a href="{{ route('manual-enrollments.create') }}" class="btn btn-success btn-sm" title="Add New Batch of Manual Enrollments">
                <i class="fa fa-plus" aria-hidden="true"></i> New Batch
            </a>
            @endpermission
        </div>
    </div>
    @permission('manual-enrollments.read')
        @if ($batches->isEmpty())
            <p>No batches to show.</p>
        @else
            @component('components.paginated-table', ['collection' => $batches])
                @slot('table')
                    @component('components.table')
                        @slot('th')
                            <th>ID</th>
                            <th>Created By</th>
                            <th>Destination</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Actions</th>
                        @endslot
                        @slot('tbody')
                            @foreach ($batches as $batch)
                                <tr>
                                    <td>{{ $batch->id }}</td>
                                    <td>{{ optional($batch->created_by_user)->email ?? '' }}</td>
                                    <td>{{ $batch->destination_type }}: {{ $batch->destination_id }}</td>
                                    <td>
                                        @include('partials.statuses.' . $batch->status)
                                    </td>
                                    <td>{{ $batch->created_at }}</td>
                                    <td class="d-flex justify-content-start align-items-center">
                                        <a href="{{ route('manual-enrollments.show', $batch) }}" class="btn btn-sm btn-info">
                                            <i class="fas fa-list"></i> Details
                                        </a>
                                        @include('manual-enrollments.partials.action-buttons')
                                    </td>
                                </tr>
                            @endforeach
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        @endif
    @endpermission
@endsection
