@extends('layouts.wrapper', [
    'pageTitle' => 'Manual Enrollment | Create'
])

@section('content')

<h2>New Manual Enrollment Batch</h2>
<p>Each set of Manual Enrollments will be created as a <strong>Batch</strong>, with each individual operation as a separate Manual Enrollment record.</p>

<hr>

{!! Form::open()->method('post')->route('manual-enrollments.store')->fill(collect(request()->old())) !!}

<div class="card mb-3">
    <div class="card-header">Destination Information</div>
    <div class="card-body">
        <div class="form-row">
            <div class="col">
                {!! Form::select('destination_type', 'Destination Type', ['section' => 'Section', 'course' => 'Course'])->required() !!}
            </div>
            <div class="col">
                {!! Form::text('destination_id', 'Destination ID')->required()->help('The Canvas Course ID or Section ID (not SIS ID)') !!}
            </div>
        </div>
        <div class="form-row">
            <div class="col text-center">
                <p>
                    <button type="button" class="btn btn-info" id="load_roles">
                        <i class="fas fa-cloud-download-alt"></i>
                        Load roles for course/section
                    </button>
                </p>
                <p>
                    <small id="loading-roles" class="d-none"><i class="fas fa-spin fa-spinner"></i> <span class="text-muted">Retrieving roles from Canvas...</span></small>
                    <small id="invalid-account-id" class="d-none"><span class="text-danger">Account roles could not be retrieved from Canvas</span></small>
                    <small id="valid-account-id" class="d-none"><span class="text-success"><i class="fa fa-check"></i> Account roles retrieved from Canvas</span></small>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="card mb-3">
    <div class="card-header">Enrollment Information</div>
    <div class="card-body">
        <div class="form-row">
            <div class="col">
                {!! Form::text('account', 'Account')->placeholder('(Load roles first)')->readonly() !!}
                {!! Form::select('role_id', 'Role', ['' => '(Load roles first)'])->required()->disabled() !!}
                {!! Form::select('user_id_type', 'User ID Type', ['login_id' => 'SIS Login ID', 'canvas_id' => 'Canvas ID', 'sis_user_id' => 'SIS ID'])->required() !!}
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="user_list">User IDs to Enroll</label>
                    <textarea rows="15" type="textarea" name="user_list" id="user_list" class="form-control" required=""></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card mb-3">
    <div class="card-header">Additional Information</div>
    <div class="card-body">
        <div class="form-group">
            <label for="notes">Notes</label>
            <textarea rows="5" type="textarea" name="notes" id="notes" class="form-control"></textarea>
        </div>
    </div>
</div>

{!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success')->attrs(['class' => 'mb-3']) !!}

@endsection

@section('scripts')

    <script>
        $("#load_roles").on('click', function() {

            $("#loading-roles").removeClass('d-none');
            $("#invalid-account-id").addClass('d-none');
            $("#valid-account-id").addClass('d-none');

            $.ajax({
                type: "GET",
                url: "{{ route('manual-enrollments.list-roles-select-element') }}",
                data: {
                    "destination_type": $("#destination_type").val(),
                    "destination_id": $("#destination_id").val(),
                }
            }).done(function (data, textStatus, jqXHR) {
                // console.log(data.data);
                $("#role_id").html(data.options).removeAttr('disabled');
                $("#account").val(data.account.id + " - " + data.account.name);
                $("#valid-account-id").removeClass('d-none');
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // console.log(errorThrown, jqXHR);
                $("#role_id").html("<option value=\"\">(Account not found)</option>").attr('disabled', 'disabled')
                $("#account").val("(Account not found)")
                $("#invalid-account-id").removeClass('d-none');
            }).always(function (data, textStatus, jqXHR) {
                $("#loading-roles").addClass('d-none');
            });

        });
    </script>

@endsection
