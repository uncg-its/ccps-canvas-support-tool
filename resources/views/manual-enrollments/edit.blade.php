@extends('layouts.wrapper', [
    'pageTitle' => 'Manual Enrollment | Edit'
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit Manual Enrollment #{{ $manualenrollment->id }}</div>
                    <div class="panel-body">
                        <a href="{{ route('manualenrollments.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <p>NOTE: If you need to edit the role, delete this item and then resubmit the form again.</p>

                        <form method="POST" action="{{ route('manualenrollments.update', $manualenrollment->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('manualenrollments.form', ['submitButtonText' => 'Update'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>

        $( "#accountId" ).change(function()
        {
            //this is the #state dom element
            var state = $(this).val();

            // parameter 1 : url
            // parameter 2: post data
            //parameter 3: callback function

            var thisAccountId = $( "#accountId option:selected" ).val();

            $.get( '{{ route('manualenrollments.list-roles-select-element') }}' + '?accountId=' + thisAccountId, { state : state } , function(htmlCode) { //htmlCode is the code returned from your controller
                $("#role-select-container").html(htmlCode);
            });
        });
    </script>

@endsection
