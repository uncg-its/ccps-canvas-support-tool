<div class="form-group {{ $errors->has('course_or_section') ? 'has-error' : ''}}">
    <label for="course_or_section" class="col-md-4 control-label">{{ 'Course Or Section' }}</label>
    <div class="col-md-6">
        <p>Can only select section for now....</p>
        <select name="course_or_section" class="form-control" id="course_or_section">
            @foreach (json_decode('{"section":"section"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($manualenrollment->course_or_section) && $manualenrollment->course_or_section == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('course_or_section', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('course_or_section_id') ? 'has-error' : ''}}">
    <label for="course_or_section_id" class="col-md-4 control-label">{{ 'Course Or Section Id' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="course_or_section_id" type="text" id="course_or_section_id"
               value="{{ $manualenrollment->course_or_section_id ?? ''}}">
        {!! $errors->first('course_or_section_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@php if(!isset($manualenrollment->user_id)): @endphp
<div class="form-group {{ $errors->has('accountId') ? 'has-error' : ''}}">
    <label for="accountId" class="col-md-4 control-label">{{ 'Account for Role Selection' }}</label>
    <div class="col-md-6">
        <select name="accountId" class="form-control" id="accountId" >
            @foreach ($accountList as $optionKey => $optionValue)
                @php $accountParts = explode('___', $optionKey) @endphp
                <option value="{{ $accountParts[1] }}" {{ (isset($manualenrollment->accountId) && $manualenrollment->accountId == $accountParts[1]) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        <small id="loading-roles" class="d-none"><i class="fas fa-spin fa-spinner"></i> <span class="text-muted">Retrieving roles from Canvas...</span></small>
        <small id="invalid-account-id" class="d-none"><span class="text-danger">Account roles could not be retrieved from Canvas</span></small>
        <small id="valid-account-id" class="d-none"><span class="text-success"><i class="fa fa-check"></i> Account roles retrieved from Canvas</span></small>
    </div>
</div>
<div id="role-select-container">
    <div class="form-group {{ $errors->has('roleId') ? 'has-error' : ''}}">
        <label for="roleId" class="col-md-4 control-label">{{ 'Role ID' }}</label>
        <div class="col-md-6">
            <select name="roleId" class="form-control" id="roleId">
                <option value="NULL" >SELECT ACCOUNT FIRST</option>
            </select>
            {!! $errors->first('roleId', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
@php endif; @endphp
<div class="form-group {{ $errors->has('user_id_type') ? 'has-error' : ''}}">
    <label for="user_id_type" class="col-md-4 control-label">{{ 'User ID Type' }}</label>
    <div class="col-md-6">
        <select name="user_id_type" class="form-control" id="user_id_type">
            @foreach (json_decode('{"0":"sis_user_id","2":"canvas_user_id","3":"login_id"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionValue }}" {{ (isset($manualenrollment->user_id_type) && $manualenrollment->user_id_type == $optionValue) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('course_or_section', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('enrollment-type') ? 'has-error' : ''}}">
    <label for="enrollment-type" class="col-md-4 control-label">{{ 'Enrollment Type' }}</label>
    <div class="col-md-6">
        <select name="enrollment-type" class="form-control" id="enrollment-type">
            @foreach (json_decode('{"0":"pasted-user-list","1":"canvas-sync"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionValue }}" {{ (isset($manualenrollment->status) && $manualenrollment->status == $optionValue) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('enrollment-type', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('paste_list') ? 'has-error' : ''}}">
    <label for="paste_list" class="col-md-4 control-label">{{ 'Paste List' }}</label>
    <div class="col-md-6">
        @if(@isset($manualenrollment->user_id))
            <input class="form-control" name="paste_list" type="text" id="paste_list" value="{{ $manualenrollment->user_id ?? ''}}">
        @else
            <textarea rows="10" class="form-control" name="paste_list"id="paste_list">{{ $manualenrollment->user_id ?? ''}}</textarea>
        @endif
        {!! $errors->first('paste_list', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-md-4 control-label">{{ 'Status' }}</label>
    <div class="col-md-6">
        <select name="status" class="form-control" id="status">
                <option value="pending-approval">pending-approval</option>
        </select>
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('notes') ? 'has-error' : ''}}">
    <label for="notes" class="col-md-4 control-label">{{ 'Notes' }}</label>
    <div class="col-md-6">
        <textarea class="form-control" rows="5" name="notes" type="textarea"
                  id="notes">{{ $manualenrollment->notes ?? ''}}</textarea>
        {!! $errors->first('notes', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText ?? 'Create' }}">
    </div>
</div>
