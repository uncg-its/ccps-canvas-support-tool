@extends('layouts.wrapper', [
    'pageTitle' => 'Manual Enrollment Batch | Show'
])

@section('content')
    {!! Breadcrumbs::render('manual-enrollments.show', $batch) !!}

    <h2>Manual Enrollment Batch #{{ $batch->id }}</h2>
    <div class="row">
        <div class="col-6">
            <div class="card mb-3">
                <div class="card-header">Batch information</div>
                <div class="card-body">
                    <p><strong>Created by</strong>: {{ optional($batch->created_by_user)->email ?? 'unknown (legacy record)' }}</p>
                    <p><strong>Created at</strong>: {{ $batch->created_at }}</p>
                    <p><strong>Destination</strong>: {{ $batch->destination_type }}: {{ $batch->destination_id }}</p>
                    <p><strong>Status</strong>: @include('partials.statuses.' . $batch->status)</p>
                    <p><strong>Notes</strong>: {{ $batch->notes }}</p>
                    <p><strong>Enrollment Count</strong>: {{ $enrollments->count() }}</p>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">Actions</div>
                <div class="card-body d-flex flex-content-start align-items-center">
                    @include('manual-enrollments.partials.action-buttons')
                </div>
            </div>

        </div>
    </div>
    <hr>
    <h3>Enrollments</h3>
    @if($batch->manual_enrollments->isEmpty())
        <p>Well that's strange. This Batch has no enrollments...</p>
    @else
        @component('components.paginated-table', ['collection' => $enrollments])
            @slot('table')
                @component('components.table')
                    @slot('th')
                        <th>User ID</th>
                        <th>User ID Type</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Canvas Enrollment ID</th>
                        <th>Actions</th>
                    @endslot
                    @slot('tbody')
                        @foreach ($enrollments as $enrollment)
                        <tr>
                            <td>{{ $enrollment->enrolled_user_id }}</td>
                            <td>{{ $enrollment->enrolled_user_id_type }}</td>
                            <td>{{ $enrollment->role_type }}</td>
                            <td>
                                @include('partials.statuses.' . $enrollment->status)
                                @if ($enrollment->status === 'error')
                                    <!-- Button trigger modal -->
                                    <span class="ml-2" style="cursor: pointer;"><i class="fas fa-info-circle" data-toggle="modal" data-target="#error_{{ $enrollment->id }}"></i></span>
                                    @component('components.modal', [
                                        'title' => 'Error Details',
                                        'id' => 'error_' . $enrollment->id
                                    ])
                                        @slot('content')
                                            {{ $enrollment->notes }}
                                        @endslot
                                    @endcomponent
                                @endif
                            </td>
                            <td>{{ $enrollment->canvas_enrollment_id }}</td>
                            <td class="d-flex justify-content-start align-items-center">
                                @permission('manual-enrollments.update')
                                @if($enrollment->status === 'error')
                                    {!! Form::open()->post()->route('manual-enrollments.retry', ['manualEnrollment' => $enrollment]) !!}
                                    {!! Form::submit('<i class="fas fa-redo"></i> Retry')->attrs(['class' => 'btn btn-xs btn-danger mr-3']) !!}
                                    {!! Form::close() !!}
                                @endif
                                @endpermission
                            </td>
                        </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    @endif
@endsection
