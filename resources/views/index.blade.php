@extends('layouts.wrapper', [
'pageTitle' => 'Home'
])

@section('content')
{!! Breadcrumbs::render('index') !!}
@if($user)
<h1>Canvas Tools</h1>
<div class="row">
    @permission('canvas.*')
    <div class="col-6 mb-3">
        <h2>Live Lookup tools</h2>
        <div class="row">
            @permission('canvas.accounts.view')
                @include('components.panel-nav', [
                    'url' => route('live.accounts.index'),
                    'fa' => 'fas fa-list',
                    'title' => 'Canvas Accounts'
                ])
            @endpermission
            @permission('canvas.users.view')
                @include('components.panel-nav', [
                    'url' => route('live.users.index'),
                    'fa' => 'fas fa-user-circle',
                    'title' => 'User Lookup'
                ])
            @endpermission
            @permission('canvas.courses.view')
                @include('components.panel-nav', [
                    'url' => route('live.courses.index'),
                    'fa' => 'fas fa-search',
                    'title' => 'Course Search'
                ])
            @endpermission
            @permission('live-lookups.view')
                @include('components.panel-nav', [
                    'url' => route('live.lookups.index'),
                    'fa' => 'fas fa-clipboard-list',
                    'title' => 'Live Lookup Log'
                ])
            @endpermission
        </div>
    </div>
    @endpermission

    @permission('crosslist.create|enrollmentrequests.*')
    <div class="col-6 mb-3">
        <h2>Instructional Technology Support</h2>
        <div class="row">
            @permission('crosslist.create')
                @include('components.panel-nav', [
                    'url' => route('crosslist.index'),
                    'fa' => 'fas fa-sync',
                    'title' => 'Crosslist'
                ])
            @endpermission
            @permission('enrollmentrequests.*')
                @include('components.panel-nav', [
                    'url' => route('enrollmentrequests.index'),
                    'fa' => 'fas fa-user-plus',
                    'title' => 'Enrollment Requests'
                ])
            @endpermission
            @permission('batch-sandbox.*')
                @include('components.panel-nav', [
                    'url' => route('batch-sandbox-courses.index'),
                    'fa' => 'fas fa-sitemap',
                    'title' => 'Batch Sandbox Courses'
                ])
            @endpermission
        </div>
    </div>
    @endpermission

    @permission('course-activity-report.edit|lti-reports.*|quiz-log-reports.*')
    <div class="col-6 mb-3">
        <h2>Reporting Tools</h2>
        <div class="row">
            @permission('course-activity-report.edit')
                @include('components.panel-nav', [
                    'url' => route('canvas.course-activity-report.index'),
                    'fa' => 'fas fa-book',
                    'title' => 'Course Activity Reports'
                ])
            @endpermission
            @permission('lti-reports.*')
                @include('components.panel-nav', [
                    'url' => route('canvas.lti-reports.index'),
                    'fa' => 'fas fa-toolbox',
                    'title' => 'LTI Reports'
                ])
            @endpermission
            @permission('quiz-log-reports.*')
                @include('components.panel-nav', [
                    'url' => route('canvas.quiz-log-reports.index'),
                    'fa' => 'fas fa-user-clock',
                    'title' => 'Quiz Log Reports'
                ])
            @endpermission
            @permission('nav-item-reports.*')
                @include('components.panel-nav', [
                    'url' => route('nav-item-reports.index'),
                    'fa' => 'fas fa-map-marked',
                    'title' => 'Nav Item Reports'
                ])
            @endpermission

        </div>
    </div>
    @endpermission

    @permission('canvas.courses.create|manual-enrollments.*|lti.*|page-views.*|nav-item-replacer.*')
    <div class="col-6 mb-3">
        <h2>Canvas Admin Support</h2>
        <div class="row">
            @permission('canvas.courses.create')
                @include('components.panel-nav', [
                    'url' => route('courses.create'),
                    'fa' => 'fas fa-plus',
                    'title' => 'Create a Course'
                ])
            @endpermission
            @permission('manual-enrollments.read')
                @include('components.panel-nav', [
                    'url' => route('manual-enrollments.index'),
                    'fa' => 'fas fa-users',
                    'title' => 'Manual Enrollments'
                ])
            @endpermission
            @permission('lti.read')
                @include('components.panel-nav', [
                    'url' => route('lti.index'),
                    'fa' => 'fab fa-skyatlas',
                    'title' => 'LTI Inventory'
                ])
            @endpermission
            @permission('page-views.read')
                @include('components.panel-nav', [
                    'url' => route('canvas.page-view-audits.index'),
                    'fa' => 'fas fa-glasses',
                    'title' => 'Page View Audit'
                ])
            @endpermission
            @permission('nav-item-replacer.edit')
                @include('components.panel-nav', [
                    'url' => route('canvas.nav-item-replacer.index'),
                    'fa' => 'fas fa-bars',
                    'title' => 'Nav Item Replacer'
                ])
            @endpermission
            @permission('lti.read')
                @include('components.panel-nav', [
                    'url' => route('lti-editor.index'),
                    'fa' => 'fas fa-pencil-alt',
                    'title' => 'LTI Editor'
                ])
            @endpermission
        </div>
    </div>
    @endpermission
</div>

@role('admin')
<div class="col-6 mb-3">
    <h2>Application Administrator</h2>
    <div class="row">
        @include('components.panel-nav', [
            'url' => route('admin'),
            'fa' => 'fas fa-key',
            'title' => 'Administrator'
        ])
    </div>
</div>
@endrole

@else
<h1>{{ config('app.name') }}</h1>
<div class="row">
    <div class="col">
        <p>You are not logged in - please <a href="{{ route('login') }}">log in</a> to proceed.</p>
    </div>
</div>
@endif
@endsection
