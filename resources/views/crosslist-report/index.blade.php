@extends('layouts.wrapper', [
    'pageTitle' => 'Crosslist Report | Index'
])

@section('content')
    {!! Breadcrumbs::render('crosslist-report.index') !!}
    <h1>Crosslist Report</h1>
    <p>This page shows a historical list of crosslist operations.</p>

    <livewire:crosslist-report />
@endsection
