@extends('layouts.wrapper', [
    'pageTitle' => 'LTI Editor | Index'
])

@section('content')
    <h1>LTI Editor</h1>
    <p>This tool allows you to do things that the Canvas GUI does not - such as edit an LTI Tool's name or launch URL. You must first select an account and load its LTI tools - then you can select one to edit.</p>
    <hr>
    {!! Form::open()->get()->route('lti-editor.edit') !!}
    <div class="row">
        <div class="col">
            <div class="card mb-3">
                <div class="card-header">Account selection</div>
                <div class="card-body">
                    {!! Form::select('account_id', 'Account ID', $accounts, old('account_id')) !!}
                    <button type="button" role="button" id="load_tools" class="btn btn-info"><i class="fas fa-truck-loading"></i> Load LTI Tools</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-3">
                <div class="card-header">Tool selection</div>
                <div class="card-body">
                    {!! Form::select('tool_id', 'Tool', [])->required() !!}
                    <div class="mb-3">
                        <small id="loading-tools" class="d-none"><i class="fas fa-spin fa-spinner"></i> <span class="text-muted">Retrieving tools from Canvas...</span></small>
                        <small id="invalid-account-id" class="d-none"><span class="text-danger">Account tools could not be retrieved from Canvas</span></small>
                        <small id="valid-account-id" class="d-none"><span class="text-success"><i class="fa fa-check"></i> Account tools retrieved from Canvas</span></small>
                    </div>
                    {!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success')->id('role-submit')->disabled(true) !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection

@section('scripts')
    <script>
        var returnValue;

        $(function() {
            $('#load_tools').on('click', function () {
                $("#loading-tools").removeClass('d-none');
                $("#invalid-account-id").addClass('d-none');
                $("#valid-account-id").addClass('d-none');

                $("#role-submit").attr('disabled', 'disabled');

                var id = $("#account_id").val().split("___")[1];

                $.ajax({
                    type: 'post',
                    url: "{{ route('lti-editor.load-tools') }}",
                    data: {
                        account_id: id,
                        _token: '{{ csrf_token() }}'
                    }
                }).done(function (data, textStatus, jqXHR) {
                    returnValue = data;

                    var $dropdown = $("#tool_id");
                    $dropdown.html('');
                    $dropdown.append('<option value="">' + '--- Select a Tool ---' + '</option>');

                    var options = data.data.tools;
                    if (options != '') {
                        $.each(options, function(key, value) {
                            $dropdown.append('<option value="' + key + '">' + value + '</option>');
                        });
                    }

                    $("#valid-account-id").removeClass('d-none');
                    $("#role-submit").attr('disabled', false);

                }).fail(function (jqXHR, textStatus, errorThrown) {
                    $("#invalid-account-id").removeClass('d-none');
                }).always(function(data, textStatus, jqXHR) {
                    $("#loading-tools").addClass('d-none');
                });
            })
        });
    </script>
@endsection
