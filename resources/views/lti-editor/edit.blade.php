@extends('layouts.wrapper', [
    'pageTitle' => 'LTI Editor | Edit'
])

@section('content')
    <h1>Edit LTI Tool: {{ $tool->name }}</h1>
    <p>Change the desired properties below, and submit the form when ready. Your changes will be sent via the Canvas API immediately.</p>
    <hr>
    <div class="row">
        <div class="col">
            <div class="card mb-3">
                <div class="card-header">LTI Tool Information</div>
                <div class="card-body">
                    {!! Form::open()->patch()->route('lti-editor.update', ['toolId' => $tool->id]) !!}
                    {!! Form::hidden('account_id', $account_id) !!}
                    {!! Form::text('name', 'Name', old('name', $tool->name))->required()->help('The internal name of the tool, displayed in the tool listing only (NOT NAV)') !!}
                    {!! Form::text('description', 'Description', old('description', $tool->description))->required()->help('The internal description of the tool, displayed in the tool listing only') !!}
                    {!! Form::text('url', 'Launch URL', old('url', $tool->url))->required()->help('The URL that the tool will point to when clicked.') !!}
                    {!! Form::text('course_navigation_text', 'Course Nav Label', old('course_navigation_text', $tool->course_navigation->text ?? $tool->course_navigation->label))->required()->help('The tool\'s name as displayed in the navigation bar') !!}
                    {!! Form::select('default_enabled', 'Enabled by default?', [0 => 'disabled', 1 => 'enabled'], old('default_enabled', ($tool->course_navigation->default ?? '') === 'enabled'))->required()->help('Whether this tool should be shown in the course nav by default') !!}
                    {!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success') !!}
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
@endsection
