@extends('layouts.wrapper', [
    'pageTitle' => 'Nav Item Reports: ' . $report->id
])

@section('head')
    @livewireStyles
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
@append

@section('content')
    <h1>Nav Item Report {{ $report->id }}</h1>
    <div class="card">
        <div class="card-header">Report Information</div>
        <div class="card-body">
            <p><strong>Created By</strong>: {{ $report->created_by_user->email }}</p>
            <p><strong>Account</strong>: {{ $report->account_name }} ({{ $report->account_id }})</p>
            <p><strong>Term</strong>: {{ $report->term_name }} ({{ $report->term_id }})</p>
            <p><strong>Status</strong>: @include('partials.statuses.' . $report->status)</p>
        </div>
    </div>

    <h2>Results</h2>
    <p>Type a search term in the box below to filter through and look for the status of a certain Navigation Item by name. <em>Example: Modules</em></p>
    <div class="card mb-4">
        <div class="card-header">Search</div>
        <div class="card-body">
            <livewire:nav-item-report-search :report="$report" />
        </div>
    </div>


@endsection

@section('scripts')
    @livewireScripts
@endsection
