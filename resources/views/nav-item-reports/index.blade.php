@extends('layouts.wrapper', [
    'pageTitle' => 'Nav Item Reports - Index'
])

@section('head')
    @livewireStyles
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
@append

@section('content')
    <div x-data="{showCreate: false}">
        <div class="d-flex justify-content-between align-items-center">
            <h1>Nav Item Reports</h1>
            <div>
                @permission('nav-item-reports.create')
                <button class="btn btn-success" x-on:click="showCreate = true">
                    <i class="fas fa-plus"></i> Create New
                </button>
                @endpermission
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p>Nav Item Reports will pull navigation menu data from all courses in an account that are both <strong>published</strong> and <strong>contain at least one enrollment</strong>. Once the report is complete, you can search the results to determine how many of those courses contain a specific menu item that has been <strong>enabled</strong> in navigation.</p>
                <p><em>Nav Item Reports are point-in-time data. Running the same report at different times will likely reveal different data - it is recommended to run a new report unless you know that the data will not have changed (e.g. old semester).</em></p>
            </div>
        </div>

        <div x-show="showCreate">
            <livewire:nav-item-report-form />
        </div>

        <div class="row mt-3">
            <div class="col">
                <h2>Existing Reports</h2>
                <livewire:nav-item-report-table />
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @livewireScripts
@endsection
