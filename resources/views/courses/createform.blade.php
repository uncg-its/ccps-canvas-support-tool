<div class="form-group {{ $errors->has('course_name') ? 'has-error' : ''}}">
    <label for="paste_list" class="col-md-4 control-label">{{ 'Course Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="course_name" type="text" id="course_name" value="">
        {!! $errors->first('course_name', '<p class="help-block">:message</p>') !!}
        <p class="help-block">If this is an Org, make sure to prefix it with (ORG) or other applicable text.</p>
    </div>
</div>
<div class="form-group {{ $errors->has('course_code') ? 'has-error' : ''}}">
    <label for="course_code" class="col-md-4 control-label">{{ 'Course Short Code' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="course_code" type="text" id="course_code" value="">
        {!! $errors->first('course_code', '<p class="help-block">:message</p>') !!}
        <p class="help-block">IE: Shortened Acronym for this course to help identify it.</p>
    </div>
</div>
<div class="form-group {{ $errors->has('start_at') ? 'has-error' : ''}}">
    <label for="start_at" class="col-md-4 control-label">{{ 'Start At' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="start_at" type="text" id="start_at" value="">
        {!! $errors->first('start_at', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('end_at') ? 'has-error' : ''}}">
    <label for="end_at" class="col-md-4 control-label">{{ 'End At' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="end_at" type="text" id="end_at" value="">
        {!! $errors->first('end_at', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('term_id') ? 'has-error' : ''}}">
    <label for="term_id" class="col-md-4 control-label">{{ 'Term ID' }}</label>
    <div class="col-md-6">
        <select name="term_id" class="form-control" id="term_id">
            @foreach ($termList as $termId => $termName)
                <option value="{{ $termId }}">{{ $termName }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group {{ $errors->has('accountId') ? 'has-error' : ''}}">
    <label for="accountId" class="col-md-4 control-label">{{ 'Account for Role Selection + Course Placement' }}</label>
    <div class="col-md-6">
        <select name="accountId" class="form-control" id="accountId">
            @foreach ($accountList as $optionKey => $optionValue)
                @php $accountParts = explode('___', $optionKey) @endphp
                <option value="{{ $accountParts[1] }}">{{ $optionValue }}</option>
            @endforeach
        </select>
        <small id="loading-roles" class="d-none"><i class="fas fa-spin fa-spinner"></i> <span class="text-muted">Retrieving roles from Canvas...</span></small>
        <small id="invalid-account-id" class="d-none"><span class="text-danger">Account roles could not be retrieved from Canvas</span></small>
        <small id="valid-account-id" class="d-none"><span class="text-success"><i class="fa fa-check"></i> Account roles retrieved from Canvas</span></small>
    </div>
</div>
<div id="role-select-container">
    <div class="form-group {{ $errors->has('roleId') ? 'has-error' : ''}}">
        <label for="roleId" class="col-md-4 control-label">{{ 'Select Role for Initial Users' }}</label>
        <div class="col-md-6">
            <select name="roleId" class="form-control" id="roleId">
                <option value="NULL" >SELECT ACCOUNT FIRST</option>
            </select>
            {!! $errors->first('roleId', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-group {{ $errors->has('user_id_type') ? 'has-error' : ''}}">
    <label for="user_id_type" class="col-md-4 control-label">{{ 'Initial User ID Type' }}</label>
    <div class="col-md-6">
        <select name="user_id_type" class="form-control" id="user_id_type">
            @foreach (json_decode('{"0":"login_id","2":"canvas_user_id","3":"sis_user_id"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionValue }}">{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('user_id_type', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('paste_list') ? 'has-error' : ''}}">
    <label for="paste_list" class="col-md-4 control-label">{{ 'List of Initial Users to Add' }}</label>
    <div class="col-md-6">
        <textarea rows="10" class="form-control" name="paste_list"id="paste_list"></textarea>
        <p class="help-block">Separated by comma, newline, or semicolon</p>
        {!! $errors->first('paste_list', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText ?? 'Create Now' }}">
    </div>
</div>
