@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas Courses | Create'
])

@section('head')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@endsection

@section('content')
    <h1>Create New Canvas Course</h1>
    <div class="card mb-3">
        <div class="card-header">Course Information</div>
        <div class="card-body">

            {!! Form::open()->method('post')->route('courses.store')->fill(collect(request()->old())) !!}
            <div class="row">
                <div class="col-6">
                    {!! Form::text('course_name', 'Course Name')->required(true)->help('If this is an Org, make sure to prefix it with (ORG) or other applicable text.') !!}
                </div>
                <div class="col-6">
                    {!! Form::text('course_code', 'Course Short Code')->required(true)->help('Shortened version of the course name. Appears when inside the course, above the nav menu.') !!}
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    {!! Form::text('start_at', 'Start At')->required(false)->help('The Start Date (optional) for the course') !!}
                </div>
                <div class="col-6">
                    {!! Form::text('end_at', 'End At')->required(false)->help('The End Date (optional) for the course') !!}
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    {!! Form::select('term_id', 'Term ID', $termList)->required(true)->help('The Term to which this course should belong') !!}
                </div>
                <div class="col-4">
                    {!! Form::select('account_id', 'Account ID', $accountList)->required(true)->help('The Account to which this course should belong. Populates roles list below upon selection.') !!}
                    <div class="mb-4">
                        <small id="loading-roles" class="d-none"><i class="fas fa-spin fa-spinner"></i> <span class="text-muted">Retrieving roles from Canvas...</span></small>
                        <small id="invalid-account-id" class="d-none"><span class="text-danger">Account roles could not be retrieved from Canvas</span></small>
                        <small id="valid-account-id" class="d-none"><span class="text-success"><i class="fa fa-check"></i> Account roles retrieved from Canvas</span></small>
                    </div>
                </div>
                <div class="col-4">
                    {!! Form::select('role_id', 'Role for initial users', ['' => 'Select Account first'])->required(true)->help('The role to give to your initial users') !!}
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-6 offset-3">
                    {!! Form::select('user_id_type', 'Initial User ID Type', ['login_id' => 'SIS Login ID', 'canvas_user_id' => 'Canvas User ID', 'sis_user_id' => 'SIS User ID'])->required(true)->help('The type of ID you will be supplying in the initial user list<ul><li><strong>SIS Login ID</strong> - UNCG username</li><li><strong>Canvas User ID</strong> - numeric value, found in URL</li><li><strong>SIS User ID</strong> - numeric value (SIS ID), found in profile</li>') !!}
                    {!! Form::textarea('paste_list', 'Initial Users list')->required(false)->help('List of initial users to add to this course. Separated by comma, newline, or semicolon.') !!}
                </div>
            </div>

            <div class="row">
                <div class="col">
                    {!! Form::submit('<i class="fas fa-check"></i> Create Now')->color('success') !!}
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')


    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script>

        $("#account_id").change(function() {

            $("#loading-roles").removeClass('d-none');
            $("#invalid-account-id").addClass('d-none');
            $("#valid-account-id").addClass('d-none');

            $.ajax({
                type: "GET",
                url: "{{ route('courses.list-roles-select-element') }}",
                data: {
                    "accountId": $("#account_id").val(),
                }
            }).done(function (data, textStatus, jqXHR) {
                console.log(data);
                $("#role_id").html(data);
                $("#valid-account-id").removeClass('d-none');
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown, jqXHR);
                $("#invalid-account-id").removeClass('d-none');
            }).always(function (data, textStatus, jqXHR) {
                $("#loading-roles").addClass('d-none');
            });

        });

        $('#start_at').datepicker();

        $('#end_at').datepicker();


    </script>

@endsection
