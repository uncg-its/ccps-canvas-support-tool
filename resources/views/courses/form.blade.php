<div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}">
    <label for="course_id" class="col-md-4 control-label">{{ 'Canvas Course ID' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="course_id" type="text" id="course_id" required
               value="{{ $course->id ?? ''}}">
        {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ 'Search Now' }}">
    </div>
</div>
{{ csrf_field() }}