@extends('layouts.wrapper', [
    'pageTitle' => 'Live Lookup Log'
])

@section('content')
    {!! Breadcrumbs::render('live.lookups.index') !!}

    <h1>Live Lookups Log</h1>
    @component('components.paginated-table', ['collection' => $lookups])
        @slot('table')
            @component('components.table')
                @slot('th')
                    <th>ID</th>
                    <th>Type</th>
                    <th>Item ID</th>
                    <th>User</th>
                    <th>Performed at</th>
                @endslot
                @slot('tbody')
                    @foreach ($lookups as $lookup)
                        <tr>
                            <td>{{ $lookup->id }}</td>
                            <td>{{ $lookup->type }}</td>
                            <td>{{ $lookup->item_id }}</td>
                            <td>{{ $lookup->user->email }}</td>
                            <td>{{ $lookup->created_at }}</td>
                        </tr>
                    @endforeach
                @endslot
            @endcomponent
        @endslot
    @endcomponent
@endsection()
