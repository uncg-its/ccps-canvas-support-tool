@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas | Live Lookup | Courses'
])

@section('content')
    <h1>Canvas Courses (Live Lookup)</h1>
    <p>This tool allows you to look up a Canvas course and get its details. You can look up a course by its <strong>Canvas ID</strong> or by its <strong>SIS ID</strong>.</p>

    <hr>

    <div class="card mb-3">
        <div class="card-header">Course search</div>
        <div class="card-body">
            {!! Form::open()->method('get')->route('live.courses.index') !!}
            <div class="form-row">
                <div class="col">
                    {!! Form::select('enrollment_term_id', 'Term ID', $terms->toArray(), old('enrollment_term_id', $enrollmentTermId ?? ''))->required() !!}
                </div>
                <div class="col">
                    {!! Form::text('search_term', 'Search Term', old('search_term', $searchTerm ?? ''))->required()->help('Searches course name, code, and full ID') !!}
                </div>
            </div>
            {!! Form::submit('<i class="fas fa-search"></i> Search')->color('info') !!}
            {!! Form::close() !!}
        </div>
    </div>

    @if(isset($results))
        <hr>
        <h2>Search results</h2>
        @if ($results->isNotEmpty())
            @component('components.paginated-table', ['collection' => $results])
                @slot('table')
                    @component('components.table')
                        @slot('th')
                            <th>Course Name</th>
                            <th>SIS ID</th>
                            <th>Term</th>
                            <th>Actions</th>
                        @endslot
                        @slot('tbody')
                            @foreach ($results as $result)
                                <tr>
                                    <td>{{ $result->name }}</td>
                                    <td>{{ $result->sis_course_id }}</td>
                                    <td>{{ $terms[$result->enrollment_term_id] }}</td>
                                    <td>
                                        <a href="{{ route('live.courses.show', ['id_type' => 'canvas_id', 'course_id' => $result->id]) }}" class="btn btn-sm btn-primary">
                                            <i class="fas fa-list"></i> Course Details
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        @else
            <p>No results for the given search term</p>
        @endif
    @endif

@endsection
