@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas Courses | Live Lookup'
])

@section('content')
    {!! Breadcrumbs::render('live.courses.show', $course) !!}

    <h1>Canvas Courses (Live Lookup)</h1>

    <h2>Canvas Course: {{ $course->name }}</h2>

    <div class="row">
        <div class="col">
            <div class="card mb-3">
                <div class="card-header">Course Information</div>
                <div class="card-body">
                    @include('partials.courses.course-info')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card mb-3">
                <div class="card-header">Section Information</div>
                <div class="card-body">
                    @include('partials.courses.section-list')
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card mb-3">
                <div class="card-header d-flex justify-content-between">
                    Enrollment List
                    <a href="{{ route('live.courses.download-enrollments', ['courseId' => $course->id]) }}" class="btn btn-sm btn-success">
                        <i class="fas fa-download"></i> Download as CSV
                    </a>
                </div>
                <div class="card-body">
                    <p class="float-right"><em>All times shown in timezeone: {{ config('app.timezone') }}</em></p>
                    @include('partials.courses.enrollment-list')
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">Navigation Tabs</div>
                <div class="card-body">
                    @include('partials.courses.tabs-list')
                </div>
            </div>

        </div>
    </div>
@endsection
