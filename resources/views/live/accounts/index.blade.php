@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas Accounts | Live Lookup'
])

@section('content')
    <h1>Canvas Accounts (Live Lookup)</h1>
    @if(count($accountList) > 0)
    <div class="table-responsive">
        <table class="table table-borderless">
            <thead>
                <tr>
                    <th>Account Name</th>
                </tr>
            </thead>
            <tbody>
            @foreach($accountList as $accountDepthAndId => $accountName)
                <tr>
                    <td>{{ $accountName }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @else

    <p>There were no accounts found.</p>

    @endif
@endsection
