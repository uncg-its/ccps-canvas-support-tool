@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas | Live Lookup | Enrollments'
])

@section('content')
    {!! Breadcrumbs::render('live.enrollments.index', $userName, $userId) !!}

    <h1>Canvas Enrollments (Live Lookup)</h1>
    <p>Showing enrollments for Canvas User: {{ $userName }}</p>
    <a href="{{ route('live.enrollments.download-user-enrollments', ['userId' => $userId]) }}" class="btn btn-success">
        <i class="fas fa-download"></i> Download all as CSV
    </a>
    <hr>
    <div class="row">
        <div class="col">
            @component('components.paginated-table', ['collection' => $courses, 'appends' => [
                'user_id' => $userId,
                'user_name' => $userName,
            ]])
                @slot('table')
                    @component('components.table', ['row' => true])
                        @slot('th')
                            <th class="col-2">Availability</th>
                            <th class="col-4">Course Name &amp; SIS ID</th>
                            <th class="col-2">Enrollment Type &amp; Status</th>
                            <th class="col-2">Course Start</th>
                            <th class="col-2">Course End</th>
                        @endslot
                        @slot('tbody')
                            @foreach ($courses as $course)
                                <tr class="row">
                                    <td class="col-2">{{ $course->workflow_state }}</td>
                                    <td class="col-4">
                                        <a href="{{ route('live.courses.show', ['id_type' => 'canvas_id', 'course_id' => $course->id]) }}">
                                            {{ $course->name }}
                                            @isset($course->sis_course_id)
                                                <br>
                                                <div class="text-break">
                                                    <em><small>{{ $course->sis_course_id }}</small></em>
                                                </div>
                                            @endisset
                                        </a>
                                    </td>
                                    <td class="col-2">
                                        {{ ucwords($course->enrollments[0]->type) }} <br>
                                        <em>({{ $course->enrollments[0]->enrollment_state }})</em>
                                    </td>
                                    <td class="col-2">
                                        {{ $course->start_at }}
                                    </td>
                                    <td class="col-2">
                                        {{ $course->end_at }}
                                    </td>
                                </tr>
                            @endforeach
                        @endslot
                    @endcomponent
                @endslot
            @endcomponent
        </div>
    </div>

@endsection
