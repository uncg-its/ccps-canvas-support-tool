@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas Users | Live Lookup'
])

@section('content')
    {!! Breadcrumbs::render('live.users.show', $canvasUser->name, $canvasUser->id) !!}

    <h1>Canvas Users (Live Lookup)</h1>
    <h2>Canvas User: {{ $canvasUser->name }}</h2>

    <div class="row">
        <div class="col-6">
            <div class="card mb-3">
                <div class="card-header">Account Information</div>
                <div class="card-body">
                    @include('partials.users.user-account-info')
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card mb-3">
                <div class="card-header">Profile Information</div>
                <div class="card-body">
                    @include('partials.users.user-profile-info')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="card mb-3">
                <div class="card-header">Actions</div>
                <div class="card-body d-flex justify-content-start align-items-center">
                    <a href="{{ route('live.enrollments.index', ['user_id' => $canvasUser->login_id, 'user_name' => $canvasUser->name]) }}" class="btn btn-sm btn-info">
                        <i class="fas fa-list"></i> List User Enrollments
                    </a>
                </div>
            </div>

        </div>
    </div>
@endsection
