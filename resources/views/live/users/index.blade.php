@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas Users | Live Lookup'
])

@section('content')
    {!! Breadcrumbs::render('live.users.index') !!}

    <h1>Canvas Users (Live Lookup)</h1>
    <div class="row">
        <div class="col">
            <div class="card mb-3">
                <div class="card-header">User search</div>
                <div class="card-body">
                    {!! Form::open()->method('get')->route('live.users.show') !!}
                    <div class="form-row">
                        <div class="col">
                        {!! Form::select('id_type', 'ID Type', ['sis_login_id' => 'SIS Login ID', 'canvas_id' => 'Canvas ID'], old('id_type'))->required() !!}
                        </div>
                        <div class="col">
                            {!! Form::text('user_id', 'User ID')->required()->help('A SIS Login ID is the user\'s UNCG username (e.g. <strong>JSMITH1</strong>).<br>A Canvas ID is a simple number, like <strong>12345</strong>') !!}
                        </div>
                    </div>
                    {!! Form::submit('<i class="fas fa-search"></i> Search')->color('info') !!}
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>


@endsection
