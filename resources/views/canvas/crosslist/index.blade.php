@extends('layouts.wrapper', [
    'pageTitle' => 'Crosslist | Index'
])

@section('content')
    {!! Breadcrumbs::render('crosslist.index') !!}
    <h1>Cross Listing</h1>

    <p>Use the <strong>Crosslist</strong> to cross-list one or many child sections into one parent. Use the <strong>De-Crosslist</strong> to undo a cross-list.</p>

    <div class="row">
        @permission('crosslist.create')
            @include('components.panel-nav', [
                'url' => route('crosslist.create'),
                'fa' => 'fas fa-object-group',
                'title' => 'Crosslist',
                'helpText' => 'Crosslist one or more child sections into one parent'
            ])
        @endpermission
        @permission('crosslist.delete')
            @include('components.panel-nav', [
                'url' => route('crosslist.decrosslist'),
                'fa' => 'fas fa-undo',
                'title' => 'De-Crosslist',
                'helpText' => 'Delete a cross-listing'
            ])
        @endpermission
        @permission('crosslist-report.view')
            @include('components.panel-nav', [
                'url' => route('crosslist-report.index'),
                'fa' => 'fas fa-clipboard-list',
                'title' => 'Crosslist Report',
                'helpText' => 'Historical report of crosslists'
            ])
        @endpermission
    </div>
@endsection
