@extends('layouts.wrapper', [
    'pageTitle' => 'Crosslist'
])

@section('head')
    @livewireStyles
@append

@section('content')
    {!! Breadcrumbs::render('crosslist.create') !!}
    @livewire('crosslist')
@endsection

@section('scripts')
    @livewireScripts
@append
