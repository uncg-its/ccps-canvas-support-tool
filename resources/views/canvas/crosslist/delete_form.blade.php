<div class="form-group {{ $errors->has('section_id_field') ? 'has-error' : ''}}">
    <label for="section_id_field" class="col-md-4 control-label">{{ 'Section ID Field' }}</label>
    <div class="col-md-6">
        <select name="section_id_field" class="form-control" id="section_id_field">
            @foreach (json_decode('{"sis_section_id":"SIS Section ID (ex. SECTION___202108___12345)","canvas":"Canvas ID (ex. 123456)"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionValue }}">{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('section_id_field', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('section_id') ? 'has-error' : ''}}">
    <label for="section_id" class="col-md-4 control-label">{{ 'Section ID' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="section_id" type="text" id="section_id"
               value="">
        {!! $errors->first('section_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText ?? 'De-Crosslist Now' }}">
    </div>
</div>
