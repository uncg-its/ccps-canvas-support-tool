@extends('layouts.wrapper', [
    'pageTitle' => 'Crosslist | De-Crosslist'
])

@section('content')
    {!! Breadcrumbs::render('crosslist.decrosslist') !!}
    <h1>Delete a Cross List</h1>
    <p>If using SIS Section ID, be sure to use the <strong>section's</strong> ID, and not the <strong>course's</strong> ID.</p>

    @permission('crosslist.delete')
    <form method="POST" action="{{ route('crosslist.destroy') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
        @include ('canvas.crosslist.delete_form')
    </form>
    @endpermission
@endsection
