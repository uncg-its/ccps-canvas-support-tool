<div class="form-group {{ $errors->has('course_id_field') ? 'has-error' : ''}}">
    <label for="course_id_field" class="col-md-4 control-label">{{ 'Course ID Field' }}</label>
    <div class="col-md-6">
        <select name="course_id_field" class="form-control" id="course_id_field">
            @foreach (json_decode('{"sis_course_id":"sis_course_id","canvas":"canvas"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionValue }}">{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('course_id_field', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}">
    <label for="course_id" class="col-md-4 control-label">{{ 'Course ID' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="course_id" type="text" id="course_id"
               value="">
        {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('section_id_field') ? 'has-error' : ''}}">
    <label for="section_id_field" class="col-md-4 control-label">{{ 'Section ID Field' }}</label>
    <div class="col-md-6">
        <select name="section_id_field" class="form-control" id="section_id_field">
            @foreach (json_decode('{"sis_section_id":"sis_section_id","canvas":"canvas"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionValue }}">{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('section_id_field', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('section_id') ? 'has-error' : ''}}">
    <label for="section_id" class="col-md-4 control-label">{{ 'Section ID' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="section_id" type="text" id="section_id"
               value="">
        {!! $errors->first('section_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText ?? 'Crosslist Now' }}">
    </div>
</div>
