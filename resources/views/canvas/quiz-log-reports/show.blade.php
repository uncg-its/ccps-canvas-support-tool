@extends('layouts.wrapper', [
    'pageTitle' => 'Quiz Log Reports | Show'
])

@section('content')
    <div class="row">
        <div class="col-6">
            <h1>Quiz Log Report {{ $report->id }}</h1>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            <a href="{{ route('canvas.quiz-log-reports.index') }}" class="btn btn-sm btn-warning">
                <i class="fas fa-arrow-left"></i> Back
            </a>
        </div>
    </div>
    <hr>
    @if ($report->items->isEmpty())
        <p>This report has no items.</p>
    @else
        @component('components.paginated-table', ['collection' => $items])
            @slot('table')
                @component('components.table')
                    @slot('th')
                        <th>ID</th>
                        <th>Course ID</th>
                        <th>User ID</th>
                        <th>Quiz ID</th>
                        <th>Submission ID</th>
                        <th>Event Data</th>
                        <th>Status</th>
                    @endslot
                    @slot('tbody')
                        @foreach ($items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->course_id }}</td>
                                <td>{{ $item->user_id }}</td>
                                <td>{{ $item->quiz_id }}</td>
                                <td>{{ $item->submission_id }}</td>
                                <td>
                                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#event_data_{{ $item->id }}">
                                        <i class="fas fa-eye"></i> Show
                                    </button>
                                    @component('components.modal', [
                                        'title' => 'Event Data',
                                        'id' => 'event_data_' . $item->id,
                                        'size' => 'lg'
                                    ])
                                        @slot('content')
                                            <pre>{{ json_encode($item->event_data, JSON_PRETTY_PRINT) }}</pre>
                                        @endslot
                                    @endcomponent
                                </td>
                                <td>@include('partials.statuses.' . $item->status)</td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    @endif
@endsection
