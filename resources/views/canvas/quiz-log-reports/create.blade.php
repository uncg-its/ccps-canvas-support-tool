@extends('layouts.wrapper', [
    'pageTitle' => 'Quiz Log Reports | Create'
])

@section('content')
    <h1>New Quiz Log Report</h1>
    <p>Create a new Quiz Log Report by selecting students and quizzes from a given course.</p>
    <hr>
    <div id="app">
        {!! Form::open()->method('post')->route('canvas.quiz-log-reports.store') !!}
            <div class="row">
                <div class="col-8">
                    {!! Form::text('course_id', 'Course ID')
                        ->attrs(['v-model' => 'course_id', ':disabled' => 'searchInProgress'])
                        ->required()
                        ->help('Enter the Canvas Course ID here (not SIS ID)') !!}
                </div>
                <div class="col pt-5">
                    <button class="btn btn-info" @click.prevent="loadCourseData" :disabled="searchInProgress"><i class="fas fa-search"></i> Search</button>
                </div>
            </div>

            <div class="alert alert-warning" v-show="searchInProgress">
                <i class="fas fa-spinner fa-spin"></i> Loading Students and Quizzes from Canvas...
            </div>
            <div class="alert alert-danger" v-show="searchError"><i class="fas fa-times-circle"></i> Course not found in Canvas.</div>
            <div class="alert alert-danger" v-show="technicalError"><i class="fas fa-times-circle"></i> Unexpected error while searching. Please contact an administrator.</div>

            <div v-show="courseFound">
                <h2>Select Students</h2>

                <div class="mb-3">
                    <button class="btn btn-sm btn-primary" @click.prevent="selectAllStudents"><i class="fas fa-check"></i> Select all</button>
                    <button class="btn btn-sm btn-outline-danger" @click.prevent="deselectAllStudents"><i class="fas fa-check"></i> Deselect all</button>
                </div>

                <div class="row">
                    <div class="col" v-if="!enrollments.length">
                        <p>This course has no students.</p>
                    </div>
                    <div class="col-3" v-for="enrollment in enrollments">
                        <label class="font-weight-normal">
                            <input type="checkbox" name="users[]" v-model="selectedEnrollments" :value="enrollment.user.id">
                            @{{ enrollment.user.sortable_name }}<br>
                            (@{{ enrollment.user.login_id }})
                        </label>
                    </div>
                </div>
                <h2>Select Quizzes</h2>

                <div class="mb-3">
                    <button class="btn btn-sm btn-primary" @click.prevent="selectAllQuizzes"><i class="fas fa-check"></i> Select all</button>
                    <button class="btn btn-sm btn-outline-danger" @click.prevent="deselectAllQuizzes"><i class="fas fa-check"></i> Deselect all</button>
                </div>

                <div class="row">
                    <div class="col" v-if="!quizzes.length">
                        <p>This course has no quizzes.</p>
                    </div>
                    <div class="col-6" v-for="quiz in quizzes" v-if="quiz.published">
                        <label class="font-weight-normal">
                            <input type="checkbox" name="quizzes[]" v-model="selectedQuizzes" :value="quiz.id">
                            @{{ quiz.title }}
                        </label>
                    </div>
                </div>
                <h2>Select Options</h2>
                <div class="row">
                    <div class="col">
                        {!! Form::select('attempts', 'Attempts', ['all' => 'All Attempts', 'latest' => 'Latest Attempt Only'])->required() !!}
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col mb-3">
                {!! Form::submit('Submit')
                    ->attrs([':disabled' => '!courseFound'])
                    ->color('success') !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
@routes {{-- Ziggy --}}
<script src="{{ asset('js/app.js') }}"></script>
<script>
const app = new Vue({
    el: '#app',
    data() {
        return {
            searchInProgress: false,
            courseFound: false,
            searchError: false,
            technicalError: false,
            course_id: null,
            enrollments: [],
            quizzes: [],
            selectedEnrollments: [],
            selectedQuizzes: [],
        }
    },
    mounted() {

    },
    methods: {
        loadCourseData() {
            this.searchInProgress = true;
            this.courseFound = false;
            this.searchError = false;
            this.technicalError = false;
            axios
                .post(route('canvas.quiz-log-reports.load-course-data', {
                    course_id: this.course_id,
                }).url())
                .then(response => {
                    console.log(response);
                    this.courseFound = true;
                    this.enrollments = response.data.data.enrollments;
                    this.quizzes = response.data.data.quizzes;
                })
                .catch(error => {
                    console.log(error.response.status);
                    if (error.response.status === 404 || error.response.status === 400) {
                        this.searchError = true;
                    } else {
                        this.technicalError = true;
                    }
                })
                .finally(response => {
                    this.searchInProgress = false;
                });
        },
        selectAllStudents() {
            for (enrollment in this.enrollments) {
                this.selectedEnrollments.push(this.enrollments[enrollment].user.id);
            }
            console.log(this.selectedEnrollments);
        },
        deselectAllStudents() {
            this.selectedEnrollments = [];
        },
        selectAllQuizzes() {
            for (quiz in this.quizzes) {
                this.selectedQuizzes.push(this.quizzes[quiz].id);
            }
            console.log(this.selectedQuizzes);
        },
        deselectAllQuizzes() {
            this.selectedQuizzes = [];
        }
    }
});
</script>
@append
