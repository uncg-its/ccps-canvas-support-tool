@extends('layouts.wrapper', [
    'pageTitle' => 'Quiz Log Reports | Index'
])

@section('content')
    <div class="row">
        <div class="col-6">
            <h1>Quiz Log Reports</h1>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            <a href="{{ route('canvas.quiz-log-reports.create') }}" class="btn btn-success"><i class="fas fa-plus"></i> Create New</a>
        </div>
    </div>
    <p>Use this tool to export Quiz Logs in bulk.</p>
    <hr>
    @if($reports->isEmpty())
        <p>No Quiz Log reports to show.</p>
    @else
        @component('components.paginated-table', ['collection' => $reports])
            @slot('table')
                @component('components.table')
                    @slot('th')
                        <th>ID</th>
                        <th>Created By</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    @endslot
                    @slot('tbody')
                        @foreach ($reports as $report)
                            <tr>
                                <td>{{ $report->id }}</td>
                                <td>{{ $report->created_by_user->email }}</td>
                                <td>@include('partials.statuses.' . $report->status)</td>
                                <td>{{ $report->created_at }}</td>
                                <td>
                                    <a href="{{ route('canvas.quiz-log-reports.show', $report) }}" class="btn btn-sm btn-info">
                                        <i class="fas fa-list"></i> Details
                                    </a>
                                    @if ($report->status === 'completed')
                                    <a href="{{ route('canvas.quiz-log-reports.download', $report) }}" class="btn btn-sm btn-success">
                                        <i class="fas fa-download"></i> Download
                                    </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    @endif
@endsection
