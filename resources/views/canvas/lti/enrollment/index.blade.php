@extends('layouts.wrapper', [
    'pageTitle' => 'LTI | Enrollment Index'
])

@section('content')
    <h1>LTI Enrollment Totals</h1>

    <p>
    <a href="{{ route('lti.index') }}" class="btn btn-info btn-sm" title="LTI Index">
        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to LTI Index
    </a>
    <a href="{{ route('lti.inventory.index') }}" class="btn btn-info btn-sm" title="LTI Inventory Index">
        <i class="fa fa-th-list" aria-hidden="true"></i> LTI Inventory Index
    </a>
    </p>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="GET" action="{{ route('lti.enrollment.index') }}" accept-charset="UTF-8" class="navbar-form navbar-right" role="search">
                            <div class="input-group {{ $errors->has('toolId') ? 'has-error' : ''}}">
                                <div class="col-md-6">
                                    <p>
                                    <select name="toolId" class="form-control" id="toolId" >
                                        @foreach ($tools as $t)
                                            <option value="{{ $t->toolId }}" {{ (isset($request->toolId) && $request->toolId == $t->toolId) ? 'selected' : ''}}>{{ $t->ltiName }}</option>
                                        @endforeach
                                    </select>
                                    </p>
                                    <p>
                                        <select multiple name="termCodeString[]" id="termCodeString">
                                            <?php foreach($termCodeArray as $termCode => $termCodeName): ?>
                                                <option name="<?php echo $termCode; ?>" <?php echo (in_array($termCode, $thisTermCodeArray)) ? 'selected' : ''; ?> value="<?php echo $termCode; ?>"><?php echo $termCodeName . ' (' . $termCode . ')'; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-offset-4 col-md-4">
                                <input class="btn btn-primary" type="submit" value="{{ $submitButtonText ?? 'Filter/Search Now' }}">
                                <a href="{{ route('lti.enrollment.index') }}" class="btn btn-info" title="LTI Enrollment Index">Reset</a>
                            </div>

                        </form>

                        <br/>
                        <br/>

                        <h6>Total Results Found: {{ $total }}</h6>

                        <h6>Results for Term Codes: {{ implode(', ', $thisTermCodeArray) }}</h6>
                        <h6> Unique User Totals:
                            <span class="badge badge-success">{{ $summaryInfo['active'] . ' Active Accts' }}</span>
                            <span class="badge badge-danger">{{ $summaryInfo['inactive'] . ' Inactive Accts' }}</span>
                            <span class="badge badge-secondary">{{ $summaryInfo['studentview'] . ' StudentView Accts' }}</span>
                        </h6>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User ID</th>
                                        <th>SIS ID</th>
                                        <th>Status</th>
                                        <th>Last Updated</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{ $loop->iteration ?? $item->toolId }}</td>
                                        <td>{{ $item->user_id }}</td>
                                        <td>{{ $item->sis_course_id }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>{{ \Carbon\Carbon::createFromTimestamp($item->enrollmentLastUpdated)->toDateTimeString() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
