@extends('layouts.wrapper', [
    'pageTitle' => 'LTI | Tool Delete'
])

@section('content')
    <h3>Delete LTI Tool #{{ $item->toolId }}</h3>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="{{ route('lti.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <h4>LTI Name: {{ $item->ltiName }}</h4>
                        <form method="POST" action="{{ route('lti.tool.destroy', ['toolId' => $item->toolId]) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('POST') }}
                            {{ csrf_field() }}

                            @include ('canvas.lti.delete_form', ['submitButtonText' => 'Delete'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
