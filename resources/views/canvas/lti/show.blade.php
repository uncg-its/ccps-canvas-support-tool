@extends('layouts.wrapper', [
    'pageTitle' => 'Manual Enrollment | Show'
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Manual Enrollment  {{ $manualenrollment->id }}</div>
                    <div class="panel-body">

                        <a href="{{ route('manualenrollments.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                        @permission('manualenrollments.update')
                        <a href="{{ route('manualenrollments.edit', $manualenrollment->id) }}" title="Edit Manualenrollment"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        @endpermission

                        @permission('manualenrollments.delete')
                        <form method="POST" action="{{ route('manualenrollments.destroy', $manualenrollment->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Manualenrollment" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        @endpermission

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $manualenrollment->id }}</td>
                                    </tr>
                                    <tr><th> Course Or Section </th><td>{{ $manualenrollment->course_or_section }}</td></tr><tr><th> Type </th><td>{{ $manualenrollment->type }}</td></tr><tr><th> User Id </th><td>{{ $manualenrollment->user_id }}</td></tr><tr><th> Course Or Section Id </th><td>{{ $manualenrollment->course_or_section_id }}</td></tr><tr><th> Role Id </th><td>{{ $manualenrollment->role_id }}</td></tr><tr><th> Status </th><td>{{ $manualenrollment->status }}</td></tr><tr><th> Notes </th><td>{{ $manualenrollment->notes }}</td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
