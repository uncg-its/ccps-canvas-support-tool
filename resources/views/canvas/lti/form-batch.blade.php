<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-md-4 control-label">{{ 'Set New Status' }}</label>
    <div class="col-md-6">
        <select name="status" class="form-control" id="status">
            'pending-approval','approved-pending-upload','successfully-uploaded','error','canceled'
            @foreach (json_decode('{"pending-approval":"Pending Approval","approved-pending-upload":"Approved Pending Upload","canceled":"Canceled"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}">{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText ?? 'Update Now' }}">
    </div>
</div>
