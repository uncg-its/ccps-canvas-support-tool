@extends('layouts.wrapper', [
    'pageTitle' => 'Manual Enrollment | Batch Status'
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h5>Change Status of Entire Batch (regardless of current status)</h5></div>
                    <div class="panel-body">
                        <a href="{{ route('manualenrollments.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />
                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <form method="POST" action="{{ route('manualenrollments.batch') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @include ('manualenrollments.form-batch')
                            <input type="hidden" name="created_at" id="created_at" value="{{ $manualenrollments[0]->created_at }}">
                        </form>
                        <br />
                        <h6>..or just requeue just the Errors</h6>
                        <form method="POST" action="{{ route('manualenrollments.requeueErrors') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="created_at" id="created_at" value="{{ $manualenrollments[0]->created_at }}">
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-4">
                                    <input class="btn btn-primary" type="submit" value="Requeue Errors Now">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h3>Enrollments to be Updated:</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>Batch</th>
                                    <th>Action</th>
                                    <th>Course Or Section</th>
                                    <th>Type</th>
                                    <th>User Id</th>
                                    <th>Current Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($manualenrollments as $item)
                                    <tr>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->action }}</td>
                                        <td>{{ $item->course_or_section }}: {{ $item->course_or_section_id }}</td>
                                        <td>{{ $item->role_label }}</td>
                                        <td>{{ $item->user_id }}</td>
                                        <td>{{ $item->status }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection