<div class="form-group {{ $errors->has('ltiName') ? 'has-error' : ''}}">
    <label for="ltiName" class="col-md-4 control-label">{{ 'LTI Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="ltiName" type="text" id="ltiName"
               value="{{ $tool->ltiName ?? ''}}">
        {!! $errors->first('ltiName', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('ltiDescription') ? 'has-error' : ''}}">
    <label for="ltiDescription" class="col-md-4 control-label">{{ 'LTI Description' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="ltiDescription" type="text" id="ltiDescription"
               value="{{ $tool->ltiDescription ?? ''}}">
        {!! $errors->first('ltiDescription', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText ?? 'Create' }}">
    </div>
</div>
