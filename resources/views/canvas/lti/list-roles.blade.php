@extends('layouts.wrapper', [
    'pageTitle' => 'Manual Enrollments | List Roles'
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <th>ID</th>
                                <th>Label</th>
                                <th>Base Type</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach($result['results'] as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->label }}</td>
                                    <td>{{ $item->base_role_type }}</td>
                                    <td><a href="{{route('manualenrollments.create')}}?type={{$item->base_role_type}}&role_id={{$item->id}}" title="Select this role to use for the manual upload"><btn class="btn btn-info">Use This Role</btn></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
