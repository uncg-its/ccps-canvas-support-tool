@extends('layouts.wrapper', [
    'pageTitle' => 'LTI | Tool Index'
])

@section('content')
    <h1>LTI Tools</h1>
    <p>
        <a href="{{ route('home') }}" class="btn btn-info btn-sm" title="Back to Canvas Index">
            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to Canvas Index
        </a>
        <a href="{{ route('lti.inventory.index') }}" class="btn btn-info btn-sm" title="LTI Course Inventory">
            <i class="fa fa-th-list" aria-hidden="true"></i> LTI Course Inventory
        </a>
        <a href="{{ route('lti.enrollment.index') }}" class="btn btn-info btn-sm" title="LTI Enrollment">
            <i class="fa fa-th-list" aria-hidden="true"></i> LTI Enrollment Totals
        </a>
        <a href="{{ route('lti.tool.create') }}" class="btn btn-success btn-sm" title="Add New LTI Tool">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New Tool
        </a>
    </p>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="GET" action="{{ route('lti.index') }}" accept-charset="UTF-8" class="navbar-form navbar-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>LTI Name</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($tools as $item)
                                    <tr>
                                        <td>{{ $loop->iteration ?? $item->toolId }}</td>
                                        <td>{{ $item->ltiName }}</td>
                                        <td>{{ $item->ltiDescription }}</td>
                                        <td>

                                            <a href="{{ route('lti.tool.edit',  ['toolId' => $item->toolId]) }}" title="Edit LTI Tool"><button class="btn btn-primary btn-xs"><i class="fa fa-edit" aria-hidden="true"></i> Update</button></a>
                                            <a href="{{ route('lti.tool.delete',  ['toolId' => $item->toolId]) }}" title="Delete Entry"><button class="btn btn-danger btn-xs"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button></a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $tools->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
