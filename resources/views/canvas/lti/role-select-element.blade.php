<div class="form-group {{ $errors->has('roleId') ? 'has-error' : ''}}">
<label for="roleId" class="col-md-4 control-label">{{ 'Role ID' }}</label>
    <div class="col-md-6">
        <select name="roleId" class="form-control" id="roleId">
            @isset($result)
            @foreach( $result['results'] as $r)
                <option value="{{ $r->id }}" >{{ $r->label }}</option>
            @endforeach
            @endisset
        </select>
        {!! $errors->first('roleId', '<p class="help-block">:message</p>') !!}
    </div>
</div>