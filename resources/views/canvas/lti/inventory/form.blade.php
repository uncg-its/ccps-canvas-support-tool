<div class="form-group {{ $errors->has('toolId') ? 'has-error' : ''}}">
    <label for="toolId" class="col-md-4 control-label">{{ 'Tool ID' }}</label>
    <div class="col-md-6">
        <select name="toolId" class="form-control" id="toolId" >
            @foreach ($tools as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($item->toolId) && $item->toolId == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group {{ $errors->has('sis_course_id') ? 'has-error' : ''}}">
    <label for="sis_course_id" class="col-md-4 control-label">{{ 'SIS Course ID' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="sis_course_id" type="text" id="sis_course_id"
               value="{{ $item->sis_course_id ?? ''}}">
        {!! $errors->first('sis_course_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText ?? 'Add' }}">
    </div>
</div>
