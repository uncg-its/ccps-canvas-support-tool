@extends('layouts.wrapper', [
    'pageTitle' => 'LTI | Inventory Delete'
])

@section('content')
    <h3>Delete LTI Inventory #{{ $item->inventoryId }}</h3>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="{{ route('lti.inventory.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <h4>SIS Course ID: {{ $item->sis_course_id }}</h4>
                        <form method="POST" action="{{ route('lti.inventory.destroy', ['inventoryId' => $item->inventoryId]) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('POST') }}
                            {{ csrf_field() }}

                            @include ('canvas.lti.inventory.delete_form', ['submitButtonText' => 'Delete'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
