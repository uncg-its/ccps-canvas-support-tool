@extends('layouts.wrapper', [
    'pageTitle' => 'LTI | Inventory Index'
])

@section('content')
    <h1>LTI Course Inventory</h1>

    <a href="{{ route('lti.index') }}" class="btn btn-info btn-sm" title="LTI Index">
        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to LTI Index
    </a>
    <a href="{{ route('lti.inventory.create') }}" class="btn btn-success btn-sm" title="Add New Course">
        <i class="fa fa-plus" aria-hidden="true"></i> Add New Course
    </a>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="GET" action="{{ route('lti.inventory.index') }}" accept-charset="UTF-8" class="navbar-form navbar-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tool</th>
                                        <th>SIS Course ID</th>
                                        <th>Last Updated</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{ $loop->iteration ?? $item->toolId }}</td>
                                        <td>{{ $tools[$item->toolId] }}</td>
                                        <td>{{ $item->sis_course_id }}</td>
                                        <td>{{ \Carbon\Carbon::createFromTimestamp($item->lastEnrollmentUpdate)->toDateTimeString() }}</td>
                                        <td>

                                            <a href="{{ route('lti.inventory.edit',  ['inventoryId' => $item->inventoryId]) }}" title="Edit Entry"><button class="btn btn-primary btn-xs"><i class="fa fa-edit" aria-hidden="true"></i> Update</button></a>
                                            <a href="{{ route('lti.inventory.delete',  ['inventoryId' => $item->inventoryId]) }}" title="Delete Entry"><button class="btn btn-danger btn-xs"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button></a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $items->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
