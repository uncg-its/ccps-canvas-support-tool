@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas Users | Live Lookup'
])

@section('content')
    <h1>Active Canvas Courses for User (Live Display)</h1>

    <p><a href="{{ route('canvas_users.show', ['user_id' => $this_user->id]) }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to User Index</button></a></p>
    <h3>User Info</h3>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('canvas.users.user')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3>Course List</h3>
    <div class="table-responsive">
        <table class="table table-borderless">
            <thead>
            <th>Name</th>
            <th>Course Code</th>
            <th>Term</th>
            <th>State</th>
            <th>&nbsp;</th>
            </thead>
            <tbody>
            @foreach($courseList as $c)
                @isset($c->name)
                <tr>
                    <td>{{  $c->name }}</td>
                    <td>{{ $c->course_code }}</td>
                    <td>{{ $c->term->name }}</td>
                    <td>{{ $c->workflow_state }}</td>
                    <td>
                        <a href="{{ route('canvas_users.course', ['course_id' => $c->id, 'user_id' => $this_user->id]) }}" title="View all Course Data"><button class="btn btn-info btn-xs">View Full Course Data <i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>
                    </td>
                </tr>
                @else
                <tr>
                    <td colspan="4">Course ID "{{  $c->id }}" Restricted by Access Dates</td>
                    <td>
                        <a href="{{ route('canvas_users.course', ['course_id' => $c->id, 'user_id' => $this_user->id]) }}" title="View all Course Data"><button class="btn btn-info btn-xs">View Full Course Data <i class="fa fa-arrow-right" aria-hidden="true"></i></button></a>
                    </td>
                </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

@endsection