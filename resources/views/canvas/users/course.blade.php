@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas User - Course | Live Lookup'
])

@section('content')
    <h1>Canvas Course (Live Display)</h1>

    <p><a href="{{ route('canvas_users.courses', ['user_id' => $this_user->id]) }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to User Course List</button></a></p>

    <h3>Canvas Course (Live Lookup Result)</h3>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th>Canvas URL</th>
                                    <td>
                                        <a title="View Course with ID {{ $course->id }} in Canvas" href="{{ 'https://' . config('canvas-api.configs.' . config('canvas-api.defaults.config') . '.host') . '/courses/' . $course->id . '/settings' }}" target="_blank">
                                            {{ 'https://' . config('canvas-api.configs.' . config('canvas-api.defaults.config') . '.host') . '/courses/' . $course->id . '/settings' }}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $course->id }}</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $course->name }}</td>
                                </tr>
                                <tr>
                                    <th>Workflow State</th>
                                    <td>{{ $course->workflow_state }}</td>
                                </tr>
                                <tr>
                                    <th>Account</th>
                                    <td>{{ $course->account->name }}</td>
                                </tr>
                                <tr>
                                    <th>Total Students</th>
                                    <td>{{ $course->total_students }}</td>
                                </tr>
                                <tr>
                                    <th>Teachers</th>
                                    <td>
                                        @foreach($course->teachers as $teacher)
                                            {{ $teacher->display_name }}<br />
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Created At</th>
                                    <td>{{ $course->created_at }}</td>
                                </tr>
                                <tr>
                                    <th>Start At</th>
                                    <td>{{ $course->start_at }}</td>
                                </tr>
                                <tr>
                                    <th>End At</th>
                                    <td>{{ $course->end_at }}</td>
                                </tr>
                                <tr>
                                    <th>Term</th>
                                    <td>{{ $course->term->name }}</td>
                                </tr>
                                <tr>
                                    <th>Is Public</th>
                                    <td>{{ $course->is_public }}</td>
                                </tr><tr>
                                    <th>Grading Standard ID</th>
                                    <td>{{ $course->grading_standard_id }}</td>
                                </tr>
                                <tr>
                                    <th>Storage Quota MB</th>
                                    <td>{{ $course->storage_quota_used_mb }} used of {{ $course->storage_quota_mb }}</td>
                                </tr>
                                <p>
                                <th>SIS Course ID</th>
                                <td>{{ $course->sis_course_id }}</td>

                                </tr>
                                </tbody>
                            </table>
                            <h5>All Course Data</h5>
                            <div class="section">
                                @php
                                    echo '<pre>'; print_r($course); echo '</pre>';
                                @endphp
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
