<div class="form-group {{ $errors->has('user_id_type') ? 'has-error' : ''}}">
    <label for="user_id_type" class="col-md-4 control-label">{{ 'Initial User ID Type' }}</label>
    <div class="col-md-6">
        <select name="user_id_type" class="form-control" id="user_id_type">
            @foreach (json_decode('{"0":"sis_login_id","2":"canvas_user_id","3":"sis_user_id"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionValue }}"{{ (isset($request->user_id_type) && $request->user_id_type == $optionValue) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        {!! $errors->first('user_id_type', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    <label for="user_id" class="col-md-4 control-label">{{ 'Canvas User ID' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="user_id" type="text" id="user_id" required value="{{ $request->user_id ?? ''}}">
        {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ 'Search Now' }}">
    </div>
</div>
{{ csrf_field() }}