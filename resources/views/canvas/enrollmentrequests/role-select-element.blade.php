@isset($result)
@foreach( $result as $r)
    @if(!in_array($r->id, $excluded))
        <option value="{{ $r->id }}" {{ $selected == $r->id ? 'selected' : ''}}>{{ $r->label }}</option>
    @endif
@endforeach
@endisset
