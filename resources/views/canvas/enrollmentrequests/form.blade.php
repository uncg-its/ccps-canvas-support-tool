<div class="row">
    <div class="col text-right">
        <p>
            <em><span class="text-danger">*</span> - required field</em>
        </p>
    </div>
</div>

<fieldset>
    <legend>Requester Info</legend>
    <div class="form-group row {{ $errors->has('requesterEmailAddress') ? 'has-error' : ''}}">
        <label for="requesterEmailAddress" class="col-3">Requester Email <span class="text-danger">*</span></label>
        <div class="col">
                <input readonly class="form-control" name="requesterEmailAddress" type="text" id="requesterEmailAddress" value="{{ old('requesterEmailAddress', $item->requesterEmailAddress ?? $requesterEmail) }}" required>
            {!! $errors->first('requesterEmailAddress', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group row {{ $errors->has('requesterAffiliation') ? 'has-error' : ''}}">
        <label for="requesterAffiliation" class="col-3">Requester Affiliation <span class="text-danger">*</span></label>
        <div class="col">
            <select name="requesterAffiliation" class="form-control" id="requesterAffiliation" required>
                @foreach ($affiliationArray as $optionKey => $optionValue)
                    <option value="{{ $optionKey }}" {{ old('requesterAffiliation', $item->requesterAffiliation ?? '') == $optionKey ? 'selected' : ''}}>{{ $optionValue }}</option>
                @endforeach
            </select>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>Course Info</legend>
    <div class="form-group row {{ $errors->has('sis_course_id') ? 'has-error' : ''}}">
        <label for="sis_course_id" class="col-3">SIS Course ID <span class="text-danger">*</span></label>
        <div class="col">
            <input class="form-control" name="sis_course_id" type="text" id="sis_course_id" value="{{ old('sis_course_id', $item->sis_course_id ?? '')}}" required>
            <small id="loading-roles" class="d-none"><i class="fas fa-spin fa-spinner"></i> <span class="text-muted">Loading roles...</span></small>
            <small id="invalid-sis-course-id" class="d-none"><span class="text-danger">SIS Course ID not found in Canvas</span></small>
            <small id="valid-sis-course-id" class="d-none"><span class="text-success"><i class="fa fa-check"></i> SIS Course ID found in Canvas</span></small>
            {!! $errors->first('sis_course_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group row {{ $errors->has('activeCourse') ? 'has-error' : ''}}">
        <label for="activeCourse" class="col-3">Active Course? <span class="text-danger">*</span></label>
        <div class="col">
            <select name="activeCourse" class="form-control" id="activeCourse" required>
                @foreach (json_decode('{"0":"NO","1":"YES"}', true) as $optionKey => $optionValue)
                    <option value="{{ $optionValue }}" {{ old('activeCourse', $item->activeCourse ?? '') == $optionValue ? 'selected' : ''}}>{{ $optionValue }}</option>
                @endforeach
            </select>
            {!! $errors->first('activeCourse', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</fieldset>

<fieldset>
    <legend>Enrolled Person Info</legend>
    <div id="role-select-container">
        <div class="form-group row {{ $errors->has('roleId') ? 'has-error' : ''}}">
            <label for="roleId" class="col-3">Enrolled Person Role <span class="text-danger">*</span></label>
            <div class="col">
                <select name="roleId" class="form-control" id="roleId" required>
                    <option value="NULL" >ENTER VALID COURSE ID FIRST</option>
                </select>
                {!! $errors->first('roleId', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('enrolledPersonName') ? 'has-error' : ''}}">
        <label for="enrolledPersonName" class="col-3">Enrolled Person Name <span class="text-danger">*</span></label>
        <div class="col">
            <input class="form-control" name="enrolledPersonName" type="text" id="enrolledPersonName" value="{{ old('enrolledPersonName', $item->enrolledPersonName ?? '')}}" required>
            {!! $errors->first('enrolledPersonName', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group row {{ $errors->has('enrolledPersonUsername') ? 'has-error' : ''}}">
        <label for="enrolledPersonUsername" class="col-3">Enrolled Person Username <span class="text-danger">*</span></label>
        <div class="col">
            <input class="form-control" name="enrolledPersonUsername" type="text" id="enrolledPersonUsername" value="{{ old('enrolledPersonUsername', $item->enrolledPersonUsername ?? '')}}" required>
            {!! $errors->first('enrolledPersonUsername', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group row {{ $errors->has('enrolledPersonAffiliation') ? 'has-error' : ''}}">
        <label for="enrolledPersonAffiliation" class="col-3">Enrolled Person Affiliation <span class="text-danger">*</span></label>
        <div class="col">
            <select name="enrolledPersonAffiliation" class="form-control" id="enrolledPersonAffiliation" required>
                @foreach ($affiliationArray as $optionKey => $optionValue)
                    <option value="{{ $optionKey }}" {{ old('enrolledPersonAffiliation', $item->enrolledPersonAffiliation ?? '') == $optionKey ? 'selected' : ''}}>{{ $optionValue }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row {{ $errors->has('enrolledPersonFerpaAttestation') ? 'has-error' : ''}}">
        <label for="enrolledPersonFerpaAttestation" class="col-3">FERPA Training Completed? <span class="text-danger">*</span></label>
        <div class="col">
            <select name="enrolledPersonFerpaAttestation" class="form-control" id="enrolledPersonFerpaAttestation" required>
                @foreach (json_decode('{"NO":"NO","YES":"YES","UNKNOWN":"UNKNOWN"}', true) as $optionKey => $optionValue)
                    <option value="{{ $optionValue }}" {{ old('enrolledPersonFerpaAttestation', $item->enrolledPersonFerpaAttestation ?? '') == $optionValue ? 'selected' : ''}}>{{ $optionValue }}</option>
                @endforeach
            </select>
            {!! $errors->first('enrolledPersonFerpaAttestation', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</fieldset>
<div class="form-group row {{ $errors->has('enrollmentReasonCategory') ? 'has-error' : ''}}">
    <label for="enrolledPersonAffiliation" class="col-3">Enrollment Reason Category <span class="text-danger">*</span></label>
    <div class="col">
        <select name="enrollmentReasonCategory" class="form-control" id="enrollmentReasonCategory" required>
            @foreach ($reasonCategoryArray as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ old('enrollmentReasonCategory', $item->enrolledPersonAffiliation ?? '') == $optionKey ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row {{ $errors->has('enrollmentReason') ? 'has-error' : ''}}">
    <label for="enrollmentReason" class="col-3">Enrollment Reason Details <span class="text-danger">*</span></label>
    <div class="col">
        <textarea rows="10" class="form-control" name="enrollmentReason" id="enrollmentReason" required>{{ old('enrollmentReason', $item->enrollmentReason ?? '')}}</textarea>
        {!! $errors->first('enrollmentReason', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('begins_at') ? 'has-error' : ''}}">
    <label for="begins_at" class="col-3">Enrollment Begin Date <span class="text-danger">*</span></label>
    <div class="col">
        <input class="form-control" name="begins_at" type="text" id="begins_at" value="{{ old('begins_at', $item->begins_at ?? now()->format('m/d/Y'))}}" required>
        {!! $errors->first('begins_at', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('ends_at') ? 'has-error' : ''}}">
    <label for="ends_at" class="col-3">Enrollment End Date</label>
    <div class="col">
        <input class="form-control" name="ends_at" type="text" id="ends_at" value="{{ old('ends_at', $item->ends_at ?? '')}}">
        {!! $errors->first('ends_at', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('enrollmentNotes') ? 'has-error' : ''}}">
    <label for="enrollmentNotes" class="col-3">Additional Comments/Notes</label>
    <div class="col">
        <textarea rows="10" class="form-control" name="enrollmentNotes" id="enrollmentNotes">{{ old('enrollmentNotes', $item->enrollmentNotes ?? '')}}</textarea>
        {!! $errors->first('enrollmentNotes', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row">
    <div class="col text-center">
        <button class="btn btn-success" type="submit">{!! $submitButtonText ?? 'Create' !!}</button>
    </div>
</div>
