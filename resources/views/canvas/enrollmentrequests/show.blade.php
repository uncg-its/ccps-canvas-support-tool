@extends('layouts.wrapper', [
    'pageTitle' => 'Enrollment Requests | Show'
])

@section('content')
    <h1>Enrollment Request #{{ $item['requestId'] }}</h1>
    <div class="row">
        <div class="col d-flex mb-3">
            <a href="{{ route('enrollmentrequests.index') }}" title="Back"><button class="btn btn-warning btn-sm mr-3"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            @permission('enrollmentrequests.admin')
            @if($item->status == 'pending approval')
            <a href="{{ route('enrollmentrequests.create-in-canvas', ['requestId' => $item->requestId]) }}" class="btn btn-info btn-sm mr-3" title="Enrollment Request Approval & Creation in Canvas">
                <i class="fa fa-check" aria-hidden="true"></i> Create in Canvas Now (1-Click)
            </a>
            <a href="{{ route('enrollmentrequests.reject', ['requestId' => $item->requestId]) }}" class="btn btn-danger btn-sm mr-3" title="Enrollment Request Rejection">
                <i class="fa fa-times" aria-hidden="true"></i> Reject Now (1-Click)
            </a>
            <form method="POST" action="{{ route('enrollmentrequests.destroy', ['requestId' => $item->requestId]) }}" accept-charset="UTF-8" style="display:inline">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger btn-sm" title="Delete Request" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
            </form>
            @endif
            @endpermission
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header">Requester Information</div>
                <div class="card-body table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <th scope="row">Email</th>
                            <td>{{ $item->requesterEmailAddress }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Affiliation</th>
                            <td>{{ $affiliationMap[$item->requesterAffiliation] }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Requested at</th>
                            <td>{{ $item->created_at }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">Course Information</div>
                <div class="card-body table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <th scope="row">SIS Course Id</th>
                            <td>{{ $item->sis_course_id }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Active Course?</th>
                            <td>{{ $item->activeCourse }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Metadata</th>
                            <td>
                                @if(!is_null($item->courseMetadata))
                                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#course_metadata_modal">
                                        <i class="fas fa-eye"></i> Show
                                    </button>
                                    @component('components.modal', [
                                        'title' => 'Course Metadata',
                                        'id' =>  'course_metadata_modal',
                                        // 'size' => 'xl'
                                    ])
                                        @slot('content')
                                            <pre>{{ json_encode(json_decode($item->courseMetadata), JSON_PRETTY_PRINT) }}</pre>
                                        @endslot
                                    @endcomponent
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-6">
            <div class="card">
                <div class="card-header">Enrolled Person Information</div>
                <div class="card-body table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <th scope="row">Name</th>
                            <td>{{ $item->enrolledPersonName }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Username</th>
                            <td>{{ $item->enrolledPersonUsername }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Affiliation</th>
                            <td>{{ $affiliationMap[$item->enrolledPersonAffiliation] }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Role</th>
                            <td>{{ $item->enrolledPersonRole }} (ID {{ $item->enrolledPersonRoleId }})</td>
                        </tr>
                        <tr>
                            <th scope="row">Completed FERPA?</th>
                            <td>{{ $item->enrolledPersonFerpaAttestation }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Metadata</th>
                            <td>
                                @if(!is_null($item->enrolledPersonMetadata))
                                    <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#enrolled_person_metadata_modal">
                                        <i class="fas fa-eye"></i> Show
                                    </button>
                                    @component('components.modal', [
                                        'title' => 'Enrolled Person Metadata',
                                        'id' => 'enrolled_person_metadata_modal',
                                        // 'size' => 'xl'
                                    ])
                                        @slot('content')
                                            <pre>{{ json_encode(json_decode($item->enrolledPersonMetadata), JSON_PRETTY_PRINT) }}</pre>
                                        @endslot
                                    @endcomponent
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">Enrollment Information</div>
                <div class="card-body table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <th scope="row">Begins</th>
                            <td>{{ $item->begins_at }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Ends</th>
                            <td>{{ $item->ends_at }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Reason Category</th>
                            <td>{{ $item->enrollmentReasonCategory }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Reason Detail</th>
                            <td>{{ $item->enrollmentReason }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Notes</th>
                            <td>{{ $item->enrollmentNotes }}</td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-4">
        <div class="col-6">
            <div class="card">
                <div class="card-header">Approval status</div>
                <div class="card-body table-responsive">
                    <table class="table table-borderless">
                        <tr>
                            <th scope="row">Status</th>
                            <td class="text-{{ $item->statusClass }}">
                                <i class="fa fa-{{ $item->statusIcon }}"></i>
                                {{ $item->status }}
                            </td>
                        </tr>
                        @if($item->status == 'approved' || $item->status == 'completed')
                            <tr>
                                <th scope="row">Approved at</th>
                                <td>{{ $item->approved_at }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Approved by</th>
                                <td>{{ optional($item->approver)->email }}</td>
                            </tr>
                        @endif
                        @if($item->status == 'rejected')
                            <tr>
                                <th scope="row">Rejected at</th>
                                <td>{{ $item->rejected_at }}</td>
                            </tr>
                            <tr>
                                <th scope="row">Rejected by</th>
                                <td>{{ optional($item->rejector)->email }}</td>
                            </tr>
                        @endif
                        @if($item->status == 'completed')
                            <tr>
                                <th scope="row">Completed at</th>
                                <td>{{ $item->completed_at }}</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
