@extends('layouts.wrapper', [
    'pageTitle' => 'Enrollment Requests | Index',
])

@section('content')
    {!! Breadcrumbs::render('enrollmentrequests.index') !!}
    @include('partials.enrollmentrequests.search-alert')
    <h1>My Enrollment Requests</h1>
    <p>
        @permission('enrollmentrequests.admin')
        <a href="{{ route('enrollmentrequests.all') }}" class="btn btn-info btn-sm" title="View All Enrollment Request">
            <i class="fa fa-th-list" aria-hidden="true"></i> View All Requests
        </a>
        @endpermission
        @permission('enrollmentrequests.create')
        <a href="{{ route('enrollmentrequests.create') }}" class="btn btn-success btn-sm" title="Add New Enrollment Request">
            <i class="fa fa-plus" aria-hidden="true"></i> Submit New Request
        </a>
        @endpermission
    </p>
    @include('partials.enrollmentrequests.table', ['target' => 'index'])
@endsection
