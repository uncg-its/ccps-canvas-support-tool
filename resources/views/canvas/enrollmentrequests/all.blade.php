@extends('layouts.wrapper', [
    'pageTitle' => 'Enrollment Requests | All'
])

@section('content')
    {!! Breadcrumbs::render('enrollmentrequests.all') !!}
    @include('partials.enrollmentrequests.search-alert')
    <div class="d-flex justify-content-between">
        <h1>All Enrollment Requests</h1>
        @permission('enrollmentrequests.read')
        <a href="{{ route('enrollmentrequests.index') }}" class="btn btn-info btn-sm d-block align-self-center" title="Enrollment Request Index">
            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back to Index
        </a>
    </div>
    @endpermission
    @include('partials.enrollmentrequests.table', ['target' => 'all'])
@endsection
