@extends('layouts.wrapper', [
    'pageTitle' => 'Enrollment Request | Create'
])

@section('head')

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@endsection

@section('content')
    {!! Breadcrumbs::render('enrollmentrequests.create') !!}
    <h1>Create New Enrollment Request</h1>
    <div class="row">
        <div class="col">
            <p>
                Use the form below to submit a request for a user to be added to a course. Upon submission, notifications will be sent to the LMS Admin team for processing. A copy will also be sent to the requester's email, and the enrolled person's email (as submitted on the form). The request will be saved for University Registrar's Office auditing purposes.
            </p>
            <p>
                If you have any questions about the functionality of this form, please contact the LMS Admins. If you have policy/FERPA questions, please contact the University Registrar's Office.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <form method="POST" action="{{ route('enrollmentrequests.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @include('canvas.enrollmentrequests.form', ['submitButtonText' => '<i class="fa fa-check"></i> Submit request'])
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script>
        function loadRoles() {
            // parameter 1 : url
            // parameter 2: post data
            //parameter 3: callback function

            $("#loading-roles").removeClass('d-none');
            $("#invalid-sis-course-id").addClass('d-none');
            $("#valid-sis-course-id").addClass('d-none');

            var thisSisCourseId = $( "#sis_course_id" ).val();
            $.ajax({
                type: "GET",
                url: "{{ route('enrollmentrequests.list-roles-select-element') }}",
                data: {
                    "inline": true,
                    "sis_course_id": thisSisCourseId,
                    "exclude": 17,
                    "selected": {{ old('roleId', 'null') }}
                }
            }).done(function (data, textStatus, jqXHR) {
                // console.log(data);
                $("#roleId").html(data);
                $("#valid-sis-course-id").removeClass('d-none');
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // console.log(errorThrown, jqXHR);
                $("#invalid-sis-course-id").removeClass('d-none');
            }).always(function (data, textStatus, jqXHR) {
                $("#loading-roles").addClass('d-none');
            });
        }


        $(function() {
            $( "#sis_course_id" ).on('change', loadRoles);

            // necessary for form error situations
            if ($("#sis_course_id").val().length != 0) {
                loadRoles();
            }
        });

        $('#begins_at').datepicker();

        $('#ends_at').datepicker();

    </script>

@endsection
