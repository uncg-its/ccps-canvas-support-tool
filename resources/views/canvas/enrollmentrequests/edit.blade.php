@extends('layouts.wrapper', [
    'pageTitle' => 'Enrollment Requests | Edit'
])

@section('content')
    <h1>Edit Enrollment Request: {{ $enrollmentRequest->requestId }}</h1>
    {!! Form::open()->method('patch')->route('enrollmentrequests.update', ['enrollmentRequest' => $enrollmentRequest])->fill($enrollmentRequest) !!}

    {!! Form::text('sis_course_id', 'SIS Course ID')->required(true) !!}
    {!! Form::text('enrolledPersonName', 'Enrolled Person Name')->required(true) !!}
    {!! Form::text('enrolledPersonUsername', 'Enrolled Person Username')->required(true) !!}

    <div id="role-select-container">
        <div class="form-group {{ $errors->has('roleId') ? 'has-error' : ''}}">
            <label for="enrolledPersonRoleId">Enrolled Person Role <span class="text-danger">*</span></label>
            <select name="enrolledPersonRoleId" class="form-control" id="enrolledPersonRoleId" required>
                <option value="NULL">Waiting on retrieval from Canvas...</option>
            </select>
            <small id="loading-roles" class="d-none"><i class="fas fa-spin fa-spinner"></i> <span class="text-muted">Retrieving roles from Canvas...</span></small>
            <small id="invalid-account-id" class="d-none"><span class="text-danger">Course roles could not be retrieved from Canvas</span></small>
            <small id="valid-account-id" class="d-none"><span class="text-success"><i class="fa fa-check"></i> Course roles retrieved from Canvas</span></small>
            {!! $errors->first('enrolledPersonRoleId', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    {!! Form::select('enrollmentReasonCategory', 'Enrollment Reason Category', $reasonCategoryArray)->required(true) !!}
    {!! Form::textarea('enrollmentReason', 'Enrollment Reason Details')->required(false) !!}

    {!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success')->attrs(['class' => 'mb-3']) !!}
    {!! Form::close() !!}
@endsection()

@section('scripts')

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>

    async function refreshRoleList() {
        $("#loading-roles").removeClass('d-none');
        $("#invalid-account-id").addClass('d-none');
        $("#valid-account-id").addClass('d-none');

        const result = await $.ajax({
            type: "GET",
            url: "{{ route('enrollmentrequests.list-roles-select-element') }}",
            data: {
                "inline": true,
                "sis_course_id": $("#sis_course_id").val(),
                "exclude": 17
            }
        }).done(function (data, textStatus, jqXHR) {
            // console.log(data);
            $("#enrolledPersonRoleId").html(data);
            $("#valid-account-id").removeClass('d-none');
        }).fail(function (jqXHR, textStatus, errorThrown) {
            // console.log(errorThrown, jqXHR);
            $("#invalid-account-id").removeClass('d-none');
        }).always(function (data, textStatus, jqXHR) {
            $("#loading-roles").addClass('d-none');
        });

        $("#enrolledPersonRoleId").val({{ $enrollmentRequest->enrolledPersonRoleId }});
    }

    $( "#sis_course_id" ).change(function() {
        refreshRoleList();
    });

    $(function() {
        refreshRoleList();
    });

</script>
@endsection
