@extends('layouts.wrapper', [
    'pageTitle' => 'Course Activity Reports'
])

@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
    <div class="row">
        <div class="col-6">
            <h1>Course Activity Reports</h1>
        </div>
        <div class="col-6 d-flex justify-content-end align-items-center">
            <a href="{{ route('canvas.course-activity-report.create') }}" class="btn btn-success btn-sm"><i
                        class="fas fa-plus"></i> New Report</a>
        </div>
    </div>

    @if($reports->isNotEmpty())
        <table class="table table-sm table-hover">
            <thead class="thead-light">
            <tr>
                <th>ID</th>
                <th>Requested By</th>
                <th>Account</th>
                <th>Term</th>
                <th>Status</th>
                <th>Queries Done</th>
                <th>Created At</th>
                <th>Completed At</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reports as $report)
                <tr>
                    <td>{{ $report->id }}</td>
                    <td>{{ $report->requestor->email }}</td>
                    <td>{{ $report->account_id }}</td>
                    <td>{{ $report->term_id }}</td>
                    <td>{!! $report->status_html !!}</td>
                    <td>{{ $report->items->count() - $report->pending_items->count() }}
                        / {{ $report->items->count() }}</td>
                    <td>{{ $report->created_at }}</td>
                    <td>{{ $report->completed_at }}</td>
                    <td>
                        <a href="{{ route('canvas.course-activity-report.show', ['report' => $report]) }}"
                           class="btn btn-info btn-sm">
                            <i class="fas fa-info-circle"></i> Details
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>No Activity Reports have been generated.</p>
    @endif

@endsection()