@extends('layouts.wrapper', [
    'pageTitle' => 'Course Activity Report | Details: ' . $report->id
])

@section('content')
    <div class="row">
        <div class="col-6">
            <h1>Course Activity Report: {{ $report->id }}</h1>
        </div>
        <div class="col-6 d-flex justify-content-end align-items-center">
            <a href="{{ route('canvas.course-activity-report.index') }}" class="btn btn-sm btn-warning">
                <i class="fas fa-arrow-left"></i> Back
            </a>
        </div>
    </div>


    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header">Basic Information</div>
                <div class="card-body">
                    <p><strong>Requested by</strong>: {{ $report->requestor->email }}</p>
                    <p><strong>Status</strong>: {!! $report->status_html !!}</p>
                    <p><strong>Created at</strong>: {{ $report->created_at }}</p>
                    <p><strong>Completed at</strong>: {{ $report->completed_at }}</p>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">Options / Search Parameters</div>
                <div class="card-body">
                    <p><strong>Account ID</strong>: <a
                                href="{{ config('cst.canvas_environment_url') }}/accounts/{{ $report->account_id }}"
                                target="_blank">{{ $report->account_id }}</a>
                    </p>
                    <p><strong>Term ID</strong>: <a
                                href="{{ config('cst.canvas_environment_url') }}/accounts/1/terms"
                                target="_blank">{{ $report->term_id }}</a>
                    </p>
                    <p><strong>Options</strong>:</p>
                    <ul>
                        @foreach($report->options as $key => $value)
                            <li><strong>{{ $key }}</strong>: {{ $value }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <div class="row">
                <div class="col-8">
                    <h1>Courses queried</h1>
                    @if(isset($report->options->since))
                        <p>Activity lower bound: <span class="text-danger">{{ $report->options->since }}</span>.
                            Activity before that time will be highlighted in <span class="text-danger">red</span>.</p>
                    @endif
                    @if(isset($report->options->until))
                        <p>Activity upper bound: <span class="text-danger">{{ $report->options->until }}</span>.
                            Activity after that time will be highlighted in <span class="text-danger">red</span>.</p>
                    @endif
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                    @if(!is_null($report->completed_at))
                        <a href="{{ route('canvas.course-activity-report.export', ['report' => $report]) }}"
                           class="btn btn-sm btn-success">
                            <i class="fas fa-file-csv"></i> Export to CSV
                        </a>
                    @endif
                </div>
            </div>
            @if($report->items->isNotEmpty())
                <table class="table table-sm table-striped">
                    <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th>Progress</th>
                        <th>Course</th>
                        <th>Status</th>
                        <th>End Date</th>
                        <th># Stu</th>
                        <th>Tot Time sec</th>
                        <th>Tch Activity</th>
                        <th>Stu View</th>
                        <th>Stu Partic</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($report->items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td class="{{ $item->status_class }}">
                                <i class="{{ $item->status_icon }}"></i>
                                {{ $item->status }}
                            </td>
                            <td>
                                <small>
                                    <a href="{{ $item->course_url }}"
                                       target="_blank">{{ $item->course_information->name }}</a>
                                </small>
                            </td>
                            <td>
                                {!! $item->course_status_html !!}
                            </td>
                            <td class="{{ $item->is_out_of_bounds($item->ends_at) ? 'text-danger' : '' }}">{{ $item->ends_at ?? 'none' }}</td>
                            <td>{{ $item->course_information->total_students }}</td>
                            <td>{{ optional($item->statistics)->total_activity_seconds ?? 'n/a' }}</td>
                            @include('canvas.course-activity-report.partials.activity-cell', ['attribute' => 'last_teacher_activity'])
                            @include('canvas.course-activity-report.partials.activity-cell', ['attribute' => 'last_student_view'])
                            @include('canvas.course-activity-report.partials.activity-cell', ['attribute' => 'last_student_participation'])

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>No items for this report.</p>
            @endif
        </div>
    </div>
@endsection()
