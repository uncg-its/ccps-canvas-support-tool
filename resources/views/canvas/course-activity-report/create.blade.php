@extends('layouts.wrapper', [
    'pageTitle' => 'Course Activity Report'
])

@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>

    <style>
        .form-check-input {
            position: static;
        }

        input[type="checkbox"] {
            margin-right: 10px;
        }
    </style>
@endsection

@section('content')
    <h1>Course Activity Report</h1>

    <p>This report will fetch activity data on each course within the search parameters.</p>

    {!! Form::open()->route('canvas.course-activity-report.store')->method('post')->fill(collect(session()->getOldInput())) !!}

    <div class="row mb-3">
        <div class="col-6">
            <div class="card">
                <div class="card-header">Required items</div>
                <div class="card-body">
                    {!! Form::select('account', 'Account', $accounts)->required(true)->help('Choose an account to search') !!}
                    {!! Form::select('term', 'Term', $terms)->required(true)->help('Choose a term to search') !!}
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">Optional items</div>
                <div class="card-body">
                    {!! Form::checkbox('hide_empty', 'Hide Empty Courses?', 1)->required(false) !!}
                    <br>
                    {!! Form::text('since', 'Since (lower bound)')->required(false)->help('Choose a date to use as the lower bound for this report. The report will look for any activity in the course(s) since this date (inclusive)') !!}
                    {!! Form::text('until', 'Until (upper bound)')->required(false)->help('Choose a date to use as the upper bound for this report. The report will look for any activity in the course(s) until this date (exclusive)') !!}
                    {!! Form::text('search_term', 'Search Term')->help('If supplied, will fetch only Courses matching the search term in the name, code, or ID. Minimum 3 characters')->required(false) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col mb-3">
            {!! Form::submit('<i class="fas fa-check"></i> Begin report generation')->color('success') !!}
        </div>
    </div>
    {!! Form::close() !!}
@endsection()

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $('#since').datepicker();
        $('#until').datepicker();
        // $('#account').select2();
        // $('#term').select2();
    </script>
@endsection
