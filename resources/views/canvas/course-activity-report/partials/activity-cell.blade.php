@if($item->status == 'complete')
    <td class="{{ $item->is_out_of_bounds(optional($item->statistics)->$attribute) ? 'text-danger' : '' }}">{{ $item->statistics->$attribute ?? 'none' }}</td>
@else
    <td>
        <small class="text-muted"><em>(pending)</em></small>
    </td>
@endif

