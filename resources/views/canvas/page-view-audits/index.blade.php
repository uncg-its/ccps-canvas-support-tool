@extends('layouts.wrapper', [
    'pageTitle' => 'Page View Audit | Index'
])

@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <h1>Page View Audit</h1>
            {!! Form::open()->route('canvas.page-view-audits.results') !!}
            <div class="row">
                <div class="col-4">
                    {!! Form::select('user_id_type', 'User ID Type', $userIdOptions) !!}
                </div>
                <div class="col">
                    {!! Form::text('user_id', 'User ID')->required(true) !!}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    {!! Form::select('result_limit', 'Limit number of results to:', $limitOptions) !!}
                </div>
                <div class="col">
                    {!! Form::text('start_date', 'Start Date')->help('Optional')->required(false) !!}
                </div>
                <div class="col">
                    {!! Form::text('end_date', 'End Date')->help('Optional')->required(false) !!}
                </div>
                <div class="col">
                    {!! Form::text('search_term', 'Search Term')->help('Optional')->required(false) !!}
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                    {!! Form::submit('<i class="fas fa-search"></i> Search') !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script>
        $('#start_date').datepicker();
        $('#end_date').datepicker();
    </script>
@endsection
