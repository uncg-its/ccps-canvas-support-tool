@extends('layouts.wrapper', [
    'pageTitle' => 'Page View Audit | Results'
])

@section('head')
    <style>
        .url-col {
            overflow-wrap: break-word;
            word-wrap: break-word;
            word-break: break-word;

            max-width: 300px;
        }

        .page-views-table {
            font-size: 10pt;
        }

        .match {
            background-color: khaki !important;
        }

        .form-check-input {
            position: static;
        }

        input[type="checkbox"] {
            margin-right: 10px;
        }


    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <h1>Page View Audit - results</h1>
            <div class="card bg-light mb-3">
                <div class="card-header">
                    <h4 class="mb-0">Request information</h4>
                </div>
                <div class="card-body">
                    <div class="card-text pb-3">
                        <ul>
                            @foreach($requestData as $key => $value)
                                @if (!empty($value))
                                    <li><strong>{{ str_replace('_', ' ', \Str::title($key)) }}</strong>: {{ $value }}
                                    </li>
                                @endif
                            @endforeach
                        </ul>

                        @if (!empty($requestData['search_term']))
                            <div class="form-row">
                                {!! Form::checkbox('only_matching', 'Show only results matching search term?') !!}
                            </div>
                        @endif
                        <p class="m-0 float-right">
                            <small><em>Results shown in timezone: {{ config('app.timezone') }}</em></small>
                        </p>
                    </div>
                </div>
            </div>

            <table class="table table-sm table-striped page-views-table">
                <thead class="thead-dark">
                <tr>
                    <th>Session ID</th>
                    <th>Date/Time</th>
                    <th class="url-col">URL</th>
                    <th>User Agent</th>
                    <th>IP</th>
                    <th>Partic.</th>
                    <th>Int. Sec.</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results as $result)
                    <tr class="{{ $result->match ? 'match' : 'non-match' }}">
                        <td>{{ $result->session_id }}</td>
                        <td>{{ $result->created_at_parsed }}</td>
                        <td class="break-urls url-col">
                            <a href="{{ $result->url }}" target="_blank">
                                {{ $result->url_parsed }}
                            </a>
                        </td>
                        <td>{{ $result->user_agent_parsed }}</td>
                        <td>
                            <a href="http://www.ip-tracker.org/locator/ip-lookup.php?ip={{ $result->remote_ip }}"
                               target="_blank">
                                {{ $result->remote_ip }}
                            </a>
                        </td>
                        <td class="text-center">
                            @if($result->participated)
                                <span class="text-success">
                                    <i class="fas fa-check-circle"></i>
                                </span>
                            @else
                                <span class="text-danger">
                                    <i class="fas fa-times-circle"></i>
                                </span>
                            @endif
                        </td>
                        <td>{{ !empty($result->interaction_seconds) ? round($result->interaction_seconds, 1) : '' }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function () {
            $('#only_matching').on('change', function () {
                if ($(this).is(':checked')) {
                    $('.non-match').hide();
                } else {
                    $('.non-match').show();
                }
            });
        });
    </script>
@endsection
