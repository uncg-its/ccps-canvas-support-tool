@extends('layouts.wrapper', [
    'pageTitle' => 'Nav Item Replacer | Index'
])

@section('head')

@endsection

@section('content')
    <div class="row">
        <div class="col">
            <h1>Nav Item Replacer</h1>

            {!! Form::open()->route('canvas.nav-item-replacer.review')->method('post') !!}

            <div class="row">
                <div class="col-6">
                    {!! Form::select('term', 'Term', $terms)->required(true) !!}
                </div>
                <div class="col-6">
                    {!! Form::text('search_term', 'Course Search Term')->help('If set, only courses with this string in the title will be affected')->required(false) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    {!! Form::text('menu_item_name', 'Name of Menu Item to replace')->required(true) !!}
                </div>
                <div class="col-6">
                    {!! Form::text('new_menu_item_name', 'Name of Menu Item to replace with')->required(true) !!}
                </div>
            </div>

            <div class="row">
                <div class="col mb-3">
                    {!! Form::submit('<i class="fas fa-file-alt"></i> Review') !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')

@endsection
