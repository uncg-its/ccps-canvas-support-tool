@extends('layouts.wrapper', [
    'pageTitle' => 'Nav Item Replacer | Review'
])

@section('head')

@endsection

@section('content')
    <div class="row">
        <div class="col">
            <h1>Nav Item Replacer - review</h1>

            <p>Based on selected parameters (term ID = {{ $term }}, search term = {{ $search_term ?? 'n/a' }}),
                {{ $courses->count() }} {{ str_plural('course', $courses->count()) }} would be inspected for nav item
                replacement.</p>

            <p>
                <button class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#courses"
                        aria-expanded="false" aria-controls="courses">
                    <i class="fas fa-eye"></i> Show affected courses
                </button>
            </p>
            <div class="collapse" id="courses">
                <div class="card card-body bg-light">
                    <ul>
                        @foreach($courses as $course)
                            <li>
                                <a href="{{ config('cst.canvas_environment_url') }}/courses/{{ $course->id }}"
                                   target="_blank">{{ $course->name }}
                                    ({{ $course->course_code }})</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <p>Continuing this process will begin queueing the replacement jobs. Nav Item Replacement jobs are currently
                set to be run {{ $queueWaitMinutes }} {{ str_plural('minute', $queueWaitMinutes) }} after queuing.</p>

            <div class="card bg-light mb-3">
                <div class="card-body text-muted">
                    Cache Key (ID) for debug purposes: {{ $key }}
                </div>
            </div>


            <div class="row">
                <div class="col mb-3">
                    {!! Form::open()->route('canvas.nav-item-replacer.replace')->method('post') !!}

                    {!! Form::hidden('key', $key) !!}

                    {!! Form::submit('<i class="fas fa-check"></i> Confirm and generate jobs')->color('success') !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection
