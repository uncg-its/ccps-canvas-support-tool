@extends('layouts.wrapper', [
    'pageTitle' => 'LTI Report ' . $report->id
])

@section('content')
    <h1>LTI Report {{ $report->id }}</h1>
	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-header">Report Information</div>
				<div class="card-body">
					<p><strong>Created At</strong>: {{ $report->created_at }}</p>
					<p><strong>Requested By</strong>: {{ $report->requestor->email }}</p>
					<p><strong>Canvas Report ID</strong>: {{ $report->canvas_report_id }} ({{ $report->canvas_report_status }})</p>
					<p>
						<strong>Status</strong>:
						<span class="text-{{ $report->local_status_class }}">
							<i class="{{ $report->local_status_icon }}"></i>
							{{ $report->local_status }}
						</span>
					</p>
					<p><strong>Job progress</strong>: {{ $report->progress }}</p>
				</div>
			</div>
		</div>
		<div class="col-6">
			<div class="card">
				<div class="card-header">Filter Information</div>
				<div class="card-body">
					@foreach (json_decode($report->filters) as $filter => $value)
						<p><strong>{{ $filter }}</strong>: {{ is_array($value) ? implode(', ', $value) : $value }}</p>
					@endforeach
				</div>
			</div>

		</div>
	</div>
	<div class="row mt-3">
		<div class="col">
			<div class="row">
				<div class="col-6">
		    		<h2>Report Items</h2>
		    		<p><em>Note: for sorting / filtering, please download CSV.</em></p>
		    	</div>
		    	<div class="col-6 d-flex justify-content-end align-items-center">
					<a href="{{ route('canvas.lti-reports.export', ['report' => $report]) }}" class="btn btn-sm btn-success">
						<i class="fas fa-file-csv"></i> Export to CSV
					</a>
		    	</div>
			</div>
			@if ($items->isNotEmpty())
				@component('components.paginated-table', ['collection' => $items])
					@slot('table')
					    @component('components.table')
					        @slot('th')
					            <th>ID</th>
					            <th>Tool Name</th>
					            <th>Course Name</th>
					            <th>Course Status</th>
					            <th>Term</th>
					            <th>Type</th>
					            <th>Department</th>
					            <th>Unit</th>
					            <th>Teachers</th>
					            <th>Students</th>
					        @endslot
					        @slot('tbody')
					            @foreach ($items as $item)
					            	<tr>
					            		<td>{{ $item->id }}</td>
					            		<td>{{ $item->tool_name }}</td>
					            		<td>{{ $item->courseName }}</td>
					            		<td>{{ $item->courseStatus }}</td>
					            		<td>{{ $item->term }}</td>
					            		<td>{{ $item->courseAffiliation['type'] }}</td>
					            		<td>{{ $item->courseAffiliation['department'] }}</td>
					            		<td>{{ $item->courseAffiliation['unit'] }}</td>
					            		<td>{{ $item->teachersCount }}</td>
					            		<td>{{ $item->studentsCount }}</td>
					            	</tr>
					            @endforeach
					        @endslot
					    @endcomponent
					@endslot
				@endcomponent
			@else
				<p>No report items to show.</p>
			@endif
		</div>
	</div>
@endsection()
