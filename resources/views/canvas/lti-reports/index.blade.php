@extends('layouts.wrapper', [
    'pageTitle' => 'LTI Reports | Index'
])

@section('content')
    <div class="row">
    	<div class="col-6">
    		<h1>LTI Reports</h1>
    	</div>
    	<div class="col-6 d-flex justify-content-end align-items-center">
    		<a href="{{ route('canvas.lti-reports.create') }}" class="btn btn-success btn-sm">
    			<i class="fa fa-plus"></i> New
    		</a>
    	</div>
    </div>
    @if ($reports->isNotEmpty())
    	@component('components.paginated-table', ['collection' => $reports])
            @slot('table')
                @component('components.table')
                    @slot('th')
                        <th>ID</th>
                        <th>Requested By</th>
                        <th>Account ID</th>
                        <th>Canvas Report ID</th>
                        <th>Status</th>
                        <th>Job Progress</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    @endslot
                    @slot('tbody')
                        @foreach ($reports as $report)
                            <tr>
                                <td>{{ $report->id }}</td>
                                <td>{{ $report->requestor->email }}</td>
                                <td>{{ $report->account_id }}</td>
                                <td>{{ $report->canvas_report_id }} ({{ $report->canvas_report_status }})</td>
                                <td class="text-{{ $report->local_status_class }}">
                                    <i class="{{ $report->local_status_icon }}"></i>
                                    {{ $report->local_status }}
                                </td>
                                <td>{{ $report->progress }}</td>
                                <td>{{ $report->created_at }}</td>
                                <td>
                                    <a href="{{ route('canvas.lti-reports.show', ['report' => $report]) }}" class="btn btn-info btn-sm">
                                        <i class="fa fa-list"></i> Details
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    @else
    	<p>No reports to display.</p>
    @endif

@endsection()
