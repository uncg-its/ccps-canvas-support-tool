@extends('layouts.wrapper', [
    'pageTitle' => 'LTI Report | Create New'
])

@section('head')
    <style>
        .form-check-input {
            position: static;
        }

        input[type="checkbox"] {
            margin-right: 10px;
        }
    </style>
@endsection

@section('content')
    <h1>LTI Report - New</h1>
    <div class="row">
        <div class="col-12">
            {!! Form::open()->method('post')->route('canvas.lti-reports.store')->fill(collect(\Session::getOldInput())) !!}
            <div class="form-row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Basic Information</div>
                        <div class="card-body">
                            <div class="form-group {{ $errors->has('accountId') ? 'has-error' : ''}}">
                                <label for="accountId" class="col control-label">{{ 'Select Account on which to run LTI Report' }}</label>
                                <div class="col">
                                    <select name="accountId" class="form-control" id="accountId">
                                        @foreach ($accountList as $optionKey => $optionValue)
                                            @php $accountParts = explode('___', $optionKey) @endphp
                                            <option value="{{ $accountParts[1] }}">{{ $optionValue }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Filtering</div>
                        <div class="card-body">
                            {!! Form::select('terms', 'Terms', $terms)->multiple()->required(false)->help('Optional: select which term(s) to include in report data. If not provided, all terms will be used.') !!}
                            <div class="form-row">
                                <div class="col">
                                    {!! Form::checkbox('include_deleted', 'Include deleted courses?')->required(false) !!}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-row my-3">
                <div class="col">{!! Form::submit('<i class="fa fa-check"></i> Submit')->color('success') !!}</div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection()
@section('scripts')
    <script>
        $( "#accountId" ).change(function() {
            //this is the #state dom element
            var state = $(this).val();

            var thisAccountId = $( "#accountId option:selected" ).val();

            $.get( '{{ route('canvas.lti-reports.reports-select-element') }}' + '?accountId=' + thisAccountId, { state : state } , function(htmlCode) { //htmlCode is the code returned from your controller
                $("#report-select-container").html(htmlCode);
            });
        });
    </script>
@endsection
