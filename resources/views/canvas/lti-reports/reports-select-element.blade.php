<div class="form-group {{ $errors->has('roleId') ? 'has-error' : ''}}">
<label for="reportId" class="col-md-4 control-label">{{ 'Report ID' }}</label>
    <div class="col-md-6">
        <select name="reportId" class="form-control" id="reportId">
            @foreach( $reports as $key => $label)
                <option value="{{ $key }}" >{{ $label }}</option>
            @endforeach
        </select>
        {!! $errors->first('reportId', '<p class="help-block">:message</p>') !!}
    </div>
</div>
