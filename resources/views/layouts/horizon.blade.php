<!DOCTYPE html>
<html>
<head>
    <title>Horizon</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/horizon/css/app.css') }}">
    <link rel="icon" href="vendor/horizon/img/favicon.png"/>
</head>

<body>
<div id="root"></div>

<div style="height: 0; width: 0; position: absolute; display: none;">
    {!! file_get_contents(public_path('/vendor/horizon/img/sprite.svg')) !!}
</div>

@if($subfolder)
    <script src="{{ asset('js/horizon-custom.js') }}"></script>
@else
    <script src="{{ asset('vendor/horizon/js/app.js') }}"></script>
@endif
</body>
</html>