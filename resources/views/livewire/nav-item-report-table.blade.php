<div>
    @if ($reports->isNotEmpty())
        @component('components.table')
            @slot('th')
                <th>ID</th>
                <th>Created By</th>
                <th>Account</th>
                <th>Term</th>
                <th>Status</th>
                <th>Actions</th>
            @endslot
            @slot('tbody')
                @foreach ($reports as $report)
                    <tr>
                        <td>{{ $report->id }}</td>
                        <td>{{ $report->created_by_user->email }}</td>
                        <td>{{ $report->account_name }}</td>
                        <td>{{ $report->term_name }}</td>
                        <td>@include('partials.statuses.' . $report->status)</td>
                        <td>
                            <a href="{{ route('nav-item-reports.show', $report) }}" class="btn btn-info btn-sm">
                                <i class="fas fa-list"></i> Details
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endslot
        @endcomponent
    @else
        <p>No reports to show.</p>
    @endif
</div>
