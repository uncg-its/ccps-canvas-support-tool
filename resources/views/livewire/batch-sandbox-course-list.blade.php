<div @if($this->batch->status === 'pending') wire:poll.5s @endif>
    <p>Status: @include('partials.statuses.' . $this->batch->status )</p>
    @component('components.table')
        @slot('th')
            <th>ID</th>
            <th>Name</th>
            <th>Instructor</th>
            <th>Status</th>
            <th>Canvas ID</th>
            <th>Completed at</th>
        @endslot
        @slot('tbody')
            @foreach ($this->batch->courses as $course)
                <tr wire:key="sbc_{{ $course->id }}">
                    <td>{{ $course->id }}</td>
                    <td>{{ $course->batch->course_name }}</td>
                    <td>{{ $course->instructor_username }}</td>
                    <td>
                        @include('partials.statuses.' . $course->status)
                        @if (in_array($course->status, ['pending', 'in-progress']))
                            <span wire:loading class="position-absolute ml-2">
                                <i class="fas fa-sync fa-spin" style="animation-duration: 1s"></i>
                            </span>
                        @endif
                    </td>
                    <td>
                        @if (!is_null($course->canvas_course_id))
                            <a href="{{ config('cst.canvas_environment_url') }}/courses/{{ $course->canvas_course_id }}" target="_blank">
                                {{ $course->canvas_course_id }} <i class="fas fa-external-link-alt"></i>
                            </a>
                        @endif
                    </td>
                    <td>{{ $course->completed_at }}</td>
                </tr>
            @endforeach
        @endslot
    @endcomponent
</div>
