<div>
    <div class="card">
        <div class="card-header">Create Nav Item Report</div>
        <div class="card-body">
            <div>
                @if ($error !== '')
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endif
            </div>

            <form wire:submit.prevent="submit">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="accountId">Account</label>
                            <select class="form-control" name="accountId" wire:model="accountId">
                                <option value="">--- Select ---</option>
                                @foreach ($accountList as $value => $text)
                                    <option value="{{ $value }}" wire:key="acct_{{ $value }}">{{ $text }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="termId">Term</label>
                            <select class="form-control" name="termId" wire:model="termId">
                                <option value="">--- Select ---</option>
                                @foreach ($termList as $value => $text)
                                    <option value="{{ $value }}" wire:key="term_{{ $value }}">{{ $text }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="d-flex">
                    <div>
                        @if (!empty($accountId) && !empty($termId))
                            <button type="submit" class="btn btn-success mr-3">
                                <i class="fas fa-check"></i> Submit
                            </button>
                        @endif
                    </div>
                    <button class="btn btn-danger" x-on:click.prevent="showCreate = false">
                        <i class="fas fa-times"></i> Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
