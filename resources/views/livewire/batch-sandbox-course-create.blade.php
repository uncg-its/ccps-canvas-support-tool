<div>
    <div class="row">
        @if ($submitError)
        <div class="col">
            <div class="alert alert-danger">
                {{ $submitError }}
            </div>
        </div>
        @endif
    </div>
    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <label for="sandboxName">Sandbox Course Name:</label>
                <input
                    type="text"
                    class="form-control"
                    name="sandboxName"
                    id="sandboxName"
                    wire:model="sandboxName"
                    required
                >
                </input>
                <small class="form-text text-muted">
                    The title for each course in this batch. Each selected username will be appended in parenthesis.<br>
                    <span>
                        @if (!empty($sandboxName))
                            example: <strong>{{ $sandboxName }} (jdoe1)</strong>
                        @endif
                    </span>
                </small>
            </div>
        </div>
        <div class="col-6">
            <div class="form-group">
                <label for="sandboxCode">Sandbox Course Code:</label>
                <input
                    type="text"
                    class="form-control"
                    name="sandboxCode"
                    id="sandboxCode"
                    wire:model="sandboxCode"
                    required
                >
                </input>
                <small class="form-text text-muted">
                    The short code for each course in this batch. Each selected username will be appended via a dash.<br>
                    <span>
                        @if (!empty($sandboxCode))
                            example: <strong>{{ $sandboxCode }}-jdoe1</strong>
                        @endif
                    </span>
                </small>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h2>Find Users</h2>
            <p>Search for users either via a direct input list, or from the Student enrollments of an existing Canvas course (for instance, a workshop course).</p>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="usernameList">List of usernames:</label>
                        <textarea
                            class="form-control"
                            name="usernameList"
                            id="usernameList"
                            wire:model="usernameList"
                        ></textarea>
                        <small class="form-text text-muted">Option 1: enter/paste a list of usernames here, each on its own line.</small>
                    </div>
                    <button class="btn btn-md btn-primary" wire:click="findUsers">
                        <i class="fas fa-search"></i> Find Canvas Users
                    </button>
                    <small class="text-muted ml-3" wire:loading wire:target="findUsers">
                        <i class="fas fa-spinner fa-spin"></i> Loading...
                    </small>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="courseSearch">Search course roster:</label>
                        <input
                            type="text"
                            class="form-control"
                            name="courseSearch"
                            id="courseSearch"
                            wire:model="courseSearch"
                            required
                        >
                        </input>
                        <small class="form-text text-muted">Option 2: enter a course Canvas ID (from URL) here to automatically pull students from its roster<br>e.g. https://uncg.instructure.com/courses/<strong>123456</strong></small>
                    </div>
                    <button class="btn btn-md btn-primary" wire:click="searchRoster">
                        <i class="fas fa-search"></i> Search Course Roster
                    </button>
                    <small class="text-muted ml-3" wire:loading wire:target="searchRoster">
                        <i class="fas fa-spinner fa-spin"></i> Loading...
                    </small>
                    @if ($searchError)
                        <div class="alert alert-danger mt-3" wire:loading.remove wire:target="searchRoster">
                            Error searching for course roster.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col-6">
            <h2>Identify Users</h2>
            <p>Identify the user population from the search results.</p>
            <div>
                @if (!empty($searchResults))
                <button class="btn btn-sm btn-success mb-3" wire:click="addAll">
                    <i class="fas fa-plus"></i> Add all
                </button>
                @endif
            </div>
            <div class="row">
                @if (!empty($searchResults))
                @foreach ($searchResults as $key => $result)
                    <div class="col-4" wire:key="'result_' . {{ $result }}">
                        <button class="btn btn-xs btn-success mr-2" wire:click="addUser('{{ $key }}')">
                            <i class="fas fa-plus"></i>
                        </button> <small>{{ $result }}</small>
                    </div>
                @endforeach
                @endif

            </div>
        </div>
        <div class="col-6">
            <h2>Selected Users</h2>
            <p>The following users will have a sandbox course created for them:</p>
            <div>
                @if (!empty($usernames))
                <button class="btn btn-sm btn-danger mb-3" wire:click="removeAll">
                    <i class="fas fa-minus"></i> Remove all
                </button>
                @endif
            </div>
            <div class="row">
                @foreach ($usernames as $key => $username)
                    <div class="col-4" wire:key="'username_' . {{ $username }}">
                        <button class="btn btn-xs btn-danger mr-2" wire:click="removeUser('{{ $key }}')">
                            <i class="fas fa-minus"></i>
                        </button> <small>{{ $username }}</small>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col text-center">
            <hr>
            @if (!empty($usernames) && !empty($sandboxCode) && !empty($sandboxName))
                <button class="btn btn-lg btn-success" wire:click="submit" wire:loading.attr="disabled" wire:target="submit">
                    <span wire:target="submit" wire:loading.remove><i class="fas fa-check"></i> Submit</span>
                    <span wire:target="submit" wire:loading><i class="fas fa-spinner fa-spin"></i> Submitting...</span>
                </button>
            @endif
        </div>
    </div>
</div>
