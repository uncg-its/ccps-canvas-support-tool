<div>
    @if ($crosslists->isEmpty())
        <p>No crosslists to show.</p>
    @else
        @component('components.paginated-table', ['collection' => $crosslists])
            @slot('table')
                @component('components.table')
                    @slot('th')
                        <th>ID</th>
                        <th>Performed By</th>
                        <th>Parent Course</th>
                        <th>Child Sections</th>
                        <th>Performed At</th>
                        <th>Errors</th>
                    @endslot
                    @slot('tbody')
                        @foreach ($crosslists as $crosslist)
                            <tr>
                                <td>{{ $crosslist->id }}</td>
                                <td>{{ $crosslist->performed_by->email }}</td>
                                <td>{{ $crosslist->parent_course_sis_id }}</td>
                                <td>{!! implode('<br>', explode(',', $crosslist->child_section_sis_ids)) !!}</td>
                                <td>{{ $crosslist->created_at }}</td>
                                <td>{!! implode('<br>', $crosslist->errors ?? []) !!}</td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @endslot
        @endcomponent
    @endif
</div>
