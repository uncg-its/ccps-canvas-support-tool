<div>
    <h1>Crosslist</h1>
    <p><em>Note: courses must have SIS IDs to be cross-listed using this tool.</em></p>
    <div class="row mb-3">
        <div class="col">
            <div class="card">
                <div class="card-header">Select Parent Course</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="account">Select School / College:</label>
                                <select
                                    class="form-control"
                                    name="account"
                                    id="account"
                                    wire:model="account"
                                >
                                    <option value="">--- Select ---</option>
                                    @foreach ($accounts as $value => $display)
                                        <option value="{{ $value }}" :key="parent_acct_{{ $value }}">{{ $display }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="term">Term:</label>
                                <select name="term" id="term" wire:model="parentTerm" class="form-control" wire:change="termChanged">
                                    <option value="">--- Select ---</option>
                                    @foreach($termsList as $key => $term)
                                        <option value="{{ $key }}" :key="{{ $key }}">{{ $term }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="courseSearch">Course Search</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    wire:model="courseSearch"
                                    name="courseSearch"
                                    wire:keydown.debounce.300ms="searchCourseInCanvas"
                                    wire:target="searchCourseInCanvas"
                                    {{ empty($account) || empty($parentTerm) ? 'disabled' : '' }}
                                />
                                <small class="text-muted">Enter a few characters of the course name or SIS ID</small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="course">Select Parent Course:</label>
                                <select
                                    name="course"
                                    id="course"
                                    class="form-control"
                                    {{ empty($courseList) ? "disabled" : "" }}
                                    wire:model="parentCourse"
                                    wire:loading.attr="disabled"
                                    wire:target="termChanged"
                                >
                                    <option value="">--- Select ---</option>
                                    @foreach ($courseList as $id => $course)
                                        <option value="{{ $id }}" :key="parent_{{ $id }}">{{ $course }}</option>
                                    @endforeach
                                </select>
                                <small class="text-info" wire:loading wire:target="searchCourseInCanvas">
                                    <i class="fas fa-spinner fa-spin"></i> Loading...
                                </small>
                            </div>
                        </div>
                    </div>
                    <div class="row" wire:loading.remove wire:target="searchCourseInCanvas">
                        @if ($courseNotFound)
                        <div class="col">
                            <div class="alert alert-warning">
                                <i class="fas fa-times"></i>
                                No courses found with valid SIS IDs.
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row mb-3">
        <div class="col">
            <div class="card">
                <div class="card-header">Select Child Sections</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="childAccount">Select School / College:</label>
                                <select
                                    class="form-control"
                                    name="childAccount"
                                    id="childAccount"
                                    wire:model="childAccount"
                                    {{ empty($parentTerm) ? 'disabled' : '' }}
                                >
                                    <option value="">--- Select ---</option>
                                    @foreach ($childAccounts as $value => $display)
                                        <option value="{{ $value }}" :key="child_acct_{{ $value }}">{{ $display }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="childSectionSearch">Section Search</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    wire:model="childSectionSearch"
                                    name="childSectionSearch"
                                    wire:keydown.debounce.300ms="searchSectionsInCanvas"
                                    wire:target="searchSectionsInCanvas"
                                    {{ empty($childAccount) || empty($parentTerm) ? "disabled" : "" }}
                                />
                                <small class="text-muted">Enter a few characters of the section name or SIS ID</small>
                            </div>
                        </div>
                    </div>
                    <div class="row d-none" wire:loading.class.remove="d-none" wire:target="searchSectionsInCanvas">
                        <div class="col">
                            <div class="alert alert-info">
                                <i class="fas fa-spinner fa-spin"></i>
                                Loading course sections...
                            </div>
                        </div>
                    </div>
                    <div class="row" wire:loading.remove wire:target="searchSectionsInCanvas">
                        @if ($sectionsNotFound)
                        <div class="col">
                            <div class="alert alert-warning">
                                <i class="fas fa-times"></i>
                                No sections found with valid SIS IDs.
                            </div>
                        </div>
                        @endif
                        @if (count($sectionList) > 25)
                        <div class="col">
                            <div class="alert alert-warning">
                                <i class="fas fa-exclamation-circle"></i>
                                Note: large resultset. For best performance try refining your search.
                            </div>
                        </div>
                        @endif
                    </div>
                    <div>
                        @if(!empty($sectionList))
                        <div class="row">
                            <div class="col">
                                @component('components.table')
                                    @slot('th')
                                        <th>Section ID</th>
                                        <th>Section Name</th>
                                        <th>Action</th>
                                    @endslot
                                    @slot('tbody')
                                        @foreach ($sectionList as $sectionId => $section)
                                            @if(!isset($newChildSections[$sectionId]))
                                            <tr :key="{{ $section }}">
                                                <td>{{ $sectionId }}</td>
                                                <td>{{ $section }}</td>
                                                <td>
                                                    <button class="btn btn-sm btn-success" wire:click="addSection('{{ $sectionId }}', '{{ $section }}')">
                                                        <i class="fas fa-plus"></i> Add
                                                    </button>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @endslot
                                @endcomponent
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(!empty($parentCourse) || !empty($newChildSections))
    <div class="row mb-3">
        <div class="col">
            <div class="card">
                <div class="card-header">Confirm</div>
                <div class="card-body">
                    @if(!empty($parentCourse))
                        <div class="row mb-3">
                            <div class="col">
                                <div class="alert alert-danger">
                                    <p>
                                        <i class="fas fa-exclamation-triangle"></i>
                                        <strong :key="parentCourseDisplay">{{ $courseList[$parentCourse] }}</strong> is selected as the PARENT course. Please ensure this is correct!
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(!empty($newChildSections))
                        <div class="row mb-3">
                            <div class="col">
                                <div class="alert alert-warning">
                                    <p :key="childInfo">
                                        <i class="fas fa-info-circle"></i> New child sections:
                                    </p>
                                    <ul>
                                        @foreach ($newChildSections as $id => $name)
                                            <li :key="newchild_{{ $id }}">
                                                {{ $id }}: {{ $name }}
                                                <button class="btn btn-link ml-2" wire:click="removeSection('{{ $id }}')">
                                                    [<i class="fas fa-times"></i> Remove]
                                                </button>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @if (!empty($parentCourse) && !empty($newChildSections))
                                <div class="col text-center">
                                    <button class="btn-lg btn-success" wire:click="performCrossList" wire:loading.attr='disabled' wire:target='performCrossList'>
                                        <span wire:loading.remove wire:target='performCrossList'>
                                            <i class="fas fa-check"></i> Confirm, and Cross-List in Canvas
                                        </span>
                                        <span wire:loading wire:target='performCrossList'>
                                            <i class="fas fa-spinner fa-spin"></i> Cross-listing...
                                        </span>
                                    </button>
                                </div>
                            @endif
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
    @endif
</div>
