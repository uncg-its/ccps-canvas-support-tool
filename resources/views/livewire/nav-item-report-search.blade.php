<div>
    <div>
        <div class="d-flex">
            <input class="form-control mr-3" type="text" wire:model="search" wire:loading.attr="disabled" wire:target="performSearch" wire:keydown.enter="performSearch" />
            <button class="btn btn-primary btn-sm" wire:click="performSearch">
                <i class="fas fa-search"></i> Search
            </button>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-12">
            <div class="alert alert-warning w-100" wire:loading wire:target="performSearch">
                <i class="fas fa-spinner fa-spin"></i> Searching...
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col">
            @if (count($results) > 0 && $searchComplete)
                <p>{{ $results->count() }} course(s) contain an active menu item containing "{{ $search }}".</p>
                @component('components.table')
                    @slot('th')
                        <th>Canvas ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    @endslot
                    @slot('tbody')
                        @foreach ($results as $result)
                            <tr wire:key="item_{{ $result->id }}">
                                <td>{{ $result->canvas_course_id }}</td>
                                <td>{{ $result->canvas_course_name }} ({{ $result->canvas_course_code }})</td>
                                <td>
                                    <a href="{{ config('cst.canvas_environment_url') }}/courses/{{ $result->canvas_course_id }}" class="btn btn-sm btn-warning" target="_blank">
                                        <i class="fas fa-external-link-alt"></i> View in Canvas
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endslot
                @endcomponent
            @elseif (!empty($search) && $searchComplete)
                <p>No courses contain an active menu item containing "{{ $search }}"</p>
            @endif
        </div>
    </div>
</div>
