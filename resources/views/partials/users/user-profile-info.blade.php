<div class="table-responsive">
    <table class="table table-borderless">
        <tbody>
            <tr>
                <th>Avatar</th>
                <td><img src="{{ $canvasUser->profile->avatar_url }}" alt="{{ $canvasUser->name }} avatar" class="img-square"></td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ $canvasUser->profile->title }}</td>
            </tr>
            <tr>
                <th>Bio</th>
                <td>{{ $canvasUser->profile->bio }}</td>
            </tr>
            <tr>
                <th>Time Zone</th>
                <td>{{ $canvasUser->profile->time_zone }}</td>
            </tr>
        </tbody>
    </table>
</div>
