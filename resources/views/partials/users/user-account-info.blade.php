<div class="table-responsive">
    <table class="table table-borderless">
        <tbody>
           <tr>
                <th>Canvas URL</th>
                <td>
                    <a title="View User with ID {{ $canvasUser->id }} in Canvas" href="{{ 'https://' . config('canvas-api.configs.' . config('canvas-api.defaults.config') . '.host') . '/accounts/1/users/' . $canvasUser->id }}" target="_blank">
                        {{ 'https://' . config('canvas-api.configs.' . config('canvas-api.defaults.config') . '.host') . '/accounts/1/users/' . $canvasUser->id }}
                    </a>
                </td>
            </tr>
            <tr>
                <th>ID</th>
                <td>{{ $canvasUser->id }}</td>
            </tr>
            <tr>
                <th>Login ID</th>
                <td>{{ $canvasUser->login_id }}</td>
            </tr>
            <tr>
                <th>Primary Email</th>
                <td>{{ $canvasUser->email }}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>{{ $canvasUser->name }}</td>
            </tr>
            <tr>
                <th>Short Name</th>
                <td>{{ $canvasUser->short_name }}</td>
            </tr>
            <tr>
                <th>Sortable Name</th>
                <td>{{ $canvasUser->sortable_name }}</td>
            </tr>
            <tr>
                <th>SIS User ID</th>
                <td>{{ $canvasUser->sis_user_id }}</td>
            </tr>
            <tr>
                <th>Locale</th>
                <td>{{ $canvasUser->locale }}</td>
            </tr>
        </tbody>
    </table>
</div>
