<div class="row">
    <div class="col-md-12">
        <div class="card mb-3">
            <div class="card-body">
                <form method="GET" action="{{ route('enrollmentrequests.' . $target ?? 'index') }}" accept-charset="UTF-8" class="navbar-form navbar-right mb-3" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                            @if(!empty(request()->search))
                            <a class="btn btn-danger text-white" href={{ route('enrollmentrequests.' . $target ?? 'index', request()->except('search')) }} title="Clear Search">
                                <i class="fa fa-undo"></i>
                            </a>
                            @endif
                            </button>
                        </span>
                    </div>
                </form>

                @component('components.paginated-table', ['collection' => $items])
                    @slot('table')
                        @component('components.table')
                            @slot('th')
                                <th>@sortablelink('created_at', 'Submitted')</th>
                                <th>@sortablelink('enrolledPersonName', 'Name')</th>
                                @if($target == 'all')
                                    <th>@sortablelink('requesterEmailAddress', 'Requester')</th>
                                @endif
                                <th>Course</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Actions</th>
                            @endslot
                            @slot('tbody')
                                @foreach($items as $item)
                                    <tr>
                                        <td>{{ $item->created_at->diffForHumans() }}</td>
                                        <td>{{ $item->enrolledPersonName }}</td>
                                        @if($target == 'all')
                                            <td>{{ $item->requesterEmailAddress }}</td>
                                        @endif
                                        <td>{{ $item->sis_course_id }}</td>
                                        <td>{{ $item->enrolledPersonRole }}</td>
                                        <td class="text-{{ $item->status_class }}">
                                            <i class="fas fa-{{  $item->status_icon }}"></i>
                                            {{ $item->status }}
                                        </td>
                                        <td class="d-fled justify-content-start align-items-center">
                                            @permission('enrollmentrequests.read')
                                            <a href="{{ route('enrollmentrequests.show',  ['requestId' => $item->requestId]) }}" title="View Request" class="btn btn-info btn-sm"><i class="fa fa-list"></i> Details</a>
                                            @endpermission
                                            @if ($item->status === 'pending approval')
                                                @permission('enrollmentrequests.update')
                                                <a href="{{ route('enrollmentrequests.edit',  $item) }}" title="Edit Request" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                                @endpermission
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                            @endslot
                        @endcomponent
                    @endslot
                @endcomponent
            </div>
        </div>
    </div>
</div>
