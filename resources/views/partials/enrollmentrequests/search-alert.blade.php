@if(request()->has('search') && !empty(request()->search))
    <div class="alert alert-info">Showing results for search term: <em>{{ request()->search }}</em></div>
@endif
