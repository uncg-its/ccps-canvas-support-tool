@component('components.paginated-table', ['collection' => $enrollments, 'appends' => request()->except('_token')])
    @slot('table')
        @component('components.table')
            @slot('th')
                <th>Name</th>
                <th>SIS Login ID</th>
                <th>Role</th>
                <th>State</th>
                <th>Created At (local)</th>
            @endslot
            @slot('tbody')
                @foreach ($enrollments as $enrollment)
                    @if ($enrollment->user->sortable_name !== 'Student, Test')
                        <tr>
                            <td>
                                <a href="{{ route('live.users.show', ['id_type' => 'sis_login_id', 'user_id' => $enrollment->user->login_id]) }}">
                                    {{ $enrollment->user->sortable_name }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('live.users.show', ['id_type' => 'sis_login_id', 'user_id' => $enrollment->user->login_id]) }}">
                                    {{ $enrollment->user->login_id }}
                                </a>
                            </td>
                            <td>{{ $enrollment->role }}</td>
                            <td>{{ $enrollment->enrollment_state }}</td>
                            <td>{{ \Carbon\Carbon::parse($enrollment->created_at)->setTimezone(config('app.timezone'))->format('Y-m-d H:i:s') }}</td>
                        </tr>
                    @endif
                @endforeach
            @endslot
        @endcomponent
    @endslot
@endcomponent
