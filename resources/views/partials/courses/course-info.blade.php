<div class="table-responsive">
    <table class="table table-borderless">
        <tbody>
            <tr>
                <th>Canvas URL</th>
                <td>
                    <a title="View course with ID {{ $course->id }} in Canvas" href="{{ 'https://' . config('canvas-api.configs.' . config('canvas-api.defaults.config') . '.host') . '/courses/' . $course->id . '/settings' }}" target="_blank">
                    {{ 'https://' . config('canvas-api.configs.' . config('canvas-api.defaults.config') . '.host') . '/courses/' . $course->id . '/settings' }}
                    </a>
                </td>
            </tr>
            <tr>
                <th>ID</th>
                <td>{{ $course->id }}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>{{ $course->name }}</td>
            </tr>
            <tr>
                <th>Workflow State</th>
                <td class="text-{{ $course->workflow_state === 'unpublished' ? 'danger' : 'success' }}">
                    <i class="fas fa-{{ $course->workflow_state === 'unpublished' ? 'times-circle' : 'check-circle' }}"></i>
                    {{ $course->workflow_state }}
                </td>
            </tr>
            <tr>
                <th>Account</th>
                <td>{{ $course->account->name }}</td>
            </tr>
            <tr>
                <th>Total Students</th>
                <td>{{ $course->total_students }}</td>
            </tr>
            <tr>
                <th>Teachers</th>
                <td>
                    @foreach($course->teachers as $teacher)
                    {{ $teacher->display_name }}<br />
                    @endforeach
                </td>
            </tr>
            <tr>
                <th>Created At</th>
                <td>{{ $course->created_at }}</td>
            </tr>
            <tr>
                <th>Start At</th>
                <td>{{ $course->start_at }}</td>
            </tr>
            <tr>
                <th>End At</th>
                <td>{{ $course->end_at }}</td>
            </tr>
            <tr>
                <th>Term</th>
                <td>{{ $course->term->name }}</td>
            </tr>
            <tr>
                <th>Is Public</th>
                <td>{{ $course->is_public }}</td>
            </tr><tr>
                <th>Grading Standard ID</th>
                <td>{{ $course->grading_standard_id }}</td>
            </tr>
            <tr>
                <th>Storage Quota MB</th>
                <td>{{ $course->storage_quota_used_mb }} used of {{ $course->storage_quota_mb }}</td>
            </tr>
            <tr>
                <th>SIS Course ID</th>
                <td>
                    <p>{{ $course->sis_course_id }}</p>
                    @permission('canvas.courses.edit')
                    @if(!empty($course->sis_course_id))
                        {!! Form::open()->method('patch')->route('courses.update') !!}
                        {!! Form::hidden('operation', 'remove_sis_course_id') !!}
                        {!! Form::hidden('course_id', $course->id) !!}
                        {!! Form::submit('<i class="fas fa-unlink"></i> Remove SIS ID')->color('danger')->attrs(['onclick' => 'return confirm("Are you sure? You cannot undo this action.")']) !!}
                        {!! Form::close() !!}
                    @endif
                    @endpermission
                </td>
            </tr>
            <tr>
                <th>Course Object Data from Canvas</th>
                <td>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#courseMeta">
                        <i class="fas fa-list"></i>
                        View course object
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="courseMeta" tabindex="-1" role="dialog" aria-labelledby="courseMetaLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="courseMetaLabel">Course Metadata</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <pre>{{ json_encode($course, JSON_PRETTY_PRINT) }}</pre>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>
