@component('components.table')
    @slot('th')
        <th>Canvas ID</th>
        <th>Name</th>
    @endslot
    @slot('tbody')
        @foreach ($course->sections as $section)
            <tr>
                <td>{{ $section->id }}</td>
                <td>{{ $section->name }}</td>
            </tr>
        @endforeach
    @endslot
@endcomponent
