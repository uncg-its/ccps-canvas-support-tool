@component('components.table')
    @slot('th')
        <th>Label</th>
        <th>Position</th>
        <th>Visibile?</th>
    @endslot
    @slot('tbody')
        @foreach ($tabs as $tab)
            <tr>
                <td>{{ $tab->label }}</td>
                <td>{{ $tab->position }}</td>
                <td>{{ ($tab->hidden ?? false) ? 'no' : 'yes' }}</td>
            </tr>
        @endforeach
    @endslot
@endcomponent
