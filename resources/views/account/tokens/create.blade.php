@extends('layouts.wrapper', [
    'pageTitle' => 'API Tokens | Create New'
])

@section('content')
    {!! Breadcrumbs::render('tokens.create') !!}
    <h1>New API Token</h1>
    <p><strong>API Tokens</strong> can be used to authenticate to this application's API endpoints as you.</p>
    <hr>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">Token Information</div>
                <div class="card-body">
                    {!! Form::open()->post()->route('account.tokens.store') !!}
                    {!! Form::text('name', 'Name', old('name'))->required()->help('A name for the token, so that you can tell it apart from others you have created. Typically this will refer in some way to how the token will be used.') !!}
                    {!! Form::select('expires', 'Expires', ['' => 'never', '1' => '1 month', '3' => '3 months', '6' => '6 months', '12' => '1 year'], old('expires'))->required(false)->help('When this token should expire and be purged from your account') !!}
                    {!! Form::submit('<i class="fas fa-check"></i> Submit')->color('success') !!}
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
@endsection
