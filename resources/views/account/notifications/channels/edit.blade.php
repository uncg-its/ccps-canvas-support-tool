@extends('layouts.wrapper', [
    'pageTitle' => 'Notifications | Edit Channel'
])

@section('content')
    <h1>Edit Notification Channel</h1>

    <div class="row">
        <div class="col-6">
            @component('bootstrap::card')
                @slot('header')
                    <h5 class="mb-0">Edit channel</h5>
                @endslot
                <div class="card-body">
                    {!! Form::open()->method('patch')->route('account.notifications.channels.update', ['channel' => $channel])->fill(collect($channel ?? old())) !!}
                    @include('notifications.channels.form')
                    {!! Form::close() !!}
                </div>
            @endcomponent
        </div>
        <div class="col-6">
            @component('bootstrap::card')
                @slot('header')
                    <h5 class="mb-0">Currently configured channels</h5>
                @endslot

                <div class="card-body">
                    @if($currentChannels->isNotEmpty())
                        <ul class="mb-0">
                            @foreach($currentChannels as $channel)
                                <li>{{ $channel->name }} ({{ $channel->type }}) - {{ $channel->key }}</li>
                            @endforeach
                        </ul>
                    @else
                        <p>No channels currently configured.</p>
                    @endif
                </div>

            @endcomponent
        </div>
    </div>

@endsection()
