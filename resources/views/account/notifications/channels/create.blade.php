@extends('layouts.wrapper', [
    'pageTitle' => 'Notifications | Create Channel'
])

@section('content')
    {!! Breadcrumbs::render('notifications.channels.create') !!}
    <h1>Create Notification Channel</h1>
    <p>Add a channel here where you want to receive notifications from this application. Currently supported channel
        types are:</p>
    <ul>
        @foreach($types as $type)
            <li>{{ $type }}</li>
        @endforeach
    </ul>
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Create new channel</h5>
                </div>
                <div class="card-body">
                    {!! Form::open()->method('post')->route('account.notifications.channels.store')->fill(collect($channel ?? old())) !!}
                    @include('account.notifications.channels.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Currently configured channels</h5>
                </div>
                <div class="card-body">
                    @if($currentChannels->isNotEmpty())
                        <ul class="mb-0">
                            @foreach($currentChannels as $channel)
                                <li>{{ $channel->name }} ({{ $channel->type }}) - {{ $channel->key }}</li>
                            @endforeach
                        </ul>
                    @else
                        <p>No channels currently configured.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection()

@section('scripts')
    <script>
        $(function () {
            $('#type').on('change', function () {
                if ($(this).val() === 'sms') {
                    $('.carrier-wrapper').removeClass('d-none');
                } else {
                    $('.carrier-wrapper').addClass('d-none');
                }
            });
        });
    </script>
@endsection
