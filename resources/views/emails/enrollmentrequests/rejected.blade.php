@extends('emails.layouts.emailwrapper')

@section('title')
    Enrollment Request Rejected
@endsection

@section('heading')
    Enrollment Request #{{ $enrollmentRequest->requestId }} Rejected
@endsection

@section('content')
    <p>You are receiving this message because a Canvas Enrollment Request request was made either by you, or on your behalf. Please view the information below for details, and the current status of the request.</p>
    <h3>Summary</h3>
    <p><strong>Request ID</strong>: {{ $enrollmentRequest->requestId }} </p>
    <p><strong>Submitted by</strong>: {{ $enrollmentRequest->requester->email }}</p>
    <p><strong>Enrolled Person username</strong>: {{ $enrollmentRequest->enrolledPersonUsername }}</p>
    <p><strong>Course SIS ID</strong>: {{ $enrollmentRequest->sis_course_id }}</p>
    <p><strong>Role requested</strong>: {{ $enrollmentRequest->enrolledPersonRole }}</p>
    <p><strong>Enrollment start date</strong>: {{ $enrollmentRequest->begins_at }}</p>
    <p><strong>Enrollment end date</strong>: {{ $enrollmentRequest->ends_at }}</p>
    <p><strong>Enrollment reason</strong>: {{ $enrollmentRequest->enrollmentReason }}</p>

    <p><strong>Rejected by</strong>: {{ $enrollmentRequest->rejector->email }}</p>

    <p>
        Requesters and Administrators can view this request via the app.
    </p>
@endsection

@section('buttonLink')
    {{ $appUrl }}
@endsection

@section('buttonText')
    Login Now
@endsection
