@extends('layouts.wrapper', [
    'pageTitle' => 'Batch Sandbox Courses | Index'
])

@section('content')
    {!! Breadcrumbs::render('batch-sandbox.index') !!}

    <div class="d-flex justify-content-between align-items-center">
        <h1>Batch Sandbox Courses</h1>
        @permission('batch-sandbox.create')
        <a href="{{ route('batch-sandbox-courses.create') }}" class="btn btn-success btn-sm">
            <i class="fas fa-plus"></i> Create New
        </a>
        @endpermission
    </div>
    <p>This function allows you to generate a batch of Sandbox Courses for a set of users, all with the same course name/code template. You can enter a user list ad-hoc, or source your user list from the Student enrollments of a Canvas course.</p>
    @permission('batch-sandbox.admin')
        <div class="alert alert-warning">
            Viewing all batches via administrator privileges
        </div>
    @endpermission

    @component('components.paginated-table', ['collection' => $batches])
        @slot('table')
            @component('components.table')
                @slot('th')
                    <th>ID</th>
                    <th>Created By</th>
                    <th>Course Prefix</th>
                    <th>Batch Size</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Actions</th>
                @endslot
                @slot('tbody')
                    @foreach ($batches as $batch)
                        <tr>
                            <td>{{ $batch->id }}</td>
                            <td>{{ $batch->created_by->email }}</td>
                            <td>{{ $batch->course_name }}</td>
                            <td>{{ $batch->courses()->count() }}</td>
                            <td>
                                @include('partials.statuses.' . $batch->status )
                            </td>
                            <td>{{ $batch->created_at }}</td>
                            <td class="d-flex">
                                <a href="{{ route('batch-sandbox-courses.show', $batch) }}" class="btn btn-info btn-sm">
                                    <i class="fas fa-list"></i> Details
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endslot
            @endcomponent
        @endslot
    @endcomponent
@endsection
