@extends('layouts.wrapper', [
    'pageTitle' => 'Batch Sandbox Course | Create'
])

@section('head')
    @livewireStyles
@append

@section('content')
    {!! Breadcrumbs::render('batch-sandbox.create') !!}
    <h1>Create new Sandbox Course Batch</h1>
    <livewire:batch-sandbox-course-create />
@endsection

@section('scripts')
    @livewireScripts
@append
