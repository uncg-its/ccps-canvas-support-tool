@extends('layouts.wrapper', [
    'pageTitle' => 'Batch Sandbox Courses | Batch ' . $batch->id
])

@section('head')
    @livewireStyles
@append

@section('content')
    {!! Breadcrumbs::render('batch-sandbox.show', $batch) !!}
    <h1>Batch Sandbox Courses - Batch {{ $batch->id }}</h1>
    <livewire:batch-sandbox-course-list :batchId="$batch->id" />
@endsection

@section('scripts')
    @livewireScripts
@append
