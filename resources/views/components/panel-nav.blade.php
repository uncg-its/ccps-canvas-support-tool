<div class="p-3">
    <div class="card">
		<a class="w-{{ $span ?? '100' }}" href="{{ $url }}">
            <div class="card-body text-center">
                <i class="{{ $fa }} fa-5x mb-3"></i><br>
                {{ $title }}
            </div>
		</a>
    </div>
    @if ($helpText ?? false)
    	<div class="card-footer text-center">
    		<small class="text-black-50" data-toggle="tooltip" data-placement="{{ $helpPlacement ?? 'bottom' }}" title="{{ $helpText }}"><i class="fa fa-info-circle"></i> Info</small>
    	</div>
    @endif
</div>
