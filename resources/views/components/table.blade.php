<table class="table table-sm table-hover">
    <thead class="thead-light">
    <tr class="{{ isset($row) && $row ? 'row' : '' }}">
        {{ $th }}
    </tr>
    </thead>
    <tbody>
    {{ $tbody }}
    </tbody>
</table>
