<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CourseReportItem extends Model
{
    protected $guarded = [];

    // relationships

    public function course_report()
    {
        return $this->belongsTo(CourseReport::class);
    }

    // aliases

    public function report()
    {
        return $this->course_report();
    }

    // accessors

    public function getStatusClassAttribute()
    {
        switch ($this->status) {
            case 'pending':
                return 'text-warning';
            case 'complete':
                return 'text-success';
            case 'error':
                return 'text-danger';
            default:
                return '';
        }
    }

    public function getStatusIconAttribute()
    {
        switch ($this->status) {
            case 'pending':
                return 'fas fa-pause';
            case 'complete':
                return 'fas fa-check';
            case 'error':
                return 'fas fa-exclamation-triangle';
            default:
                return '';
        }
    }

    public function getCourseUrlAttribute()
    {
        return config('cst.canvas_environment_url') . '/courses/' . $this->course_information->id;
    }

    public function getCourseStatusHtmlAttribute()
    {
        switch ($this->course_information->workflow_state) {
            case 'unpublished':
                return '<span class="text-danger">unpublished</span>';
            case 'available':
                return '<span class="text-success">available</span>';
            case 'completed':
                return '<span class="text-warning">completed</span>';
            case 'deleted':
                return '<span class="text-danger">deleted</span>';
            default:
                return '';
        }
    }

    public function getTeacherEmailsAttribute()
    {
        $teachers = $this->course_information->teachers;
        if (is_null($teachers) || count($teachers) == 0) {
            return [];
        }

        return array_map(function ($teacher) {
            return strtolower($teacher->login_id . '@uncg.edu');
        }, $teachers);
    }

    public function getEndsAtAttribute()
    {
        if (!isset($this->course_information->end_date) || is_null($this->course_information->end_date)) {
            return '';
        }

        return Carbon::parse($this->course_information->end_date)->toDateString();
    }

    // mutators

    public function getStatisticsAttribute($value)
    {
        return json_decode($value);
    }

    public function getCourseInformationAttribute($value)
    {
        return json_decode($value);
    }

    // other methods

    public function is_out_of_bounds($date)
    {
        $since = $this->report->options->since;
        $until = $this->report->options->until;
        if (is_null($since) && is_null($until)) {
            return false;
        }

        if (is_null($date) || empty($date)) {
            return true;
        }

        $date = Carbon::parse($date);

        return ($date->lt(Carbon::parse($since))) || ($date->gt(Carbon::parse($until)));
    }
}
