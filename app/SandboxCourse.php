<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SandboxCourse extends Model
{
    protected $casts = [
        'completed_at' => 'datetime'
    ];

    protected $guarded = [];

    // relationships

    public function sandbox_course_batch()
    {
        return $this->belongsTo(SandboxCourseBatch::class);
    }

    // aliases

    public function batch()
    {
        return $this->sandbox_course_batch();
    }

    // accessors

    public function getProcessedAttribute()
    {
        return !in_array($this->status, ['pending', 'in-progress']);
    }
}
