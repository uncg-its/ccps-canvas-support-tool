<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $guarded = [];

    // relationships

    public function child_accounts()
    {
        return $this->hasMany(Account::class, 'parent_account_id', 'canvas_id')->with(['child_accounts']);
    }

    public function parent_account()
    {
        return $this->belongsTo(Account::class, 'parent_account_id', 'canvas_id');
    }
}
