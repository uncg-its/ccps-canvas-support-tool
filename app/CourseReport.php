<?php

namespace App;

use App\CcpsCore\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CourseReport extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    protected $dates = [
        'created_at',
        'completed_at',
    ];

    // relationships

    public function requestor()
    {
        return $this->belongsTo(User::class, 'requested_by');
    }

    public function course_report_items()
    {
        return $this->hasMany(CourseReportItem::class);
    }

    // aliases

    public function items()
    {
        return $this->course_report_items();
    }

    // accessors

    public function getPendingItemsAttribute()
    {
        return $this->course_report_items()->where('status', '=', 'pending')->get();
    }

    public function getStatusHtmlAttribute()
    {
        if (is_null($this->completed_at)) {
            return '<span class="text-info"><i class="fas fa-pause"></i> pending</span>';
        }

        if ($this->items()->where('status', 'error')->get()->isNotEmpty()) {
            return '<span class="text-warning"><i class="fas fa-exclamation-triangle"></i> complete (with errors)</span>';
        }

        return '<span class="text-success"><i class="fas fa-check"></i> complete</span>';
    }

    public function getCsvNameAttribute()
    {
        $now = Carbon::now()->format('Ymd_His');

        return "course-activity-report-{$this->id}-{$now}.csv";
    }

    // mutators

    public function getOptionsAttribute($value)
    {
        return json_decode($value);
    }
}
