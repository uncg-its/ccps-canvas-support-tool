<?php

namespace App;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;

class SandboxCourseBatch extends Model
{
    protected $guarded = [];

    // realationships

    public function sandbox_courses()
    {
        return $this->hasMany(SandboxCourse::class);
    }

    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }

    // aliases

    public function courses()
    {
        return $this->sandbox_courses();
    }
}
