<?php

namespace App\Csthelpers;

use App\Account;

class Generatearraytreeforaccountlist
{
    public static function createSimpleAccountsArrayForSelectElement()
    {
        $accountList = Account::all()->keyBy('canvas_id');
        $rootAccount = $accountList->firstWhere('parent_account_id', null);

        $tree = $rootAccount->child_accounts->toArray();

        self::array_to_flat_array_with_depth($tree, $result);

        return $result;
    }

    public static function array_to_flat_array_with_depth($in, &$out, $level = '')
    {
        if (!$level) {
            $out = [];
        } // Make sure $out is an empty array at the beginning
        foreach ($in as $key => $item) { // Loop items
            $thisLevel = ($level) ? "$level." . ($key + 1) : ($key + 1); // Get this level as string
            // check to see if this ID has already been added

            $out[$thisLevel . '___' . $item['canvas_id']] = str_repeat('-', substr_count($thisLevel, '.')) . $item['name']; // Add this item to $out

            if (isset($item['child_accounts']) && is_array($item['child_accounts']) && count($item['child_accounts'])) {
                self::array_to_flat_array_with_depth($item['child_accounts'], $out, $thisLevel); // Recurse children of this item
            }
        }
    }
}
