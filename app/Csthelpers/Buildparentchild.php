<?php

namespace App\Csthelpers;

class Buildparentchild
{
    private $data = [];
    public $rendered;

    public function __construct(&$Input)
    {
        foreach ($Input as $Item) {
            $this->data['items'][$Item['canvas_id']] = $Item;
            $this->data['parents'][$Item['parent_account_id']][] = $Item['canvas_id'];
            if (!isset($this->top_level) || $this->top_level > $Item['parent_account_id']) {
                $this->top_level = $Item['parent_account_id'];
            }
        }
        return $this;
    }

    public function build($id)
    {
        $return[$id] = [];
        foreach ($this->data['parents'][$id] as $child) {
            $build = $this->data['items'][$child];
            if (isset($this->data['parents'][$child])) {
                $build['has_children'] = true;
                $build['children'] = $this->build($child);
            } else {
                $build['has_children'] = false;
            }
            $return[$id][] = $build;
        }

        return (array) $return[$id];
    }

    public function render()
    {
        if (!isset($this->rendered) || !is_array($this->rendered)) {
            $this->rendered = $this->build($this->top_level);
        }
        return $this->rendered;
    }
}
