<?php

namespace App;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;

class QuizLogReport extends Model
{
    protected $guarded = [];

    protected $casts = [
        'parameters' => 'json'
    ];

    // relationships

    public function created_by_user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function quiz_log_report_items()
    {
        return $this->hasMany(QuizLogReportItem::class);
    }

    public function quiz_log_report_files()
    {
        return $this->hasManyThrough(QuizLogReportFile::class, QuizLogReportItem::class);
    }

    // aliases

    public function items()
    {
        return $this->quiz_log_report_items();
    }

    public function files()
    {
        return $this->quiz_log_report_files();
    }
}
