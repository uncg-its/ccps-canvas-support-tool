<?php

namespace App\Listeners\CcpsCore;

use App\Events\CcpsCore\StaleLockFileDetected;
use Uncgits\Ccps\Listeners\NotificationSubscriber as BaseListener;

class NotificationSubscriber extends BaseListener
{
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\CcpsCore\CronjobFailed',
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );

        $events->listen(
            StaleLockFileDetected::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );


        parent::subscribe($events);
    }
}
