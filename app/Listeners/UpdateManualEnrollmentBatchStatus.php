<?php

namespace App\Listeners;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateManualEnrollmentBatchStatus implements ShouldQueue
{
    use Queueable, InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->onQueue('low');
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $batch = $event->manualEnrollment->batch;

        $pendingEnrollments = $batch->manual_enrollments()->where('status', 'queued')->count();
        if ($pendingEnrollments !== 0) {
            return;
        }

        $enrollmentsByStatus = \DB::table('manual_enrollments')
            ->select('status', \DB::raw('COUNT(id) as count'))
            ->groupBy('status')
            ->where('batch_id', $batch->id)
            ->get()
            ->keyBy('status');

        $batch->status = isset($enrollmentsByStatus['error']) ? 'error' : 'uploaded';
        $batch->save();

        \Log::channel('manual-enrollments')->info('Updated Batch status', [
            'category'  => 'manual-enrollments',
            'operation' => 'batch-status',
            'result'    => 'success',
            'data'      => [
                'batch_id' => $batch->id,
                'status'   => $batch->status,
            ]
        ]);

        return;
    }
}
