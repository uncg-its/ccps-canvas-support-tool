<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizLogReportItem extends Model
{
    protected $guarded = [];

    protected $casts = [
        'event_data'      => 'json',
        'submission_data' => 'json'
    ];

    // relationships

    public function quiz_log_report()
    {
        return $this->belongsTo(QuizLogReport::class);
    }

    public function quiz_log_report_files()
    {
        return $this->hasMany(QuizLogReportFile::class);
    }

    // aliases

    public function report()
    {
        return $this->quiz_log_report();
    }

    public function files()
    {
        return $this->quiz_log_report_files();
    }
}
