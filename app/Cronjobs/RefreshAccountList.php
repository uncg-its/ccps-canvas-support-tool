<?php

namespace App\Cronjobs;

use App\Account;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;

class RefreshAccountList extends Cronjob
{
    protected $schedule = '0 0 * * *'; // default schedule (overridable in database)
    protected $display_name = 'RefreshAccountList'; // default display name (overridable in database)
    protected $description = 'Refreshes the Account List from Canvas for use in the app'; // default description name (overridable in database)
    protected $tags = []; // default tags (overridable in database)

    protected function execute()
    {
        // Root account

        $rootAccountLookup = \CanvasApi::using('accounts')->getAccount(1);

        if ($rootAccountLookup->getStatus() !== 'success') {
            $message = 'Error lookup up root account: ' . $rootAccountLookup->getLastResult()['code'] . ' - ' . $rootAccountLookup->getLastResult()['reason'];
            \Log::channel('general')->warning($message);
            return new CronjobResult(false, $message);
        }

        $rootAccount = $rootAccountLookup->getContent();
        $rootAccount->canvas_id = $rootAccount->id;
        unset($rootAccount->id);
        Account::updateOrCreate(['canvas_id' => $rootAccount->canvas_id], (array) $rootAccount);

        // subaccounts

        $accountList = \CanvasApi::using('accounts')->addParameters(['recursive' => 'true'])->getSubaccounts(1);
        if ($rootAccountLookup->getStatus() !== 'success') {
            $message = 'Error lookup up subaccounts: ' . $accountList->getLastResult()['code'] . ' - ' . $accountList->getLastResult()['reason'];
            \Log::channel('general')->warning($message);
            return new CronjobResult(false, $message);
        }

        foreach ($accountList->getContent() as $account) {
            $account->canvas_id = $account->id;
            unset($account->id);
            Account::updateOrCreate(['canvas_id' => $account->canvas_id], (array) $account);
        }

        return new CronjobResult(true);
    }
}
