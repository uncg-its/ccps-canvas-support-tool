<?php

namespace App\Cronjobs;

use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;

class Ltigetenrollments extends Cronjob
{
    protected $schedule = '*/5 * * * *'; // default schedule (overridable in database)
    protected $display_name = 'LTI Inventory - Get Enrollments'; // default display name (overridable in database)
    protected $description = 'Get enrollments for all courses added to LTI Inventory, for tracking purposes.'; // default description name (overridable in database)

    protected function execute()
    {
        $lastEarliestTimestampRecord = \DB::table('lti_inventory')->orderBy('lastEnrollmentUpdate', 'asc')->first();

        if (!$lastEarliestTimestampRecord) {
            return new CronjobResult(true);
        }

        $enrollments = \CanvasApi::using('enrollments')->listCourseEnrollments('sis_course_id:' . $lastEarliestTimestampRecord->sis_course_id);

        $thisTime = time();

        if ($enrollments->getStatus() !== 'success') {
            return new CronjobResult(false, 'Canvas API error: ' . $enrollments->getLastResult()['code'] . ' - ' . $enrollments->getLastResult()['reason']);
        }

        // now get all the existing enrollments, so we can update the necessary rows, and add new ones where needed.
        $existingLocalEnrollments = \DB::table('view_lti_enrollment')->where('inventoryId', '=', $lastEarliestTimestampRecord->inventoryId)->get();

        $existingMap = [];
        $existingMapUserToEnrollmentId = [];
        foreach ($existingLocalEnrollments as $e) {
            $existingMap[$e->enrollmentId] = $e->user_id;
            $existingMapUserToEnrollmentId[$e->user_id] = $e->enrollmentId;
        }

        $newEnrollmentsMap = [];

        foreach ($enrollments->getContent() as $enrollmentRecord) {
            if (isset($enrollmentRecord->user->sis_user_id) && !empty($enrollmentRecord->user->sis_user_id)) {
                $userId = $enrollmentRecord->user->sis_user_id;
            } else {
                $userId = 'CANVAS_USER_ID:' . $enrollmentRecord->user->id . ' (' . $enrollmentRecord->user->name . ')';
            }

            $newEnrollmentsMap[] = $userId;

            // if the userId exists already in the map, then it's an update, otherwise it's an add

            if (in_array($userId, $existingMap)) {
                $updateData = [
                        'lastUpdated'   => $thisTime,
                        'status'        => 'active',
                    ];

                \DB::table('lti_enrollment')->where('enrollmentId', '=', $existingMapUserToEnrollmentId[$userId])->update($updateData);
            } else {
                $insertData = [
                        'inventoryId'   => $lastEarliestTimestampRecord->inventoryId,
                        'user_id'       => $userId,
                        'lastUpdated'   => $thisTime,
                        'status'        => 'active',

                    ];

                \DB::table('lti_enrollment')->insert($insertData);
            }
        }

        // now update any existing enrollments that are NOT in the new enrollments map, to mark them as inactive
        foreach ($existingMap as $enrollmentId => $user_id) {
            if (!in_array($user_id, $newEnrollmentsMap)) {
                $updateData = [
                        'lastUpdated'   => $thisTime,
                        'status'        => 'inactive',
                    ];

                \DB::table('lti_enrollment')->where('enrollmentId', '=', $enrollmentId)->update($updateData);
            }
        }

        $updateData = [
            'lastEnrollmentUpdate'   => $thisTime,
        ];

        \DB::table('lti_inventory')->where('inventoryId', '=', $lastEarliestTimestampRecord->inventoryId)->update($updateData);

        return new CronjobResult(true);
    }
}
