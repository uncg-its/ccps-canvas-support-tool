<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateManualEnrollmentBatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermission('manual-enrollments.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'destination_type' => 'required|in:section,course',
            'destination_id'   => 'required',
            'account'          => 'required',
            'role_id'          => 'required',
            'user_id_type'     => 'required|in:login_id,canvas_id,sis_user_id',
            'user_list'        => 'required',
        ];
    }
}
