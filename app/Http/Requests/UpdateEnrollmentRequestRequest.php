<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEnrollmentRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermission('enrollmentrequests.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sis_course_id'            => 'required',
            'enrolledPersonName'       => 'required',
            'enrolledPersonUsername'   => 'required',
            'enrolledPersonRoleId'     => 'required|numeric',
            'enrollmentReasonCategory' => 'required'
        ];
    }
}
