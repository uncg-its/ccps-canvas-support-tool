<?php

namespace App\Http\Requests;

use App\LtiReport;
use Carbon\Carbon;
use App\Jobs\CheckReportCompletion;
use Illuminate\Foundation\Http\FormRequest;

class CreateLtiReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'accountId'       => 'required|numeric',
            'include_deleted' => 'sometimes',
            'terms'           => 'sometimes',
        ];
    }

    public function persist()
    {
        $options = $this->has('include_deleted') ? ['parameters' => ['include_deleted' => 1]] : [];

        $result = \CanvasApi::using('AccountReports')
            ->addParameters($options)
            ->startReport($this->accountId, 'lti_report_csv');

        if ($result->getStatus() !== 'success') {
            flash('API error when creating report. Please contact an administrator.', 'danger');
            \Log::channel('lti-reports')->error('API error when creating lti_csv_report: ' . $result->getLastResult()['reason']);
            return false;
        }

        $reportDetails = $result->getContent();

        // filters
        $filters = [];
        if (isset($this->terms) && count($this->terms) > 0) {
            $filters['terms'] = $this->terms;
        }

        $report = LtiReport::create([
            'requestor_id'         => auth()->user()->id,
            'account_id'           => $this->accountId,
            'report_name'          => 'lti_report_csv',
            'canvas_report_id'     => $reportDetails->id,
            'canvas_report_status' => $reportDetails->status,
            'filters'              => json_encode($filters),
            'created_at'           => Carbon::parse($reportDetails->created_at)->toDateTimeString()
        ]);

        dispatch(new CheckReportCompletion($report))->delay(now()->addSeconds(30));

        flash('Report created (ID ' . $report->id . '). Status will be updated as the report finishes on the Canvas side.', 'success');

        return true;
    }
}
