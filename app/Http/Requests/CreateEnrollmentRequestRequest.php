<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Uncgits\CanvasApiLaravel\Rules\UserExistsInCanvas;

class CreateEnrollmentRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::user()->hasPermission('enrollmentrequests.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'requesterEmailAddress'          => 'required',
            'requesterAffiliation'           => 'required',
            'sis_course_id'                  => 'required',
            'activeCourse'                   => 'required',
            'enrolledPersonName'             => 'required',
            'enrolledPersonUsername'         => [
                'required',
                new UserExistsInCanvas('sis_login_id')
            ],
            'enrolledPersonAffiliation'      => 'required',
            'roleId'                         => 'required',
            'enrolledPersonFerpaAttestation' => 'required',
            'begins_at'                      => 'required|date',
            'ends_at'                        => 'nullable|date',
            'enrollmentReasonCategory'       => 'required',
            'enrollmentReason'               => 'required',
            'enrollmentNotes'                => 'nullable',
        ];
    }
}
