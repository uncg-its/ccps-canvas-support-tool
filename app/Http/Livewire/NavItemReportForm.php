<?php

namespace App\Http\Livewire;

use App\NavItemReport;
use Livewire\Component;
use App\NavItemReportItem;
use App\Jobs\ProcessNavItemReportItem;
use App\Csthelpers\Generatearraytreeforaccountlist;

class NavItemReportForm extends Component
{
    public $accountId = '';
    public $termId = '';

    public $error = '';

    public $accountList = [];
    public $termList = [];

    public function mount()
    {
        $accounts = Generatearraytreeforaccountlist::createSimpleAccountsArrayForSelectElement();
        $accountList = collect($accounts)->mapWithKeys(function ($name, $cstKey) {
            $accountId = explode('___', $cstKey)[1];
            return [$accountId => $name];
        })->toArray();

        $termResult = \CanvasApi::using('EnrollmentTerms')->listEnrollmentTerms(config('cst.key_account_ids.root'));

        $terms = [];
        foreach ($termResult->getContent() as $term) {
            $terms[$term->id] = $term->name;
        }

        $this->accountList = $accountList;
        $this->termList = $terms;
    }

    public function render()
    {
        return view('livewire.nav-item-report-form');
    }

    public function updating($field, $value)
    {
        $this->reset(['error']);
    }

    public function submit()
    {
        $this->validate([
            'accountId' => 'required|numeric',
            'termId'    => 'required|numeric',
        ]);

        // fetch published course list from API so that we can queue up items
        $courseListResult = \CanvasApi::using('accounts')
            ->addParameters([
                'published'          => true,
                'with_enrollments'   => true,
                'enrollment_term_id' => $this->termId,
            ])->listActiveCoursesInAccount($this->accountId);

        if ($courseListResult->getStatus() !== 'success') {
            $this->error = 'Error fetching course list from Canvas API. Report generation canceled.';
            return;
        }

        $report = NavItemReport::create([
            'created_by'     => \Auth::user()->id,
            'account_id'     => $this->accountId,
            'account_name'   => str_replace('-', '', $this->accountList[$this->accountId]),
            'term_id'        => $this->termId,
            'term_name'      => $this->termList[$this->termId],
            'status'         => 'pending',
        ]);

        if (count($courseListResult->getContent()) === 0) {
            $report->status = 'completed';
            $report->save();
            return;
        }

        foreach ($courseListResult->getContent() as $course) {
            $item = NavItemReportItem::create([
                'nav_item_report_id' => $report->id,
                'canvas_course_id'   => $course->id,
                'canvas_course_name' => $course->name,
                'canvas_course_code' => $course->course_code,
            ]);

            dispatch(new ProcessNavItemReportItem($item));
        }

        $this->emit('navItemReportCreated', $report);
    }
}
