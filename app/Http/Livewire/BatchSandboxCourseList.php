<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\SandboxCourseBatch;

class BatchSandboxCourseList extends Component
{
    public $batchId;

    public function mount($batchId)
    {
        $this->batchId = $batchId;
    }

    public function getBatchProperty()
    {
        return SandboxCourseBatch::findOrFail($this->batchId);
    }

    public function render()
    {
        return view('livewire.batch-sandbox-course-list');
    }
}
