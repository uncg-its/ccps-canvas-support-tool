<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\SandboxCourseBatch;
use App\Jobs\CreateSandboxCourse;

class BatchSandboxCourseCreate extends Component
{
    public $sandboxName;
    public $sandboxCode;
    public $usernameList;
    public $courseSearch;

    public $searchResults;
    public $searchError = false;
    public $submitError = false;

    public $usernames = [];

    public function mount()
    {
        //
    }

    public function render()
    {
        return view('livewire.batch-sandbox-course-create');
    }

    public function findUsers()
    {
        $usernames = \Str::of($this->usernameList)->explode("\n")->map(function ($username) {
            return (string) \Str::of($username)->trim()->replace('@uncg.edu', '');
        });
        $results = $usernames->filter(function ($username) {
            $userResult = \CanvasApi::using('users')->showUserDetails('sis_login_id:' . $username);
            return ($userResult->getStatus() === 'success');
        })->toArray();

        $this->searchResults = array_diff($results, $this->usernames);
    }

    public function searchRoster()
    {
        $this->reset(['searchError']);

        $studentUsers = \CanvasApi::using('enrollments')
            ->addParameters(['type' => [
                'StudentEnrollment'
            ]])->listCourseEnrollments($this->courseSearch);

        if ($studentUsers->getStatus() !== 'success') {
            $this->searchError = 'Error searching course roster';
            return;
        }

        $results = collect($studentUsers->getContent())->map(function ($user) {
            return strtolower($user->user->login_id);
        })->toArray();

        $this->searchResults = array_diff($results, $this->usernames);
    }

    public function addUser($key)
    {
        $this->usernames[] = $this->searchResults[$key];
        unset($this->searchResults[$key]);
    }

    public function removeUser($key)
    {
        unset($this->usernames[$key]);
    }

    public function removeAll()
    {
        $this->usernames = [];
    }

    public function addAll()
    {
        foreach ($this->searchResults as $key => $result) {
            $this->addUser($key);
        }
    }

    public function submit()
    {
        $this->reset(['submitError']);

        try {
            \DB::beginTransaction();
            // create the batch
            $batch = SandboxCourseBatch::create([
                'created_by_id' => \Auth::id(),
                'course_name'   => trim($this->sandboxName),
                'course_code'   => trim($this->sandboxCode),
                'status'        => 'pending',
            ]);

            // iterate and create the course entries
            foreach ($this->usernames as $username) {
                $course = $batch->courses()->create([
                    'instructor_username' => strtolower($username),
                    'status'              => 'pending',
                ]);
                // upon creation of an entry, dispatch a job to do the rest on the queue
                dispatch(new CreateSandboxCourse($course));
            }
            \DB::commit();

            flash('Sandbox Course Batch queued successfully! Please check back for updates on the batch status.')->success();
            return redirect()->route('batch-sandbox-courses.show', $batch);
        } catch (\Throwable $th) {
            \DB::rollBack();
            $this->submitError = 'Error creating the batch. Please contact an administrator.';
            report($th);
        }
    }
}
