<?php

namespace App\Http\Livewire;

use Livewire\Component;

class NavItemReportSearch extends Component
{
    public $report;
    public $items;

    public $search;
    public $results = [];
    public $searchComplete = false;

    public function mount($report)
    {
        $this->report = $report;
        $this->items = $report->items;
    }

    public function render()
    {
        return view('livewire.nav-item-report-search');
    }

    public function updating($field, $value)
    {
        $this->searchComplete = false;
    }

    public function performSearch()
    {
        $this->results = $this->items->filter(function ($item) {
            return collect($item->nav_items)->filter(function ($navItem) {
                return strpos(strtolower($navItem['label']), strtolower(trim($this->search))) !== false && !isset($navItem['hidden']);
            })->isNotEmpty();
        });

        $this->searchComplete = true;
    }
}
