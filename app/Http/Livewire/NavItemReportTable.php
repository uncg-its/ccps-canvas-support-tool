<?php

namespace App\Http\Livewire;

use App\NavItemReport;
use Livewire\Component;

class NavItemReportTable extends Component
{
    public $reports;

    protected $listeners = ['navItemReportCreated'];

    public function mount()
    {
        $this->reports = NavItemReport::orderByDesc('created_at')->get();
    }

    public function render()
    {
        return view('livewire.nav-item-report-table');
    }

    public function navItemReportCreated($report)
    {
        $report = NavItemReport::findOrFail($report['id']);

        $this->reports->prepend($report); // will always be first in line
    }
}
