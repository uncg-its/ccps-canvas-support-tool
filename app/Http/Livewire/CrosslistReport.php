<?php

namespace App\Http\Livewire;

use App\Crosslist;
use Livewire\Component;
use Livewire\WithPagination;

class CrosslistReport extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.crosslist-report')->with([
            'crosslists' => Crosslist::orderByDesc('created_at')->paginate(25)
        ]);
    }
}
