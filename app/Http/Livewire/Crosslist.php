<?php

namespace App\Http\Livewire;

use App\Account;
use App\Crosslist as CrosslistModel;
use Livewire\Component;

class Crosslist extends Component
{
    public $accounts;
    public $childAccounts;
    public $termsList;
    public $parentCourse = null;
    public $childSections = [];

    public $courseSearch = '';
    public $childSectionSearch = '';
    public $account = '';
    public $childAccount = '';

    public $parentTerm = '';

    public $courseList = [];
    public $sectionList = [];
    public $newChildSections = [];

    public $courseNotFound = false;
    public $sectionsNotFound = false;

    public function mount()
    {
        $this->reset();

        $accountsInCoursesSubaccount = Account::where('parent_account_id', config('cst.key_account_ids.courses'))
            ->select('canvas_id', 'name')
            ->get()
            ->mapWithKeys(function ($account) {
                return [$account->canvas_id => $account->name];
            })->toArray();

        $this->accounts = $accountsInCoursesSubaccount;
        $this->childAccounts = $accountsInCoursesSubaccount;
        $termResult = \CanvasApi::using('EnrollmentTerms')->listEnrollmentTerms(config('cst.key_account_ids.root'));

        $this->termsList = collect($termResult->getContent())
            ->sortByDesc('end_at')
            ->mapWithKeys(fn ($term) => ['term_' . $term->id => $term->name])
            ->toArray();
    }

    public function render()
    {
        return view('livewire.crosslist');
    }

    public function searchCourseInCanvas()
    {
        $this->reset(['courseList', 'courseNotFound', 'parentCourse']);

        if (strlen($this->courseSearch) < 3) {
            return;
        }

        $account = explode('___', $this->account);
        $account = array_pop($account);

        $parentTerm = str_replace('term_', '', $this->parentTerm);

        $courseSearchResult = \CanvasApi::using('accounts')
            ->addParameters([
                'search_term'        => $this->courseSearch,
                'enrollment_term_id' => $parentTerm
            ])
            ->listActiveCoursesInAccount($account);

        if ($courseSearchResult->getStatus() !== 'success') {
            $this->courseNotFound = true;
            return;
        }

        $this->courseList = collect($courseSearchResult->getContent())
            ->reject(function ($result) {
                return is_null($result->sis_course_id);
            })->mapWithKeys(function ($courseResult) {
                return [$courseResult->id => $courseResult->name];
            })->toArray();

        if (empty($this->courseList)) {
            $this->courseNotFound = true;
        }
    }

    public function searchSectionsInCanvas()
    {
        $this->sectionsNotFound = false;

        if (empty($this->childSectionSearch)) {
            $this->reset(['sectionList']);
        }

        if (strlen($this->childSectionSearch) < 3) {
            return;
        }

        $account = explode('___', $this->childAccount);
        $account = array_pop($account);

        $parentTerm = str_replace('term_', '', $this->parentTerm);

        $courseSearchResult = \CanvasApi::using('accounts')
            ->addParameters([
                'search_term'        => $this->childSectionSearch,
                'enrollment_term_id' => $parentTerm,
            ])
            ->listActiveCoursesInAccount($account);

        if ($courseSearchResult->getStatus() !== 'success' || empty($courseSearchResult->getContent())) {
            $this->reset(['sectionList']);
            $this->sectionsNotFound = true;
            return;
        }

        // remove any sections already in the parent course
        $courseSectionsResult = \CanvasApi::using('sections')
            ->listCourseSections($this->parentCourse);

        $existingCourseSectionIds = collect($courseSectionsResult->getContent())
            ->pluck('sis_section_id')
            ->reject(function ($result) {
                return is_null($result);
            })->toArray();

        $this->sectionList = collect($courseSearchResult->getContent())
            ->reject(function ($courseResult) {
                return is_null($courseResult->sis_course_id);
            })->mapWithKeys(function ($courseResult) {
                $sectionId = str_replace('COURSE', 'SECTION', $courseResult->sis_course_id);
                return [$sectionId => $courseResult->name];
            })->reject(function ($section, $sectionId) use ($existingCourseSectionIds) {
                return in_array($sectionId, $existingCourseSectionIds) || isset($this->newChildSections[$sectionId]);
            })->toArray();

        $this->newChildSections = array_diff_key($this->newChildSections, array_flip($existingCourseSectionIds));
    }

    public function termChanged()
    {
        $this->reset(['courseList']);
    }

    public function addSection($id, $name)
    {
        $this->newChildSections[$id] = $name;
    }

    public function removeSection($id)
    {
        unset($this->newChildSections[$id]);
    }

    public function updatedParentCourse($value)
    {
        $this->searchSectionsInCanvas();
    }

    public function performCrossList()
    {
        $parentCourse = \CanvasApi::using('courses')->getCourse($this->parentCourse);
        if ($parentCourse->getStatus() !== 'success') {
            flash('Error: parent course not found')->error();
            return redirect()->route('crosslist.index');
        }
        $parentCourse = $parentCourse->getContent();

        $failures = [];
        foreach ($this->newChildSections as $childId => $childName) {
            $result = \CanvasApi::using('sections')->crossListSection('sis_section_id:' . $childId, $this->parentCourse);
            if ($result->getStatus() !== 'success') {
                $failures[] = $childId;
            }
        }

        $crosslist = CrosslistModel::create([
            'performed_by_id'       => \Auth::user()->id,
            'parent_course_sis_id'  => $parentCourse->sis_course_id,
            'child_section_sis_ids' => implode(',', array_keys($this->newChildSections)),
            'parent_course_meta'    => $parentCourse,
        ]);

        if (empty($failures)) {
            flash('Bulk cross list completed successfully!')->success();

            \Log::channel('crosslist')->info('Bulk crosslist completed', [
                'category'  => 'crosslist',
                'operation' => 'bulk',
                'result'    => 'success',
                'data'      => [
                    'crosslist' => $crosslist
                ]
            ]);
        } else {
            flash('Error cross listing section(s): ' . implode(', ', $failures))->error();
            $crosslist->errors = $failures;
            $crosslist->save();

            \Log::channel('crosslist')->info('Bulk crosslist failed', [
                'category'  => 'crosslist',
                'operation' => 'bulk',
                'result'    => 'failure',
                'data'      => [
                    'crosslist' => $crosslist
                ]
            ]);
        }

        return redirect()->route('crosslist.index');
    }
}
