<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;
use App\ManualEnrollmentBatch;
use App\Models\ManualEnrollment;
use App\Jobs\ProcessManualEnrollment;
use Uncgits\Ccps\Support\CcpsPaginator;
use App\Csthelpers\Generatearraytreeforaccountlist;
use App\Http\Requests\CreateManualEnrollmentBatchRequest;
use App\Http\Requests\UpdateManualEnrollmentBatchRequest;

class ManualEnrollmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:manual-enrollments.create')->only(['create', 'store']);
        $this->middleware('permission:manual-enrollments.read')->only(['index', 'show']);
        $this->middleware('permission:manual-enrollments.update')->only(['edit', 'update', 'retry']);
    }

    public function listRolesSelectElement(Request $request)
    {
        $courseId = $request->destination_id;

        // the course has the account ID on it... so if we are given a section the first step is getting the course ID
        if ($request->destination_type === 'section') {
            $sectionLookup = \CanvasApi::using('sections')
                ->getSectionInformation($request->destination_id);
            $courseId = $sectionLookup->getContent()->course_id;
        }

        // get account from parent course
        $courseLookup = \CanvasApi::using('courses')
            ->getCourse($courseId);
        $course = $courseLookup->getContent();
        $account = Account::where('canvas_id', $course->account_id)->firstOrFail();

        $roleLookup = \CanvasApi::using('roles')
            ->addParameters(['show_inherited' => true])
            ->listRoles($account->canvas_id);


        $options = view('manual-enrollments.role-select-element')->with([
            'roles' => $roleLookup->getContent()
        ])->render();

        $data = [
            'options' => $options,
            'account' => [
                'id'   => $account->canvas_id,
                'name' => $account->name,
            ]
        ];

        return response()->json($data, 200);
    }

    public function index(Request $request)
    {
        // TODO: search and filter

        $batches = ManualEnrollmentBatch::orderByDesc('created_at')->get();

        return view('manual-enrollments.index')->with([
            'batches' => new CcpsPaginator($batches)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $accountList = Generatearraytreeforaccountlist::createSimpleAccountsArrayForSelectElement();

        return view('manual-enrollments.create')->with([
            'accountList' => $accountList
        ]);
    }

    public function store(CreateManualEnrollmentBatchRequest $request)
    {
        try {
            \DB::beginTransaction();

            // create batch
            $batch = ManualEnrollmentBatch::create([
                'created_by'       => \Auth::user()->id,
                'destination_type' => $request->destination_type,
                'destination_id'   => $request->destination_id,
                'status'           => 'pending',
                'notes'            => $request->notes
            ]);

            // create enrollment records
            $accountId = explode(' - ', $request->account)[0];

            $usersToEnroll = collect(preg_split('/[\r\n,;]+/', strtolower($request->user_list), -1, PREG_SPLIT_NO_EMPTY));
            $roleLookup = \CanvasApi::using('roles')->getRole($accountId, $request->role_id);

            if ($roleLookup->getStatus() !== 'success') {
                throw new \Exception('Error during API call to look up role information');
            }

            $roleToEnrollWith = $roleLookup->getContent();

            $usersToEnroll->each(function ($user) use ($batch, $request, $accountId, $roleToEnrollWith) {
                ManualEnrollment::create([
                    'batch_id'              => $batch->id,
                    'canvas_account_id'     => $accountId,
                    'enrolled_user_id'      => strtoupper($user),
                    'enrolled_user_id_type' => $request->user_id_type,
                    'role_type'             => $roleToEnrollWith->base_role_type,
                    'role_id'               => $roleToEnrollWith->id,
                    'status'                => 'pending',
                ]);
            });

            \DB::commit();
            flash('Manual Enrollment Batch queued successfully.')->success();
            \Log::channel('manual-enrollments')->info('Manual Enrollment Batch created', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'success',
                'data'      => [
                    'batch_id'         => $batch->id,
                    'enrollment_count' => $usersToEnroll->count(),
                    'current_user'     => \Auth::user(),
                ]
            ]);
        } catch (\Exception $e) {
            flash('Could not create Manual Enrollment Batch. Please contact an administrator.')->error();
            \Log::channel('manual-enrollments')->error('Error during Manual Enrollment Batch creation', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'error',
                'data'      => [
                    'message'      => $e->getMessage(),
                    'current_user' => \Auth::user(),
                    'request'      => $request->except('_token'),
                ]
            ]);
            \DB::rollBack();
        }

        return redirect()->route('manual-enrollments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show(ManualEnrollmentBatch $manualEnrollmentBatch)
    {
        $manualEnrollmentBatch->load('manual_enrollments');
        $manualEnrollments = new CcpsPaginator($manualEnrollmentBatch->manual_enrollments);

        return view('manual-enrollments.show')->with([
            'batch'       => $manualEnrollmentBatch,
            'enrollments' => $manualEnrollments
        ]);
    }

    public function update(UpdateManualEnrollmentBatchRequest $request, ManualEnrollmentBatch $manualEnrollmentBatch)
    {
        if ($request->operation === 'approve') {
            try {
                \DB::beginTransaction();

                $manualEnrollmentBatch->status = 'approved';
                $manualEnrollmentBatch->save();

                $manualEnrollmentBatch->manual_enrollments->each(function ($enrollment) {
                    dispatch(new ProcessManualEnrollment($enrollment));
                    $enrollment->status = 'queued';
                    $enrollment->save();
                });

                \DB::commit();
                \Log::channel('manual-enrollments')->info('Batch approved', [
                    'category'  => 'manual-enrollments',
                    'operation' => 'approve',
                    'result'    => 'success',
                    'data'      => [
                        'batch_id'    => $manualEnrollmentBatch->id,
                        'approved_by' => \Auth::user()->id
                    ]
                ]);
                flash('Enrollment Batch approved.')->success();
            } catch (\Throwable $th) {
                flash('Error while approving Enrollment Batch.')->error();
                \Log::channel('manual-enrollments')->error('Error approving Batch', [
                    'category'  => 'manual-enrollments',
                    'operation' => 'approve',
                    'result'    => 'error',
                    'data'      => [
                        'batch_id'    => $manualEnrollmentBatch->id,
                        'approved_by' => \Auth::user()->id,
                        'message'     => $th->getMessage()
                    ]
                ]);

                \DB::rollBack();
            }
        }

        if ($request->operation === 'cancel') {
            try {
                \DB::beginTransaction();

                $manualEnrollmentBatch->status = 'canceled';
                $manualEnrollmentBatch->save();

                $manualEnrollmentBatch->manual_enrollments->each(function ($enrollment) {
                    $enrollment->status = 'canceled';
                    $enrollment->save();
                });

                \DB::commit();
                flash('Enrollment Batch canceled.')->success();

                \Log::channel('manual-enrollments')->info('Batch canceled', [
                    'category'  => 'manual-enrollments',
                    'operation' => 'cancel',
                    'result'    => 'success',
                    'data'      => [
                        'batch_id'    => $manualEnrollmentBatch->id,
                        'canceled_by' => \Auth::user()->id
                    ]
                ]);
            } catch (\Throwable $th) {
                flash('Error while canceling Enrollment Batch.')->error();
                \Log::channel('manual-enrollments')->error('Error canceling Batch', [
                    'category'  => 'manual-enrollments',
                    'operation' => 'cancel',
                    'result'    => 'error',
                    'data'      => [
                        'batch_id'    => $manualEnrollmentBatch->id,
                        'canceled_by' => \Auth::user()->id,
                        'message'     => $th->getMessage()
                    ]
                ]);

                \DB::rollBack();
            }
        }

        return redirect()->back();
    }

    public function retry(ManualEnrollment $manualEnrollment)
    {
        dispatch(new ProcessManualEnrollment($manualEnrollment));

        $manualEnrollment->status = 'queued';
        $manualEnrollment->save();

        flash('Manual Enrollment re-queued successfully')->success();

        return redirect()->back();
    }

    public function retryAll(ManualEnrollmentBatch $manualEnrollmentBatch)
    {
        $toRetry = $manualEnrollmentBatch->manual_enrollments()
            ->errored()
            ->get();

        $toRetry->each(function ($enrollment) {
            dispatch(new ProcessManualEnrollment($enrollment));
            $enrollment->status = 'queued';
            $enrollment->save();
        });

        flash('Re-queued ' . $toRetry->count() . ' errored enrollment(s).')->success();

        return redirect()->back();
    }
}
