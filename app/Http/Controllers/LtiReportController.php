<?php

namespace App\Http\Controllers;

use App\LtiReport;
use Carbon\Carbon;
use League\Csv\Writer;
use Illuminate\Http\Request;
use Uncgits\Ccps\Support\CcpsPaginator;
use App\Http\Requests\CreateLtiReportRequest;
use App\Csthelpers\Generatearraytreeforaccountlist;

class LtiReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:lti-reports.view')->only(['index', 'show', 'export']);
        $this->middleware('permission:lti-reports.create')->only(['create', 'store']);
    }

    public function index()
    {
        $reports = LtiReport::orderBy('created_at', 'desc')->paginate();
        return view('canvas.lti-reports.index')->with([
            'reports' => $reports
        ]);
    }

    public function create()
    {
        // to get available types, we need accounts.
        $accountList = Generatearraytreeforaccountlist::createSimpleAccountsArrayForSelectElement();

        // terms
        $termResult = \CanvasApi::using('EnrollmentTerms')->listEnrollmentTerms(1);
        $terms = [];
        foreach ($termResult->getContent() as $term) {
            $terms[$term->id] = $term->name;
        }

        return view('canvas.lti-reports.create')->with([
            'accountList' => $accountList,
            'terms'       => $terms
        ]);
    }

    public function store(CreateLtiReportRequest $request)
    {
        $request->persist();
        return redirect()->route('canvas.lti-reports.index');
    }

    public function show(Request $request, LtiReport $report)
    {
        $items = new CcpsPaginator($report->lti_report_items, null, $request->page ?? 1);
        return view('canvas.lti-reports.show')->with(compact(['report', 'items']));
    }

    public function getReportTypesSelectElementForAccount(Request $request)
    {
        $result = \CanvasApi::using('account-reports')->listAvailableReports(1);

        if ($result->getStatus() !== 'success') {
            \Log::warning('ERROR trying to retrieve reports for account ID ' . $request->accountId . ': '
                . $result->getLastResult()['reason']);
            return false;
        }

        $body = collect($result->getContent());

        $reports = $body->mapWithKeys(function ($item) {
            return [$item->report => $item->title];
        });

        return view('canvas.lti-reports.reports-select-element', compact('reports'))->render();
    }

    public function export(LtiReport $report)
    {
        ini_set('max_execution_time', 300); // 5 minutes

        \Log::channel('lti-reports')->info('Report ID ' . $report->id . ' exported to CSV by user ' . auth()->user()->email);

        $csv = Writer::createFromString('');

        $headers = [
            'CST Report Item ID',
            'Tool Name',
            'Course ID',
            'Course Name',
            'Course Status',
            'Term',
            'Course Type',
            'Department',
            'Unit',
            '# Teachers',
            'Teacher Usernames',
            '# TAs',
            'TA Usernames',
            '# Students',
        ];

        $items = $report->items->map(function ($item) {
            return [
                $item->id,
                $item->tool_name,
                $item->courseId,
                $item->courseName,
                $item->courseStatus,
                $item->term,
                $item->courseAffiliation['type'],
                $item->courseAffiliation['department'],
                $item->courseAffiliation['unit'],
                $item->teachersCount,
                $item->teachersUsernames->implode(','),
                $item->tasCount,
                $item->tasUsernames->implode(','),
                $item->studentsCount,
            ];
        });

        $csv->insertOne($headers);
        $csv->insertAll($items);

        // serve it up
        $name = Carbon::now()->format('Ymd_his') . '_lti_report.csv';
        $csv->output($name);
    }
}
