<?php

namespace App\Http\Controllers;

use App\QuizLogReport;
use App\QuizLogReportItem;
use Illuminate\Http\Request;
use App\Jobs\FetchQuizLogForReportItem;
use Uncgits\Ccps\Support\CcpsPaginator;

class QuizLogReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:quiz-log-reports.view'])->only(['index', 'show', 'download']);
        $this->middleware(['permission:quiz-log-reports.create'])->only(['create', 'store']);
    }

    public function index()
    {
        $reports = QuizLogReport::latest()->paginate();
        return view('canvas.quiz-log-reports.index')->with([
            'reports' => $reports
        ]);
    }

    public function show(QuizLogReport $report, Request $request)
    {
        return view('canvas.quiz-log-reports.show')->with([
            'report' => $report,
            'items'  => new CcpsPaginator($report->items)
        ]);
    }

    public function create()
    {
        return view('canvas.quiz-log-reports.create')->with([]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'course_id' => 'required',
            'users'     => 'required',
            'quizzes'   => 'required',
            'attempts'  => 'required',
        ]);

        // fetch submissions.
        $submissionsByQuiz = collect($request->quizzes)->mapWithKeys(function ($quiz) use ($request) {
            $submissionsResult = \CanvasApi::using('QuizSubmissions')
                ->getAllQuizSubmissions($request->course_id, $quiz);
            if ($submissionsResult->getStatus() !== 'success') {
                throw new \Exception('Error retrieving submissions for quiz ' . $quiz);
            }

            $submissions = collect($submissionsResult->getContent());

            // filter for queried users
            $submissions = $submissions->filter(function ($quiz) use ($request) {
                return in_array($quiz->user_id, $request->users);
            });

            // filter submissions for latest, if asked to.
            if ($request->attempts === 'latest') {
                $submissions = $submissions->filter(function ($quiz) {
                    return $quiz->attempts_left === 0;
                });
            }

            return [$quiz => $submissions];
        });

        // turn on quiz log auditing for the course
        $auditingResult = \CanvasApi::using('FeatureFlags')
            ->addParameters(['state' => 'on'])
            ->setCourseFeatureFlag($request->course_id, 'quiz_log_auditing');

        if ($auditingResult->getStatus() !== 'success') {
            throw new \Exception('Error enabling quiz log auditing for course!');
        }

        try {
            \DB::beginTransaction();
            // create new report
            $report = QuizLogReport::create([
                'created_by' => \Auth::user()->id,
                'parameters' => $request->except('_token'),
                'status'     => 'pending',
            ]);

            // queue a job to pull the reports.
            $submissionsByQuiz->flatten()->each(function ($submission) use ($request, $report) {
                $item = QuizLogReportItem::create([
                    'quiz_log_report_id' => $report->id,
                    'course_id'          => $request->course_id,
                    'user_id'            => $submission->user_id,
                    'quiz_id'            => $submission->quiz_id,
                    'submission_id'      => $submission->id,
                    'submission_data'    => $submission,
                ]);
                dispatch(new FetchQuizLogForReportItem($item));
            });

            flash('Report created, and items queued successfully')->success();

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollBack();
            flash('Report was NOT created due to an error.')->error();
        }

        return redirect()->route('canvas.quiz-log-reports.index');
    }

    public function loadCourseData(Request $request)
    {
        if (!$request->has('course_id')) {
            return response()->json(['errors' => ['Course ID is missing']], 400);
        }

        $enrollmentResult = \CanvasApi::using('enrollments')
            ->addParameters([
                'type' => ['StudentEnrollment']
            ])->listCourseEnrollments($request->course_id);

        if ($enrollmentResult->getLastResult()['code'] === 404) {
            return response()->json(['errors' => ['Specified Course ID not found in Canvas']], 404);
        }

        $quizResult = \CanvasApi::using('quizzes')
            ->listQuizzesInCourse($request->course_id);

        return response()->json([
            'data' => [
                'enrollments' => $enrollmentResult->getContent(),
                'quizzes'     => $quizResult->getContent(),
            ]
        ], 200);
    }

    public function download(QuizLogReport $report)
    {
        // check status
        if ($report->status !== 'completed') {
            flash('Cannot download a report that is not successfully completed.')->warning();
            return redirect()->back();
        }

        // zip up the report's files
        $zipFile = now()->format('Ymd_His') . '_quiz_log_report_' . $report->id . '.zip';
        $zip = new \ZipArchive;
        $zip->open($zipFile, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        foreach ($report->files as $file) {
            $zip->addFile(storage_path('app/reports/quiz-logs/' . $file->filename), $file->filename);
        }

        $zip->close();

        // present as download
        return response()->download($zipFile)->deleteFileAfterSend(true);
    }
}
