<?php

namespace App\Http\Controllers;

class CanvasController extends Controller
{
    public function __construct()
    {
        // Basic ACL middleware using Laratrust
        $this->middleware('role:canvas.admin|servicedesk.member|lti.reader|itc');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }
}
