<?php

namespace App\Http\Controllers\Canvas\Live;

use Carbon\Carbon;
use App\LiveLookup;
use League\Csv\Writer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use App\Http\Requests\CourseSearchRequest;
use App\Http\Requests\ShowLiveCourseRequest;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:canvas.courses.view')->only(['index', 'show']);
    }

    public function index(CourseSearchRequest $request)
    {
        // get terms
        $termsResult = \CanvasApi::using('EnrollmentTerms')
            ->listEnrollmentTerms(1);
        if ($termsResult->getStatus() !== 'success') {
            flash('API error during term fetch. Contact an administrator.')->error();
            return redirect()->back();
        }
        $termsById = collect($termsResult->getContent())
            ->sortByDesc('id')
            ->mapWithKeys(function ($term) {
                return [$term->id => $term->name];
            });

        // do the course search
        if ($request->filled('enrollment_term_id') && $request->filled('search_term')) {
            $courseResults = \CanvasApi::using('accounts')
                ->addParameters([
                    'search_term'        => $request->search_term,
                    'enrollment_term_id' => $request->enrollment_term_id,
                ])->listActiveCoursesInAccount(1);

            if ($courseResults->getStatus() !== 'success') {
                flash('API error during search. Contact an administrator.')->error();
                return redirect()->back();
            }

            return view('live.courses.index')->with([
                'results'          => new CcpsPaginator(collect($courseResults->getContent())),
                'terms'            => $termsById,
                'enrollmentTermId' => $request->enrollment_term_id,
                'searchTerm'       => $request->search_term,
            ]);
        }

        return view('live.courses.index')->with([
            'terms' => $termsById
        ]);
    }

    public function show(ShowLiveCourseRequest $request)
    {
        $id = $request->course_id;
        if ($request->id_type === 'sis_course_id') {
            $id = 'sis_course_id:' . $id;
        }

        $result = \CanvasApi::using('courses')
            ->addParameters(['include' => [
                'account',
                'total_students',
                'teachers',
                'term',
                'storage_quota_used_mb',
                'sections'
            ]])->getCourse($id);

        $lookupRecord = LiveLookup::create([
            'type'       => 'course',
            'item_id'    => $id,
            'user_id'    => \Auth::user()->id,
            'status'     => $result->getStatus(),
            'created_at' => now()
        ]);

        if ($result->getStatus() !== 'success') {
            flash('Course ID ' . $id . ' was not found with ID type ' . $request->id_type)->warning();
            return redirect()->route('live.courses.index');
        }

        $course = $result->getContent();

        // get enrollments
        $enrollmentsResult = \CanvasApi::using('enrollments')
            ->listCourseEnrollments($course->id);

        $enrollments = collect($enrollmentsResult->getContent());

        if ($enrollmentsResult->getStatus() !== 'success') {
            flash('Error looking up enrollments for course');
            $enrollments = collect();
        }

        // get tabs for course
        $tabsResult = \CanvasApi::using('tabs')
            ->listTabsForCourse($course->id);

        $tabs = collect($tabsResult->getContent());

        if ($tabsResult->getStatus() !== 'success') {
            flash('Error looking up navigation tabs for course');
            $tabs = collect();
        }


        return view('live.courses.show')->with([
            'course'         => $course,
            'allEnrollments' => $enrollments,
            'enrollments'    => new CcpsPaginator($enrollments),
            'tabs'           => $tabs,
        ]);
    }

    public function downloadEnrollments(Request $request)
    {
        $filePath = \Storage::disk('temp')->path('enrollments-' . $request->courseId . '.csv');

        $headers = ['name', 'sis_user_id', 'sis_login_id', 'role', 'state', 'created_at_local', 'last_activity_local'];

        $writer = Writer::createFromPath($filePath, 'w+');
        $writer->insertOne($headers);

        // get enrollments
        $enrollmentsResult = \CanvasApi::using('enrollments')
            ->listCourseEnrollments($request->courseId);

        $enrollments = collect($enrollmentsResult->getContent());

        if ($enrollmentsResult->getStatus() !== 'success') {
            flash('Error looking up enrollments for course');
            return redirect()->back();
        }

        $e = $enrollments->map(function ($enrollment) {
            return [
                $enrollment->user->sortable_name,
                $enrollment->user->sis_user_id,
                $enrollment->user->login_id,
                $enrollment->role,
                $enrollment->enrollment_state,
                Carbon::parse($enrollment->created_at)->setTimezone(config('app.timezone'))->format('Y-m-d H:i:s'),
                Carbon::parse($enrollment->last_activity_at)->setTimezone(config('app.timezone'))->format('Y-m-d H:i:s')
            ];
        });

        $writer->insertAll($e);

        return response()->download($filePath)->deleteFileAfterSend(true);
    }
}
