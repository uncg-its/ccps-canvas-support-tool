<?php

namespace App\Http\Controllers\Canvas\Live;

use App\LiveLookup;
use League\Csv\Writer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use App\Http\Requests\ShowLiveEnrollmentRequest;

class EnrollmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:canvas.users.view');
    }

    public function index(ShowLiveEnrollmentRequest $request)
    {
        $id = 'sis_login_id:' . $request->user_id;

        // actually going to do this by Courses, since the Enrollments API doesn't give course info.
        $coursesResult = \CanvasApi::using('courses')
            ->addParameters([
                'state' => [
                    'unpublished',
                    'available',
                    'completed'
                ]
            ])
            ->listCoursesForUser('sis_login_id:' . $request->user_id);
        // $enrollmentsResult = \CanvasApi::using('enrollments')
        //     ->addParameters([
        //         'state' => [
        //             'current_and_future',
        //         ]
        //     ])->listUserEnrollments('sis_login_id:' . $request->user_id);

        // dd($enrollmentsResult->getContent());

        $lookupRecord = LiveLookup::create([
            'type'       => 'enrollments',
            'item_id'    => $id,
            'user_id'    => \Auth::user()->id,
            'status'     => $coursesResult->getStatus(),
            'created_at' => now()
        ]);

        if ($coursesResult->getStatus() !== 'success') {
            flash('There was a problem getting live enrollment data for user ' . $id . '. Please contact an administrator.')->error();
            \Log::channel('live')->error('Error looking up live enrollments', [
                'category'  => 'live',
                'operation' => 'enrollments',
                'result'    => 'error',
                'data'      => [
                    'requesting_user' => \Auth::user(),
                    'request'         => $request->except('_token'),
                    'reason'          => $coursesResult->getLastResult()['reason']
                ]
            ]);
            return redirect()->route('live.users.show', ['id_type' => 'sis_login_id','user_id' => $id]);
        }

        return view('live.enrollments.index')->with([
            'courses'  => new CcpsPaginator(collect($coursesResult->getContent())),
            'userId'   => $request->user_id,
            'userName' => $request->user_name,
        ]);
    }

    public function download(Request $request)
    {
        $filePath = \Storage::disk('temp')->path('user-enrollments-' . $request->userId . '-' . now()->format('YmdHis') . '.csv');

        $headers = ['course_canvas_id', 'course_name', 'course_sis_id', 'course_availability', 'enrollment_type', 'enrollment_status', 'course_start', 'course_end'];

        $writer = Writer::createFromPath($filePath, 'w+');
        $writer->insertOne($headers);

        // get enrollments by way of courses
        $enrollmentsResult = \CanvasApi::using('courses')
            ->addParameters([
                'state' => [
                    'unpublished',
                    'available',
                    'completed'
                ]
            ])
            ->listCoursesForUser('sis_login_id:' . $request->userId);

        $enrollments = collect($enrollmentsResult->getContent());

        if ($enrollmentsResult->getStatus() !== 'success') {
            flash('Error looking up enrollments for course');
            return redirect()->back();
        }

        $e = $enrollments->map(function ($course) {
            return [
                $course->id,
                $course->name,
                $course->sis_course_id ?? '',
                $course->workflow_state,
                ucwords($course->enrollments[0]->type),
                $course->enrollments[0]->enrollment_state,
                $course->start_at,
                $course->end_at,
            ];
        });

        $writer->insertAll($e);

        return response()->download($filePath)->deleteFileAfterSend(true);
    }
}
