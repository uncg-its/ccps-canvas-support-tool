<?php

namespace App\Http\Controllers\Canvas\Live;

use App\Http\Controllers\Controller;
use App\Csthelpers\Generatearraytreeforaccountlist;

class AccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:canvas.accounts.view');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $accountList = Generatearraytreeforaccountlist::createSimpleAccountsArrayForSelectElement();
        return view('live.accounts.index', compact('accountList'));
    }
}
