<?php

namespace App\Http\Controllers\Canvas\Live;

use App\LiveLookup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ShowLiveUserRequest;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:canvas.users.view')->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('live.users.index');
    }

    public function show(ShowLiveUserRequest $request)
    {
        $id = $request->user_id;
        if ($request->id_type === 'sis_login_id') {
            $id = 'sis_login_id:' . $id;
        }

        $result = \CanvasApi::using('users')
            ->getUser($id);

        $lookupRecord = LiveLookup::create([
            'type'       => 'user',
            'item_id'    => $id,
            'user_id'    => \Auth::user()->id,
            'status'     => $result->getStatus(),
            'created_at' => now()
        ]);


        if ($result->getStatus() !== 'success') {
            flash('User ID ' . $id . ' was not found with ID type ' . $request->id_type)->warning();
            return redirect()->route('live.users.index');
        }

        $user = $result->getContent();

        $profileResult = \CanvasApi::using('users')
            ->getUserProfile($id);

        $user->profile = $profileResult->getContent();

        return view('live.users.show')->with([
            'canvasUser' => $user,
        ]);
    }

    public function course(Request $request)
    {
        $result = \CanvasApi::using('courses')->getCourse($request->course_id);

        if ($result->getStatus() !== 'success') {
            flash('Course ID ' . $request->course_id . ' Not Found. Try Again.', 'warning');
            return redirect(route('canvas_users.index'));
        }

        $course = $result->getContent();

        $userResult = \CanvasApi::using('users')->getUser($request->user_id);

        if ($userResult->getStatus() !== 'success') {
            flash('User ID ' . $request->user_id . ' Not Found. Try Again.', 'warning');
            return redirect(route('canvas_users.index'));
        }

        $this_user = $userResult->getContent();
        return view('canvas.users.course', compact('result', 'course', 'this_user'));
    }

    public function courses(Request $request)
    {
        $userId = $request->user_id;
        if ($request->user_id_type !== 'canvas_user_id') {
            $userId = $request->user_id_type . ':' . $userId;
        }

        $userResult = \CanvasApi::using('users')
            ->getUserProfile($userId);

        if ($userResult->getStatus() !== 'success') {
            flash('User ID ' . $request->user_id . ' Not Found. Try Again.', 'warning');
            return redirect(route('canvas_users.index'));
        }

        $coursesResult = \CanvasApi::using('courses')->listCoursesForUser($userId);

        if ($coursesResult->getStatus() !== 'success') {
            flash('User ID ' . $request->user_id . ' Not Found. Try Again.', 'warning');
            return redirect(route('canvas_users.index'));
        }

        $this_user = $userResult->getContent();
        $courseList = $coursesResult->getContent();

        // TODO: enrollment term ID doesn't work here.

        return view('canvas.users.courses', compact('this_user', 'request', 'courseList'));
    }
}
