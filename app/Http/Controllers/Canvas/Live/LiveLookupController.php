<?php

namespace App\Http\Controllers\Canvas\Live;

use App\LiveLookup;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;

class LiveLookupController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:live-lookups.view');
    }

    public function index()
    {
        $lookups = LiveLookup::orderByDesc('created_at')->with('user')->get();

        return view('live.lookups.index')->with([
            'lookups' => new CcpsPaginator($lookups),
        ]);
    }
}
