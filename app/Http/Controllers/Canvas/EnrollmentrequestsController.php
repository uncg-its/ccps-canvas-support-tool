<?php

namespace App\Http\Controllers\Canvas;

use Carbon\Carbon;
use Illuminate\Config;
use Illuminate\Http\Request;
use App\Models\EnrollmentRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEnrollmentRequestRequest;
use App\Mail\EnrollmentRequestApproved;
use App\Mail\EnrollmentRequestRejected;
use Uncgits\Ccps\Support\CcpsPaginator;
use App\Mail\EnrollmentRequestSubmitted;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\UpdateEnrollmentRequestRequest;

class EnrollmentrequestsController extends Controller
{
    public function __construct()
    {
        // Basic ACL middleware using Laratrust
        $this->middleware('permission:enrollmentrequests.create')->only(['create', 'store', 'listRolesSelectElement', 'getAffiliationArray', 'getReasonCategoryArray']);
        $this->middleware('permission:enrollmentrequests.read')->only(['index', 'show']);
        $this->middleware('permission:enrollmentrequests.delete')->only(['destroy']);
        $this->middleware('permission:enrollmentrequests.admin')->only(['all','reject', 'createInCanvas']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $searchFieldArray = [
                'sis_course_id',
                'requesterEmailAddress',
                'requesterAffiliation',
                'enrolledPersonName',
                'enrolledPersonUsername',
                'enrollmentNotes',
                'enrolledPersonRole',
                'enrollmentReasonCategory',
                'enrollmentReason'
            ];

            $items = EnrollmentRequest::where('requesterEmailAddress', '=', auth()->user()->email)
                ->where(function ($query) use ($searchFieldArray, $keyword) {
                    foreach ($searchFieldArray as $field) {
                        $query->orWhere($field, 'LIKE', '%'.$keyword.'%');
                    }
                })
                ->sortable(['created_at' => 'desc'])
                ->get();
        } else {
            $items = EnrollmentRequest::where('requesterEmailAddress', auth()->user()->email)
                ->sortable(['created_at' => 'desc'])
                ->get();
        }

        return view('canvas.enrollmentrequests.index', [
            'items' => new CcpsPaginator($items)
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function all(Request $request)
    {
        try {
            $this->authorize('all', EnrollmentRequest::class);
        } catch (AuthorizationException $e) {
            flash('You are not permitted to view all Enrollment Requests.', 'warning');
            return redirect(route('enrollmentrequests.index'));
        }

        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $searchFieldArray = [
                'sis_course_id',
                'requesterEmailAddress',
                'requesterAffiliation',
                'enrolledPersonName',
                'enrolledPersonUsername',
                'enrollmentNotes',
                'enrolledPersonRole',
                'enrollmentReasonCategory',
                'enrollmentReason'
            ];

            $items = EnrollmentRequest::where(function ($query) use ($searchFieldArray, $keyword) {
                foreach ($searchFieldArray as $field) {
                    $query->orWhere($field, 'LIKE', '%'.$keyword.'%');
                }
            })
                ->sortable(['created_at' => 'desc'])
                ->get();
        } else {
            $items = EnrollmentRequest::sortable(['created_at' => 'desc'])->get();
        }

        return view('canvas.enrollmentrequests.all', [
            'items' => new CcpsPaginator($items)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show(Request $request)
    {
        $requestId = $request->requestId;

        $item = EnrollmentRequest::findOrFail($requestId);

        // if this is not an admin or the requester / enrolled person, then don't allow them to view it
        try {
            $this->authorize('view', $item);
        } catch (AuthorizationException $e) {
            flash('You are not permitted to view this Enrollment Request. You must be the requester, the enrolled person, or an administrator', 'warning');
            return redirect(route('enrollmentrequests.index'));
        }

        $affiliationMap = $this->getAffiliationArray();
        $reasonMap = $this->getReasonCategoryArray();

        return view('canvas.enrollmentrequests.show', compact('item', 'affiliationMap', 'reasonMap'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        //dd(__LINE__, $accountList);

        $affiliationArray = $this->getAffiliationArray();

        $reasonCategoryArray = $this->getReasonCategoryArray();

        $requesterEmail = auth()->user()->email;

        return view('canvas.enrollmentrequests.create')->with([
            'affiliationArray'    => $affiliationArray,
            'reasonCategoryArray' => $reasonCategoryArray,
            'requesterEmail'      => $requesterEmail
        ]);
    }

    public function listRolesSelectElement(Request $request)
    {
        $inline = $request->get('inline', 'false') == 'true';
        $excluded = explode(',', $request->get('exclude', ''));
        // need to get account ID for the course specified
        $thisCourse = \CanvasApi::using('courses')->getCourse('sis_course_id:' . $request->sis_course_id);
        $result = \CanvasApi::using('roles')->listRoles($thisCourse->getContent()->account_id)->getContent();

        $selected = $request->get('selected', null);

        return view('canvas.enrollmentrequests.role-select-element', compact('result', 'inline', 'excluded', 'selected'))->render();
    }

    public function store(CreateEnrollmentRequestRequest $request)
    {
        // get the type for the selected role
        $thisCourse = \CanvasApi::using('courses')->getCourse('sis_course_id:' . $request->sis_course_id);
        $roles = \CanvasApi::using('roles')->listRoles($thisCourse->getContent()->account_id)->getContent();

        $thisRole = collect($roles)->firstWhere('id', $request->roleId);

        $insertArray = [
            'requesterEmailAddress'             => auth()->user()->email,
            'requesterAffiliation'              => $request->requesterAffiliation,
            'sis_course_id'                     => $request->sis_course_id,
            'activeCourse'                      => $request->activeCourse,
            'enrolledPersonName'                => $request->enrolledPersonName,
            'enrolledPersonUsername'            => strtolower($request->enrolledPersonUsername),
            'enrolledPersonAffiliation'         => $request->enrolledPersonAffiliation,
            'enrolledPersonRole'                => $thisRole->base_role_type,
            'enrolledPersonRoleId'              => $thisRole->id,
            'enrolledPersonFerpaAttestation'    => $request->enrolledPersonFerpaAttestation,
            'begins_at'                         => Carbon::parse($request->begins_at),
            'ends_at'                           => empty($request->ends_at) ? null : Carbon::parse($request->ends_at),
            'enrollmentReasonCategory'          => $request->enrollmentReasonCategory,
            'enrollmentReason'                  => $request->enrollmentReason,
            'enrollmentNotes'                   => $request->enrollmentNotes,
            'courseMetadata'                    => json_encode($thisCourse->getContent()),
        ];

        \Log::channel('enrollmentrequests')->info('Enrollment Request Data JSON Encoded. INSERT ARRAY: ' . json_encode($insertArray));

        $thisRequest = EnrollmentRequest::create($insertArray);

        \Mail::to(auth()->user()->email)
            ->bcc(config('app-enrollmentrequests.enrollment_request_notification_email'))
            ->send(new EnrollmentRequestSubmitted($thisRequest));

        \Log::channel('enrollmentrequests')->info('Store Enrollment Request Performed by: ' . auth()->user()->email);

        flash('Enrollment Request created successfully', 'success');

        return redirect(route('enrollmentrequests.index'));
    }

    private function getAffiliationArray()
    {
        return [
            'BE'    => 'Bryan School of Business and Economics',
            'AS'    => 'College of Arts and Sciences',
            'VP'    => 'College of Visual and Performing Arts',
            'IT'    => 'Information Technology Services',
            'ED'    => 'School of Education',
            'HH'    => 'School of Health and Human Sciences',
            'NU'    => 'School of Nursing',
            'OO'    => 'Other',
        ];
    }

    private function getReasonCategoryArray()
    {
        return [
            'Accreditation-Related Work'                        => 'Accreditation-Related Work',
            'Faculty Collaboration'                             => 'Faculty Collaboration',
            'ITC faculty Assistance'                            => 'ITC faculty Assistance',
            'New Teacher of Previously Taught Course'           => 'New Teacher of Previously Taught Course',
            'Peer Review of Active Course'                      => 'Peer Review of Active Course',
            'Peer Review of Previous/Non-Active Course'         => 'Peer Review of Previous/Non-Active Course',
            'Replacement / Substitute for Instructor of Record' => 'Replacement / Substitute for Instructor of Record',
            'Request by Dept Head / Dean / Provost / VC'        => 'Request by Dept Head / Dean / Provost / VC',
            'Team-Teaching / Co-Teaching'                       => 'Team-Teaching / Co-Teaching',
            'Other'                                             => 'Other (please explain below)',
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $thisRecord = EnrollmentRequest::findOrFail($request->requestId);

        EnrollmentRequest::destroy($request->requestId);

        \Log::stack(['general', 'enrollmentrequests'])->info('Record Delete Performed by: ' . auth()->user()->email . '. ID: ' . $request->requestId . ', Data Deleted: ' . json_encode($thisRecord));

        flash('Enrollment Request deleted!', 'info');

        return redirect(route('enrollmentrequests.index'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function reject(Request $request)
    {
//        $this->validate($request, [
        //			'title' => 'required|max:10'
        //		]);

        $enrollmentRequest = EnrollmentRequest::findOrFail($request->requestId);

        $enrollmentRequest->rejected_at = now();
        $enrollmentRequest->rejector_id = auth()->user()->id;
        $enrollmentRequest->save();

        \Mail::to($enrollmentRequest->requesterEmailAddress)
            // ->cc($enrollmentRequest->enrolledPersonUsername . '@' . config('app-enrollmentrequests.enrollment_request_email_domain'))
            ->bcc(config('app-enrollmentrequests.enrollment_request_notification_email'))
            ->send(new EnrollmentRequestRejected($enrollmentRequest));


        \Log::stack(['general', 'enrollmentrequests'])->info('Record Rejection Performed by: ' . auth()->user()->email . '. ID: ' . $request->requestId . ', Request Data: ' . json_encode($enrollmentRequest));

        flash('Enrollment Request rejected', 'success');

        return redirect(route('enrollmentrequests.show', ['requestId' => $request->requestId]));
    }

    public function createInCanvas(Request $request)
    {
        $enrollmentRequest = EnrollmentRequest::findOrFail($request->requestId);

        $thisCourseApiResult = \CanvasApi::using('courses')->getCourse('sis_course_id:' . $enrollmentRequest->sis_course_id);

        if ($thisCourseApiResult->getStatus() !== 'success') {
            flash('Course ID ' . $enrollmentRequest->sis_course_id . ' Not Found. Try Again.', 'warning');

            return redirect(route('enrollmentrequests.show', ['requestId' => $enrollmentRequest->requestId]));
        }

        $thisCourse = $thisCourseApiResult->getContent();

        // most of these are probably for old courses, so let's change dates so that we know we can make the enrollment
        $changeCourseDatesResult = \CanvasApi::using('courses')
            ->addParameters([
                'course' => [
                    'start_at'                             => now()->subDays(1)->format('Y-m-d\TH:i\Z'),
                    'end_at'                               => now()->addDays(1)->format('Y-m-d\TH:i\Z'),
                    'restrict_enrollments_to_course_dates' => true
                ]
            ])->updateCourse($thisCourse->id);

        // do the creation in canvas

        $options = ['enrollment' => [
            'user_id'                   => 'sis_login_id:' . $enrollmentRequest->enrolledPersonUsername,
            'type'                      => $enrollmentRequest->enrolledPersonRole,
            'role_id'                   => $enrollmentRequest->enrolledPersonRoleId,
            'enrollment_state'          => 'active',
        ]];

        $result = \CanvasApi::using('enrollments')
            ->addParameters($options)
            ->enrollUserInCourse($thisCourse->id);

        if ($result->getStatus() === 'success') {
            // everything is fine. Update the record as successful
            $enrollmentRequest->approved_at = now();
            $enrollmentRequest->approver_id = auth()->user()->id;
            $enrollmentRequest->completed_at = now();
            $enrollmentRequest->enrolledPersonMetadata = json_encode($result->getContent());

            $enrollmentRequest->save();

            \Log::stack(['general', 'enrollmentrequests'])->info('Record Approval and Creation Performed by: ' . auth()->user()->email . '. ID: ' . $request->requestId . ', Request Data: ' . json_encode($enrollmentRequest));

            flash('Enrollment Request approved & created in Canvas!', 'success');

            \Mail::to($enrollmentRequest->requesterEmailAddress)
                ->cc($enrollmentRequest->enrolledPersonUsername . '@' . config('app-enrollmentrequests.enrollment_request_email_domain'))
                ->bcc(config('app-enrollmentrequests.enrollment_request_notification_email'))
                ->send(new EnrollmentRequestApproved($enrollmentRequest));
        } else {
            $enrollmentRequest->save();

            \Log::stack(['general', 'enrollmentrequests'])->info('FAILED: Record Approval and Creation Performed by: ' . auth()->user()->email . '. ID: ' . $request->requestId . ', API Result Data: ' . json_encode($result));

            flash('Failure while approving & creating enrollment request. Check log for reason.', 'danger');
        }

        // return to original dates and settings

        $resetCourseDateResult = \CanvasApi::using('courses')
            ->addParameters([
                'course' => [
                    'start_at'                             => $thisCourse->start_at ?? '',
                    'end_at'                               => $thisCourse->end_at ?? '',
                    'restrict_enrollments_to_course_dates' => $thisCourse->restrict_enrollments_to_course_dates
                ]
            ])->updateCourse($thisCourse->id);

        return redirect(route('enrollmentrequests.show', ['requestId' => $request->requestId]));
    }

    public function edit(EnrollmentRequest $enrollmentRequest)
    {
        return view('canvas.enrollmentrequests.edit')->with([
            'enrollmentRequest'   => $enrollmentRequest,
            'reasonCategoryArray' => $this->getReasonCategoryArray()

        ]);
    }

    public function update(UpdateEnrollmentRequestRequest $request, EnrollmentRequest $enrollmentRequest)
    {
        try {
            $thisCourse = \CanvasApi::using('courses')->getCourse('sis_course_id:' . $request->sis_course_id);
            $roles = \CanvasApi::using('roles')->listRoles($thisCourse->getContent()->account_id)->getContent();

            $thisRole = collect($roles)->firstWhere('id', $request->enrolledPersonRoleId);

            $updateArray = [
                'sis_course_id'                     => $request->sis_course_id,
                'enrolledPersonName'                => $request->enrolledPersonName,
                'enrolledPersonUsername'            => strtolower($request->enrolledPersonUsername),
                'enrolledPersonRole'                => $thisRole->base_role_type,
                'enrolledPersonRoleId'              => $thisRole->id,
                'enrollmentReasonCategory'          => $request->enrollmentReasonCategory,
                'enrollmentReason'                  => $request->enrollmentReason,
                'courseMetadata'                    => json_encode($thisCourse->getContent()),
            ];

            \Log::stack(['general', 'enrollmentrequests'])->info('Enrollment Request Data JSON Encoded. Update array: ' . json_encode($updateArray));
            $enrollmentRequest = $enrollmentRequest->update($updateArray);

            flash('Enrollment request updated successfully.')->success();
        } catch (\Exception $e) {
            \Log::channel('enrollmentrequests')->error('Error updating request. ' . $e->getMessage());
            flash('There was a problem updating the request. Please contact an administrator.')->error();
        }

        return redirect()->route('enrollmentrequests.index');
    }
}
