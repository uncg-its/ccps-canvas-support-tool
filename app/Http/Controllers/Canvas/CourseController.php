<?php

namespace App\Http\Controllers\Canvas;

use App\Account;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateCourseRequest;
use App\Csthelpers\Generatearraytreeforaccountlist;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:canvas.courses.create')->only([
            'create',
            'store',
            'removeSisId',
            'listRolesSelectElement'
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $accountList = Generatearraytreeforaccountlist::createSimpleAccountsArrayForSelectElement();

        $termResult = \CanvasApi::using('EnrollmentTerms')->listEnrollmentTerms(1);

        $termList = [];
        foreach ($termResult->getContent() as $term) {
            $termList[$term->id] = $term->name;
        }

        return view('courses.create', compact('accountList', 'termList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $request->validate([
            'course_name'   => 'required',
            'course_code'   => 'required',
            'start_at'      => 'nullable|date',
            'end_at'        => 'nullable|date',
            'term_id'       => 'required|numeric',
            'account_id'    => 'required',
            'role_id'       => 'nullable|numeric',
            'user_id_type'  => 'nullable',
            'paste_list'    => 'nullable'
        ]);

        $accountId = explode('___', $request->account_id)[1];

        $options = [
            'account_id' => $accountId,
            'course'     => [
                'name'        => $request->course_name,
                'course_code' => $request->course_code,
                'term_id'     => $request->term_id,
            ],
        ];

        if (isset($request->start_at)) {
            $options['course']['start_at'] = Carbon::parse($request->start_at)->toIso8601String();
        }

        if (isset($request->end_at)) {
            $options['course']['end_at'] = Carbon::parse($request->end_at)->toIso8601String();
        }

        $courseApiResult = \CanvasApi::using('courses')
            ->addParameters($options)
            ->createCourse($accountId);

        \Log::channel('courses')->info('Course created', [
            'category'  => 'course-create',
            'operation' => 'create-course',
            'result'    => 'success',
            'data'      => [
                'course'       => $courseApiResult->getContent(),
                'current_user' => \Auth::user()
            ]
        ]);

        flash('Course created successfully. ID in Canvas: ' . $courseApiResult->getContent()->id)->success();

        // now enroll the initial users

        // get the type for the selected role
        $roleResult = \CanvasApi::using('roles')
            ->getRole($accountId, $request->role_id);

        $userArray = preg_split('/[\r\n,;]+/', strtolower($request->paste_list), -1, PREG_SPLIT_NO_EMPTY);

        // make upper case because that's how canvas stores it
        $userArray = array_map('strtoupper', $userArray);
        $userArray = array_map('trim', $userArray);

        // process them
        foreach ($userArray as $user) {
            $userIdPrefix = '';

            if ($request->user_id_type == 'login_id') {
                $userIdPrefix = 'sis_login_id:';
            } elseif ($request->user_id_type != 'canvas_user_id') {
                $userIdPrefix = $request->user_id_type . ':';
            }

            $options = [
                'enrollment' => [
                    'user_id'          => $userIdPrefix . $user,
                    'type'             => $roleResult->getContent()->base_role_type,
                    'role_id'          => $roleResult->getContent()->id,
                    'enrollment_state' => 'active',
                ]
            ];

            $enrollmentResult = \CanvasApi::using('enrollments')
                ->addParameters($options)
                ->enrollUserInCourse($courseApiResult->getContent()->id);

            if ($enrollmentResult->getStatus() === 'success') {
                // everything is fine. Update the record as successful

                flash($user . ' enrolled successfully')->success();
                \Log::channel('courses')->info('User enrolled into created course', [
                    'category'  => 'course-create',
                    'operation' => 'enroll-user',
                    'result'    => 'success',
                    'data'      => [
                        'user'   => $user,
                        'course' => $courseApiResult->getContent()
                    ]
                ]);
            } else {
                flash($user . ' NOT enrolled successfully')->warning();
                \Log::channel('courses')->error('User not enrolled into created course', [
                    'category'  => 'course-create',
                    'operation' => 'enroll-user',
                    'result'    => 'error',
                    'data'      => [
                        'user'   => $user,
                        'course' => $courseApiResult->getContent(),
                        'reason' => $enrollmentResult->getLastResult()['reason']
                    ]
                ]);
            }
        }

        return redirect()->route('live.courses.show', [
            'id_type'   => 'canvas_id',
            'course_id' => $courseApiResult->getContent()->id
        ]);
    }

    /**
     * Update an existing resource
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function removeSisId(Request $request)
    {
//        $this->validate($request, [
        //          'title' => 'required|max:10'
        //      ]);
        $requestData = $request->all();

        //dd($requestData);

        $options = [
            'course' => [
                'sis_course_id' => '',
            ],
        ];

        //dd($options);

        $courseApiResult = \CanvasApi::using('courses')
            ->addParameters($options)
            ->updateCourse($requestData['course_id']);

        Log::stack([
            'general',
            'courses'
        ])->info('Canvas Course ID "' . $requestData['course_id'] . '"" updated by ' . auth()->user()->email . '. SIS ID Removed');

        //dd($apiResult);

        flash('Course Updated Successfully. SIS ID Removed');

        return redirect(route('canvas_courses.show', ['course_id' => $requestData['course_id']]));
    }

    /*
     * List all roles for account but just for a select element purpose
     *
     */
    public function listRolesSelectElement(Request $request)
    {
        $id = explode('___', $request->accountId)[1];

        $tree = [];
        $accounts = Account::all()->keyBy('canvas_id');

        do {
            if (!isset($accounts[$id])) {
                break;
            }
            $tree[] = $accounts[$id];
            $id = $accounts[$id]->parent_account_id;
        } while (!is_null($id));

        $tree = collect(array_reverse($tree));

        // which parent account do we need roles for?
        if ($tree->contains('canvas_id', config('cst.lti_reports.key_account_ids.courses'))) {
            $parentAccount = config('cst.lti_reports.key_account_ids.courses');
        } elseif ($tree->contains('canvas_id', config('cst.lti_reports.key_account_ids.noncourses'))) {
            $parentAccount = config('cst.lti_reports.key_account_ids.noncourses');
        } elseif ($tree->contains('canvas_id', config('cst.lti_reports.key_account_ids.sandbox'))) {
            $parentAccount = config('cst.lti_reports.key_account_ids.sandbox');
        } else {
            $parentAccount = $id; // not sure if this is right but it'll do for now
        }

        $result = \CanvasApi::using('roles')->listRoles($parentAccount);
        $result = $result->getContent();

        return view('courses.role-select-element', compact('result'))->render();
    }

    public function update(UpdateCourseRequest $request)
    {
        $operation = $request->get('operation', null);

        if (is_null($operation)) {
            flash('Could not determine operation to perform on course.')->warning();
            return redirect()->back();
        }

        if ($operation === 'remove_sis_course_id') {
            $options = [
                'course' => [
                    'sis_course_id' => '',
                ],
            ];

            $courseApiResult = \CanvasApi::using('courses')
                ->addParameters($options)
                ->updateCourse($request->course_id);

            if ($courseApiResult->getStatus() !== 'success') {
                flash('API error during the course update operation.')->error();
                \Log::channel('courses')->error('API error', [
                    'category'  => 'courses',
                    'operation' => 'remove_sid_id',
                    'result'    => 'error',
                    'data'      => [
                        'api_result' => $courseApiResult,
                        'request'    => $request->except('_token')
                    ]
                ]);
                return redirect()->route('live.courses.show', ['id_type' => 'canvas_id', 'course_id' => $request->course_id]);
            }

            flash('Successfully removed SIS ID from course.')->success();
            \Log::channel('courses')->info('Removed SIS ID', [
                'category'  => 'courses',
                'operation' => 'remove_sis_id',
                'result'    => 'success',
                'data'      => [
                    'request' => $request->except('_token')
                ]
            ]);
            return redirect()->route('live.courses.show', ['id_type' => 'canvas_id', 'course_id' => $request->course_id]);
        }
    }
}
