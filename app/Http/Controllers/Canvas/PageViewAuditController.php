<?php

namespace App\Http\Controllers\Canvas;

use App\Csthelpers\Parseuseragent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class PageViewAuditController extends Controller
{
    protected $userIdOptions = [
        'sis_login_id' => 'SIS Login ID',
        'canvas_id'    => 'Canvas ID'
    ];

    protected $limitOptions = [
        '300'  => '300 (default)',
        '1000' => '1000',
        '2000' => '2000',
        '5000' => '5000',
        '9999' => '9999',
    ];

    public function __construct()
    {
        // Basic ACL middleware using Laratrust
        $this->middleware('permission:page-views.read')->only(['index', 'results']);
    }

    public function index()
    {
        return view('canvas.page-view-audits.index')->with([
            'userIdOptions' => $this->userIdOptions,
            'limitOptions'  => $this->limitOptions,
        ]);
    }

    public function results(Request $request)
    {
        $request->validate([
            'user_id_type' => ['required', Rule::in(array_keys($this->userIdOptions))],
            'user_id'      => 'required',
            'result_limit' => 'required|numeric',
            'start_date'   => 'nullable|date',
            'end_date'     => 'nullable|date',
        ]);

        Log::channel('page-views')->debug('New request by user ' . auth()->user()->email . ': ' . json_encode($request->except('_token')));

        $parameters = [
            'per_page' => $request->result_limit,
        ];

        if (!is_null($request->start_date)) {
            $parameters['start_time'] = Carbon::parse($request->start_date)->setTimezone(config('app.timezone'))->format('Y-m-d');
        }

        if (!is_null($request->end_date)) {
            $parameters['end_time'] = Carbon::parse($request->end_date)->setTimezone(config('app.timezone'))->format('Y-m-d');
        }

        $userId = $request->user_id;
        if ($request->user_id_type === 'sis_login_id') {
            $userId = 'sis_login_id:' . $userId;
        }

        $result = \CanvasApi::using('users')
            ->setPerPage(100)
            ->setMaxResults($request->result_limit)
            ->addParameters($parameters)
            ->listUserPageViews($userId);

        $pageViews = $result->getContent();

        if ($result->getStatus() !== 'success') {
            flash('Error: unable to locate specified user. Be sure you are using the correct User ID type.')->error();
            return redirect()->back();
        }

        foreach ($pageViews as $view) {
            $view->created_at_parsed = Carbon::parse($view->created_at)->setTimezone(config('app.timezone'))->format('m/d/Y h:i:s a');
            $view->url_parsed = str_replace(config('cst.canvas_environment_url'), '', $view->url);

            $browser = Parseuseragent::parse($view->user_agent);
            $view->user_agent_parsed = $browser['browser'];
            if (!empty($browser['version'])) {
                $view->user_agent_parsed .= ' ' . $browser['version'];
            }
            if (!empty($browser['platform'])) {
                $view->user_agent_parsed .= ' - ' . $browser['platform'];
            }

            if (empty($view->user_agent_parsed)) {
                $view->user_agent_parsed = 'unknown';
            }

            $view->match = (!empty($request->search_term) && strpos($view->url, $request->search_term) !== false);
        }

        Log::channel('page-views')->debug('Found ' . count($pageViews) . ' ' . \Str::plural(
            'record',
                count($pageViews)
        ) . ' for query.');

        return view('canvas.page-view-audits.results')->with([
            'results'     => $pageViews,
            'requestData' => $request->only([
                'user_id_type',
                'user_id',
                'result_limit',
                'start_date',
                'end_date',
                'search_term'
            ])
        ]);
    }
}
