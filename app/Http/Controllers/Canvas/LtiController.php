<?php

namespace App\Http\Controllers\Canvas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Lti\Tools as LtiTools;
use App\Models\Lti\Inventory as LtiInventory;
use App\Models\Lti\View\Enrollment as ViewEnrollment;

class LtiController extends Controller
{
    public function __construct()
    {
        // Basic ACL middleware using Laratrust
        $this->middleware('permission:lti.read')->only(['index', 'inventory', 'enrollment']);
        $this->middleware('permission:lti.create')->only(['toolcreate', 'toolstore', 'inventorycreate', 'inventorystore']);
        $this->middleware('permission:lti.update')->only(['tooledit', 'toolupdate','inventoryedit', 'inventoryupdate']);
        $this->middleware('permission:lti.delete')->only(['tooldelete', 'tooldestroy','inventorydelete', 'inventorydestroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $tools = LtiTools::where('ltiName', 'LIKE', "%$keyword%")
                ->orWhere('ltiDescription', 'LIKE', "%$keyword%")
                ->orWhere('lastUpdated', 'LIKE', "%$keyword%")
                ->orderBy('ltiName', 'asc')
                ->paginate(config('ccps.paginator_per_page'));
        } else {
            $tools = LtiTools::orderBy('ltiName', 'asc')->paginate(config('ccps.paginator_per_page'));
        }

        return view('canvas.lti.index', compact('tools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function toolcreate()
    {
        return view('canvas.lti.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function toolstore(Request $request)
    {
//        $this->validate($request, [
        //			'title' => 'required|max:10'
        //		]);
        $requestData = $request->all();

        //dd($requestData);

        //dd($thisRole, $userArray, $insertArray);

        $insertArray = [
            'ltiName'        => $requestData['ltiName'],
            'ltiDescription' => $requestData['ltiDescription'],
            'lastUpdated'    => 0,
        ];

        \DB::table('lti_tools')->insert($insertArray);

        \Log::stack(['general', 'lti'])->info('Store Tool Performed by: ' . auth()->user()->email . ', JSON Encoded Object Added: ' . json_encode($insertArray));

        flash('Tool Added Successfully: ' . $requestData['ltiName'], 'info');

        return redirect(route('lti.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function tooledit(Request $request)
    {
        $tool = LtiTools::findOrFail($request->toolId);

        return view('canvas.lti.edit', compact('tool'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function toolupdate(Request $request)
    {
        $requestData = $request->all();

        $tool = LtiTools::findOrFail($request->toolId);
        $tool->update($requestData);

        \Log::stack(['general', 'lti'])->info('Record Update Performed by: ' . auth()->user()->email . '. ID: ' . $request->toolId . ', Request Data: ' . json_encode($requestData));

        flash('LTI Tool updated!', 'info');

        return redirect(route('lti.index'));
    }

    /**
     * Show the form for deleting the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function tooldelete(Request $request)
    {
        $item = LtiTools::findOrFail($request->toolId);

        //dd($item);

        return view('canvas.lti.delete', compact('item'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function tooldestroy(Request $request)
    {
        $thisRecord = LtiTools::findOrFail($request->toolId);

        LtiTools::destroy($request->toolId);

        \Log::stack(['general', 'lti'])->info('Record Delete Performed by: ' . auth()->user()->email . '. ID: ' . $request->toolId . ', Data Deleted: ' . json_encode($thisRecord));

        flash('LTI Tool deleted!', 'info');

        return redirect(route('lti.index'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function enrollment(Request $request)
    {
        $requestData = $request->all();

        $tools = LtiTools::orderBy('ltiName', 'asc')->get();

        // get summary info
        $summaryInfo = [
            'active'      => 0,
            'inactive'    => 0,
            'studentview' => 0,
        ];

        $userStatusArray = [
            'active'      => [],
            'inactive'    => [],
            'studentview' => [],
        ];

        $termCodeArray = [
            '201501'      => 'SP15',
            '201505'      => 'SU15',
            '201508'      => 'FA15',
            '201601'      => 'SP16',
            '201605'      => 'SU16',
            '201608'      => 'FA16',
            '201701'      => 'SP17',
            '201705'      => 'SU17',
            '201708'      => 'FA17',
            '201801'      => 'SP18',
            '201805'      => 'SU18',
            '201808'      => 'FA18',
            '201901'      => 'SP19',
            '201905'      => 'SU19',
            '201908'      => 'FA19',
            '202001'      => 'SP20',
            '202005'      => 'SU20',
            '202008'      => 'FA20',
            'SANDBOX'     => 'SANDBOX',
            'NON-COURSES' => 'NON-COURSES',
        ];

        if (isset($request->termCodeString)) {
            $thisTermCodeArray = $requestData['termCodeString'];
        } else {
            $thisTermCodeArray = ['201808' => '201808'];
        }

        if (isset($requestData['toolId'])) {
            $toolId = $request->toolId;

//            $items = \DB::table('view_lti_enrollment')
//                ->where('toolId', '=', $toolId)
//                ->where(function ($query) use($thisTermCodeArray) {
//
//                    foreach($thisTermCodeArray as $termCode){
//                        $query->orWhere('sis_course_id', 'LIKE', '%'.$termCode.'%');
//                    }
//
//                })
//                ->paginate(config('ccps.paginator_per_page'));

            $items = \DB::table('view_lti_enrollment')
                ->where('toolId', '=', $toolId)
                ->where(function ($query) use ($thisTermCodeArray) {
                    foreach ($thisTermCodeArray as $termCode) {
                        $query->orWhere('sis_course_id', 'LIKE', '%'.$termCode.'%');
                    }
                })
                ->orderBy('sis_course_id', 'ASC')
                ->get();
        } else {
            //$items = ViewEnrollment::orderBy('user_id', 'asc')->paginate(config('ccps.paginator_per_page'));
            $items = ViewEnrollment::orderBy('sis_course_id', 'asc')->get();
        }

        $total = count($items);

        //ugly but it works for now :-) You're welcome.

        foreach ($items as $i) {
            if ($i->status == 'active' && !isset($userStatusArray['active'][$i->user_id])) {
                $userStatusArray['active'][$i->user_id] = $i->user_id;

                // see if it needs to be unset from the inactive array too
                if (isset($userStatusArray['inactive'][$i->user_id])) {
                    unset($userStatusArray['inactive'][$i->user_id]);
                }
            } elseif ($i->status == 'inactive' && !isset($userStatusArray['inactive'][$i->user_id]) && !isset($userStatusArray['active'][$i->user_id])) {
                $userStatusArray['inactive'][$i->user_id] = $i->user_id;
            } elseif (strstr($i->user_id, 'CANVAS_USER_ID')) {
                $userStatusArray['studentview'][$i->user_id] = $i->user_id;
            }
        }

        $summaryInfo['active'] = count($userStatusArray['active']);
        $summaryInfo['inactive'] = count($userStatusArray['inactive']);
        $summaryInfo['studentview'] = count($userStatusArray['studentview']);

        //dd($summaryInfo, $userStatusArray);


        return view('canvas.lti.enrollment.index', compact('items', 'tools', 'request', 'total', 'summaryInfo', 'termCodeArray', 'thisTermCodeArray'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function inventory(Request $request)
    {
        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $items = LtiInventory::where('sis_course_id', 'LIKE', "%$keyword%")
                ->orWhere('lastEnrollmentUpdate', 'LIKE', "%$keyword%")
                ->orWhere('lastUpdated', 'LIKE', "%$keyword%")
                ->orderBy('sis_course_id', 'asc')
                ->paginate(config('ccps.paginator_per_page'));
        } else {
            $items = LtiInventory::orderBy('sis_course_id', 'asc')->paginate(config('ccps.paginator_per_page'));
        }

        $allTools = LtiTools::all();

        $tools = [];
        foreach ($allTools as $t) {
            $tools[$t->toolId] = $t->ltiName;
        }

        return view('canvas.lti.inventory.index', compact('items', 'tools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function inventorycreate()
    {
        $allTools = LtiTools::all();

        $tools = [];
        foreach ($allTools as $t) {
            $tools[$t->toolId] = $t->ltiName;
        }

        return view('canvas.lti.inventory.create', compact('tools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function inventorystore(Request $request)
    {
//        $this->validate($request, [
        //			'title' => 'required|max:10'
        //		]);
        $requestData = $request->all();

        //dd($requestData);

        //dd($thisRole, $userArray, $insertArray);

        $insertArray = [
            'toolId'               => $requestData['toolId'],
            'sis_course_id'        => $requestData['sis_course_id'],
            'lastUpdated'          => 0,
            'lastEnrollmentUpdate' => 0,
        ];

        \DB::table('lti_inventory')->insert($insertArray);

        \Log::stack(['general', 'lti'])->info('Store Inventory Performed by: ' . auth()->user()->email . ', JSON Encoded Object Added: ' . json_encode($insertArray));

        flash('Inventory Added Successfully: ' . $requestData['sis_course_id'], 'info');

        return redirect(route('lti.inventory.index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function inventoryedit(Request $request)
    {
        $item = LtiInventory::findOrFail($request->inventoryId);

        $allTools = LtiTools::all();

        $tools = [];
        foreach ($allTools as $t) {
            $tools[$t->toolId] = $t->ltiName;
        }

        //dd($item);

        return view('canvas.lti.inventory.edit', compact('item', 'tools'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function inventoryupdate(Request $request)
    {
        $requestData = $request->all();

        $item = LtiInventory::findOrFail($request->inventoryId);
        $item->update($requestData);

        \Log::stack(['general', 'lti'])->info('Record Update Performed by: ' . auth()->user()->email . '. ID: ' . $request->inventoryId . ', Request Data: ' . json_encode($requestData));

        flash('LTI Inventory updated: ' . $requestData['sis_course_id'], 'info');

        return redirect(route('lti.inventory.index'));
    }

    /**
     * Show the form for deleting the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function inventorydelete(Request $request)
    {
        $item = LtiInventory::findOrFail($request->inventoryId);

        $allTools = LtiTools::all();

        $tools = [];
        foreach ($allTools as $t) {
            $tools[$t->toolId] = $t->ltiName;
        }

        //dd($item);

        return view('canvas.lti.inventory.delete', compact('item', 'tools'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function inventorydestroy(Request $request)
    {
        $thisRecord = LtiInventory::findOrFail($request->inventoryId);

        LtiInventory::destroy($request->inventoryId);

        \Log::stack(['general', 'lti'])->info('Record Delete Performed by: ' . auth()->user()->email . '. ID: ' . $request->inventoryId . ', Data Deleted: ' . json_encode($thisRecord));

        flash('LTI Inventory deleted!', 'info');

        return redirect(route('lti.inventory.index'));
    }
}
