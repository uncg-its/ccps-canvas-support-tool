<?php

namespace App\Http\Controllers\Canvas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CrosslistController extends Controller
{
    public function __construct()
    {
        // Basic ACL middleware using Laratrust
        $this->middleware('permission:crosslist.create')->except(['index']);
        $this->middleware('permission:crosslist.delete')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('canvas.crosslist.index');
    }

    public function decrosslist()
    {
        return view('canvas.crosslist.decrosslist');
    }

    public function destroy(Request $request)
    {
        $sectionId = $request->get('section_id_field', '') !== 'canvas' ?
            'sis_section_id:' . $request->section_id :
            $request->section_id;

        $result = \CanvasApi::using('sections')->decrosslistSection($sectionId);

        $messageLevel = 'success';
        $message = 'De-Crosslist Successful: Section ' . $request->section_id;

        if ($result->getStatus() !== 'success') {
            $messageLevel = 'error';
            $lastResult = $result->getLastResult();
            $message = 'De-Crosslist NOT Successful: Section ' . $request->section_id . '. CODE:REASON: ' . $lastResult['code'] . ':' . $lastResult['reason'];
        }

        flash($message)->$messageLevel();

        \Log::channel('crosslist')->info($message, [
            'category'  => 'crosslist',
            'operation' => 'decrosslist',
            'result'    => $messageLevel,
            'data'      => [
                'user'                 => \Auth::user()->email,
                'child_section_sis_id' => $request->section_id,
            ]
        ]);

        return redirect(route('crosslist.index'));
    }



    public function create()
    {
        return view('canvas.crosslist.create');
    }
}
