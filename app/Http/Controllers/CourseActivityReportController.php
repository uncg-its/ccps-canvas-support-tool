<?php

namespace App\Http\Controllers;

use App\CourseReport;
use App\CourseReportItem;
use App\Csthelpers\Generatearraytreeforaccountlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\Csv\Writer;

class CourseActivityReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:course-activity-report.edit');
    }

    public function index()
    {
        $reports = CourseReport::orderBy('created_at', 'desc')->with('course_report_items')->get();

        return view('canvas.course-activity-report.index')->with([
            'reports' => $reports
        ]);
    }

    public function create()
    {
        $accounts = Generatearraytreeforaccountlist::createSimpleAccountsArrayForSelectElement();
        // terms
        $termResult = \CanvasApi::using('EnrollmentTerms')->listEnrollmentTerms(1);

        $terms = [];
        foreach ($termResult->getContent() as $term) {
            $terms[$term->id] = $term->name;
        }

        return view('canvas.course-activity-report.create')->with([
            'accounts' => $accounts,
            'terms'    => $terms,
        ]);
    }

    public function store(Request $request)
    {
        $today = Carbon::now()->toDateString();

        $request->validate(
            [
                'account'     => 'required|regex:/^[0-9.]+___[0-9]+$/',
                'term'        => 'required|numeric',
                'since'       => 'sometimes | nullable | date | before:' . $today,
                'until'       => 'sometimes | nullable | date | after:' . Carbon::parse($request->since)->toDateString(),
                'hide_empty'  => 'sometimes | nullable | boolean',
                'search_term' => 'sometimes | nullable | min:3'
            ],
            [
                'since.before'  => 'The chosen date must be in the past',
                'until.after'   => 'The chosen date must be after the "since" date',
                'account.regex' => 'Invalid account value. Contact an administrator.'

            ]
        );

        // parse account
        $accountBits = explode('___', $request->account);
        $account = $accountBits[1];

        // parse options from form input
        $options = [
            'enrollment_term_id' => $request->term,
            'include'            => [
                'total_students',
                'storage_quota_used_mb',
            ]
        ];


        if ($request->has('hide_empty') && $request->hide_empty == true) {
            $options['with_enrollments'] = true;
        }
        if ($request->has('search_term')) {
            $options['search_term'] = $request->search_term;
        }

        $coursesToCheck = \CanvasApi::using('accounts')
            ->addParameters($options)
            ->listActiveCoursesInAccount($account);

        if (count($coursesToCheck->getContent()) === 0) {
            flash('No Courses were returned with the given parameters. Report not generated', 'info');
            return redirect()->back();
        }

        try {
            DB::beginTransaction();

            // generate new Course Report
            $report = CourseReport::create([
                'requested_by' => auth()->user()->id,
                'account_id'   => $account,
                'term_id'      => $request->term,
                'options'      => json_encode([
                    'since'       => $request->input('since', ''),
                    'until'       => $request->input('until', ''),
                    'hide_empty'  => $request->input('hide_empty', 0),
                    'search_term' => $request->input('search_term', ''),
                ]),
                'created_at'   => Carbon::now()->toDateTimeString(),
            ]);

            foreach ($coursesToCheck->getContent() as $course) {

                // generate new item
                $item = CourseReportItem::create([
                    'course_report_id'   => $report->id,
                    'course_information' => json_encode([
                        'id'                    => $course->id,
                        'name'                  => $course->name,
                        'code'                  => $course->course_code,
                        'total_students'        => $course->total_students,
                        'workflow_state'        => $course->workflow_state,
                        'end_date'              => $course->end_at,
                        'storage_quota_used_mb' => $course->storage_quota_used_mb,
                    ]),
                    'status'             => 'pending'
                ]);

                // job to be kicked off in CourseReportItemObserver
            }

            \Log::channel('course-activity-report')->info('Report ID ' . $report->id . ' created by user ' . auth()->user()->email . '. Courses to check: ' . count($coursesToCheck->getContent()) . '. Options: ' . json_encode($report->options));

            flash('Report ID ' . $report->id . ' queued successfully. ', 'success');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::channel('course-activity-report')->error('Error during Report creation. ' . $e->getMessage());
            flash('Error creating Course Access Report. Please contact an administrator.', 'danger');
        }

        return redirect()->route('canvas.course-activity-report.index');
    }

    public function show(CourseReport $report)
    {
        $report->load('items');

        return view('canvas.course-activity-report.show')->with([
            'report' => $report,
        ]);
    }

    public function export(CourseReport $report)
    {
        \Log::channel('course-activity-report')->info('Report ID ' . $report->id . ' exported to CSV by user ' . auth()->user()->email);

        $csv = Writer::createFromString('');

        $headers = [
            'query_id',
            'course_id',
            'course_name',
            'course_url',
            'course_workflow_state',
            'teacher_emails',
            'end_date',
            'total_students',
            'total_activity_seconds',
            'last_teacher_activity',
            'last_student_view',
            'last_student_participation',
            'views_in_bounds',
            'participations_in_bounds',
            'lower_date_bound',
            'upper_date_bound',
        ];

        $items = $report->items->map(function ($item) use ($report) {
            return [
                $item->id,
                $item->course_information->id,
                $item->course_information->name,
                $item->course_url,
                $item->course_information->workflow_state,
                implode(',', $item->teacher_emails),
                $item->ends_at,
                $item->course_information->total_students,
                $item->statistics->total_activity_seconds,
                $item->statistics->last_teacher_activity,
                $item->statistics->last_student_view,
                $item->statistics->last_student_participation,
                $item->statistics->total_views,
                $item->statistics->total_participations,
                $report->options->since ?? '',
                $report->options->until ?? '',
            ];
        });

        $csv->insertOne($headers);
        $csv->insertAll($items);

        // serve it up
        $csv->output($report->csv_name);
    }
}
