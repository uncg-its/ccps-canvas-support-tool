<?php

namespace App\Http\Controllers;

use App\SandboxCourseBatch;
use Uncgits\Ccps\Support\CcpsPaginator;

class BatchSandboxCourseController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:batch-sandbox.view'])->only(['index', 'show']);
        $this->middleware(['permission:batch-sandbox.create'])->only(['create']);
    }


    public function index()
    {
        if (\Auth::user()->hasPermission('batch-sandbox.admin')) {
            $batches = SandboxCourseBatch::orderByDesc('created_at')->get();
        } else {
            $batches = \Auth::user()->sandbox_course_batches()->orderBy('created_at')->get();
        }

        return view('batch-sandbox.index')->with(['batches' => new CcpsPaginator($batches)]);
    }

    public function show(SandboxCourseBatch $batch)
    {
        return view('batch-sandbox.show')->with([
            'batch'   => $batch,
            'courses' => new CcpsPaginator($batch->courses)
        ]);
    }

    public function create()
    {
        return view('batch-sandbox.create');
    }
}
