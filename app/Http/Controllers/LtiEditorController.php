<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Csthelpers\Generatearraytreeforaccountlist;
use App\Http\Requests\UpdateLtiToolRequest;

class LtiEditorController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:lti.read')->only('index');
        $this->middleware('permission:lti.update')->only(['edit, update']);
    }

    public function index()
    {
        $accounts = Generatearraytreeforaccountlist::createSimpleAccountsArrayForSelectElement();


        return view('lti-editor.index')->with([
            'accounts' => $accounts
        ]);
    }

    public function edit(Request $request)
    {
        if (! $request->has('tool_id')) {
            flash('Error: you must select a valid LTI Tool.')->error();
            return redirect()->back();
        }

        if (! $request->has('account_id')) {
            flash('Error: you must supply a valid Account ID.')->error();
            return redirect()->back();
        }

        $accountId = explode('___', $request->account_id)[1];

        $toolResult = \CanvasApi::using('ExternalTools')
            ->getSingleExternalToolInAccount($accountId, $request->tool_id);

        if ($toolResult->getStatus() !== 'success') {
            flash('API Error when retrieving LTI Tool information. Please contact an administrator.')->error();
            return redirect()->back();
        }

        return view('lti-editor.edit')->with([
            'tool'       => $toolResult->getContent(),
            'account_id' => $accountId,
        ]);
    }

    public function update(UpdateLtiToolRequest $request, $toolId)
    {
        $parameters = [
            'name'              => $request->name,
            'description'       => $request->description,
            'url'               => $request->url,
            'course_navigation' => [
                'text'    => $request->course_navigation_text,
                'default' => $request->default_enabled ? 'enabled' : 'disabled'
            ],
        ];

        $updateRequest = \CanvasApi::using('ExternalTools')
            ->addParameters($parameters)
            ->editExternalToolInAccount($request->account_id, $toolId);

        if ($updateRequest->getStatus() !== 'success') {
            flash('API Error when updating LTI Tool.')->error();
        } else {
            flash('LTI Tool updated successfully!')->success();
        }

        return redirect()->back();
    }

    public function loadTools(Request $request)
    {
        if (! $request->has('account_id')) {
            return response()->json([
                'errors' => [
                    'You must supply an account_id'
                ]
            ], 400);
        }

        $toolResult = \CanvasApi::using('ExternalTools')
            ->listExternalToolsForAccount($request->account_id);

        if ($toolResult->getStatus() !== 'success') {
            return response()->json([
                'errors' => [
                    'API error: ' . $toolResult->getLastResult()['reason'],
                ]
            ], 424);
        }

        $tools = collect($toolResult->getContent())->mapWithKeys(function ($tool) {
            return [$tool->id => $tool->name];
        })->toArray();

        $toolsArray = [
            'data' => [
                'tools' => $tools
            ]
        ];

        return response()->json($toolsArray, 200);
    }
}
