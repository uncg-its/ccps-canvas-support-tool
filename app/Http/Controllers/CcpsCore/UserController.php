<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\UserController as BaseController;

class UserController extends BaseController
{
    public function __construct() {
        parent::__construct();
    }
}
