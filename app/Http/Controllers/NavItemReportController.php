<?php

namespace App\Http\Controllers;

use App\NavItemReport;

class NavItemReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:nav-item-reports.view']);
    }

    public function index()
    {
        return view('nav-item-reports.index');
    }

    public function show(NavItemReport $report)
    {
        return view('nav-item-reports.show')->with([
            'report' => $report
        ]);
    }
}
