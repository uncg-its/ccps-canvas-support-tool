<?php

namespace App\Http\Controllers;

class CrosslistController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:crosslist-report.view');
    }

    public function index()
    {
        return view('crosslist-report.index');
    }
}
