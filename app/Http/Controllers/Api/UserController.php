<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    private function guessCurrentTerm()
    {
        $year = now()->format('Y');
        $month = now()->format('n');

        if ($month >= 8) {
            $month = '08';
        } elseif ($month < 5) {
            $month = '01';
        } else {
            $month = '05';
        }
        return $year . $month;
    }

    public function show($query)
    {
        $result = \CanvasApi::using('users')
            ->showUserDetails('sis_login_id:' . $query);

        if ($result->getStatus() !== 'success') {
            return response()->json(['error' => $result->getLastResult()['reason']], $result->getLastResult()['code']);
        }

        return response()->json(['query' => $query, 'user' => $result->getContent()], 200);
    }

    public function courses($query)
    {
        $term = request()->get('term', $this->guessCurrentTerm());

        $result = \CanvasApi::using('courses')
            ->addParameters([
                'state'   => ['unpublished', 'available', 'completed'],
                'include' => ['term']
            ])
            ->listCoursesForUser('sis_login_id:' . $query);

        if ($result->getStatus() !== 'success') {
            return response()->json(['error' => $result->getLastResult()['reason']], $result->getLastResult()['code']);
        }

        $termCourses = collect($result->getContent())->filter(function ($course) use ($term) {
            return $course->term->sis_term_id === $term;
        });

        return response()->json(['query' => $query, 'term' => $term, 'courses' => $termCourses->values()], 200);
    }

    public function quota($query)
    {
        $userString = 'sis_login_id:' . $query;

        $result = \CanvasApi::using('files')
            ->asUser($userString)
            ->getQuotaInformationForUser($userString);

        if ($result->getStatus() !== 'success') {
            return response()->json(['error' => $result->getLastResult()['reason']], $result->getLastResult()['code']);
        }

        return response()->json(['query' => $query, 'quota_information' => $result->getContent()]);
    }
}
