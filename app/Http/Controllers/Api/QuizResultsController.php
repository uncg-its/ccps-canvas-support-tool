<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uncgits\CanvasApi\CanvasApiResult;
use Uncgits\CanvasApi\Exceptions\CanvasApiException;

class QuizResultsController extends Controller
{
    public function get(Request $request)
    {
        if (! \Auth::user()->tokenCan('quiz-results.get')) {
            return response()->json(['errors' => ['Insufficient privileges for token']], 403);
        }

        $validated = $request->validate([
            'canvas_course_id' => ['required', 'numeric'],
            'canvas_quiz_id'   => ['nullable', 'numeric'],
        ]);

        $courseId = $validated['canvas_course_id'];
        $quizId = $validated['canvas_quiz_id'] ?? null;

        try {
            $response = [];

            $courseResult = \CanvasApi::using('courses')->getCourse($courseId);
            if ($this->getLastCallCode($courseResult) === 404) {
                return response()->json(['errors' => ['Course ID ' . $courseId . ' was not found in Canvas. Please try again.']], 404);
            }
            if ($courseResult->getStatus() !== 'success') {
                throw new CanvasApiException('API error when retrieving Canvas course ' . $courseId);
            }
            $course = $courseResult->getContent();

            $response['course_id'] = $course->id;
            $response['course_name'] = $course->name;

            if (!is_null($quizId)) {
                $quizResult = \CanvasApi::using('quizzes')->getQuiz($courseId, $quizId);
                if ($this->getLastCallCode($quizResult) === 404) {
                    return response()->json(['errors' => ['Quiz ID ' . $quizId . ' was not found in Canvas course ' . $courseId . '. Please try again.']], 404);
                }
                if ($quizResult->getStatus() !== 'success') {
                    throw new CanvasApiException('API error when retrieving Canvas quiz ' . $quizId);
                }
                $quizzes = [$quizResult->getContent()];
            } else {
                $quizResult = \CanvasApi::using('quizzes')->listQuizzesInCourse($courseId);
                if ($quizResult->getStatus() !== 'success') {
                    throw new CanvasApiException('API error when retrieving quiz list for Canvas course ' . $courseId);
                }
                $quizzes = $quizResult->getContent();
                if (count($quizzes) === 0) {
                    return response()->json(['errors' => ['No quizzes were found in Canvas course ' . $courseId . '. Please try again.']], 404);
                }
            }

            $enrollmentResult = \CanvasApi::using('enrollments')->listCourseEnrollments($courseId);
            if ($enrollmentResult->getStatus() !== 'success') {
                throw new CanvasApiException('API error when retrieving enrollment list for Canvas course ' . $courseId);
            }
            $enrollments = collect($enrollmentResult->getContent())->keyBy('user_id'); // key by this so we can easily correlate with results

            foreach ($quizzes as $quiz) {
                $quizName = $quiz->title;
                $quizPointsPossible = $quiz->points_possible;

                $submissionResult = \CanvasApi::using('QuizSubmissions')
                    ->getAllQuizSubmissions($courseId, $quizId);
                if ($submissionResult->getStatus() !== 'success') {
                    throw new CanvasApiException('API error when retrieving submission list for Canvas quiz ' . $quizId);
                }
                $scores = collect($submissionResult->getContent())
                    ->reject(function ($submission) {
                        return is_null($submission->finished_at);
                    })->mapWithKeys(
                        // since Canvas gives us back each user's latest attempt ONLY, this is rather easy, BUT...
                        // it does not necessarily give us the date of the KEPT attempt - only the latest.
                        fn ($submission, $userId) => [
                            $userId => [
                                'euid'              => $enrollments[$submission->user_id]->user->sis_user_id,
                                'username'          => $enrollments[$submission->user_id]->user->login_id,
                                'quiz_id'           => $quizId,
                                'quiz_name'         => $quizName,
                                'score'             => $submission->kept_score,
                                'points_possible'   => $quizPointsPossible,
                                'score_percent'     => round($submission->kept_score / $quizPointsPossible, 4) * 100,
                                'latest_attempt_at' => $submission->finished_at,
                            ]
                        ]
                    );
            }

            // $response['results'] = $scores->toArray(); // 2024-04-26 Nick Young, John Shaver 
            // Commented out to fix bug related to pagination
            // replaced with following, to ensure formatted result array is the same no matter
            // how many results come back from Canvas
            
            $response['results'] = array_values($scores->toArray()); // new version as of 2024-04-26

            return response()->json($response, 200);
        } catch (CanvasApiException $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        } catch (\Exception $e) {
            return response()->json(['errors' => ['Server Error']], 500);
        }
    }

    protected function getLastCallCode(CanvasApiResult $result)
    {
        return $result->getLastCall()['response']['code'];
    }
}
