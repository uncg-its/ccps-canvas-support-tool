<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    private function guessCurrentTerm()
    {
        $year = now()->format('Y');
        $month = now()->format('n');

        if ($month >= 8) {
            $month = '08';
        } elseif ($month < 5) {
            $month = '01';
        } else {
            $month = '05';
        }
        return $year . $month;
    }

    public function search(Request $request)
    {
        $query = $request->get('query', null);

        if (is_null($query)) {
            return response()->json(['error' => 'Query must be provided'], 400);
        }

        $term = $request->get('term', $this->guessCurrentTerm());

        $result = \CanvasApi::using('accounts')
            ->addParameters([
                'enrollment_term_id' => 'sis_term_id:' . $term,
                'search_term'        => $query
            ])->listActiveCoursesInAccount(config('cst.key_account_ids.courses'));

        if ($result->getStatus() !== 'success') {
            return response()->json(['error' => $result->getLastResult()['reason']], $result->getLastResult()['code']);
        }

        return response()->json(['query' => $query, 'term' => $term, 'courses' => $result->getContent()], 200);
    }

    public function show(Request $request, $id)
    {
        $result = \CanvasApi::using('courses')
            ->addParameters([
                'include' => [
                    'total_students',
                    'term',
                    'teachers',
                    'concluded'
                ]
            ])
            ->getCourse($id);

        if ($result->getStatus() !== 'success') {
            return response()->json(['error' => $result->getLastResult()['reason']], $result->getLastResult()['code']);
        }

        return response()->json(['id' => $id, 'course' => $result->getContent()], 200);
    }

    public function enrollments($id)
    {
        $result = \CanvasApi::using('enrollments')
            ->listCourseEnrollments($id);

        if ($result->getStatus() !== 'success') {
            return response()->json(['error' => $result->getLastResult()['reason']], $result->getLastResult()['code']);
        }

        return response()->json(['id' => $id, 'enrollments' => $result->getContent()], 200);
    }

    public function quota($id)
    {
        $result = \CanvasApi::using('files')
            ->getQuotaInformationForCourse($id);

        if ($result->getStatus() !== 'success') {
            return response()->json(['error' => $result->getLastResult()['reason']], $result->getLastResult()['code']);
        }

        return response()->json(['id' => $id, 'quota_information' => $result->getContent()]);
    }
}
