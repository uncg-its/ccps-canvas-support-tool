<?php

namespace App\Http\Controllers;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use App\Jobs\InspectAndReplaceCourseNav;

class NavItemReplacerController extends Controller
{
    private function log($message, $severity = 'info')
    {
        Log::channel('nav-item-replacer')->$severity($message);
    }

    public function __construct()
    {
        $this->middleware('permission:nav-item-replacer.edit');
    }


    public function index()
    {
        $result = \CanvasApi::using('EnrollmentTerms')->listEnrollmentTerms(1);
        if (count($result->getContent()) > 0) {
            $terms = collect($result->getContent())->mapWithKeys(function ($item, $key) {
                return [$item->id => $item->name];
            })->prepend('--- Select ---', '')->toArray();
        }

        return view('canvas.nav-item-replacer.index')->with([
            'terms' => $terms
        ]);
    }

    public function review(Request $request)
    {
        $this->log('New request from user ' . auth()->user()->email . ': ' .
            json_encode($request->only(['term', 'search_term', 'menu_item_name', 'new_menu_item_name'])));

        $validated = $request->validate([
            'term'               => 'required|numeric',
            'search_term'        => 'nullable|min:3',
            'menu_item_name'     => 'required',
            'new_menu_item_name' => 'required',
        ]);

        $minutes = config('cst.cache_ttl.nav_item_replacer'); // minutes for cache to hold results
        $this->log('Cache TTL: ' . $minutes, 'debug');

        $options = [
            'enrollment_term_id' => $request->term,
            'with_enrollments'   => 'true',
            'per_page'           => '500',
        ];

        if ($request->has('search_term')) {
            $options['search_term'] = $request->search_term;
        }

        $coursesResult = \CanvasApi::using('accounts')
            ->addParameters($options)
            ->listActiveCoursesInAccount(1);

        $courses = collect($coursesResult->getContent());

        $uuid = Uuid::uuid1()->toString();
        $key = 'nir-' . $uuid;
        $this->log('Cache key: ' . $key, 'debug');

        Cache::put($key . '-courses', $courses, $minutes);
        Cache::put($key . '-find', $request->menu_item_name, $minutes);
        Cache::put($key . '-replace', $request->new_menu_item_name, $minutes);

        return view('canvas.nav-item-replacer.review')->with(([
            'courses'          => $courses,
            'key'              => $key,
            'minutes'          => $minutes,
            'term'             => $request->term,
            'search_term'      => $request->search_term ?? null,
            'queueWaitMinutes' => config('cst.queue_wait_minutes.nav_item_replacer')
        ]));
    }

    public function replace(Request $request)
    {
        $this->log('Request approved; looking for cache items with key ' . $request->key, 'debug');
        $jobsCount = 0;

        try {
            $courses = Cache::get($request->key . '-courses', null);
            $find = Cache::get($request->key . '-find', null);
            $replace = Cache::get($request->key . '-replace', null);

            if (is_null($courses) || is_null($find) || is_null($replace)) {
                flash('Cache information missing - your request may have timed out. Please try your request again.')->error();
                throw new \Exception('Cache information was missing. Try the request again.');
            }

            $queueDelay = config('cst.queue_wait_minutes.nav_item_replacer');
            $this->log('Queue delay minutes set to: ' . $queueDelay, 'debug');

            $courses->each(function ($course) use ($find, $replace, &$jobsCount, $queueDelay) {
                InspectAndReplaceCourseNav::dispatch(
                    $course->id,
                    $find,
                    $replace
                )->onQueue('medium')->delay(now()->addMinutes($queueDelay));

                $jobsCount++;
            });

            $this->log('Jobs queued from batch ID ' . $request->key . ': ' . $jobsCount);
            flash('Success: Jobs queued: ' . $jobsCount, 'success');
        } catch (\Exception $e) {
            flash('An error occurred. Please check logs or contact an admin. Jobs queued successfully: ' . $jobsCount)
                ->error();
            $this->log('Error while generating nav item replacement jobs: ' . $e->getMessage(), 'error');
        }

        return redirect()->route('canvas.nav-item-replacer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
