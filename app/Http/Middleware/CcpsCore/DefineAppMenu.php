<?php

namespace App\Http\Middleware\CcpsCore;

use Closure;
use Illuminate\Support\Facades\Auth;
use Lavary\Menu\Facade as Menu;

class DefineAppMenu
{
    private function generateModuleLinks($menu)
    {
        if ($user = Auth::user()) {
            // read modules from config and auto-generate menu
            $adminPrivs = [];
            foreach (config('ccps.modules') as $key => $module) {
                if ($user->hasPermission($module['required_permissions'])) {
                    $adminPrivs[] = $module;
                }
            }

            if (count($adminPrivs) > 0) {
                // Define Admin menu
                $menu->add('Admin', route('admin'));

                foreach ($adminPrivs as $priv => $module) {
                    if ($module['parent'] == 'admin') {
                        $menu->admin->add($module['title'], route($module['index']));
                    } else {
                        if (empty($module['parent'])) {
                            $menu->add($module['title'], route($module['index']));
                        }
                    }
                }
            }
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // define application menus here

        Menu::make('nav', function ($menu) {

            // for logged-in users
            if ($user = Auth::user()) {
                if ($user->hasRole('canvas.admin|servicedesk.member|itc')) {
                    $menu->add('Canvas Tools', route('home'));
                }
            }

            // add custom here to show before Admin menu
            // $menu->add('MyLink', route('myroute');

            // for authenticated users, generate links and Admin menu from modules
            $this->generateModuleLinks($menu);

            // add custom here to show after Admin menu
            // $menu->add('MyLink', route('myroute');
        });

        return $next($request);
    }
}
