<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Redis;

class LimitNavItemReports
{
    /**
     * Process the queued job.
     *
     * @param  mixed  $job
     * @param  callable  $next
     * @return mixed
     */
    public function handle($job, $next)
    {
        Redis::throttle('nav-item-report')
            ->allow(180)
            ->every(60)
            ->then(function () use ($job, $next) {
                $next($job);
            }, function () use ($job) {
                $job->release(30);
            });
    }
}
