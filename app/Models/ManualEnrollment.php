<?php

namespace App\Models;

use App\ManualEnrollmentBatch;
use Illuminate\Database\Eloquent\Model;

class ManualEnrollment extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    // relationships

    public function manual_enrollment_batch()
    {
        return $this->belongsTo(ManualEnrollmentBatch::class, 'batch_id');
    }

    // aliases

    public function batch()
    {
        return $this->manual_enrollment_batch();
    }

    // scopes

    public function scopeErrored($query)
    {
        return $query->whereIn('status', ['error']);
    }
}
