<?php

namespace App\Models;

use App\CcpsCore\User;
use App\Traits\HasSuccessStatus;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class EnrollmentRequest extends Model
{
    use Sortable, HasSuccessStatus;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'enrollment_requests';

    protected $dates = [
        'created_at',
        'updated_at',
        'begins_at',
        'ends_at'
    ];

    protected $sortable = [
        'requesterEmailAddress',
        'enrolledPersonName',
        'created_at'
    ];

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'requestId';

    protected $guarded = [];

    // relationships

    public function requester()
    {
        return $this->belongsTo(User::class, 'requesterEmailAddress', 'email');
    }

    public function approver()
    {
        return $this->belongsTo(User::class, 'approver_id');
    }

    public function rejector()
    {
        return $this->belongsTo(User::class, 'rejector_id');
    }

    // accessors

    public function getStatusAttribute()
    {
        if (!is_null($this->completed_at)) {
            return 'completed';
        }

        if (!is_null($this->rejected_at)) {
            return 'rejected';
        }

        if (!is_null($this->approved_at)) {
            return 'approved';
        }

        return 'pending approval';
    }

    public function getStatusClass()
    {
        switch ($this->status) {
            case 'completed':
            case 'approved':
                return 'success';
            case 'rejected':
                return 'danger';
            case 'pending approval':
                return 'warning';
            default:
                return 'info';
        }
    }

    public function getStatusIcon()
    {
        switch ($this->status) {
            case 'completed':
            case 'approved':
                return 'check';
            case 'rejected':
                return 'times';
            case 'pending approval':
                return 'pause';
            default:
                return 'question';
        }
    }
}
