<?php

namespace App\Models\Lti\View;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'view_lti_enrollment';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'enrollmentId';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [];

    
}
