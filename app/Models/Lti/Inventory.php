<?php

namespace App\Models\Lti;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lti_inventory';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'inventoryId';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['toolId', 'sis_course_id', 'lastUpdated', 'lastEnrollmentUpdate'];

    
}
