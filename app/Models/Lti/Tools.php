<?php

namespace App\Models\Lti;

use Illuminate\Database\Eloquent\Model;

class Tools extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lti_tools';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'toolId';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ltiName', 'ltiDescription', 'lastUpdated'];

    
}
