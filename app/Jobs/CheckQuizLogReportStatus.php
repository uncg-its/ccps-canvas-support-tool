<?php

namespace App\Jobs;

use App\QuizLogReport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckQuizLogReportStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 25;
    public $maxExceptions = 1;
    protected $report;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(QuizLogReport $report)
    {
        $this->onQueue('low');
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pendingItems = $this->report->items()->whereIn('status', ['pending', 'processing'])->count();
        if ($pendingItems > 0) {
            // default is pending, so get outta here.
            return;
        }

        $failedItems = $this->report->items()->where('status', 'failed')->count();
        if ($failedItems > 0) {
            $this->report->status = 'failed';
            $this->report->save();
            return;
        }

        $this->report->status = 'completed';
        $this->report->save();
        return;
    }
}
