<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\CourseReport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckCourseActivityCompletion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $report;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CourseReport $report)
    {
        $this->onQueue('low');

        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // check for other pending items
        if ($this->report->pending_items->isEmpty()) {
            $this->report->update([
                'completed_at' => Carbon::now()->toDateTimeString()
            ]);
            \Log::channel('course-activity-report')->info('Marking Report ' . $this->report->id . ' as completed.');
        }
    }
}
