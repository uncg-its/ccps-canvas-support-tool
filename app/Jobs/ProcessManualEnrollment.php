<?php

namespace App\Jobs;

use App\Events\ManualEnrollmentProcessed;
use App\Http\Middleware\FunnelManualEnrollments;
use Illuminate\Bus\Queueable;
use App\Models\ManualEnrollment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessManualEnrollment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $manualEnrollment;
    protected $course;
    protected $datesChanged = false;
    protected $errorMessage = null;

    public $tries = 50;
    public $maxExceptions = 1;

    public function middleware()
    {
        return [new FunnelManualEnrollments]; // all of these jobs should go through rate limiting
    }

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ManualEnrollment $manualEnrollment)
    {
        $manualEnrollment->load('batch');
        $this->manualEnrollment = $manualEnrollment;

        $this->onQueue('low');
    }

    public function parseErrorMessage($result, $operation = '')
    {
        $errorMessage = 'API call failed: ' . $result->getLastResult()['code'] . ': ' . $result->getLastResult()['reason'];

        if (isset($result->getContent()->message)) {
            $errorMessage .= '; error message: ' . $result->getContent()->message;
        }

        if ($operation !== '') {
            $errorMessage .= ' (operation: ' . $operation . ')';
        }

        return $errorMessage;
    }

    protected function resetCourseEnrollmentDates()
    {
        $resetCourseDateResult = \CanvasApi::using('courses')
            ->addParameters([
                'course' => [
                    'start_at'                             => $this->course->start_at ?? '',
                    'end_at'                               => $this->course->end_at ?? '',
                    'restrict_enrollments_to_course_dates' => $this->course->restrict_enrollments_to_course_dates
                ]
            ])->updateCourse($this->course->id);
        if ($resetCourseDateResult->getStatus() !== 'success') {
            $errorMessage = $this->parseErrorMessage($resetCourseDateResult, 'Reset course dates');
            throw new \Exception($errorMessage);
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userId = $this->manualEnrollment->enrolled_user_id;
        if ($this->manualEnrollment->enrolled_user_id_type === 'login_id') {
            $userId = 'sis_login_id:' . $userId;
        }

        $parameters = [
            'enrollment' => [
                'user_id'                   => $userId,
                'type'                      => $this->manualEnrollment->role_type,
                'role_id'                   => $this->manualEnrollment->role_id,
                'enrollment_state'          => 'active',
            ]
        ];

        try {
            // if this is a section-based enrollment, then we need to call the API and get the course it belongs to.
            if ($this->manualEnrollment->batch->destination_type === 'section') {
                $sectionResult = \CanvasApi::using('sections')->getSectionInformation($this->manualEnrollment->batch->destination_id);
                if ($sectionResult->getStatus() !== 'success') {
                    $errorMessage = $this->parseErrorMessage($sectionResult, 'Get section info');
                    throw new \Exception($errorMessage);
                }
                $section = $sectionResult->getContent();
                $courseId = $section->course_id;
            } else {
                $courseId = $this->manualEnrollment->batch->destination_id;
            }

            // get course info to see if it is concluded
            $courseResult = \CanvasApi::using('courses')->getCourse($courseId);
            if ($courseResult->getStatus() !== 'success') {
                $errorMessage = $this->parseErrorMessage($courseResult, 'Get course info');
                throw new \Exception($errorMessage);
            }
            $this->course = $courseResult->getContent();

            // adjust dates temporarily and store old dates, just in case.
            $changeCourseDatesResult = \CanvasApi::using('courses')
                ->addParameters([
                    'course' => [
                        'start_at'                             => now()->subDays(1)->format('Y-m-d\TH:i\Z'),
                        'end_at'                               => now()->addDays(1)->format('Y-m-d\TH:i\Z'),
                        'restrict_enrollments_to_course_dates' => true
                    ]
                ])->updateCourse($courseId);
            if ($changeCourseDatesResult->getStatus() !== 'success') {
                $errorMessage = $this->parseErrorMessage($changeCourseDatesResult, 'Change course dates');
                throw new \Exception($errorMessage);
            }
            $this->datesChanged = true;

            // do enrollment
            if ($this->manualEnrollment->batch->destination_type === 'course') {
                $result = \CanvasApi::using('enrollments')
                    ->addParameters($parameters)
                    ->enrollUserInCourse($this->manualEnrollment->batch->destination_id);
            } else {
                $result = \CanvasApi::using('enrollments')
                    ->addParameters($parameters)
                    ->enrollUserInSection($this->manualEnrollment->batch->destination_id);
            }

            if ($result->getStatus() === 'success') {
                $this->manualEnrollment->status = 'uploaded';
                $this->manualEnrollment->canvas_enrollment_id = $result->getContent()->id;
                $this->manualEnrollment->save();

                \Log::channel('manual-enrollments')->info('Manual Enrollment processed successfully', [
                    'category'  => 'manual-enrollments',
                    'operation' => 'process',
                    'result'    => 'success',
                    'data'      => [
                        'manual_enrollment_id' => $this->manualEnrollment->id,
                        'enrolled_user_id'     => $this->manualEnrollment->enrolled_user_id,
                        'batch_info'           => $this->manualEnrollment->batch,
                    ]
                ]);

                event(new ManualEnrollmentProcessed($this->manualEnrollment));

                // if enrollment dates were adjusted, adjust them back.
                $this->resetCourseEnrollmentDates();

                return;
            }

            $errorMessage = $this->parseErrorMessage($result, 'Add enrollment');
            throw new \Exception($errorMessage);
        } catch (\Exception $e) {
            $this->errorMessage = $e->getMessage();

            \Log::channel('manual-enrollments')->error('Error processing Manual Enrollment', [
                'category'  => 'manual-enrollments',
                'operation' => 'process',
                'result'    => 'error',
                'data'      => [
                    'manual_enrollment_id' => $this->manualEnrollment->id,
                    'enrolled_user_id'     => $this->manualEnrollment->enrolled_user_id,
                    'batch_info'           => $this->manualEnrollment->batch,
                    'message'              => $this->errorMessage,
                ]
            ]);

            if ($this->datesChanged) {
                $this->resetCourseEnrollmentDates();
            }

            throw $e;
        }
    }

    public function failed(\Exception $e)
    {
        $this->manualEnrollment->status = 'error';
        $this->manualEnrollment->notes = $e->getMessage() ?? 'unknown error';
        $this->manualEnrollment->save();

        event(new ManualEnrollmentProcessed($this->manualEnrollment));
    }
}
