<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class InspectAndReplaceCourseNav implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $courseId;
    protected $navItem;
    protected $navItemToReplaceWith;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($courseId, $navItem, $navItemToReplaceWith)
    {
        $this->courseId = $courseId;
        $this->navItem = $navItem;
        $this->navItemToReplaceWith = $navItemToReplaceWith;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // get current nav
        $courseNavResult = \CanvasApi::using('tabs')->listTabsForCourse($this->courseId);
        $tabs = collect($courseNavResult->getContent());

        $tab = $tabs->where('label', $this->navItem)->where('hidden', false)->first();

        if (!is_null($tab)) {
            $position = $tab->position;

            \CanvasApi::using('tabs')
                ->addParameters(['hidden' => true])
                ->updateTabForCourse($this->courseId, $tab->id);

            $newTab = $tabs->where('label', $this->navItemToReplaceWith)->first();

            if (!is_null($newTab)) {
                \CanvasApi::using('tabs')
                    ->addParameters(['hidden' => false, 'position' => $position])
                    ->updateTabForCourse($this->courseId, $newTab->id);
            }
        }
    }
}
