<?php

namespace App\Jobs;

use App\LtiReportItem;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use App\Exceptions\CourseDataRetrievalException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetEnrollmentDataForLtiReportItem implements ShouldQueue
{
    public $tries = 5;

    protected $reportItem;

    private $result;

    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(LtiReportItem $reportItem)
    {
        $this->onQueue('cst_api');
        $this->reportItem = $reportItem;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Redis::throttle('canvas_api')
            ->allow(120)
            ->every(60)
            ->block(60)
            ->then(function () {
                $this->execute();
            }, function () {
                // could not obtain lock
                \Log::channel('queue')->debug('Job ' . $this->job->getJobId() . ' could not obtain lock.');
                return $this->release(60);
            });
    }

    public function execute()
    {
        try {
            \DB::beginTransaction();
            $courseId = json_decode($this->reportItem->report_data)->context_id;

            $options = [
                'include' => [
                    'total_students',
                    'teachers',
                    'concluded',
                    'term'
                ],
            ];

            if ($this->reportItem->lti_report->include_deleted) {
                $options['include'][] = 'all_courses';
            }

            // step 1 - get course data and proceed with enrollment data only if the filters check out
            $this->result = \CanvasApi::using('courses')
                ->addParameters($options)
                ->getCourse($courseId);

            if ($this->result->getStatus() !== 'success') {
                if ($this->result->getLastResult()['code'] == 404) {
                    throw new CourseDataRetrievalException('Course ' . $courseId . ' not found; assumed deleted.'); // don't fail the job, this is OK.
                }

                throw new \Exception('Error: could not look up course ' . $courseId . '; API returned ' . $this->result->getLastResult()['code'] . ' with message ' . $this->result->getLastResult()['reason']); // fail the job
            }


            // filter out unwanted items
            $filters = json_decode($this->reportItem->lti_report->filters, true);
            if (isset($filters['terms'])) {
                if (!in_array($this->result->getContent()->enrollment_term_id, $filters['terms'])) {
                    $this->reportItem->delete();
                    \DB::commit();
                    return;
                }
            }

            // pare down and keep only what we care about
            $courseData = $this->result->getContent();
            collect([
                'uuid',
                'grading_standard_id',
                'default_view',
                'license',
                'public_syllabus',
                'public_syllabus_to_auth',
                'is_public_to_auth_users',
                'apply_assignment_group_weights',
                'calendar',
                'time_zone',
                'blueprint',
                'sis_import_id',
                'integration_id',
                'enrollments',
                'restrict_enrollments_to_course_dates',
            ])->each(function ($unset) use (&$courseData) {
                unset($courseData->$unset);
            });

            $this->reportItem->course_data = json_encode($courseData);

            // step 2 - detailed enrollment data
            $this->result = \CanvasApi::using('enrollments')
                ->setPerPage(100)
                ->listCourseEnrollments($courseId);


            if ($this->result->getStatus() !== 'success') {
                if ($this->result->getLastResult()['code'] === 404) {
                    throw new CourseDataRetrievalException('Course ' . $courseId . ' not found; assumed deleted.'); // don't fail the job, this is OK.
                }

                throw new \Exception('Error: could not look up course ' . $courseId . '; API returned ' . $this->result->getLastResult()['code'] . ' with message ' . $this->result->getLastResult()['reason']); // fail the job
            }

            // pare down and keep only what we care about
            $enrollments = collect($this->result->getContent())->mapWithKeys(function ($enrollment) {
                return [
                    $enrollment->user_id => [
                        'user_id'          => $enrollment->user_id,
                        'login_id'         => $enrollment->user->login_id ?? '',
                        'type'             => $enrollment->type,
                        'role'             => $enrollment->role,
                        'enrollment_state' => $enrollment->enrollment_state,
                        'last_activity_at' => $enrollment->last_activity_at,
                        'name'             => $enrollment->user->name
                    ]
                ];
            })->groupBy('type');

            $this->reportItem->course_enrollment_data = json_encode($enrollments);

            $this->reportItem->response_code = $this->result->getLastResult()['code'];
            $this->reportItem->response_message = $this->result->getLastResult()['reason'];
            $this->reportItem->status = 'complete';
            $this->reportItem->save();
            \DB::commit();
        } catch (CourseDataRetrievalException $e) {
            \Log::channel('lti-reports')->notice($e->getMessage());

            $this->reportItem->response_code = $this->result->getLastResult()->code;
            $this->reportItem->response_message = $this->result->getLastResult()->reason;
            $this->reportItem->status = 'complete';
            $this->reportItem->save();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::channel('lti-reports')->error('ERROR while getting data for LTI Report Item '
                . $this->reportItem->id . ': ' . $e->getMessage());

            \DB::transaction(function () {
                if (isset($this->result)) {
                    $this->reportItem->response_code = $this->result['response']['httpCode'];
                    $this->reportItem->response_message = $this->result['response']['httpReason'];
                }
                $this->reportItem->status = 'error';
                $this->reportItem->save();
            });

            throw $e;
        }
    }
}
