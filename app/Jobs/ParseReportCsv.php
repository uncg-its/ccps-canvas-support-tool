<?php

namespace App\Jobs;

use App\LtiReport;
use App\LtiReportItem;
use GuzzleHttp\Client;
use League\Csv\Reader;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ParseReportCsv implements ShouldQueue
{
    protected $report;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(LtiReport $report)
    {
        $this->onQueue('cst_api');
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $contents = file_get_contents($this->report->attachment_url);
        // Guzzle time.
        $client = new Client;

        $options = [];
        if (config('ccps.http_proxy.enabled')) {
            $options = [
                'proxy' => config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port')
            ];
        }

        $contents = $client->get($this->report->attachment_url, $options)->getBody()->getContents();

        $reader = Reader::createFromString($contents);
        $reader->setHeaderOffset(0);
        $header = $reader->getHeader();
        $reportItems = collect($reader->getRecords());

        try {
            \DB::beginTransaction();

            $reportItems->each(function ($item) use ($header) {
                $reportData = array_combine($header, $item);

                // pare down
                collect(['account_name', 'tool_created_at', 'launch_url', 'custom_fields'])
                    ->each(function ($unset) use (&$reportData) {
                        unset($reportData[$unset]);
                    });

                $ltiReportItem = LtiReportItem::create([
                    'lti_report_id' => $this->report->id,
                    'tool_name'     => $reportData['tool_type_name'],
                    'report_data'   => json_encode($reportData)
                ]);

                // job will be kicked off in model observer
            });

            $this->report->parsed = true;
            $this->report->save();

            \DB::commit();
        } catch (\Exception $e) {
            \Log::channel('lti-reports')->warning('LTI Report generation failed. ' . $e->getMessage());
            \DB::rollBack();
            throw $e; // fail
        }
    }
}
