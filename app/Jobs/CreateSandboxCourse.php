<?php

namespace App\Jobs;

use App\SandboxCourse;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uncgits\CanvasApi\Exceptions\CanvasApiException;

class CreateSandboxCourse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $course;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SandboxCourse $course)
    {
        $this->onQueue('medium');
        $this->course = $course;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // status to 'in-progress'
        $this->course->status = 'in-progress';
        $this->course->save();

        try {
            if (is_null($this->course->canvas_course_id)) {
                $accountId = config('cst.key_account_ids.sandbox');

                // create the course
                $courseResult = \CanvasApi::using('courses')
                ->addParameters([
                    'course' => [
                        'name'        => $this->course->batch->course_name . " ({$this->course->instructor_username})",
                        'course_code' => $this->course->batch->course_code . "-{$this->course->instructor_username}",
                    ]
                ])->createCourse($accountId);

                if ($courseResult->getStatus() !== 'success') {
                    throw new CanvasApiException('Error creating course in Canvas: ' . $courseResult->getLastResult()['code'] . ': ' . $courseResult->getLastResult()['reason']);
                }

                $courseId = $courseResult->getContent()->id;
                $this->course->canvas_course_id = $courseId;
                $this->course->save();
            } else {
                $courseId = $this->course->canvas_course_id;
            }

            // enroll the instructor
            $enrollmentResult = \CanvasApi::using('enrollments')
                ->addParameters([
                    'enrollment' => [
                        'user_id'          => 'sis_login_id:' . $this->course->instructor_username,
                        'type'             => 'TeacherEnrollment',
                        'enrollment_state' => 'active',
                        'notify'           => false,
                    ]
                ])->enrollUserInCourse($courseId);

            if ($enrollmentResult->getStatus() !== 'success') {
                throw new CanvasApiException('Error creating enrollment in Canvas: ' . $enrollmentResult->getLastResult()['code'] . ': ' . $courseResult->getLastResult()['reason']);
            }

            // status to 'completed'
            $this->course->status = 'completed';
            $this->course->completed_at = now();
            $this->course->canvas_enrollment_id = $enrollmentResult->getContent()->id;
            $this->course->save();

            dispatch(new CheckSandboxCourseBatchStatus($this->course->batch));
        } catch (\Throwable $th) {
            // status to 'pending' for retry
            $this->course->status = 'pending';
            $this->course->save();

            throw $th;
        }
    }

    public function failed(\Exception $e)
    {
        $this->course->status = 'failed';
        $this->course->save();
    }
}
