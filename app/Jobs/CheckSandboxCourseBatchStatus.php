<?php

namespace App\Jobs;

use App\SandboxCourseBatch;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckSandboxCourseBatchStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $batch;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SandboxCourseBatch $batch)
    {
        $this->onQueue('low');
        $this->batch = $batch;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->batch->sandbox_courses()->whereIn('status', ['pending', 'in-progress'])->count() === 0) {
            if ($this->batch->sandbox_courses()->where('status', 'failed')->count() !== 0) {
                $this->batch->status = 'failed';
            } else {
                $this->batch->status = 'completed';
            }
            $this->batch->save();
        }
    }
}
