<?php

namespace App\Jobs;

use App\NavItemReportItem;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Middleware\LimitNavItemReports;
use App\Exceptions\CourseDataRetrievalException;

class ProcessNavItemReportItem implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $item;

    public $tries = 25;
    public $maxExceptions = 1;

    public function middleware()
    {
        return [new LimitNavItemReports];
    }

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(NavItemReportItem $item)
    {
        $this->onQueue('low');
        $this->item = $item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // fetch the nav from Canvas
        $navResult = \CanvasApi::using('tabs')
            ->listTabsForCourse($this->item->canvas_course_id);

        if ($navResult->getStatus() !== 'success') {
            throw new CourseDataRetrievalException('Could not fetch Navigation for course: ' . $this->item->canvas_course_id . '. API error: ' . $navResult->getLastResult()['code'] . ' - ' . $navResult->getLastResult()['reason']);
        }

        $this->item->nav_items = $navResult->getContent();
        $this->item->save();

        dispatch(new CheckNavItemReportCompletion($this->item->report));
    }
}
