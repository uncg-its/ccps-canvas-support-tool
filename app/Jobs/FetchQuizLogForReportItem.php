<?php

namespace App\Jobs;

use App\QuizLogReportItem;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Middleware\LimitQuizLogReports;

class FetchQuizLogForReportItem implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 25;
    public $maxExceptions = 1;
    protected $item;

    public function middleware()
    {
        return [new LimitQuizLogReports]; // all of these jobs should go through rate limiting
    }

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(QuizLogReportItem $item)
    {
        $this->onQueue('low');
        $this->item = $item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Note: quiz log auditing must be enabled...

        // get the quiz log
        $eventsResult = \CanvasApi::using('QuizSubmissionEvents')
            ->retrieveCapturedEvents($this->item->course_id, $this->item->quiz_id, $this->item->submission_id);

        if ($eventsResult->getStatus() !== 'success') {
            throw new \Exception('Submission ' . $this->item->submission_id . ': API responded with ' . $eventsResult->getLastResult()['code'] . ' - ' . $eventsResult->getLastResult['reason']);
        }

        $events = collect($eventsResult->getContent());

        // parse it. this is hairy.

        $submissionInfo = $events->where('event_type', 'submission_created')->first();
        if (is_null($submissionInfo)) {
            throw new \Exception('Could not get submission info from submission ' . $this->item->submission_id);
        }

        $quizVersion = $submissionInfo->event_data->quiz_version;

        $questions = collect($submissionInfo->event_data->quiz_data)->mapWithKeys(function ($question) {
            return [
                $question->id => [
                    'position' => $question->position,
                    'name'     => $question->name,
                    'text'     => $question->question_text,
                    'type'     => $question->question_type,
                    'answers'  => collect($question->answers)->keyBy('id')
                ]
            ];
        });

        $events = $events->whereNotIn('event_type', ['submission_created'])->keyBy('created_at');

        $this->item->event_data = [
            'quiz_version' => $quizVersion,
            'questions'    => $questions,
            'events'       => $events,
        ];
        $this->item->status = 'processing';
        $this->item->save();

        // dispatch a processing job
        dispatch(new ProcessQuizLog($this->item));
    }

    public function failed(\Exception $e)
    {
        $this->item->status = 'failed';
        $this->item->save();
    }
}
