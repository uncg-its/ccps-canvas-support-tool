<?php

namespace App\Jobs;

use League\Csv\Writer;
use App\QuizLogReportFile;
use App\QuizLogReportItem;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Middleware\LimitQuizLogReports;

class ProcessQuizLog implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 25;
    public $maxExceptions = 1;
    protected $item;

    public function middleware()
    {
        return [new LimitQuizLogReports]; // all of these jobs should go through rate limiting
    }

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(QuizLogReportItem $item)
    {
        $this->onQueue('low');
        $this->item = $item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // collect information about the user and quiz
        $userResult = \CanvasApi::using('users')
            ->showUserDetails($this->item->user_id);
        $user = $userResult->getContent();

        $quizResult = \CanvasApi::using('quizzes')
            ->getSingleQuiz($this->item->course_id, $this->item->quiz_id);
        $quiz = $quizResult->getContent();


        $events = collect($this->item->event_data['events'])->map(function ($event, $time) use ($user, $quiz) {
            $questionMap = $this->item->event_data['questions'];

            $question = '';
            $answer = '';
            $interacted = [];
            $userAgent = '';
            $score = '';
            $timeSpent = '';
            $pointsPossible = '';

            $data = $event['event_data']; // contains actual event information
            if (!is_null($data)) {
                if (isset($data[0]['quiz_question_id'])) {
                    // an answered question
                    $question = $questionMap[$data[0]['quiz_question_id']];
                    $interacted[] = $question['position'] . ' (' . $question['name'] . ')';
                    $answer = $data[0]['answer'];
                    if (isset($question['answers'][$answer])) {
                        $answer = $question['answers'][$answer]['text'];
                    }
                } elseif (isset($data['user_agent'])) {
                    $userAgent = $data['user_agent'];
                    // append quiz score and time information to this row only.
                    $score = $this->item->submission_data['score'];
                    $pointsPossible = $this->item->submission_data['quiz_points_possible'];
                    $timeSpent = $this->item->submission_data['time_spent'];
                } elseif (isset($data['flagged'])) {
                    $question = $data['questionId'];
                    $interacted[] = $questionMap[$question]['position'] . ' (' . $questionMap[$question]['name'] . ')';
                } else {
                    // one or more viewed questions - in an array
                    foreach ($data as $question) {
                        $interacted[] = $questionMap[$question]['position'] . ' (' . $questionMap[$question]['name'] . ')';
                    }
                }
            }
            return [
                'event_id'           => $event['id'],
                'user_id'            => $this->item->user_id,
                'user_login_id'      => $user->login_id,
                'quiz_id'            => $this->item->quiz_id,
                'quiz_title'         => $quiz->title,
                'submission_id'      => $this->item->submission_id,
                'quiz_version'       => $this->item->event_data['quiz_version'],
                'time'               => $time,
                'event'              => $event['event_type'],
                'user_agent'         => $userAgent,
                'questions'          => implode(',', $interacted),
                'answer'             => $answer,
                'score'              => $score,
                'points_possible'    => $pointsPossible,
                'time_spent_seconds' => $timeSpent,
            ];
        });

        $reportData = [
            'quiz'   => $quiz->title,
            'user'   => $user->login_id,
            'events' => $events
        ];

        // generate a CSV titled by quiz and taker
        $filename = \Str::snake(\Str::lower(now()->format('Ymd_His_') . $reportData['quiz'] . '_' . $reportData['user'])) . '.csv';
        $path = storage_path('app/reports/quiz-logs/' . $filename);
        $writer = Writer::createFromPath($path, 'w+');
        $headers = array_keys($reportData['events']->first());

        $writer->insertOne($headers);
        $writer->insertAll($reportData['events']);

        $file = QuizLogReportFile::create([
            'quiz_log_report_item_id' => $this->item->id,
            'file_type'               => 'csv',
            'filename'                => $filename,
        ]);

        $this->item->status = 'completed';
        $this->item->save();

        // queue a followup job to check status of the other jobs.
        dispatch(new CheckQuizLogReportStatus($this->item->quiz_log_report));
    }

    public function failed(\Exception $e)
    {
        $this->item->status = 'failed';
        $this->item->save();
    }
}
