<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\CourseReportItem;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetCourseActivity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 100;
    public $maxExceptions = 10;

    /**
     * @var CourseReportItem
     */
    private $item;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CourseReportItem $item)
    {
        $this->onQueue('low');
        $this->delay(15);

        $this->item = $item;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $throttleConfig = config('cst.throttling');

        if ($throttleConfig['active']) {
            \Redis::throttle('canvas-api')
                ->allow($throttleConfig['job_limit'])
                ->every($throttleConfig['timespan_seconds'])
                ->then(function () {
                    $this->execute();
                }, function () use ($throttleConfig) {
                    // could not obtain lock.
                    return $this->release($throttleConfig['release_seconds']);
                });
        } else {
            $this->execute();
        }
    }

    protected function execute()
    {
        try {
            DB::beginTransaction();

            // first, get participation data.
            $result = \CanvasApi::using('analytics')->getCourseLevelParticipationData($this->item->course_information->id);
            $unpublishedCourse = $result->getLastResult()['code'] === 404;
            if ($result->getStatus() !== 'success') {
                // there was a problem
                if (! $unpublishedCourse) {
                    \Log::channel('course-activity-report')->error('Problem during getCourseLevelParticipationData call for course: ' . $this->item->course_information->id . '. Result: ' . json_encode($result->getContent()));
                    throw new \Exception('There was a problem with the API call.');
                }
            }

            $sinceString = $this->item->report->options->since ?: '1/1/2000';
            $since = Carbon::parse($sinceString);

            $untilString = $this->item->report->options->until ?: '1/1/2099';
            $until = Carbon::parse($untilString);


            $lastView = null;
            $lastParticipation = null;
            $totalViews = 0;
            $totalParticipations = 0;

            if (! $unpublishedCourse) {
                foreach ($result->getContent() as $activity) {
                    $date = Carbon::parse($activity->date);
                    if ($date > $lastView && $activity->views > 0) {
                        $lastView = $date;
                    }

                    if ($date > $lastParticipation && $activity->participations > 0) {
                        $lastParticipation = $date;
                    }

                    if ($date->gte($since) && $date->lt($until)) {
                        $totalParticipations += $activity->participations;
                        $totalViews += $activity->views;
                    }
                }
            }

            $statistics = [
                'last_student_view'          => optional($lastView)->toDateString(),
                'last_student_participation' => optional($lastParticipation)->toDateString(),
                'total_participations'       => $totalParticipations,
                'total_views'                => $totalViews,
            ];

            // next, get teacher enrollments to parse their activity
            $options = [
                'type' => 'TeacherEnrollment' // will include Leaders as well since they are based off of Teacher
            ];

            $result = \CanvasApi::using('enrollments')
                ->addParameters($options)
                ->listCourseEnrollments($this->item->course_information->id);

            if ($result->getStatus() !== 'success') {
                // there was a problem
                \Log::channel('course-activity-report')->error('Problem during listEnrollmentsForCourse call for course: ' . $this->item->course_information->id . '. Result: ' . json_encode($result));
                throw new \Exception('There was a problem with the API call.');
            }

            $lastTeacherActivity = null;

            $teachers = [];
            $totalActivitySeconds = 0;

            foreach ($result->getContent() as $enrollment) {
                // activity
                $date = Carbon::parse($enrollment->last_activity_at);
                if ($date > $lastTeacherActivity) {
                    $lastTeacherActivity = $date;
                }

                // activity seconds
                $totalActivitySeconds += $enrollment->total_activity_time;

                // teacher info
                $teachers[] = [
                    'id'          => $enrollment->user->id,
                    'name'        => $enrollment->user->name,
                    'sis_user_id' => $enrollment->user->sis_user_id,
                    'login_id'    => $enrollment->user->login_id ?? '',
                ];
            }

            $statistics['last_teacher_activity'] = optional($lastTeacherActivity)->toDateString();
            $statistics['total_activity_seconds'] = $totalActivitySeconds;

            $courseInfo = $this->item->course_information;
            $courseInfo->teachers = $teachers;

            $this->item->update([
                'course_report_id'   => $this->item->report->id,
                'course_information' => json_encode($courseInfo),
                'statistics'         => json_encode($statistics),
                'status'             => 'complete'
            ]);

            dispatch(new CheckCourseActivityCompletion($this->item->report));

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::channel('course-activity-report')->error('Error when getting course activity from API. ' . $e->getMessage());
            throw $e;
        }
    }

    public function failed($e)
    {
        \Log::channel('course-activity-report')->error('Job failed! Report Item ' . $this->item->id . '; exception message: ' . $e->getMessage());

        // mark item with 'error' status
        $this->item->update([
            'course_report_id' => $this->item->report->id,
            'status'           => 'error'
        ]);

        dispatch(new CheckCourseActivityCompletion($this->item->report));
    }
}
