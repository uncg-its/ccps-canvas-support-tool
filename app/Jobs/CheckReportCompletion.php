<?php

namespace App\Jobs;

use App\LtiReport;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckReportCompletion implements ShouldQueue
{
    protected $report;

    public $tries = 3;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(LtiReport $report)
    {
        $this->onQueue('cst_api');
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            \DB::beginTransaction();

            $result = \CanvasApi::using('AccountReports')
                ->getStatusOfReport($this->report->account_id, $this->report->report_name, $this->report->canvas_report_id);

            if ($result->getStatus() !== 'success') {
                $message = 'API Error when checking status of report ' . $this->report->id
                    . ': ' . $result['response']['httpReason'];
                \Log::channel('lti-reports')->error($message);
                throw new \Exception($message); // fail the job
            }

            $reportStatus = $result->getContent();
            $newStatus = $reportStatus->status;

            $this->report->canvas_report_status = $newStatus;

            if ($newStatus !== 'complete') {
                $this->report->save();
                $job = dispatch(new CheckReportCompletion($this->report))
                        ->delay(now()->addMinutes(1));
                return;
            }

            $this->report->attachment_url = $reportStatus->attachment->url;
            $this->report->completed_at = Carbon::parse($reportStatus->ended_at)->toDateTimeString();
            $this->report->save();
            \DB::commit();

            $job = dispatch(new ParseReportCsv($this->report))->delay(now()->addSeconds(15));
        } catch (\Exception $e) {
            \Log::channel('lti-reports')->error('ERROR: could not check report completion. ' . $e->getMessage());
            \DB::rollBack();
            throw $e;
        }
    }
}
