<?php

namespace App\Jobs;

use App\NavItemReport;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckNavItemReportCompletion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $report;

    public $tries = 25;
    public $maxExceptions = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(NavItemReport $report)
    {
        $this->onQueue('low');
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pendingItems = $this->report->items()->whereNull('nav_items')->count();

        if ($pendingItems === 0) {
            $this->report->status = 'completed';
            $this->report->save();
        }
    }
}
