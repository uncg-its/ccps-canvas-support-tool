<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\EnrollmentRequest;

class EnrollmentRequestSubmitted extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;


    protected $enrollmentRequest;

    protected $appUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(EnrollmentRequest $enrollmentRequest)
    {
        $this->enrollmentRequest = $enrollmentRequest;

        $this->appUrl = config('app.url');

        $this->onQueue('low');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.enrollmentrequests.submitted')
            ->subject('Canvas Enrollment Request Submitted')
            ->with([
                'enrollmentRequest' => $this->enrollmentRequest,
                'appUrl'            => $this->appUrl . '/canvas/enrollmentrequests/show?requestId=' . $this->enrollmentRequest->requestId,
            ]);
    }
}
