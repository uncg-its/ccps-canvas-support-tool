<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LtiReportItem extends Model
{
    protected $guarded = [];

    // relationships

    public function lti_report()
    {
        return $this->belongsTo(LtiReport::class);
    }

    // accessors

    public function getCourseIdAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        return json_decode($this->course_data)->id;
    }

    public function getCourseNameAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        return json_decode($this->course_data)->name;
    }

    public function getCourseStatusAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        return json_decode($this->course_data)->workflow_state;
    }

    public function getTeachersCountAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        return collect(json_decode($this->course_enrollment_data))
            ->only(['TeacherEnrollment'])
            ->flatten()
            ->count();
    }

    public function getTeachersUsernamesAttribute()
    {
        if (is_null($this->course_enrollment_data)) {
            return collect([]);
        }

        return collect(json_decode($this->course_enrollment_data))
            ->only(['TeacherEnrollment'])
            ->flatten()
            ->pluck('login_id')
            ->unique();
    }

    public function getTasCountAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        return collect(json_decode($this->course_enrollment_data))
            ->only(['TaEnrollment'])
            ->flatten()
            ->count();
    }

    public function getTasUsernamesAttribute()
    {
        if (is_null($this->course_enrollment_data)) {
            return collect([]);
        }

        return collect(json_decode($this->course_enrollment_data))
            ->only(['TaEnrollment'])
            ->flatten()
            ->pluck('login_id')
            ->unique();
    }

    public function getStudentsCountAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        return json_decode($this->course_data)->total_students;
    }

    public function getAccountIdAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        return json_decode($this->course_data)->account_id;
    }

    public function getAccountNameAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        return Account::where('canvas_id', $this->accountId)->first()->name;
    }

    public function getTermAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        return json_decode($this->course_data)->term->name;
    }

    public function getAccountTreeAttribute()
    {
        if (is_null($this->course_data)) {
            return null;
        }

        $tree = [];
        $accounts = Account::all()->keyBy('canvas_id');

        $id = $this->accountId;
        do {
            if (!isset($accounts[$id])) {
                break;
            }
            $tree[] = $accounts[$id];
            $id = $accounts[$id]->parent_account_id;
        } while (!is_null($id));

        return collect(array_reverse($tree));
    }

    public function getCourseAffiliationAttribute()
    {
        if (is_null($this->course_data)) {
            return [
                'type'       => 'DELETED',
                'department' => null,
                'unit'       => null
            ];
        }

        $tree = $this->accountTree;

        if ($tree->contains('canvas_id', config('cst.lti_reports.key_account_ids.courses'))) {
            return [
                'type'       => 'Courses',
                'department' => $tree->pop()->name,
                'unit'       => $tree->pop()->name
            ];
        }

        if ($tree->contains('canvas_id', config('cst.lti_reports.key_account_ids.noncourses'))) {
            return [
                'type'       => 'Noncourses',
                'department' => null,
                'unit'       => null
            ];
        }

        if ($tree->contains('canvas_id', config('cst.lti_reports.key_account_ids.sandbox'))) {
            return [
                'type'       => 'Sandbox',
                'department' => null,
                'unit'       => null
            ];
        }

        return [
            'type'       => 'Unknown',
            'department' => null,
            'unit'       => null
        ];
    }
}
