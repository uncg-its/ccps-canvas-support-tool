<?php

namespace App;

use App\CcpsCore\User;
use App\Models\ManualEnrollment;
use Illuminate\Database\Eloquent\Model;

class ManualEnrollmentBatch extends Model
{
    protected $guarded = ['id', 'updated_at'];

    // relationships

    public function manual_enrollments()
    {
        return $this->hasMany(ManualEnrollment::class, 'batch_id');
    }

    public function created_by_user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
