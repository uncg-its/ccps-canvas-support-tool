<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NavItemReportItem extends Model
{
    protected $casts = [
        'nav_items' => 'array'
    ];

    protected $guarded = [];

    // relationships

    public function nav_item_report()
    {
        return $this->belongsTo(NavItemReport::class);
    }

    // aliases

    public function report()
    {
        return $this->nav_item_report();
    }
}
