<?php

namespace App\Observers;

use App\Jobs\GetCourseActivity;
use App\CourseReportItem;

class CourseReportItemObserver
{
    /**
     * Handle the org report item "created" event.
     *
     * @param  \App\CourseReportItem $orgReportItem
     *
     * @return void
     */
    public function created(CourseReportItem $courseReportItem)
    {
        dispatch(new GetCourseActivity($courseReportItem));
    }

    /**
     * Handle the org report item "updated" event.
     *
     * @param  \App\CourseReportItem $courseReportItem
     *
     * @return void
     */
    public function updated(CourseReportItem $courseReportItem)
    {
        //
    }

    /**
     * Handle the org report item "deleted" event.
     *
     * @param  \App\CourseReportItem $courseReportItem
     *
     * @return void
     */
    public function deleted(CourseReportItem $courseReportItem)
    {
        //
    }

    /**
     * Handle the org report item "restored" event.
     *
     * @param  \App\CourseReportItem $courseReportItem
     *
     * @return void
     */
    public function restored(CourseReportItem $courseReportItem)
    {
        //
    }

    /**
     * Handle the org report item "force deleted" event.
     *
     * @param  \App\CourseReportItem $courseReportItem
     *
     * @return void
     */
    public function forceDeleted(CourseReportItem $courseReportItem)
    {
        //
    }
}
