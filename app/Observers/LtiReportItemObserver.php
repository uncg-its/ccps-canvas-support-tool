<?php

namespace App\Observers;

use App\LtiReportItem;
use App\Jobs\GetEnrollmentDataForLtiReportItem;

class LtiReportItemObserver
{
    public function created(LtiReportItem $ltiReportItem)
    {
        dispatch(new GetEnrollmentDataForLtiReportItem($ltiReportItem))->delay(now()->addSeconds(15));
    }
}
