<?php

namespace App\Providers;

use App\LtiReportItem;
use App\CourseReportItem;
use Illuminate\Support\ServiceProvider;
use App\Observers\LtiReportItemObserver;
use App\Observers\CourseReportItemObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        CourseReportItem::observe(CourseReportItemObserver::class);
        LtiReportItem::observe(LtiReportItemObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
