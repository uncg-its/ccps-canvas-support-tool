<?php

namespace App\Providers;

use App\Events\ManualEnrollmentProcessed;
use Illuminate\Support\Facades\Event;
use App\Listeners\CcpsCore\NotificationSubscriber;
use App\Listeners\UpdateManualEnrollmentBatchStatus;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $subscribe = [
        NotificationSubscriber::class,
    ];

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        // Database errors
        'App\Events\CcpsCore\UncaughtQueryException' => [
            'App\Listeners\CcpsCore\LogUncaughtQueryException',
        ],

        // Email
        'Illuminate\Mail\Events\MessageSent' => [
            'App\Listeners\CcpsCore\LogSentEmail',
        ],

        // Cron Jobs
        'App\Events\CcpsCore\CronjobStarted' => [
            'App\Listeners\CcpsCore\PreCronjob',
        ],
        'App\Events\CcpsCore\CronjobFinished' => [
            'App\Listeners\CcpsCore\PostCronjob',
        ],

        // Login / Logout
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\CcpsCore\LogLoginData',
            'App\Listeners\CcpsCore\RecordLoginTimestampInUsersTable'
        ],
        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\CcpsCore\LogLogoutData',
        ],
        'Illuminate\Auth\Events\Failed' => [
            'App\Listeners\CcpsCore\LogFailedLoginData',
        ],

        // ACL
        'App\Events\CcpsCore\AclChanged' => [
            '\App\Listeners\CcpsCore\LogAclChange'
        ],

        // Cache
        'App\Events\CcpsCore\CacheCleared' => [
            'App\Listeners\CcpsCore\LogCacheClear',
        ],

        // Config
        'App\Events\CcpsCore\ConfigUpdated' => [
            'App\Listeners\CcpsCore\LogConfigUpdate',
        ],

        // Backups
        'Spatie\Backup\Events\BackupManifestWasCreated' => [
            'App\Listeners\CcpsCore\LogBackupManifestInfo',
        ],

        'Spatie\Backup\Events\BackupZipWasCreated' => [
            'App\Listeners\CcpsCore\LogBackupZipCreationInfo',
        ],

        'Spatie\Backup\Events\BackupWasSuccessful' => [
            'App\Listeners\CcpsCore\LogSuccessfulBackup',
        ],

        'Spatie\Backup\Events\BackupHasFailed' => [
            'App\Listeners\CcpsCore\LogFailedBackup',
        ],

        'Spatie\Backup\Events\CleanupWasSuccessful' => [
            'App\Listeners\CcpsCore\LogSuccessfulCleanup'
        ],

        'Spatie\Backup\Events\CleanupHasFailed' => [
            'App\Listeners\CcpsCore\LogFailedCleanup'
        ],

        // Socialite (Azure)
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\Azure\AzureExtendSocialite@handle',
        ],

        // Notification Logging
        'Illuminate\Notifications\Events\NotificationSent' => [
            'App\Listeners\CcpsCore\LogNotificationSent',
        ],

        ManualEnrollmentProcessed::class => [
            UpdateManualEnrollmentBatchStatus::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
