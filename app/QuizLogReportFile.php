<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizLogReportFile extends Model
{
    protected $guarded = [];

    // relationships

    public function quiz_log_report_item()
    {
        return $this->belongsTo(QuizLogReportItem::class);
    }

    // aliases

    public function item()
    {
        return $this->quiz_log_report_item();
    }
}
