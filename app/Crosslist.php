<?php

namespace App;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crosslist extends Model
{
    use HasFactory;

    protected $casts = [
        'parent_course_meta' => 'object',
        'errors'             => 'array'
    ];

    protected $guarded = ['id'];

    public function performed_by()
    {
        return $this->belongsTo(User::class, 'performed_by_id');
    }
}
