<?php

namespace App\Policies;

use App\CcpsCore\User;
use App\Models\EnrollmentRequest;
use Illuminate\Auth\Access\HandlesAuthorization;

class EnrollmentRequestPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->hasRole(['admin', 'canvas.admin']) || $user->hasPermission('enrollmentrequests.admin')) {
            return true;
        }
    }
    /**
     * Determine whether the user can view all enrollment request.
     *
     * @param  \App\CcpsCore\User  $user
     * @return mixed
     */
    public function all(User $user)
    {
        return $user->hasPermission('enrollmentrequests.read');
    }

    /**
     * Determine whether the user can view the enrollment request.
     *
     * @param  \App\CcpsCore\User  $user
     * @param  \App\Models\EnrollmentRequest  $enrollmentRequest
     * @return mixed
     */
    public function view(User $user, EnrollmentRequest $enrollmentRequest)
    {
        return $user->email == $enrollmentRequest->requesterEmailAddress && $user->hasPermission('enrollmentrequests.read');
    }

    /**
     * Determine whether the user can create enrollment requests.
     *
     * @param  \App\CcpsCore\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermission('enrollmentrequests.create');
    }

    /**
     * Determine whether the user can update the enrollment request.
     *
     * @param  \App\CcpsCore\User  $user
     * @param  \App\Models\EnrollmentRequest  $enrollmentRequest
     * @return mixed
     */
    public function update(User $user, EnrollmentRequest $enrollmentRequest)
    {
        return $user->hasPermission('enrollmentrequests.update') && $user->email == $enrollmentRequest->requesterEmailAddress;
    }

    /**
     * Determine whether the user can delete the enrollment request.
     *
     * @param  \App\CcpsCore\User  $user
     * @param  \App\Models\EnrollmentRequest  $enrollmentRequest
     * @return mixed
     */
    public function delete(User $user, EnrollmentRequest $enrollmentRequest)
    {
        return $user->hasPermission('enrollmentrequests.delete') && $user->email == $enrollmentRequest->requesterEmailAddress;
    }
}
