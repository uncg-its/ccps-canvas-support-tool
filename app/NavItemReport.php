<?php

namespace App;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;

class NavItemReport extends Model
{
    protected $guarded = [];

    // relationships

    public function created_by_user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function nav_item_report_items()
    {
        return $this->hasMany(NavItemReportItem::class);
    }

    // aliases

    public function items()
    {
        return $this->nav_item_report_items();
    }
}
