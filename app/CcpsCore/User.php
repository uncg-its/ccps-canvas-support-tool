<?php

namespace App\CcpsCore;

use App\LtiReport;
use App\LiveLookup;
use App\CourseReport;
use App\NavItemReport;
use App\QuizLogReport;
use App\Crosslist;
use App\SandboxCourseBatch;
use App\ManualEnrollmentBatch;
use App\Models\EnrollmentRequest;
use Uncgits\Ccps\Models\User as BaseModel;

class User extends BaseModel
{
    public function crosslists()
    {
        return $this->hasMany(Crosslist::class, 'performed_by_id');
    }

    public function nav_item_reports()
    {
        return $this->hasMany(NavItemReport::class, 'created_by');
    }

    public function course_reports()
    {
        return $this->hasMany(CourseReport::class);
    }

    public function lti_reports()
    {
        return $this->hasMany(LtiReport::class, 'requestor_id');
    }

    public function approved_enrollment_requests()
    {
        return $this->hasMany(EnrollmentRequest::class, 'approver_id');
    }

    public function rejected_enrollment_requests()
    {
        return $this->hasMany(EnrollmentRequest::class, 'rejector_id');
    }

    public function manual_enrollment_batches()
    {
        return $this->hasMany(ManualEnrollmentBatch::class, 'created_by');
    }

    public function live_lookups()
    {
        return $this->hasMany(LiveLookup::class);
    }

    public function quiz_log_reports()
    {
        return $this->hasMany(QuizLogReport::class, 'created_by');
    }

    public function sandbox_course_batches()
    {
        return $this->hasMany(SandboxCourseBatch::class, 'created_by_id');
    }
}
