<?php

namespace App;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;

class LtiReport extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at',
        'completed_at'
    ];

    // public $with = ['lti_report_items'];

    // relationships

    public function requestor()
    {
        return $this->belongsTo(User::class, 'requestor_id');
    }

    public function lti_report_items()
    {
        return $this->hasMany(LtiReportItem::class);
    }

    // accessors

    public function getLocalStatusAttribute()
    {
        if (!$this->parsed) {
            return 'pending';
        }

        $pendingJobs = $this->items()->where('status', 'pending')->count();
        $errorJobs = $this->items()->where('status', 'error')->count();

        if ($pendingJobs > 0) {
            return 'pending';
        }

        if ($errorJobs > 0) {
            return 'complete (with errors)';
        }

        return 'complete';
    }

    public function getProgressAttribute()
    {
        $items = $this->items()->count();
        $pending = $this->items()->where('status', 'pending')->count();

        return $items - $pending . ' / ' . $items;
    }

    public function getLocalStatusIconAttribute()
    {
        switch ($this->localStatus) {
            case 'pending':
                return 'fas fa-pause';
            case 'complete':
                return 'fas fa-check';
            case 'complete (with errors)':
                return 'fas fa-exclamation-circle';
        }
    }

    public function getLocalStatusClassAttribute()
    {
        switch ($this->localStatus) {
            case 'pending':
                return 'info';
            case 'complete':
                return 'success';
            case 'complete (with errors)':
                return 'warning';
        }
    }

    // aliases

    public function items()
    {
        return $this->lti_report_items();
    }
}
