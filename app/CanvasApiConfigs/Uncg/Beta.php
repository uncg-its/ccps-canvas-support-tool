<?php

namespace App\CanvasApiConfigs\Uncg;

use Uncgits\CanvasApi\CanvasApiConfig;

class Beta extends CanvasApiConfig
{
    public function __construct()
    {
        $this->setApiHost(config('canvas-api.configs.uncg.beta.host'));
        $this->setToken(config('canvas-api.configs.uncg.beta.token'));

        if (config('canvas-api.configs.uncg.beta.proxy.use', false)) {
            $this->setUseProxy(true);
            $this->setProxyHost(config('canvas-api.configs.uncg.beta.proxy.host'));
            $this->setProxyPort(config('canvas-api.configs.uncg.beta.proxy.port'));
        }
    }
}
