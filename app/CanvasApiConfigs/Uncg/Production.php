<?php

namespace App\CanvasApiConfigs\Uncg;

use Uncgits\CanvasApi\CanvasApiConfig;

class Production extends CanvasApiConfig
{
    public function __construct()
    {
        $this->setApiHost(config('canvas-api.configs.uncg.production.host'));
        $this->setToken(config('canvas-api.configs.uncg.production.token'));

        if (config('canvas-api.configs.uncg.production.proxy.use', false)) {
            $this->setUseProxy(true);
            $this->setProxyHost(config('canvas-api.configs.uncg.production.proxy.host'));
            $this->setProxyPort(config('canvas-api.configs.uncg.production.proxy.port'));
        }
    }
}
