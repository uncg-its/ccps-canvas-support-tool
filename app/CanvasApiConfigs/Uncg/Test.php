<?php

namespace App\CanvasApiConfigs\Uncg;

use Uncgits\CanvasApi\CanvasApiConfig;

class Test extends CanvasApiConfig
{
    public function __construct()
    {
        $this->setApiHost(config('canvas-api.configs.uncg.test.host'));
        $this->setToken(config('canvas-api.configs.uncg.test.token'));

        if (config('canvas-api.configs.uncg.test.proxy.use', false)) {
            $this->setUseProxy(true);
            $this->setProxyHost(config('canvas-api.configs.uncg.test.proxy.host'));
            $this->setProxyPort(config('canvas-api.configs.uncg.test.proxy.port'));
        }
    }
}
