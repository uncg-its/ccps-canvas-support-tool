<?php

namespace App\Events;

use App\Models\ManualEnrollment;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ManualEnrollmentProcessed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $manualEnrollment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ManualEnrollment $manualEnrollment)
    {
        $this->manualEnrollment = $manualEnrollment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
