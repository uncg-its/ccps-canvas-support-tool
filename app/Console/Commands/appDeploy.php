<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use Uncgits\Ccps\Exceptions\LockFileWriteException;
use Uncgits\Ccps\Helpers\Composer;

class appDeploy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:deploy {--p|prompt : Steps through each choice with yes/no prompts, allowing you to choose which to apply}';

    /**
     * Name of the signature file that we will install in upgrades/ folder after successful init (with extension)
     *
     * @var string
     */
    protected $signatureFilename = 'app-deploy.txt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy instance of this CCPS CST';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {
            $this->info('Initializing CCPS CST Instance ...');

            $this->checkForSignatureFile();

            // ------ fill in Mail info
            $this->setUpVarsInEnv();

            // ------ write signature file
            $this->writeSignatureFile();

            // FINAL CONFIRMATION
            $this->info('------------------------------------------');
            $this->info('CCPS CST DEPLOYMENT IS COMPLETE!');
            $this->info('------------------------------------------');

        } catch(\Exception $e) {
            $this->error('CCPS CST Deployment Failed: ' . $e->getMessage());
        }

        return true;

    }

    protected function setUpVarsInEnv() {
        if (!$this->option('prompt') || $this->confirm('Set up Canvas variables in .env?')) {

            $fields = [
                    'CANVAS_URL' => ['type' => 'choice', 'values' => ['uncg.test.instructure.com','uncg.beta.instructure.com','uncg.instructure.com'], 'extraText' => 'IE: uncg.test.instructure.com'],
                    'CANVAS_API_KEY' => ['type' => 'anticipate', 'values' => [''], 'extraText' => 'Copied from the Profile page inside Canvas'],
                    'CANVAS_API_CACHING' => ['type' => 'choice', 'values' => ['on','off'], 'extraText' => 'On or Off'],
                    'CANVAS_API_CACHE_MINUTES' => ['type' => 'choice', 'values' => ['0','5', '10', '30', '60', '120', '720', '1440'], 'extraText' => 'Number of minutes for the cached API data'],
                    'CANVAS_DEBUG_MODE' => ['type' => 'choice', 'values' => ['true','false'], 'extraText' => 'True or False to toggle debug mode'],
                    'CANVAS_NOTIFICATION_MODE' => ['type' => 'choice', 'values' => ['','flash','log', 'flash,log'], 'extraText' => 'No notifications, flash, log, or both'],
                    'ENROLLMENT_REQUEST_NOTIFICATION_EMAIL' => ['type' => 'choice', 'values' => ['lms-admins-l@uncg.edu',''], 'extraText' => 'LMS Admins, Blank (Fill in later)'],
                    'ENROLLMENT_REQUEST_EMAIL_DOMAIN' => ['type' => 'choice', 'values' => ['uncg.edu','uncg.net',''], 'extraText' => 'uncg.edu/net, Blank (Fill in later)'],
            ];

            $this->updateEnvWithUserInput($fields);

        } else {
            $this->info('Skipped Canvas setup');
        }
    }

    protected function writeSignatureFile() {
        $installedUpgradesFolder = base_path('upgrades');
        if (!File::isDirectory($installedUpgradesFolder)) {
            $this->info('/upgrades folder does not exist; creating.');
            File::makeDirectory($installedUpgradesFolder);
        }

        // is app/upgrades folder writable?
        if (!File::isWritable($installedUpgradesFolder)) {
            throw new \Exception('app/upgrades folder is not writable! Cannot write signature file. Check permissions.');
        }

        // put timestamp data in new signature file, to be inserted into app/upgrades
        $contents = Carbon::now()->toDateTimeString();
        $bytesWritten = File::put($installedUpgradesFolder . '/' . $this->signatureFilename, $contents);
        if ($bytesWritten === false) {
            throw new Exception('Initialization completed but corresponding signature file could not be written to app/upgrades. Please check before continuing.');
        } else {
            $this->info('Initialization signature file ' . $this->signatureFilename . ' written to /upgrades successfully.');
        }
    }

    protected function checkForSignatureFile() {
        $commandName = $this->getName();
        if (File::exists(base_path('upgrades/' . $this->signatureFilename))) {
            $this->error('WARNING: It appears that you have already run the ' . $commandName . ' command!');
            if (!$this->confirm('Continue anyway?')) {
                throw new \Exception('Operation aborted');
            }
        }
    }

    protected function updateEnvWithUserInput($fields, $propertyToUpdate = null) {
        $env = file_get_contents('.env');

        foreach($fields as $field => $info) {
            $placeholder = '*' . $field . '*';
            $promptText = 'Value for ' . $field;

            if (isset($info['extraText'])) {
                $promptText .= ' ' . $info['extraText'];
            }

            if (strpos($env, $placeholder)) {
                $inputType = $info['type'];
                switch ($inputType) {
                    case 'anticipate':
                    case 'choice':
                        $value = $this->$inputType($promptText, $info['values']);
                        break;
                    case 'secret':
                    case 'ask':
                        $value = $this->$inputType($promptText);
                        break;
                    default:
                        $value = $this->ask($promptText);
                        break;
                }

                $env = str_replace($placeholder, $value, $env);

                // update local object property if we need to do so
                if (!is_null($propertyToUpdate)) {
                    $this->$propertyToUpdate[$field] = $value;
                }

                $this->info('Value set.');
            } else {
                $this->info('Placeholder value for ' . $field . ' not found - value must already be entered. Skipping.');
            }
        }

        if (file_put_contents('.env', $env)) {
            $this->info('New .env written successfully.');
        } else {
            $this->error('New .env file could not be written!');
        }
    }
}
