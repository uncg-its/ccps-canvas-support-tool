<?php


namespace App\Traits;

trait HasSuccessStatus
{
    public function getStatusIconAttribute()
    {
        switch ($this->status) {
            case 'pending':
            case 'pending approval':
                return 'fas fa-pause';
            case 'approved':
                return 'fas fa-thumbs-up';
            case 'completed':
                return 'fas fa-check';
            case 'rejected':
                return 'fas fa-times';
            default:
                throw new \OutOfBoundsException('Invalid Status ID - could not parse status icon');
        }
    }

    public function getStatusClassAttribute()
    {
        switch ($this->status) {
            case 'pending':
            case 'pending approval':
                return 'warning';
            case 'approved':
                return 'info';
            case 'completed':
                return 'success';
            case 'rejected':
                return 'danger';
            default:
                throw new \OutOfBoundsException('Invalid Status ID - could not parse status icon');
        }
    }
}
