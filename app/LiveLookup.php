<?php

namespace App;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;

class LiveLookup extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'created_at' => 'datetime'
    ];

    // relationships

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
