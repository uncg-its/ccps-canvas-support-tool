<?php

use App\CcpsCore\Role;
use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class OverhaulAcl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $rolesToDelete = [
            'manualenrollments.viewer',
            'manualenrollments.editor',
            'crosslist.manager',
            'lti.reader',
            'lti.admin',
            'enrollmentrequests.reader',
            'enrollmentrequests.requester',
            'enrollmentrequests.admin',
            'page-views.reader',
            'nav-item-replacer.editor',
            'course-activity-report.editor',
            'lti-reports.viewer',
            'lti-reports.creator',
        ];

        Role::whereIn('name', $rolesToDelete)->delete();

        Permission::where('name', 'canvas_courses.view')->update(['name' => 'canvas.courses.view']);
        Permission::where('name', 'canvas_courses.create')->update(['name' => 'canvas.courses.create']);
        Permission::where('name', 'canvas_users.view')->update(['name' => 'canvas.users.view']);
        Permission::where('name', 'canvas_accounts.view')->update(['name' => 'canvas.accounts.view']);

        Permission::where('name', 'canvas.admin')->delete();

        $ltiPermissions = Permission::whereIn('name', ['lti-reports.view', 'lti-reports.create'])->get();

        Role::where('name', 'canvas.admin')->first()->permissions()->attach($ltiPermissions);

        $permission = Permission::create([
            'source_package' => 'app',
            'name'           => 'canvas.courses.edit',
            'display_name'   => 'Canvas Courses - Edit',
            'description'    => 'Edit Canvas Course Details',
            'created_at'     => now(),
            'updated_at'     => now(),
        ]);

        Role::whereIn('name', ['admin', 'canvas.admin'])->get()->each(function ($role) use ($permission) {
            $role->permissions()->attach($permission);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // there is no going back. it's for your own good.
    }
}
