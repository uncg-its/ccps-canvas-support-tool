<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsToLtiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create LTI Tools Table

        Schema::table('lti_tools', function (Blueprint $table) {
            $table->timestamps();
        });

        // Create LTI Tools Table
        Schema::table('lti_inventory', function (Blueprint $table) {
            $table->timestamps();
        });

        // Create LTI Tools Table
        Schema::table('lti_enrollment', function (Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
