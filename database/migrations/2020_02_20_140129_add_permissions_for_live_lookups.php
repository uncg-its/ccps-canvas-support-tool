<?php

use App\CcpsCore\Role;
use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class AddPermissionsForLiveLookups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            $permission = Permission::create([
                'source_package' => 'app',
                'name'           => 'live-lookups.view',
                'display_name'   => 'Live Lookups Log - View',
                'description'    => 'Can view the Live Lookups Log',
            ]);

            $roles = Role::whereIn('name', ['admin', 'canvas.admin'])->get();

            $roles->each(function ($role) use ($permission) {
                $role->permissions()->attach($permission);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            $permission = Permission::where('name', 'live-lookups.view')->findOrFail();

            $roles = Role::whereIn('name', ['admin', 'canvas.admin'])->get();

            $roles->each(function ($role) use ($permission) {
                $role->permissions()->detach($permission);
            });

            $permission->delete();
        });
    }
}
