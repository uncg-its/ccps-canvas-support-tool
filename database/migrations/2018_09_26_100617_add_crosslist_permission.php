<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use App\CcpsCore\User;

class AddCrosslistPermission extends Migration
{
    // role/permission bindings
    protected $bindings;

    /**
     * Constructor
     */
    public function __construct() {
        $this->bindings = [
            'crosslist.manager' => [
                'crosslist.create',
                'crosslist.delete',
            ],
            'admin' => [
                'crosslist.create',
                'crosslist.delete',
            ],
        ];
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\CrosslistRolesSeeder',
            '--force' => 'true'
        ]);


        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\CrosslistPermissionsSeeder',
            '--force' => 'true'
        ]);


        // attach the roles and permissions to one another
        foreach ($this->bindings as $role => $permissions) {
            $roleModel = Role::where('name', $role)->first();
            $roleModel->attachPermissions($permissions);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
