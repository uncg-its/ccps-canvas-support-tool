<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLtiReportItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lti_report_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lti_report_id');
            $table->string('tool_name');
            $table->json('report_data');
            $table->json('course_data')->nullable();
            $table->json('course_enrollment_data')->nullable();
            $table->enum('status', ['pending', 'complete', 'error'])->default('pending');
            $table->unsignedSmallInteger('response_code')->nullable();
            $table->string('response_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lti_report_items');
    }
}
