<?php

use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class OverhaulManualEnrollmentsAcl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::where('name', 'manualenrollments.create')->update([
            'name'         => 'manual-enrollments.create',
            'display_name' => 'Manual Enrollments - create',
            'description'  => 'Create Manual Enrollment requests'
        ]);

        Permission::where('name', 'manualenrollments.read')->update([
            'name'         => 'manual-enrollments.read',
            'display_name' => 'Manual Enrollments - read',
            'description'  => 'View Manual Enrollment requests'
        ]);


        Permission::where('name', 'manualenrollments.update')->update([
            'name'         => 'manual-enrollments.update',
            'display_name' => 'Manual Enrollments - update',
            'description'  => 'Update Manual Enrollment requests'
        ]);

        Permission::where('name', 'manualenrollments.delete')->update([
            'name'         => 'manual-enrollments.delete',
            'display_name' => 'Manual Enrollments - delete',
            'description'  => 'Delete Manual Enrollment requests'
        ]);

        Permission::where('name', 'manualenrollments.batch')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
