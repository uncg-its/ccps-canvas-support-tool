<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateManualenrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manualenrollments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('course_or_section');
            $table->string('type');
            $table->string('user_id')->nullable();
            $table->string('course_or_section_id')->nullable();
            $table->integer('role_id')->unsigned();
            $table->enum('status', ['pending-approval','approved-pending-upload','successfully-uploaded','error','canceled']);
            $table->longText('notes')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manualenrollments');
    }
}
