<?php

use App\CcpsCore\Role;
use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class AddBatchSandboxCoursePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            $permissions = [
                [
                    'source_package' => 'app',
                    'name'           => 'batch-sandbox.view',
                    'display_name'   => 'Batch Sandbox Courses - View',
                    'description'    => 'Can view Batch Sandbox Course operations',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'batch-sandbox.create',
                    'display_name'   => 'Batch Sandbox Courses - Create',
                    'description'    => 'Can create Batch Sandbox Courses',
                ],
            ];

            $roles = Role::whereIn('name', ['admin', 'canvas.admin', 'itc'])->get();

            foreach ($permissions as $permission) {
                $newPermission = Permission::create($permission);
                $roles->each(function ($role) use ($newPermission) {
                    $role->permissions()->attach($newPermission);
                });
            }

            // admin

            $permissions = [
                [
                    'source_package' => 'app',
                    'name'           => 'batch-sandbox.admin',
                    'display_name'   => 'Batch Sandbox Courses - Admin',
                    'description'    => 'Can administer and view all Batch Sandbox Courses',
                ],
            ];

            $roles = Role::whereIn('name', ['admin', 'canvas.admin'])->get();

            foreach ($permissions as $permission) {
                $newPermission = Permission::create($permission);
                $roles->each(function ($role) use ($newPermission) {
                    $role->permissions()->attach($newPermission);
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            $permissions = Permission::whereIn('name', ['batch-sandbox.view','batch-sandbox.create'])->get();

            $roles = Role::whereIn('name', ['admin', 'canvas.admin', 'itc'])->get();

            $roles->each(function ($role) use ($permissions) {
                $role->permissions()->detach($permissions);
            });

            $permissions->each(function ($permission) {
                $permission->delete();
            });

            // admin

            $permissions = Permission::whereIn('name', ['batch-sandbox.admin'])->get();

            $roles = Role::whereIn('name', ['admin', 'canvas.admin'])->get();

            $roles->each(function ($role) use ($permissions) {
                $role->permissions()->detach($permissions);
            });

            $permissions->each(function ($permission) {
                $permission->delete();
            });
        });
    }
}
