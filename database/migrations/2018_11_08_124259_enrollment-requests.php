<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnrollmentRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollment_requests', function (Blueprint $table) {
            $table->bigIncrements('requestId');
            $table->unsignedBigInteger('requestTimestamp')->default(0);
            $table->string('requesterEmailAddress', 255)->nullable();
            $table->string('requesterAffiliation', 255)->nullable();
            $table->string('sis_course_id', 255)->nullable();
            $table->string('activeCourse')->default(0);
            $table->string('enrolledPersonName', 255)->nullable();
            $table->string('enrolledPersonUsername', 255)->nullable();
            $table->string('enrolledPersonEmailAddress', 255)->nullable();
            $table->string('enrolledPersonAffiliation', 255)->nullable();
            $table->string('enrolledPersonRole', 255)->nullable();
            $table->string('enrolledPersonRoleId', 255)->nullable();
            $table->string('enrolledPersonFerpaAttestation', 255)->nullable();
            $table->unsignedBigInteger('enrollmentBeginTimestamp')->default(0);
            $table->unsignedBigInteger('enrollmentEndTimestamp')->nullable();
            $table->string('enrollmentReasonCategory', 255)->nullable();
            $table->longText('enrollmentReason');
            $table->longText('enrollmentNotes');
            $table->enum('requestStatus', ['pending-approval','approved-pending-upload','rejected','canceled','deactivate', 'complete-in-canvas'])->nullable();
            $table->longText('adminNotes')->nullable();
            $table->longText('enrolledPersonMetadata')->nullable();
            $table->longText('courseMetadata')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
