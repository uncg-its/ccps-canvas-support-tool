<?php

use Carbon\Carbon;
use App\Models\EnrollmentRequest;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConvertTimestampFieldsOnEnrollmentRequestsTableToCarbon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->timestamp('begins_at')->nullable()->after('enrollmentBeginTimestamp');
            $table->timestamp('ends_at')->nullable()->after('enrollmentEndTimestamp');
        });

        $existingRequests = EnrollmentRequest::all();
        $existingRequests->each(function ($request) {
            $request->begins_at = Carbon::createFromTimestamp($request->enrollmentBeginTimestamp)->toDateTimeString();
            $request->ends_at = Carbon::createFromTimestamp($request->enrollmentEndTimestamp)->toDateTimeString();
            $request->save();
        });

        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->dropColumn('enrollmentBeginTimestamp');
            $table->dropColumn('enrollmentEndTimestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            Schema::table('enrollment_requests', function (Blueprint $table) {
                $table->unsignedBigInteger('enrollmentBeginTimestamp')->default(0)->after('begins_at');
                $table->unsignedBigInteger('enrollmentEndTimestamp')->nullable()->after('ends_at');
            });

            $existingRequests = EnrollmentRequest::all();
            $existingRequests->each(function ($request) {
                $request->enrollmentBeginTimestamp = $request->begins_at->getTimestamp();
                $request->enrollmentEndTimestamp = $request->ends_at->getTimestamp();
                $request->save();
            });

            Schema::table('enrollment_requests', function (Blueprint $table) {
                $table->dropColumn('begins_at');
                $table->dropColumn('ends_at');
            });
        });
    }
}
