<?php

use App\CcpsCore\Role;
use App\CcpsCore\User;
use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class AddItcRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            $itcRole = Role::create([
                'source_package' => 'app',
                'name'           => 'itc',
                'display_name'   => 'ITC',
                'description'    => 'Instructional Technology Consultant, support role for academic units on campus',
                'editable'       => false
            ]);

            $itcPermissions = Permission::whereIn('name', [
                'enrollmentrequests.create',
                'enrollmentrequests.read',
            ])->get();

            $itcRole->attachPermissions($itcPermissions);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $itcRole = Role::where('name', 'itc')->with('permissions')->firstOrFail();
        $itcRole->syncPermissions([]);

        User::whereRoleIs('itc')->get()->each(function ($user) use ($itcRole) {
            $user->detachRoles([$itcRole]);
        });

        $itcRole->delete();
    }
}
