<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use App\CcpsCore\User;

class AddCanvasPermission extends Migration
{
    // role/permission bindings
    protected $bindings;

    /**
     * Constructor
     */
    public function __construct() {
        $this->bindings = [
            'admin' => [
                'canvas.admin',
            ],
        ];
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\CanvasadminPermissionsSeeder',
            '--force' => 'true'
        ]);


        // attach the roles and permissions to one another
        foreach ($this->bindings as $role => $permissions) {
            $roleModel = Role::where('name', $role)->first();
            $roleModel->attachPermissions($permissions);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
