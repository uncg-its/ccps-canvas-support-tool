<?php

use App\CcpsCore\Role;
use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class AddCanvasSupportRoleToAcl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = Role::firstOrCreate(
            ['name' => 'canvas.support'],
            [
                'source_package' => 'app',
                'display_name' => 'Canvas - Support',
                'description' => 'Support Agent for Canvas (non-administrator)',
                'created_at'     => now(),
                'updated_at'     => now(),
            ]
        );

        $permissionsToGrant = Permission::whereIn('name', ['page-views.read', 'enrollmentrequests.create', 'enrollmentrequests.read', 'enrollmentrequests.update', 'enrollmentrequests.delete', 'enrollmentrequests.admin', 'canvas.accounts.view', 'canvas.courses.view', 'canvas.users.view'])->get();

        $role->permissions()->attach($permissionsToGrant);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // there is no going back. it's for your own good.
    }
}
