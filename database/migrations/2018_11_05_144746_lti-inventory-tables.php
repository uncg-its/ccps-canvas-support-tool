<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LtiInventoryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create LTI Tools Table
        Schema::create('lti_tools', function (Blueprint $table) {
            $table->bigIncrements('toolId')->unsigned();
            $table->string('ltiName', 180)->nullable();
            $table->string('ltiDescription', 180)->nullable();
            $table->bigInteger('lastUpdated')->unsigned()->default(0);
        });

        // Create LTI Tools Table
        Schema::create('lti_inventory', function (Blueprint $table) {
            $table->bigIncrements('inventoryId');
            $table->bigInteger('toolId')->unsigned();
            $table->string('sis_course_id', 180)->nullable();
            $table->bigInteger('lastUpdated')->unsigned()->default(0);
            $table->bigInteger('lastEnrollmentUpdate')->nullable();
            $table->foreign('toolId')->references('toolId')->on('lti_tools')->onDelete('cascade')->onUpdate('cascade');
        });

        // Create LTI Tools Table
        Schema::create('lti_enrollment', function (Blueprint $table) {
            $table->bigIncrements('enrollmentId');
            $table->bigInteger('inventoryId')->unsigned();
            $table->string('user_id', 180)->nullable();
            $table->bigInteger('lastUpdated')->unsigned()->default(0);
            $table->foreign('inventoryId')->references('inventoryId')->on('lti_inventory')->onDelete('cascade')->onUpdate('cascade');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
