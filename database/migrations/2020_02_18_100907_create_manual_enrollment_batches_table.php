<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManualEnrollmentBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual_enrollment_batches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('created_by')->nullable();
            $table->string('destination_type');
            $table->string('destination_id')->nullable();
            $table->text('status');
            $table->longText('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manual_enrollment_batches');
    }
}
