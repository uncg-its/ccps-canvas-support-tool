<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrosslistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crosslists', function (Blueprint $table) {
            $table->id();
            $table->foreignId('performed_by_id');
            $table->string('parent_course_sis_id');
            $table->text('child_section_sis_ids');
            $table->json('parent_course_meta');
            $table->text('errors')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crosslists');
    }
}
