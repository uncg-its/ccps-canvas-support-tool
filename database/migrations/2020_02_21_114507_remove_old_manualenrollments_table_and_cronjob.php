<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;

class RemoveOldManualenrollmentsTableAndCronjob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::dropIfExists('manualenrollments');

        CronjobMeta::where('class', 'App\Cronjobs\ProcessManualEnrollments')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // can't go back.
    }
}
