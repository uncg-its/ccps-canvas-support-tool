<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowNullOnEnrollmentNotesForEnrollmentRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->longText('enrollmentNotes')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->longText('enrollmentNotes')->nullable(false)->change();
        });
    }
}
