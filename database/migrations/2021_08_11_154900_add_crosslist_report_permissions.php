<?php

use App\CcpsCore\Role;
use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class AddCrosslistReportPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            $permissions = [
                [
                    'source_package' => 'app',
                    'name'           => 'crosslist-report.view',
                    'display_name'   => 'Crosslist Report - View',
                    'description'    => 'Can view Crosslist Report',
                ],
            ];

            $roles = Role::whereIn('name', ['admin', 'canvas.admin'])->get();

            foreach ($permissions as $permission) {
                $newPermission = Permission::create($permission);
                $roles->each(function ($role) use ($newPermission) {
                    $role->permissions()->attach($newPermission);
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            $permissions = Permission::whereIn('name', ['crosslist-report.view'])->get();

            $roles = Role::whereIn('name', ['admin', 'canvas.admin'])->get();

            $roles->each(function ($role) use ($permissions) {
                $role->permissions()->detach($permissions);
            });

            $permissions->each(function ($permission) {
                $permission->delete();
            });
        });
    }
}
