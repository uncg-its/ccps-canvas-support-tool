<?php

use Illuminate\Database\Migrations\Migration;

class ChangeStatusToEnumForManualEnrollments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // removed and replaced due to issues with deployment. This was fixed in the earlier migration, and manually handled in the already deployed apps
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
