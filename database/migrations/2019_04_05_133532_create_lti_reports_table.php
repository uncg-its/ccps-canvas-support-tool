<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLtiReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lti_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('requestor_id');
            $table->unsignedSmallInteger('account_id');
            $table->string('report_name');
            $table->json('filters');
            $table->unsignedInteger('canvas_report_id');
            $table->string('canvas_report_status');
            $table->text('attachment_url')->nullable();
            $table->boolean('parsed')->default(false);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('completed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lti_reports');
    }
}
