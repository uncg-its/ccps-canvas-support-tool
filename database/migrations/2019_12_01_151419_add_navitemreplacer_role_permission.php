<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use App\CcpsCore\User;

class AddNavitemreplacerRolePermission extends Migration
{
    // role/permission bindings
    protected $bindings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bindings = [
            'nav-item-replacer.editor' => [
                'nav-item-replacer.edit',
            ],
            'canvas.admin'             => [
                'nav-item-replacer.edit',
            ],
        ];
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\Permissions20190104Seeder',
            '--force' => 'true'
        ]);

        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\Roles20190104Seeder',
            '--force' => 'true'
        ]);

        // attach the roles and permissions to one another
        foreach ($this->bindings as $role => $permissions) {

            $roleModel = Role::where('name', $role)->first();
            $roleModel->attachPermissions($permissions);

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            // remove permission
            $permission = Permission::where('name', 'nav-item-replacer.edit')->firstOrFail();
            $permission->roles()->sync([]);
            $permission->users()->sync([]);
            $permission->delete();

            // remove role
            $role = Role::where('name', 'nav-item-replacer.editor')->firstOrFail();
            $role->users()->sync([]);
            $role->delete();
        });

    }
}
