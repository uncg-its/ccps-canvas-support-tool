<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizLogReportItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_log_report_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('quiz_log_report_id');
            $table->string('course_id');
            $table->string('user_id');
            $table->string('quiz_id');
            $table->string('submission_id');
            $table->json('event_data')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_log_report_items');
    }
}
