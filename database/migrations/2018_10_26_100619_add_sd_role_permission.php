<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use App\CcpsCore\User;

class AddSdRolePermission extends Migration
{
    // role/permission bindings
    protected $bindings;

    /**
     * Constructor
     */
    public function __construct() {
        $this->bindings = [
            'servicedesk.member' => [
                'canvas_courses.view',
                'canvas_users.view',
            ],
            'canvas.admin' => [
                'canvas_accounts.view',
                'canvas_courses.create',
                'canvas_courses.view',
                'canvas_users.view',
                'canvas.admin',
                'manualenrollments.batch',
                'manualenrollments.create',
                'manualenrollments.delete',
                'manualenrollments.read',
                'manualenrollments.update',
                'crosslist.create',
                'crosslist.delete'
            ],
        ];
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\Roles20181026Seeder',
            '--force' => 'true'
        ]);


        // attach the roles and permissions to one another
        foreach ($this->bindings as $role => $permissions) {
            $roleModel = Role::where('name', $role)->first();
            $roleModel->attachPermissions($permissions);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
