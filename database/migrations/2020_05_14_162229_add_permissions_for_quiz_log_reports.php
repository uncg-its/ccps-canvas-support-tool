<?php

use App\CcpsCore\Role;
use App\CcpsCore\Permission;
use Illuminate\Database\Migrations\Migration;

class AddPermissionsForQuizLogReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            $permissions = [
                [
                    'source_package' => 'app',
                    'name'           => 'quiz-log-reports.view',
                    'display_name'   => 'Quiz Log Reports - View',
                    'description'    => 'Can view the Quiz Log Reports',
                ],
                [
                    'source_package' => 'app',
                    'name'           => 'quiz-log-reports.create',
                    'display_name'   => 'Quiz Log Reports - Create',
                    'description'    => 'Can create Quiz Log Reports',
                ],
            ];

            $roles = Role::whereIn('name', ['admin', 'canvas.admin'])->get();

            foreach ($permissions as $permission) {
                $newPermission = Permission::create($permission);
                $roles->each(function ($role) use ($newPermission) {
                    $role->permissions()->attach($newPermission);
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            $permissions = Permission::whereIn('name', ['quiz-log-reports.view','quiz-log-reports.create'])->get();

            $roles = Role::whereIn('name', ['admin', 'canvas.admin'])->get();

            $roles->each(function ($role) use ($permissions) {
                $role->permissions()->detach($permissions);
            });

            $permissions->each(function ($permission) {
                $permission->delete();
            });
        });
    }
}
