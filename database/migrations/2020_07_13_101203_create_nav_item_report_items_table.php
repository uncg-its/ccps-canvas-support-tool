<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavItemReportItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nav_item_report_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('nav_item_report_id');
            $table->unsignedBigInteger('canvas_course_id');
            $table->string('canvas_course_name');
            $table->string('canvas_course_code');
            $table->json('nav_items')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nav_item_report_items');
    }
}
