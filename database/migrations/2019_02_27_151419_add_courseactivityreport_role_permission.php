<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use App\CcpsCore\User;

class AddCourseactivityreportRolePermission extends Migration
{
    // role/permission bindings
    protected $bindings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bindings = [
            'course-activity-report.editor' => [
                'course-activity-report.edit',
            ],
            'canvas.admin'                  => [
                'course-activity-report.edit',
            ],
        ];
    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\Permissions20190227Seeder',
            '--force' => 'true'
        ]);

        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\Roles20190227Seeder',
            '--force' => 'true'
        ]);

        // attach the roles and permissions to one another
        foreach ($this->bindings as $role => $permissions) {

            $roleModel = Role::where('name', $role)->first();
            $roleModel->attachPermissions($permissions);

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // remove permission
        $permission = Permission::where('name', 'course-activity-report.edit')->firstOrFail();
        $permission->roles()->sync([]);
        $permission->users()->sync([]);
        $permission->delete();

        // remove role
        $role = Role::where('name', 'course-activity-report.editor')->firstOrFail();
        $role->users()->sync([]);
        $role->delete();
    }
}
