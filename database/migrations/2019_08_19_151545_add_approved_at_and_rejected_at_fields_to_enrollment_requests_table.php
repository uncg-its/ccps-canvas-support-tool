<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApprovedAtAndRejectedAtFieldsToEnrollmentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->timestamp('approved_at')->nullable()->after('requestStatus');
            $table->unsignedInteger('approver_id')->nullable()->after('approved_at');
            $table->timestamp('rejected_at')->nullable()->after('approver_id');
            $table->unsignedInteger('rejector_id')->nullable()->after('rejected_at');
            $table->timestamp('completed_at')->nullable()->after('updated_at');

            $table->dropColumn('requestStatus');
            $table->dropColumn('adminNotes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->dropColumn('approved_at');
            $table->dropColumn('approver_id');
            $table->dropColumn('rejected_at');
            $table->dropColumn('rejector_id');
            $table->dropColumn('completed_at');

            $table->enum('requestStatus', ['pending-approval','approved-pending-upload','rejected','canceled','deactivate', 'complete-in-canvas'])->nullable()->after('enrollmentNotes');
            $table->longText('adminNotes')->nullable()->after('requestStatus');
        });
    }
}
