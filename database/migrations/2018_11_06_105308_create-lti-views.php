<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateLtiViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $tablePrefix = DB::getTablePrefix();


        DB::statement( 'CREATE VIEW ' . $tablePrefix . 'view_lti_enrollment AS SELECT
           `a`.`toolId` AS `toolId`,
           `a`.`ltiName` AS `ltiName`,
           `a`.`lastUpdated` AS `toolLastUpdated`,
           `a`.`ltiDescription` AS `ltiDescription`,
           `b`.`inventoryId` AS `inventoryId`,
           `b`.`sis_course_id` AS `sis_course_id`,
           `b`.`lastUpdated` AS `inventoryLastUpdated`,
           `c`.`enrollmentId` AS `enrollmentId`,
           `c`.`user_id` AS `user_id`,
           `c`.`lastUpdated` AS `enrollmentLastUpdated`
        FROM ((`' . $tablePrefix . 'lti_tools` `a` join `' . $tablePrefix . 'lti_inventory` `b`) join `' . $tablePrefix . 'lti_enrollment` `c`) where `a`.`toolId` = `b`.`toolId` and `b`.`inventoryId` = `c`.`inventoryId`;' );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
