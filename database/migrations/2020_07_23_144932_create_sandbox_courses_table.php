<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSandboxCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sandbox_courses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sandbox_course_batch_id');
            $table->string('instructor_username');
            $table->string('status');
            $table->string('canvas_course_id')->nullable();
            $table->string('canvas_enrollment_id')->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sandbox_courses');
    }
}
