<?php

use Illuminate\Database\Migrations\Migration;

class ChangeEnumValuesForLoginId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE " . env('DB_TABLE_PREFIX') . "manualenrollments CHANGE COLUMN user_id_type user_id_type ENUM('sis_login_id','login_id','sis_user_id','canvas_user_id')");

        $affected = DB::table('manualenrollments')->where('user_id_type', '=', 'sis_login_id')->update(['user_id_type' => 'login_id']);

        DB::statement("ALTER TABLE " . env('DB_TABLE_PREFIX') . "manualenrollments CHANGE COLUMN user_id_type user_id_type ENUM('login_id','sis_user_id','canvas_user_id')");

        $affected = DB::table('manualenrollments')->where('action', '=', null)->update(['action' => 'add']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
