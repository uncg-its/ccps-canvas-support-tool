<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRequestTimestampColumnFromEnrollmentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->dropColumn('requestTimestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->unsignedBigInteger('requestTimestamp')->default(0)->after('requestId');
        });
    }
}
