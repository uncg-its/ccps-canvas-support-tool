<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppVarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('canvas_id');
            $table->string('name');
            $table->string('workflow_state');
            $table->unsignedInteger('parent_account_id')->nullable();
            $table->unsignedInteger('root_account_id')->nullable();
            $table->string('uuid');
            $table->unsignedInteger('default_storage_quota_mb');
            $table->unsignedInteger('default_user_storage_quota_mb');
            $table->unsignedInteger('default_group_storage_quota_mb');
            $table->string('default_time_zone');
            $table->string('sis_account_id')->nullable();
            $table->unsignedInteger('sis_import_id')->nullable();
            $table->unsignedInteger('integration_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
