<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveEnrolledPersonEmailAddressColumnFromEnrollmentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->dropColumn('enrolledPersonEmailAddress');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enrollment_requests', function (Blueprint $table) {
            $table->string('enrolledPersonEmailAddress', 255)->nullable()->after('enrolledPersonUsername');
        });
    }
}
