<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseReportItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_report_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('course_report_id');
            $table->json('course_information');
            $table->json('statistics')->nullable();
            $table->enum('status', ['pending', 'complete', 'error'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_report_items');
    }
}
