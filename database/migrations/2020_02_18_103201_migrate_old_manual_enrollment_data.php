<?php

use App\ManualEnrollmentBatch;
use App\Models\ManualEnrollment;
use Illuminate\Database\Migrations\Migration;

class MigrateOldManualEnrollmentData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            $oldEnrollments = \DB::table('manualenrollments')->get()
                ->groupBy('created_at');

            $enrollmentStatusMap = [
                'pending-approval'        => 'pending',
                'approved-pending-upload' => 'approved',
                'successfully-uploaded'   => 'uploaded',
                'error'                   => 'error',
                'canceled'                => 'canceled'
            ];

            $oldEnrollments->each(function ($enrollments, $created) use ($enrollmentStatusMap) {
                // new batch
                $batch = ManualEnrollmentBatch::create([
                    'created_by'       => null, // can't tell this from existing data
                    'destination_type' => 'section',
                    'destination_id'   => $enrollments->first()->course_or_section_id,
                    'status'           => $enrollmentStatusMap[$enrollments->first()->status],
                    'notes'            => '',
                    'created_at'       => $created,
                ]);


                // attach enrollments
                $enrollments->each(function ($enrollment) use ($batch, $enrollmentStatusMap) {
                    $newEnrollment = ManualEnrollment::create([
                        'batch_id'              => $batch->id,
                        'canvas_account_id'     => $enrollment->account_id,
                        'enrolled_user_id'      => $enrollment->user_id,
                        'enrolled_user_id_type' => $enrollment->user_id_type,
                        'role_type'             => $enrollment->role_label,
                        'role_id'               => $enrollment->role_id,
                        'status'                => $enrollmentStatusMap[$enrollment->status],
                        'canvas_enrollment_id'  => $enrollment->enrollment_id,
                        'notes'                 => $enrollment->notes,
                    ]);
                });
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
