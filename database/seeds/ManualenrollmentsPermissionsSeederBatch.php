<?php

namespace App\Seeders;

use App\CcpsCore\Permission;
use Illuminate\Support\Facades\App;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class ManualenrollmentsPermissionsSeederBatch extends CcpsValidatedSeeder
{
    public $permissions = [
        [
            'name' => 'manualenrollments.batch',
            'display_name' => 'Manualenrollments - Batch',
            'description' => 'Manage Batches of Manualenrollments',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            $mergeData = [
                'source_package' => 'app',
                'created_at' => date("Y-m-d H:i:s", time()),
                'updated_at' => date("Y-m-d H:i:s", time()),
                'editable' => 1
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);

        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
