<?php

namespace App\Seeders;

use App\CcpsCore\Role;
use Illuminate\Support\Facades\App;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class LtiReportRolesTableSeeder extends CcpsValidatedSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        $roles = [
            [
                "name"         => "lti-reports.viewer",
                "display_name" => "LTI Reports - View",
                "description"  => "View LTI Reports"
            ],
            [
                "name"         => "lti-reports.creator",
                "display_name" => "LTI Reports - Create",
                "description"  => "Create LTI Reports"
            ],

        ];

        // validate
        try {
            $this->validateSeedData($roles, $this->roleArrayConstruction);
            $this->checkForExistingSeedData($roles, Role::all());

            $mergeData = [
                'source_package' => 'app',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 1
            ];

            $this->commitSeedData($roles, 'ccps_roles', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
