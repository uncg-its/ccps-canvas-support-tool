<?php

namespace App\Seeders;

use App\CcpsCore\Role;
use Illuminate\Support\Facades\App;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class Roles20181112Seeder extends CcpsValidatedSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        $roles = [
            [
                'name' => 'enrollmentrequests.reader',
                'display_name' => 'Enrollment Requests - Reader',
                'description' => 'Can perform tasks assigned for viewing Enrollment Requests Information',
            ],
            [
                'name' => 'enrollmentrequests.requester',
                'display_name' => 'Enrollment Requests - Requester',
                'description' => 'Can perform tasks assigned for requesting Enrollment Requests Information',
            ],
            [
                'name' => 'enrollmentrequests.admin',
                'display_name' => 'Enrollment Requests - Admin',
                'description' => 'Can perform tasks assigned for administering Enrollment Requests Information',
            ],
        ];



        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($roles, $this->roleArrayConstruction);
            $this->checkForExistingSeedData($roles, Role::all());

            $mergeData = [
                'source_package' => 'app',
                'created_at' => date("Y-m-d H:i:s", time()),
                'updated_at' => date("Y-m-d H:i:s", time()),
                'editable' => 1
            ];

            $this->commitSeedData($roles, 'ccps_roles', $mergeData);

        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
