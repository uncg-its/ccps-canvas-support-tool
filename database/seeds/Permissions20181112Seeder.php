<?php

namespace App\Seeders;

use App\CcpsCore\Permission;
use Illuminate\Support\Facades\App;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class Permissions20181112Seeder extends CcpsValidatedSeeder
{
    public $permissions = [
        [
            'name' => 'enrollmentrequests.create',
            'display_name' => 'Enrollment Requests - Create',
            'description' => 'Create Enrollment Requests',
        ],
        [
            'name' => 'enrollmentrequests.read',
            'display_name' => 'Enrollment Requests - Read',
            'description' => 'Read / View Enrollment Requests',
        ],
        [
            'name' => 'enrollmentrequests.update',
            'display_name' => 'Enrollment Requests - Update',
            'description' => 'Update / Edit Enrollment Requests',
        ],
        [
            'name' => 'enrollmentrequests.delete',
            'display_name' => 'Enrollment Requests - Delete',
            'description' => 'Delete Enrollment Requests',
        ],
        [
            'name' => 'enrollmentrequests.admin',
            'display_name' => 'Enrollment Requests - Admin',
            'description' => 'Administer All Enrollment Requests',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            $mergeData = [
                'source_package' => 'app',
                'created_at' => date("Y-m-d H:i:s", time()),
                'updated_at' => date("Y-m-d H:i:s", time()),
                'editable' => 1
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);

        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
