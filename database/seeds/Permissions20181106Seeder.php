<?php

namespace App\Seeders;

use App\CcpsCore\Permission;
use Illuminate\Support\Facades\App;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class Permissions20181106Seeder extends CcpsValidatedSeeder
{
    public $permissions = [
        [
            'name' => 'lti.create',
            'display_name' => 'LTI - Create',
            'description' => 'Create LTI',
        ],
        [
            'name' => 'lti.read',
            'display_name' => 'LTI - Read',
            'description' => 'Read / View LTI',
        ],
        [
            'name' => 'lti.update',
            'display_name' => 'LTI - Update',
            'description' => 'Update / Edit LTI',
        ],
        [
            'name' => 'lti.delete',
            'display_name' => 'LTI - Delete',
            'description' => 'Delete LTI',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            $mergeData = [
                'source_package' => 'app',
                'created_at' => date("Y-m-d H:i:s", time()),
                'updated_at' => date("Y-m-d H:i:s", time()),
                'editable' => 1
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);

        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
