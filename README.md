# CCPS Canvas Support Tool (CST)

This application provides administrators and support staff of Canvas with administrative tools and reporting.

# Installation

Clone the repo, then run composer install.

```
$ composer install
```

```
$ php artisan ccps:deploy

followed by

$ php artisan app:deploy
```

Check the .env file to make sure all the values are as you want them to be

```
$ vi .env
```

Migrate the database

```
$ php artisan migrate
```

Login to the app, add the permissions to the roles, and assign roles to the Admin user or anyone else necessary

If all works, you should be able to view the list of accounts in the drop-down menu as part of the manual enrollment feature
(https://app.whatever/manualenrollments)

# Version History

## 2.12.0

- Quiz results API endpoint
- Remove old "basic crosslist" and repurpose "bulk crosslist" to simply "crosslist"
- Cross-listing - reorder terms descending by date
- De-cross-listing - add brief help documentation / examples
- Basic crosslist report

## 2.11.4

- Change config for key account ids to be able to set them via ENV variables

## 2.11.3

- Adds missing column to accounts table, for account refresh cronjob

## 2.11.2

- Fix for exporting enrollments to CSV, similar to previous patch

## 2.11.1

- Fix for listing a user's current enrollments, and for pagination on same page

## 2.11

- CCPS Core 2.3

## 2.10.3

- logging for Splunk

## 2.10.2

- fix for boostrap forms `fill()` method

## 2.10.1

- Re-publish Livewire assets

## 2.10

- CCPS Core 2.2.*
- Laravel 8.0+
- PHP 8 readiness

## 2.9

- adds ability to download user enrollments list as CSV

## 2.8

- add extra info to enrollment list CSV for filtering by last activity

## 2.7

- add endpoints to retrieve quota information for user and course

## 2.6.2

- bugfix for manual enrollment retries

## 2.6.1

- Add special result parsing for conclude/delete course

## 2.6

- Adds enrollment creation date (and CSV download) to the course lookup

## 2.5.1

- Update to Laravel Framework ^7.22 (security patch)

## 2.5

- Batch Sandbox Course creation

## 2.4.1

- Nav Item Report UI tweaks

## 2.4

- Nav Item Reports
- CCPS Bot compatibility
- Fix for Account Tree generation from database, using Eloquent for better performance.

## 2.3.2

- Further tweaking for bulk crosslisting when happy-path is not followed (empty result sets, large result sets, etc.)

## 2.3.1

- Enhancements for bulk crosslisting
- Fix for local account list refresh cronjob - now recursive and traverses entire tree
- Breadcrumbs for crosslisting
- Better documentation for crosslisting tools

## 2.3

- Fix redirect destination for authenticated users
- Add bulk crosslisting

## 2.2

- Add nav tabs list to live lookup for courses

## 2.1

- CCPS Core 2.0
- Cronjob refactoring to fit within requirements of CCPS Core 2.0

## 2.0.0

- Semantic Versioning
- Implements Quiz Log Report feature

## 1.2.0

- CCPS Core upgrade to 1.6.0
- CCPS Heartbeats configuration

## 1.1.8

- LTI Report v2

## 1.1.7

- LTI Report v1

## 1.1.6

- Course Activity Report v2

## 1.1.5

- Adds Course Activity Report

## 1.1.4

- Fix for app timezone display on Page View Audit

## 1.1.3

- Page view audit, CCPS Core 1.3.0, nav item replacer

## 1.1.2

- Enrollment Request process, including email notifications

## 1.1.1 & 1.1.1.1 & 1.1.1.2

- LTI Inventory panel added and associated cron job
- 1.1.1.1,2 fixes bug that should have been caught in the release before tagging 1.1.1

## 1.1.0

- Fixed bug with logging in Manual Enrollments process.

## 1.0.9

- Added Service Desk Role, Canvas Admin Role
- Changed canvas tools panel to be permission based (laratrust)

## 1.0.8

- CCPS Core 1.2.0
- Course lookup bug for user fixed
- Azure login bug fixed

## 1.0.7

- Link added to course in canvas from course lookup result blade
- User Lookup
- Course Lookup for User
- Course details display for user

## 1.0.6

- Canvas Course Creation
- Canvas Course Lookup
- Canvas Index/Panel using Laratrust middleware
- Remove SIS ID From Course Button added to Course Show blade

## 1.0.5

- Added Canvas Accounts Lookup
- Added Admin Panel for Crosslist, Manual Enrollments, Course Settings Lookup and List Accounts

## 1.0.4

- Added loading animation for crosslist process

## 1.0.3

- Added laratrust to the crosslist controller methods

## 1.0.2

- Fixed course picker bug that didn't show up until deployment in prod

## 1.0.1

- Added course picker for crosslist flow, per request from LMS Admins

## 1.0

- Initial release
- Includes Manual Enrollments
- Includes Crosslist and De-Crosslist feature
