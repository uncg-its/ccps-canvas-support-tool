<?php

// HOME
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push("Home", route('home'));
});

// ACCOUNT
Breadcrumbs::register('account', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Account", route('account'));
});
Breadcrumbs::register('profile.show', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("My Profile", route('profile.show'));
});
Breadcrumbs::register('profile.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Edit Profile", route('profile.edit'));
});
Breadcrumbs::register('account.settings', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push("Account Settings", route('account.settings'));
});

// Notifications
Breadcrumbs::register('notifications.index', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Notifications', route('account.notifications.index'));
});
Breadcrumbs::register('notifications.channels.create', function ($breadcrumbs) {
    $breadcrumbs->parent('notifications.index');
    $breadcrumbs->push('Add New Channel', route('account.notifications.channels.create'));
});
Breadcrumbs::register('notifications.configure', function ($breadcrumbs) {
    $breadcrumbs->parent('notifications.index');
    $breadcrumbs->push('Configure', route('account.notifications.configure'));
});

// API Tokens
Breadcrumbs::register('tokens.index', function ($breadcrumbs) {
    $breadcrumbs->parent('account');
    $breadcrumbs->push('API Tokens', route('account.tokens.index'));
});
Breadcrumbs::register('tokens.create', function ($breadcrumbs) {
    $breadcrumbs->parent('tokens.index');
    $breadcrumbs->push('Create API Token', route('account.tokens.create'));
});
Breadcrumbs::register('tokens.edit', function ($breadcrumbs, $token) {
    $breadcrumbs->parent('tokens.index');
    $breadcrumbs->push('Edit API Token ' . $token->id, route('account.tokens.edit', $token));
});

// ADMIN
Breadcrumbs::register('admin', function ($breadcrumbs) {
    $breadcrumbs->push("Admin", route('admin'));
});

// Package breadcrumbs
foreach (config('ccps.modules') as $name => $module) {
    require(base_path('vendor/' . $module['package'] . '/src/breadcrumbs/' . $name . '.php'));
}

// Canvas
Breadcrumbs::register('index', function ($breadcrumbs) {
    $breadcrumbs->push('Canvas Tools', route('home'));
});

// Enrollment Requests
Breadcrumbs::register('enrollmentrequests.home', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Enrollment Requests', route('enrollmentrequests.index'));
});
Breadcrumbs::register('enrollmentrequests.index', function ($breadcrumbs) {
    $breadcrumbs->parent('enrollmentrequests.home');
    $breadcrumbs->push('My Enrollment Requests', route('enrollmentrequests.index'));
});
Breadcrumbs::register('enrollmentrequests.all', function ($breadcrumbs) {
    $breadcrumbs->parent('enrollmentrequests.home');
    $breadcrumbs->push('All Enrollment Requests', route('enrollmentrequests.all'));
});
Breadcrumbs::register('enrollmentrequests.create', function ($breadcrumbs) {
    $breadcrumbs->parent('enrollmentrequests.home');
    $breadcrumbs->push('Create New', route('enrollmentrequests.create'));
});

// manual enrollments
Breadcrumbs::register('manual-enrollments.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Manual Enrollments', route('manual-enrollments.index'));
});
Breadcrumbs::register('manual-enrollments.show', function ($breadcrumbs, $batch) {
    $breadcrumbs->parent('manual-enrollments.index');
    $breadcrumbs->push('Manual Enrollments: Batch ' . $batch->id, route('manual-enrollments.show', $batch));
});

// users
Breadcrumbs::register('live.users.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Live User Lookup', route('live.users.index'));
});
Breadcrumbs::register('live.users.show', function ($breadcrumbs, $name, $id) {
    $breadcrumbs->parent('live.users.index');
    $breadcrumbs->push('User: ' . $name, route('live.users.show', ['id_type' => 'canvas_id', 'user_id' => $id]));
});
Breadcrumbs::register('live.enrollments.index', function ($breadcrumbs, $name, $id) {
    $breadcrumbs->parent('live.users.show', $name, $id);
    $breadcrumbs->push('Enrollments: ' . $name, route('live.enrollments.index', ['user_id' => $id, 'user_name' => $name]));
});

// courses
Breadcrumbs::register('live.courses.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Live Course Lookup', route('live.courses.index'));
});
Breadcrumbs::register('live.courses.show', function ($breadcrumbs, $course) {
    $breadcrumbs->parent('live.courses.index');
    $breadcrumbs->push('Course: ' . $course->name, route('live.courses.show', ['id_type' => 'canvas_id', 'course_id' => $course->id]));
});

// live lookups
Breadcrumbs::register('live.lookups.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Live Lookup Log', route('live.lookups.index'));
});

// Cross Listing
Breadcrumbs::register('crosslist.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Cross Listing', route('crosslist.index'));
});
Breadcrumbs::register('crosslist.create', function ($breadcrumbs) {
    $breadcrumbs->parent('crosslist.index');
    $breadcrumbs->push('Crosslist', route('crosslist.create'));
});
Breadcrumbs::register('crosslist.decrosslist', function ($breadcrumbs) {
    $breadcrumbs->parent('crosslist.index');
    $breadcrumbs->push('De-Crosslist', route('crosslist.index'));
});
Breadcrumbs::register('crosslist-report.index', function ($breadcrumbs) {
    $breadcrumbs->parent('crosslist.index');
    $breadcrumbs->push('Crosslist Report', route('crosslist-report.index'));
});

// Batch Sandbox
Breadcrumbs::register('batch-sandbox.index', function ($breadcrumbs) {
    $breadcrumbs->parent('index');
    $breadcrumbs->push('Batch Sandbox Courses', route('batch-sandbox-courses.index'));
});
Breadcrumbs::register('batch-sandbox.show', function ($breadcrumbs, $batch) {
    $breadcrumbs->parent('batch-sandbox.index');
    $breadcrumbs->push('Batch ' . $batch->id, route('batch-sandbox-courses.show', $batch));
});
Breadcrumbs::register('batch-sandbox.create', function ($breadcrumbs) {
    $breadcrumbs->parent('batch-sandbox.index');
    $breadcrumbs->push('Create New Batch', route('batch-sandbox-courses.create'));
});
