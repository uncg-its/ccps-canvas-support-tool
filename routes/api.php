<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->middleware(['auth:sanctum'])->group(function () {
    Route::prefix('users')->group(function () {
        Route::get('{query}', 'Api\UserController@show');
        Route::get('{query}/courses', 'Api\UserController@courses');
        Route::get('{query}/quota', 'Api\UserController@quota');
    });

    Route::prefix('courses')->group(function () {
        Route::get('search', 'Api\CourseController@search');
        Route::get('{id}', 'Api\CourseController@show');
        Route::get('{id}/enrollments', 'Api\CourseController@enrollments');
        Route::get('{id}/quota', 'Api\CourseController@quota');
    });

    Route::prefix('quiz-results')->group(function () {
        Route::get('/', 'Api\QuizResultsController@get');
    });
});


// TODO: - contains course_id / course_name, and EUID-keyed results


// t
