<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'account'], function () {
    Route::get('/', 'CcpsCore\AccountController@index')->name('account');
    Route::get('/settings', 'CcpsCore\AccountController@settings')->name('account.settings');
    Route::post('/settings', 'CcpsCore\AccountController@updateSettings')->name('account.settings.update');
    Route::get('/profile', 'CcpsCore\AccountController@profile')->name('profile.show');
    Route::get('/profile/edit', 'CcpsCore\AccountController@editProfile')->name('profile.edit');
    Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');

    Route::prefix('tokens')->name('account.tokens.')->group(function () {
        Route::get('/', 'CcpsCore\AccountController@tokens')->name('index');
        Route::get('create', 'CcpsCore\AccountController@createToken')->name('create');
        Route::post('/', 'CcpsCore\AccountController@storeToken')->name('store');
        Route::get('{token}', 'CcpsCore\AccountController@editToken')->name('edit');
        Route::patch('{token}', 'CcpsCore\AccountController@updateToken')->name('update');
        Route::delete('{token}', 'CcpsCore\AccountController@revokeToken')->name('revoke');
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', 'CcpsCore\AccountController@notifications')->name('account.notifications.index');
        Route::get('/channels/create', 'CcpsCore\AccountController@createChannel')->name('account.notifications.channels.create');
        Route::post('/channels/create', 'CcpsCore\AccountController@storeChannel')->name('account.notifications.channels.store');
        Route::delete('/channels/{channel}', 'CcpsCore\AccountController@destroyChannel')->name('account.notifications.channels.destroy');
        Route::get(
            '/channels/{channel}/resend-verification',
            'CcpsCore\ChannelVerificationController@resend'
        )->name('account.notifications.channels.resend-verification');
        Route::get(
            '/channels/{channel}/verify',
            'CcpsCore\ChannelVerificationController@verify'
        )->name('account.notifications.channels.verify');
        Route::get(
            '/channels/{channel}/test',
            'CcpsCore\ChannelVerificationController@test'
        )->name('account.notifications.channels.test');

        Route::get('/configure', 'CcpsCore\AccountController@configure')->name('account.notifications.configure');
        Route::post('/configure', 'CcpsCore\AccountController@configureSave')->name('account.notifications.configure.save');
    });
});

// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();

// For local auth
Auth::routes();

// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', 'CcpsCore\AuthController@redirectToProvider')->name('oauth');
Route::get('auth/{provider}/callback', 'CcpsCore\AuthController@handleProviderCallback')->name('oauth.callback');

Route::get('/', 'CanvasController@index')->name('home');

Route::prefix('live')->name('live.')->group(function () {
    Route::get('accounts', 'Canvas\Live\AccountsController@index')->name('accounts.index');


    Route::prefix('courses')->name('courses.')->group(function () {
        Route::get('/', 'Canvas\Live\CourseController@index')->name('index');
        Route::get('/show', 'Canvas\Live\CourseController@show')->name('show');
        Route::get('/download-enrollments', 'Canvas\Live\CourseController@downloadEnrollments')->name('download-enrollments');
    });

    Route::prefix('users')->name('users.')->group(function () {
        Route::get('/', 'Canvas\Live\UserController@index')->name('index');
        Route::get('/show', 'Canvas\Live\UserController@show')->name('show');
        Route::get('/course', 'Canvas\Live\UserController@course')->name('course');
        Route::get('/courses', 'Canvas\Live\UserController@courses')->name('courses');
    });

    Route::prefix('enrollments')->name('enrollments.')->group(function () {
        Route::get('/', 'Canvas\Live\EnrollmentController@index')->name('index');
        Route::get('/download', 'Canvas\Live\EnrollmentController@download')->name('download-user-enrollments');
    });

    Route::prefix('lookups')->name('lookups.')->group(function () {
        Route::get('/', 'Canvas\Live\LiveLookupController@index')->name('index');
    });
});

Route::prefix('courses')->name('courses.')->group(function () {
    Route::get('/create', 'Canvas\CourseController@create')->name('create');
    Route::post('/create', 'Canvas\CourseController@store')->name('store');
    Route::patch('/update', 'Canvas\CourseController@update')->name('update');

    Route::get('/list-roles-select-element', 'Canvas\CourseController@listRolesSelectElement')->name('list-roles-select-element');
    Route::post('/removeSisId', 'Canvas\CourseController@removeSisId')->name('removeSisId');
});

Route::group(['prefix' => 'crosslist'], function () {
    Route::get('/index', 'Canvas\CrosslistController@index')->name('crosslist.index');
    Route::get('/create', 'Canvas\CrosslistController@create')->name('crosslist.create');
    Route::redirect('/bulk', '/crosslist/create');
    Route::get('/decrosslist', 'Canvas\CrosslistController@decrosslist')->name('crosslist.decrosslist');
    Route::post('/destroy', 'Canvas\CrosslistController@destroy')->name('crosslist.destroy');
    Route::get('/report', 'CrosslistController@index')->name('crosslist-report.index');
});

Route::group(['prefix' => 'enrollmentrequests'], function () {
    Route::get('/', 'Canvas\EnrollmentrequestsController@index')->name('enrollmentrequests.index');
    Route::get('/all', 'Canvas\EnrollmentrequestsController@all')->name('enrollmentrequests.all');
    Route::get('/create', 'Canvas\EnrollmentrequestsController@create')->name('enrollmentrequests.create');
    Route::get('/show', 'Canvas\EnrollmentrequestsController@show')->name('enrollmentrequests.show');
    Route::get('/{enrollmentRequest}/edit', 'Canvas\EnrollmentrequestsController@edit')->name('enrollmentrequests.edit');
    Route::patch('/{enrollmentRequest}', 'Canvas\EnrollmentrequestsController@update')->name('enrollmentrequests.update');
    Route::get('/delete', 'Canvas\EnrollmentrequestsController@delete')->name('enrollmentrequests.delete');

    Route::get(
        '/create-in-canvas',
        'Canvas\EnrollmentrequestsController@createInCanvas'
    )->name('enrollmentrequests.create-in-canvas');
    Route::get('/reject', 'Canvas\EnrollmentrequestsController@reject')->name('enrollmentrequests.reject');

    Route::post('/store', 'Canvas\EnrollmentrequestsController@store')->name('enrollmentrequests.store');
    Route::delete(
        '/destroy',
        'Canvas\EnrollmentrequestsController@destroy'
    )->name('enrollmentrequests.destroy');

    Route::get(
        '/list-accounts',
        'Canvas\EnrollmentrequestsController@listAccounts'
    )->name('enrollmentrequests.list-accounts');
    Route::get(
        '/list-roles',
        'Canvas\EnrollmentrequestsController@listRoles'
    )->name('enrollmentrequests.list-roles');
    Route::get(
        '/list-roles-select-element',
        'Canvas\EnrollmentrequestsController@listRolesSelectElement'
    )->name('enrollmentrequests.list-roles-select-element');
});


Route::group(['prefix' => 'lti'], function () {
    Route::get('/', 'Canvas\LtiController@index')->name('lti.index');
    Route::get('/tool-edit', 'Canvas\LtiController@tooledit')->name('lti.tool.edit');
    Route::get('/tool-delete', 'Canvas\LtiController@tooldelete')->name('lti.tool.delete');
    Route::get('/tool-create', 'Canvas\LtiController@toolcreate')->name('lti.tool.create');
    Route::patch('/tool-update', 'Canvas\LtiController@toolupdate')->name('lti.tool.update');
    Route::post('/tool-store', 'Canvas\LtiController@toolstore')->name('lti.tool.store');
    Route::post('/tool-destroy', 'Canvas\LtiController@tooldestroy')->name('lti.tool.destroy');

    Route::get('/inventory', 'Canvas\LtiController@inventory')->name('lti.inventory.index');
    Route::get('/inventory-edit', 'Canvas\LtiController@inventoryedit')->name('lti.inventory.edit');
    Route::get('/inventory-delete', 'Canvas\LtiController@inventorydelete')->name('lti.inventory.delete');
    Route::get('/inventory-create', 'Canvas\LtiController@inventorycreate')->name('lti.inventory.create');
    Route::patch('/inventory-update', 'Canvas\LtiController@inventoryupdate')->name('lti.inventory.update');
    Route::post('/inventory-store', 'Canvas\LtiController@inventorystore')->name('lti.inventory.store');
    Route::post('/inventory-destroy', 'Canvas\LtiController@inventorydestroy')->name('lti.inventory.destroy');

    Route::get('/enrollment', 'Canvas\LtiController@enrollment')->name('lti.enrollment.index');
});

Route::group(['prefix' => 'page-view-audits'], function () {
    Route::get('/', 'Canvas\PageViewAuditController@index')->name('canvas.page-view-audits.index');
    Route::post('/results', 'Canvas\PageViewAuditController@results')->name('canvas.page-view-audits.results');
});

Route::group(['prefix' => 'nav-item-replacer'], function () {
    Route::get('/', 'NavItemReplacerController@index')->name('canvas.nav-item-replacer.index');
    Route::post('/review', 'NavItemReplacerController@review')->name('canvas.nav-item-replacer.review');
    Route::post('/', 'NavItemReplacerController@replace')->name('canvas.nav-item-replacer.replace');
});

Route::group(['prefix' => 'course-activity-report'], function () {
    Route::get('/', 'CourseActivityReportController@index')->name('canvas.course-activity-report.index');
    Route::get('/create', 'CourseActivityReportController@create')->name('canvas.course-activity-report.create');
    Route::post('/create', 'CourseActivityReportController@store')->name('canvas.course-activity-report.store');
    Route::get('/{report}', 'CourseActivityReportController@show')->name('canvas.course-activity-report.show');
    Route::get(
        '/{report}/export',
        'CourseActivityReportController@export'
    )->name('canvas.course-activity-report.export');
});

Route::prefix('quiz-log-reports')->name('canvas.quiz-log-reports.')->group(function () {
    Route::get('/', 'QuizLogReportController@index')->name('index');
    Route::get('create', 'QuizLogReportController@create')->name('create');
    Route::get('{report}', 'QuizLogReportController@show')->name('show');
    Route::get('{report}/download', 'QuizLogReportController@download')->name('download');
    Route::post('create', 'QuizLogReportController@store')->name('store');
    Route::post('loadCourseData', 'QuizLogReportController@loadCourseData')->name('load-course-data');
});

Route::group(['prefix' => 'lti-reports'], function () {
    Route::get('/', 'LtiReportController@index')->name('canvas.lti-reports.index');
    Route::get('/create', 'LtiReportController@create')->name('canvas.lti-reports.create');
    Route::post('/create', 'LtiReportController@store')->name('canvas.lti-reports.store');

    Route::get('/reports-select-element', 'LtiReportController@getReportTypesSelectElementForAccount')
        ->name('canvas.lti-reports.reports-select-element');

    Route::get('/{report}', 'LtiReportController@show')->name('canvas.lti-reports.show');
    Route::get('/{report}/export', 'LtiReportController@export')->name('canvas.lti-reports.export');
});


Route::prefix('manual-enrollments')->name('manual-enrollments.')->group(function () {
    Route::get('/', 'ManualEnrollmentController@index')->name('index');
    Route::get('/create', 'ManualEnrollmentController@create')->name('create');
    Route::get('/list-roles-select-element', 'ManualEnrollmentController@listRolesSelectElement')->name('list-roles-select-element');
    Route::post('/store', 'ManualEnrollmentController@store')->name('store');
    Route::patch('/{manualEnrollmentBatch}', 'ManualEnrollmentController@update')->name('update');
    Route::get('/{manualEnrollmentBatch}', 'ManualEnrollmentController@show')->name('show');
    Route::post('/{manualEnrollment}/retry', 'ManualEnrollmentController@retry')->name('retry');
    Route::post('/{manualEnrollmentBatch}/retry-all', 'ManualEnrollmentController@retryAll')->name('retry-all');
});

Route::prefix('lti-editor')->name('lti-editor.')->group(function () {
    Route::get('/', 'LtiEditorController@index')->name('index');
    Route::get('/edit', 'LtiEditorController@edit')->name('edit');
    Route::patch('/{toolId}', 'LtiEditorController@update')->name('update');
    Route::post('/load-tools', 'LtiEditorController@loadTools')->name('load-tools');
});

Route::prefix('nav-item-reports')->name('nav-item-reports.')->group(function () {
    Route::get('/', 'NavItemReportController@index')->name('index');
    Route::get('{report}', 'NavItemReportController@show')->name('show');
});

Route::prefix('batch-sandbox-courses')->name('batch-sandbox-courses.')->group(function () {
    Route::get('/', 'BatchSandboxCourseController@index')->name('index');
    Route::get('create', 'BatchSandboxCourseController@create')->name('create');
    Route::get('{batch}', 'BatchSandboxCourseController@show')->name('show');
});

// Horizon override
Route::group([
    'prefix'     => 'horizon',
    'namespace'  => 'Laravel\Horizon\Http\Controllers',
    'middleware' => config('horizon.middleware', 'web'),
], function () {
    // Catch-all Route...
    Route::get('/{view?}', '\App\Http\Controllers\CcpsCore\HomeController@horizon')->where(
        'view',
        '(.*)'
    )->name('horizon.index');
});
