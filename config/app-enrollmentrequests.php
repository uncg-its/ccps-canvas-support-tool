<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Notification Email
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'enrollment_request_notification_email' => env('ENROLLMENT_REQUEST_NOTIFICATION_EMAIL', 'lms-admins-l@uncg.edu'),

    'enrollment_request_email_domain' => env('ENROLLMENT_REQUEST_EMAIL_DOMAIN', 'uncg.edu'),



];
