<?php /* UNCG WEB3 WRAPPER::SITE VARIABLES::VERSION 3.0::11/30/2014  */

return [
    "unit_name" => env('APP_NAME', 'CCPS CST'),

    "dept_name" => env('APP_NAME', 'CCPS CST'),

    "unit_sub_title" => "Cloud Collaboration &amp; Productivity Services",

    // additional header information - false = disabled, or should be a path to a blade file.
    "page_header" => false,

    // top level navigation. for now this is disabled and will do nothing. future may allow this to be specified manually, but for now we are pulling nav in another way.
    "unit_goldbar" => false,
    // top level navigation for mobile. for now this is disabled and will do nothing. future may allow this to be specified manually, but for now we are pulling nav in another way.
    "mobile_menu" => false,


    // unit menu on left sidebar. false = disabled, otherwise should be a path to a blade file that has the menu HTML.
    "unit_menu" => false,

    // right sidebar. false = disabled, otherwise should be a path to a blade file that has the sidebar HTML.
    "page_sidebar" => false,

    // department / unit / site owner info
    // if false, will not be shown at all. expects string otherwise, no HTML.
    "dept_info" => [
        "address"  => false,
        "address2" => false,
        "phone"    => false,
        "fax"      => false,
        "email"    => false,
    ],

    // information on the website administrator, displayed in footer. false = disabled, otherwise should be a path to a blade file that has the section HTML.
    "admin_info" => false,

    // the code received when you requested a customized Google Search collection (https://its.uncg.edu/Web_Development/GSA/Collection/)
    "search_unit" => false,

    // custom css filename (expected in public/uncg/css). set to false if not using.
    "custom_css" => false,

    // custom js filename (expected in public/uncg/js).  set to false if not using.
    "custom_js" => false,

    // view locations
    "views" => [
        // path to nav blade file - false = disabled
        "nav" => 'components.nav',

        // path to unit contact info blade file - false = disabled
        "unit_contact" => 'uncgtheme::components.unit_contact',
    ]
];


/* UNIT_NAME is used as part of the page title displayed in the browser tab */
//define("UNIT_NAME", "CCPS Framework");

/* DEPT_NAME is displayed at the top of the page in an <h1> tag */
//define("DEPT_NAME", "CCPS Framework");

/* UNIT_SUB_TITLE is displayed at the top of the page in an <h2> tag */
//define("UNIT_SUB_TITLE", "Cloud Collaboration &amp; Productivity Services");

/* UNIT_GOLDBAR is the path to your horizontal navigation menu. Commenting this line out will hide the horizontal menu. */
//define("UNIT_GOLDBAR", "vendor/uncg/wrapper/unit_goldbar.htm");

/* UNIT_MENU is the path to your left sidebar content. Uncommenting this line will cause the left sidebar to show. */
//define("UNIT_MENU", "vendor/uncg/wrapper/unit_menu.htm");

/* PAGE_SIDEBAR is the path to your right sidebar content. Uncommenting this line will cause the right sidebar to show. */
//define('PAGE_SIDEBAR', 'wrapper/page_sidebar.htm');

/* DEPT_ fields define contact information for your department and will be displayed in the site footer. */
//define("DEPT_ADDRESS", "");
//define("DEPT_PHONE", "");
//define("DEPT_FAX", "");
//define("DEPT_EMAIL", "");

/* UNIT_ADMIN is the path to a file containing website administrator information. This information is displayed in the footer. */
//define("UNIT_ADMIN", "vendor/uncg/wrapper/unit_admin.htm");

/* MOBILE_MENU is the path to your mobile menu content. This content is shown only on tablet and phone size screens when the menu icon is tapped. */
//define("MOBILE_MENU", "vendor/uncg/wrapper/mobile_menu.htm");

/* SEARCH_UNIT is the code received when you requested a customized Google Search collection (https://its.uncg.edu/Web_Development/GSA/Collection/) */
//define("SEARCH_UNIT", "");

/* CUSTOM_CSS points to a file in which you can place your custom CSS. */
//define("CUSTOM_CSS", "vendor/uncg/wrapper/custom.css");
