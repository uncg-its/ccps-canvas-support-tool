<?php

return [
    'canvas_environment_url' => 'https://' . config('canvas-api.configs.' . config('canvas-api.defaults.config') . '.host'),

    'queue_wait_minutes' => [
        'nav_item_replacer' => env('NAV_ITEM_QUEUE_WAIT_MINUTES', 1),
    ],

    'cache_ttl' => [
        'nav_item_replacer' => env('NAV_ITEM_CACHE_TTL', 15),
    ],

    'lti_reports' => [
        'key_account_ids' => [
            'courses'    => 109,
            'noncourses' => 176,
            'sandbox'    => 108,
        ],
        'traversal_levels' => [
            'courses' => [
                'department' => 1,
                'unit'       => 2
            ],
        ],
    ],

    'key_account_ids' => [
        'root'       => env('ACCOUNT_ID_ROOT', 1),
        'courses'    => env('ACCOUNT_ID_COURSES', 109),
        'noncourses' => env('ACCOUNT_ID_NONCOURSES', 176),
        'sandbox'    => env('ACCOUNT_ID_SANDBOX', 149),
    ],

    'api_queue_batch_size' => env('API_QUEUE_BATCH_SIZE', 50),

    'throttling' => [
        'active'           => env('THROTTLING_ACTIVE', false),
        'job_limit'        => env('THROTTLING_JOB LIMIT', 300),
        'timespan_seconds' => env('THROTTLING_TIMESPAN_SECONDS', 60),
        'release_seconds'  => env('THROTTLING_RELEASE_SECONDS', 60),
    ]

];
