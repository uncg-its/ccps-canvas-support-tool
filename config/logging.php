<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver'   => 'stack',
            'channels' => ['single'],
        ],

        'single' => [
            'driver' => 'daily',
            'path'   => storage_path('logs/laravel.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 3),
        ],

        /* Examples

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 7,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
        */

        /*
         * Default CCPS Core channels
         */

        'general'        => [
            'driver'   => 'stack',
            'channels' => ['general-local', 'general-splunk']
        ],
        'general-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/general.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'general-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/general.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'queue'        => [
            'driver'   => 'stack',
            'channels' => ['queue-local', 'queue-splunk']
        ],
        'queue-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/queue.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'queue-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/queue.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'cron'        => [
            'driver'   => 'stack',
            'channels' => ['cron-local', 'cron-splunk']
        ],
        'cron-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/cron.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'cron-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/cron.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'access'        => [
            'driver'   => 'stack',
            'channels' => ['access-local', 'access-splunk']
        ],
        'access-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/access.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'access-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/access.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'database'        => [
            'driver'   => 'stack',
            'channels' => ['database-local', 'database-splunk']
        ],
        'database-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/database.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'database-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/database.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'acl'        => [
            'driver'   => 'stack',
            'channels' => ['acl-local', 'acl-splunk']
        ],
        'acl-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/acl.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'acl-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/acl.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'backup'        => [
            'driver'   => 'stack',
            'channels' => ['backup-local', 'backup-splunk']
        ],
        'backup-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/backup.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'backup-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/backup.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'notifications'        => [
            'driver'   => 'stack',
            'channels' => ['notifications-local', 'notifications-splunk']
        ],
        'notifications-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/notifications.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'notifications-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/notifications.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'application-snapshots'        => [
            'driver'   => 'stack',
            'channels' => ['application-snapshots-local', 'application-snapshots-splunk']
        ],
        'application-snapshots-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/application-snapshots.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'application-snapshots-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/application-snapshots.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'exceptions'        => [
            'driver'   => 'stack',
            'channels' => ['exceptions-local', 'exceptions-splunk']
        ],
        'exceptions-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/exceptions.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'exceptions-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/exceptions.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'crosslist'        => [
            'driver'   => 'stack',
            'channels' => ['crosslist-local', 'crosslist-splunk']
        ],
        'crosslist-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/crosslist.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'crosslist-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/crosslist.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'manual-enrollments'        => [
            'driver'   => 'stack',
            'channels' => ['manual-enrollments-local', 'manual-enrollments-splunk']
        ],
        'manual-enrollments-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/manual-enrollments.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'manual-enrollments-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/manual-enrollments.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'courses'        => [
            'driver'   => 'stack',
            'channels' => ['courses-local', 'courses-splunk']
        ],
        'courses-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/courses.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'courses-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/courses.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'live'        => [
            'driver'   => 'stack',
            'channels' => ['live-local', 'live-splunk']
        ],
        'live-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/live.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'live-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/live.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'lti'        => [
            'driver'   => 'stack',
            'channels' => ['lti-local', 'lti-splunk']
        ],
        'lti-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/lti.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'lti-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/lti.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'enrollmentrequests'        => [
            'driver'   => 'stack',
            'channels' => ['enrollmentrequests-local', 'enrollmentrequests-splunk']
        ],
        'enrollmentrequests-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/enrollmentrequests.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'enrollmentrequests-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/enrollmentrequests.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'page-views'        => [
            'driver'   => 'stack',
            'channels' => ['page-views-local', 'page-views-splunk']
        ],
        'page-views-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/page-views.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'page-views-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/page-views.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'nav-item-replacer'        => [
            'driver'   => 'stack',
            'channels' => ['nav-item-replacer-local', 'nav-item-replacer-splunk']
        ],
        'nav-item-replacer-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/nav-item-replacer.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'nav-item-replacer-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/nav-item-replacer.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'course-activity-report'        => [
            'driver'   => 'stack',
            'channels' => ['course-activity-report-local', 'course-activity-report-splunk']
        ],
        'course-activity-report-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/course-activity-report.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'course-activity-report-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/course-activity-report.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],

        'lti-reports'        => [
            'driver'   => 'stack',
            'channels' => ['lti-reports-local', 'lti-reports-splunk']
        ],
        'lti-reports-local'  => [
            'driver' => 'daily',
            'path'   => storage_path('logs/lti-reports.log'),
            'level'  => 'debug',
            'days'   => env('APP_LOG_MAX_FILES', 7)
        ],
        'lti-reports-splunk' => [
            'driver'    => 'daily',
            'path'      => storage_path('logs/splunk/lti-reports.log'),
            'level'     => 'info',
            'days'      => env('APP_LOG_MAX_FILES_SPLUNK', 3),
            'formatter' => \Uncgits\Ccps\Components\Log\Formatter\SplunkJsonFormatter::class,
        ],
    ],
];
